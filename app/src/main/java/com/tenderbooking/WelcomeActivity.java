package com.tenderbooking;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.tenderbooking.HelperClasses.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;


public class WelcomeActivity extends AppCompatActivity {

    public static JSONArray jsonArray;
    public static String welcomeLang;
    static int x = 12;
    View decorView;
    JSONObject jsonObject;
    String next, prev, close, skip;
    String wS1, wS2, wS3, wS4, wS5, wS6, wS7, wS8, wS9, wS10,
            wSS1, wSS2, wSS3, wSS4, wSS5, wSS6, wSS7, wSS8, wSS9, wSS10;
    int calling = 0;
    String setTitle, setSubTitle;
    HtmlTextView wT1, wT2, wT3, wT4, wT5, wT6, wT7, wT8, wT9, wT10;


    WebView webView1, webView2, webView3, webView4, webView5, webView6, webView7, webView8, webView9, webView10;
    SessionManager sessionManager;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private int[] layouts;
    private int[] allLayout;
    private JSONArray jsonArray1;
    private Button btnNext, btnSkip;
    //	viewpager change listener setting
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            start();


            if (position == 0) {
                btnSkip.setText(skip);
            } else {
                btnSkip.setText(prev);
            }


            // changing the next button texttt
            if (position == layouts.length - 1) {
                // last page setting finish
                btnNext.setVisibility(View.VISIBLE);
                btnNext.setText(close);

            } else {
                btnNext.setVisibility(View.VISIBLE);
                // still pages are left
                btnNext.setText(next);

            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    public void start() {
        decorView = getWindow().getDecorView();

        int uiOptions = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        decorView.setSystemUiVisibility(uiOptions);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(WelcomeActivity.this);
        jsonArray1 = jsonArray;


        if (Signup.signLang) {

            welcomeLang = Signup.selectetLanguage;
            Signup.signLang = false;

        }


        if (Login.wizardUser == 0) {
            launchHomeScreen();
            finish();
        }


        start();


        setContentView(R.layout.activity_welcome);


        AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);


        viewPager = (ViewPager) findViewById(R.id.view_pager);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setVisibility(View.VISIBLE);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnSkip.setVisibility(View.VISIBLE);


        switch (welcomeLang) {

            case "english":

                next = "NEXT";
                prev = "PREV";
                skip = "SKIP";
                close = "FINISH";

                break;


            case "serbian":

                next = "SLEDEĆI";
                prev = "PRETHODNI";
                skip = "PRESKOČI";
                close = "KRAJ";


                break;

            case "bosnian":

                next = "SLEDEĆI";
                prev = "PRETHODNI";
                skip = "PRESKOČI";
                close = "KRAJ";

                break;


            default:
                next = "SLEDEĆI";
                prev = "PRETHODNI";
                skip = "PRESKOČI";
                close = "KRAJ";

                break;


        }


        btnNext.setText(next);
        btnSkip.setText(skip);


        allLayout = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,
                R.layout.welcome_slide4,
                R.layout.welcome_slide5,
                R.layout.welcome_slide6,
                R.layout.welcome_slide7,
                R.layout.welcome_slide8,
                R.layout.welcome_slide9,
                R.layout.welcome_slide10};


        for (int i = 0; i <= jsonArray1.length() - 1; i++) {
            calling++;
            try {
                jsonObject = (JSONObject) jsonArray1.get(i);
                setTitle = jsonObject.optString("slideTitle");
                setSubTitle = jsonObject.optString("slideContent");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (calling == 1) {
                setString1(setTitle, setSubTitle);
            }
            if (calling == 2) {
                setString2(setTitle, setSubTitle);
            }
            if (calling == 3) {
                setString3(setTitle, setSubTitle);
            }
            if (calling == 4) {
                setString4(setTitle, setSubTitle);
            }
            if (calling == 5) {
                setString5(setTitle, setSubTitle);
            }
            if (calling == 6) {
                setString6(setTitle, setSubTitle);
            }
            if (calling == 7) {
                setString7(setTitle, setSubTitle);
            }
            if (calling == 8) {
                setString8(setTitle, setSubTitle);
            }
            if (calling == 9) {
                setString9(setTitle, setSubTitle);
            }
            if (calling == 10) {
                setString10(setTitle, setSubTitle);
            }


        }


        layouts = new int[jsonArray1.length()];


        Log.e("ARRAY  SIZE", Integer.toString(jsonArray1.length()));


        for (int i = 0; i <= jsonArray1.length() - 1; i++) {
            layouts[i] = allLayout[i];

        }


        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int current = getItem(+1);
                if (current == 1) {
                    launchHomeScreen();
                } else {
                    current = current - 2;
                    viewPager.setCurrentItem((current));
                }


            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
                // checking for last page
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        startActivity(new Intent(WelcomeActivity.this, Home.class));
        finish();
    }

    //transprant notification bar
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void setString1(String wt, String wst) {
        wS1 = wt;
        wSS1 = wst;

    }

    public void setString2(String wt, String wst) {
        wS2 = wt;
        wSS2 = wst;


    }

    public void setString3(String wt, String wst) {
        wS3 = wt;
        wSS3 = wst;


    }

    public void setString4(String wt, String wst) {
        wS4 = wt;
        wSS4 = wst;


    }

    public void setString5(String wt, String wst) {
        wS5 = wt;
        wSS5 = wst;


    }

    public void setString6(String wt, String wst) {
        wS6 = wt;
        wSS6 = wst;


    }

    public void setString7(String wt, String wst) {
        wS7 = wt;
        wSS7 = wst;


    }

    public void setString8(String wt, String wst) {
        wS8 = wt;
        wSS8 = wst;


    }

    public void setString9(String wt, String wst) {
        wS9 = wt;
        wSS9 = wst;


    }

    public void setString10(String wt, String wst) {
        wS10 = wt;
        wSS10 = wst;


    }


    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {


            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View view = layoutInflater.inflate(layouts[position], container, false);

            //change the text dynamically
            switch (position) {

                case 0:

                    wT1 = (HtmlTextView) view.findViewById(R.id.WT1);
                    wT1.setHtml(wS1, new HtmlHttpImageGetter(wT1));
                    webView1 = (WebView) view.findViewById(R.id.WV1);
                    webView1.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }


                        }
                    });
                    webView1.loadData(wSS1, "text/html", null);

                    break;

                case 1:

                    wT2 = (HtmlTextView) view.findViewById(R.id.WT2);
                    wT2.setHtml(wS2, new HtmlHttpImageGetter(wT2));
                    webView2 = (WebView) view.findViewById(R.id.WV2);
                    webView2.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView2.loadData(wSS2, "text/html", null);
                    break;


                case 2:
                    wT3 = (HtmlTextView) view.findViewById(R.id.WT3);
                    wT3.setHtml(wS3, new HtmlHttpImageGetter(wT3));
                    webView3 = (WebView) view.findViewById(R.id.WV3);
                    webView3.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView3.loadData(wSS3, "text/html", null);
                    break;

                case 3:
                    webView4 = (WebView) view.findViewById(R.id.WV4);
                    webView4.loadData(wSS4, "text/html", null);
                    wT4 = (HtmlTextView) view.findViewById(R.id.WT4);
                    webView4.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    wT4.setHtml(wS4, new HtmlHttpImageGetter(wT4));
                    break;


                case 4:
                    webView5 = (WebView) view.findViewById(R.id.WV5);
                    webView5.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView5.loadData(wSS5, "text/html", null);
                    wT5 = (HtmlTextView) view.findViewById(R.id.WT5);
                    wT5.setHtml(wS5, new HtmlHttpImageGetter(wT5));

                    break;


                case 5:
                    webView6 = (WebView) view.findViewById(R.id.WV6);
                    webView6.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView6.loadData(wSS6, "text/html", null);
                    wT6 = (HtmlTextView) view.findViewById(R.id.WT6);
                    wT6.setHtml(wS6, new HtmlHttpImageGetter(wT6));
                    break;


                case 6:
                    webView7 = (WebView) view.findViewById(R.id.WV7);
                    webView7.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView7.loadData(wSS7, "text/html", null);
                    wT7 = (HtmlTextView) view.findViewById(R.id.WT7);
                    wT7.setHtml(wS7, new HtmlHttpImageGetter(wT7));
                    break;


                case 7:
                    webView8 = (WebView) view.findViewById(R.id.WV8);
                    webView8.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView8.loadData(wSS8, "text/html", null);
                    wT8 = (HtmlTextView) view.findViewById(R.id.WT8);
                    wT8.setHtml(wS8, new HtmlHttpImageGetter(wT8));
                    break;

                case 8:
                    webView9 = (WebView) view.findViewById(R.id.WV9);
                    webView9.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView9.loadData(wSS9, "text/html", null);
                    wT9 = (HtmlTextView) view.findViewById(R.id.WT9);
                    wT9.setHtml(wS9, new HtmlHttpImageGetter(wT9));
                    break;

                case 9:
                    webView10 = (WebView) view.findViewById(R.id.WV10);
                    webView10.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this);
                            String message = "SSL Certificate error.";
                            switch (error.getPrimaryError()) {
                                case SslError.SSL_UNTRUSTED:
                                    message = "The certificate authority is not trusted.";
                                    break;
                                case SslError.SSL_EXPIRED:
                                    message = "The certificate has expired.";
                                    break;
                                case SslError.SSL_IDMISMATCH:
                                    message = "The certificate Hostname mismatch.";
                                    break;
                                case SslError.SSL_NOTYETVALID:
                                    message = "The certificate is not yet valid.";
                                    break;
                            }
                            message += " Do you want to continue anyway?";

                            builder.setTitle("SSL Certificate Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.proceed();
                                }
                            });
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.cancel();
                                }
                            });
                            final AlertDialog dialog = builder.create();
                            if (x == 12) {
                                handler.proceed();
                            } else {
                                dialog.show();
                            }

                        }
                    });
                    webView10.loadData(wSS10, "text/html", null);
                    wT10 = (HtmlTextView) view.findViewById(R.id.WT10);
                    wT10.setHtml(wS10, new HtmlHttpImageGetter(wT10));
                    break;
            }


            container.addView(view);
            return view;


        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }


    }
}

