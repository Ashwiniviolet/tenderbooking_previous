package com.tenderbooking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.MainTab.MainTabs;

public class MainActivity extends AppCompatActivity {
    Button signup, login, privacy, ns, mainTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signup = (Button) findViewById(R.id.createAccount);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Signup.class));
            }
        });


        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Login.class));
            }
        });

        privacy = (Button) findViewById(R.id.privacy);

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PrivacyPolicy.class));
            }
        });
        ns = (Button) findViewById(R.id.notificationSettings);

        ns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NotificationSettings.class));
            }
        });
        Button aa = (Button) findViewById(R.id.askAdvisor);

        aa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(MainActivity.this, AskAdvisor.class));
                GlobalDataAcess globalDataAcess = new GlobalDataAcess();
                globalDataAcess.getAskQuesDialog(MainActivity.this, "0", "0", 0);
            }
        });
        Button ab = (Button) findViewById(R.id.about);

        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, About.class));
            }
        });

        Button fs = (Button) findViewById(R.id.fontSettings);

        fs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FontSettings.class));
            }
        });

        mainTab = (Button) findViewById(R.id.mainTab);
        mainTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainTabs.class);
                startActivity(intent);
            }
        });


    }
}
