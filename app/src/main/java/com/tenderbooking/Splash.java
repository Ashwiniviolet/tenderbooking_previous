package com.tenderbooking;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.NukeSSLCerts;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;

import java.lang.reflect.InvocationTargetException;

public class Splash extends AppCompatActivity {

    public static String PackageName;
    public static int group_clicked = -1;
    public boolean isAdviNoti = false;
    SessionManager sessionManager;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NukeSSLCerts.nuke();

//for tender list maintaining
        GlobalDataAcess.mainValue = -1;
        GlobalDataAcess.subValue = null;
        sessionManager = new SessionManager(Splash.this);

        Intent intent = getIntent();
        if (intent.hasExtra(GlobalDataAcess.Notification_type)) {
            isAdviNoti = intent.getBooleanExtra(GlobalDataAcess.Notification_type, false);
        }
        if (intent.hasExtra(GlobalDataAcess.Menu_option)) {
            GlobalDataAcess.group_selected = intent.getIntExtra(GlobalDataAcess.Menu_option, -1);
        }

        if (intent.hasExtra(GlobalDataAcess.MenuOptionAdvice)) {
            GlobalDataAcess.group_select_advice = intent.getIntExtra(GlobalDataAcess.MenuOptionAdvice, -1);
        }
        if (intent.hasExtra(GlobalDataAcess.MenuOptionAdviceMain)) {
            GlobalDataAcess.group_select_advice_main = intent.getIntExtra(GlobalDataAcess.MenuOptionAdviceMain, -1);
        }

        if (sessionManager.getIsAppKill()) {
            GlobalDataAcess.group_selected = -1;
            sessionManager.setIsAppKill(false);
        }

        if (isAdviNoti && sessionManager.IsUserLogin()) {
            Intent intent2 = new Intent(Splash.this, Home.class);
            intent2.putExtra(GlobalDataAcess.Notification_type, isAdviNoti);
            intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent2);
            finish();
        }

        setContentView(R.layout.activity_splash);

        PackageName = getApplicationContext().getPackageName();
        // sessionManager.SetSignature(GlobalDataAcess.DefaultAPiSig);
        //temp condition change
/*
        Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (sessionManager.IsUserLogin()){
                                    // if (1==1){
                                    if (UtillClasses.isNetworkAvailable(Splash.this)){
                                        Intent intent = new Intent(Splash.this, Home.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        builder = UtillClasses.GetInternetAlertDialog(Splash.this);
                                        builder.setPositiveButton(Splash.this.getResources().getString(R.string.conntoInt), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    UtillClasses.setMobileDataEnabled(Splash.this,true);
                                                } catch (ClassNotFoundException e) {
                                                    e.printStackTrace();
                                                } catch (NoSuchFieldException e) {
                                                    e.printStackTrace();
                                                } catch (IllegalAccessException e) {
                                                    e.printStackTrace();
                                                } catch (NoSuchMethodException e) {
                                                    e.printStackTrace();
                                                } catch (InvocationTargetException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        builder.setNegativeButton(Splash.this.getResources().getString(R.string.goOffline), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Splash.this, Home.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                                    }
                                }else {
                                    Intent intent =new Intent(Splash.this, Login.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        },2500);
                    }
                };
                runnable.run();*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (sessionManager.IsUserLogin()) {
                            // if (1==1){
                            if (GlobalDataAcess.hasConnection(Splash.this)) {
                                Intent intent = new Intent(Splash.this, Home.class);
                                intent.putExtra(GlobalDataAcess.Notification_type, isAdviNoti);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                //  GlobalDataAcess.apiDialog(Splash.this,getResources().getString(R.string.network_error),getResources().getString(R.string.error_msg));
                                builder = UtillClasses.GetInternetAlertDialog(Splash.this);
                                builder.setPositiveButton(Splash.this.getResources().getString(R.string.conntoInt), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            UtillClasses.setMobileDataEnabled(Splash.this, true);
                                        } catch (ClassNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (NoSuchFieldException e) {
                                            e.printStackTrace();
                                        } catch (IllegalAccessException e) {
                                            e.printStackTrace();
                                        } catch (NoSuchMethodException e) {
                                            e.printStackTrace();
                                        } catch (InvocationTargetException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                builder.setNegativeButton(Splash.this.getResources().getString(R.string.goOffline), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Splash.this, Home.class);
                                        intent.putExtra(GlobalDataAcess.Notification_type, isAdviNoti);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                builder.setCancelable(false);
                                builder.create();
                                builder.show();
                            }
                        } else {
                            Intent intent = new Intent(Splash.this, Login.class);
                            intent.putExtra(GlobalDataAcess.Notification_type, isAdviNoti);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, 2500);
            }
        };
        runnable.run();
    }
}
