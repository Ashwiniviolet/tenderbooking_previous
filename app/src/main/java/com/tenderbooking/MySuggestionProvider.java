package com.tenderbooking;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Violet on 11/28/2017.
 */

public class MySuggestionProvider extends SearchRecentSuggestionsProvider {

    public final static String AUTHORITY = "com.tenderbooking.MySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;


    public MySuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);


    }


}
