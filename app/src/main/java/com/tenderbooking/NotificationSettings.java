package com.tenderbooking;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.tenderbooking.HelperClasses.SessionManager;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DecimalFormat;
import java.util.Calendar;

public class NotificationSettings extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {


    public static final String TAG = NotificationSettings.class.getSimpleName();
    Switch notiSet_allwayNoti;
    TextView from_time, to_time;
    SessionManager sessionManager;
    int whichtime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sessionManager = new SessionManager(NotificationSettings.this);


        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //ab.setTitle("Do Not Disturb");
        ab.setTitle(getResources().getString(R.string.do_not_distrub));

        //sessionManager = new SessionManager(NotificationSettings.this);
        notiSet_allwayNoti = (Switch) findViewById(R.id.notiSet_allwayNoti);
        from_time = (TextView) findViewById(R.id.from_time);
        to_time = (TextView) findViewById(R.id.to_time);


        final String startdate = sessionManager.GetNotiEnableStartTime();
        final String stopdate = sessionManager.GetNotiEnableStopTime();
        /*final String startdate = sessionManager.GetNotiEnableStartTime();
        final String stopdate = sessionManager.GetNotiEnableStopTime();
        */

        from_time.setText(startdate);
        to_time.setText(stopdate);

        Log.e(TAG, "start date hour : " + startdate.substring(0, 2) + " minute : " + startdate.substring(3, 4));
        Log.e(TAG, "start date hour : " + stopdate.substring(0, 2) + " minute : " + stopdate.substring(3, 4));

// is change se DND wala ho jayega ok.DND STATUS
        if (sessionManager.GetNotiEnableTimeInterval()) {
            notiSet_allwayNoti.setChecked(true);
        } else {
            notiSet_allwayNoti.setChecked(false);
        }

        notiSet_allwayNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sessionManager.SetNotiEnableTimeInterval(isChecked);

            }
        });

        from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date_start = from_time.getText().toString();
                Log.e(TAG, "date start " + date_start + " hour is " + date_start.substring(0, 2) + " minutes " + date_start.substring(3, 5));
                ShowDialog("Select time", Integer.parseInt(date_start.substring(0, 2)), Integer.parseInt(date_start.substring(3, 5)));
                whichtime = 0;
            }
        });

        to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date_stop = to_time.getText().toString();
                Log.e(TAG, "date stop " + date_stop + " hour is " + date_stop.substring(0, 2) + " minutes " + date_stop.substring(3, 5));
                ShowDialog("Select time", Integer.parseInt(date_stop.substring(0, 2)), Integer.parseInt(date_stop.substring(3, 5)));
                whichtime = 1;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {


            this.finish();

            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    public void ShowDialog(String title, int hour, int minute) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(NotificationSettings.this,
                hour, minute, false);
     /*  TimePickerDialog timePickerDialog =TimePickerDialog.newInstance(NotificationSettings.this,
               now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),false );
*/
        timePickerDialog.setThemeDark(false);
        timePickerDialog.vibrate(true);
        timePickerDialog.dismissOnPause(true);
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);
        timePickerDialog.setAccentColor(ContextCompat.getColor(NotificationSettings.this, R.color.colorPrimary));
        timePickerDialog.setTitle(title);
        timePickerDialog.setTimeInterval(1, 5);
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        timePickerDialog.show(getFragmentManager(), TAG);

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        String hour = hourOfDay + "";
       /* if (hourOfDay< 9){
             hour = "0"+hourOfDay;
        }*/


        DecimalFormat format = new DecimalFormat("00");


        String selected_time = format.format(hourOfDay) + ":" + format.format(minute);

        if (whichtime == 0) {
            sessionManager.SetNotiEnableStartTime(selected_time);
            from_time.setText(selected_time);
        } else {
            sessionManager.SetNotiEnableStopTime(selected_time);
            to_time.setText(selected_time);
        }

    }
}
