package com.tenderbooking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.HelperClasses.CenteredToolbar;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.DeviceName;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Interface.InterfaceDemoAccount;
import com.tenderbooking.Model.ModelCheckUser;
import com.tenderbooking.Volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class Login extends AppCompatActivity implements InterfaceDemoAccount {

    public static String TAG = Login.class.getSimpleName();
    public static EditText email_et, pass_et, phone_et;
    static String language;
    static int wizardUser;
    public final int REQuest_Read_Phone_State = 11;
    public boolean dataLoaded = false;
    public ModelCheckUser modelCheckUser;
    public String uuid = "1";
    public String uuidobe = "1";
    CenteredToolbar mToolbar;
    LinearLayout progress_ll;
    String lang;


    Button login_tech_suport;
    SessionManager sessionManager;
    private int versioncode;
    private String versionName;
    private ProgressDialog mProgress;

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Locale.getDefault().getDisplayLanguage();


        sessionManager = new SessionManager(Login.this);


        setContentView(R.layout.activity_login);

        mToolbar = (CenteredToolbar) findViewById(R.id.login_toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mToolbar.setTitle("Login");

        /*ActionBar ab=getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(false);0
        ab.setTitle("Login");*/

        progress_ll = (LinearLayout) findViewById(R.id.progress_ll);

        mProgress = new ProgressDialog(Login.this);
        mProgress.setMessage(getResources().getString(R.string.please_wait));
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setCancelable(false);

//        materialDialogBuilder = new MaterialDialog.Builder(this);
//        materialDialogBuilder.content(getResources().getString(R.string.please_wait));
//        materialDialogBuilder.cancelable(true);
//        materialDialog = materialDialogBuilder.build();

        //modelUserTagArrayList = new ArrayList<>();
        //modelUserFiltersArrayList = new ArrayList<>();
        Button loginbtn = (Button) findViewById(R.id.login);
        login_tech_suport = (Button) findViewById(R.id.login_tech_suport);
        Button signupbtn = (Button) findViewById(R.id.signup);
        email_et = (EditText) findViewById(R.id.login_email);
        pass_et = (EditText) findViewById(R.id.login_pass);
        phone_et = (EditText) findViewById(R.id.login_mob);

        firebaseAnalytics = FirebaseAnalytics.getInstance(Login.this);

        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setSessionTimeoutDuration(20000);
        firebaseAnalytics.setMinimumSessionDuration(500);
        firebaseAnalytics.setCurrentScreen(Login.this, "Login_Screen", "Login_screen");

        GetUUID();

        login_tech_suport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
                String locale = ims.getLocale();
                String localeDefault = Locale.getDefault().getDisplayLanguage();
                String Applanguage = getResources().getConfiguration().locale.getDisplayLanguage();

                Log.e(TAG," Local Lang Name "+locale+" local Default langualge "+localeDefault+" app language "+Applanguage);
*/
                Log.i("Send email", "");
                //String[] TO = {""};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:podrska@boljibiznis.com"));


                //emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{getResources().getString(R.string.email_address_feedback)});
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.techsuprt_subject));
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(emailIntent);
                    //Intent.createChooser(emailIntent, getResources().getString(R.string.send_mail))

                    Log.i(TAG, "finsih mailing");
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(Login.this, getResources().getString(R.string.no_client_app), Toast.LENGTH_SHORT).show();
                }

            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Login.this,Home.class));
                int passlength = pass_et.getText().toString().trim().replace(" ", "").length();
                Log.e(TAG, "password length " + passlength);

                if (email_et.getText().toString().trim().length() == 0) {
                    email_et.setError(getResources().getString(R.string.enter_valid_email));
                    email_et.requestFocus();
                } else if (!GlobalDataAcess.isValidEmail(email_et.getText().toString().trim())) {
                    email_et.setError(getResources().getString(R.string.enter_password));
                    email_et.requestFocus();
                } else if (pass_et.getText().toString().trim().length() == 0) {
                    pass_et.setError(getResources().getString(R.string.enter_password));
                    pass_et.requestFocus();
                } else if (phone_et.getText().toString().trim().length() <= 0) {
                    phone_et.setError(getResources().getString(R.string.enter_mobile));
                    phone_et.requestFocus();
                } else if (uuid.equals(1)) {
                    if (phone_et != null)
                        Snackbar.make(phone_et, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                } else {
                    //checking internet connextion

                    if (GlobalDataAcess.hasConnection(Login.this)) {


                        GlobalDataAcess.hideSoftKeyboard(Login.this);
                        modelCheckUser = new ModelCheckUser();

                        Map<String, String> deviceINfo = DeviceName.getDeviceName();

                        modelCheckUser.setMobDevicName(deviceINfo.get(UtillClasses.device_devName));
                        modelCheckUser.setApiKey(UtillClasses.ApiKeyValue);
                        modelCheckUser.setEmail(email_et.getText().toString().trim());
                        //modelCheckUser.setPassword(pass_et.getText().toString().trim());
                        modelCheckUser.setPassword(GlobalDataAcess.MD5(pass_et.getText().toString().trim()));
                        modelCheckUser.setMobile(phone_et.getText().toString().trim());
                        modelCheckUser.setUuid(uuid);

                        //set application version code
                        try {
                            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            versioncode = pInfo.versionCode;
                            versionName = pInfo.versionName;

                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                            versioncode = 0;
                            versionName = null;
                        }
                        if (versioncode != 0) {
                            modelCheckUser.setAppVer(versionName + "");
                        }
                        modelCheckUser.setMobModel(deviceINfo.get(UtillClasses.device_model));
                        modelCheckUser.setMobPlatform("Android");
                        //modelCheckUser.setMobPlatVer(Build.VERSION.PREVIEW_SDK_INT+"");
                        modelCheckUser.setMobPlatVer(deviceINfo.get(UtillClasses.device_platmVer));
                        Log.e(TAG, " ANDROID PLATFORM CODE " + Build.VERSION.SDK_INT);

                        //temp login due to changind  signature id
                        //tempLogin(modelCheckUser);


                        LoginUser(modelCheckUser);

                    } else {
                        GlobalDataAcess.apiDialog(Login.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));

                    }

                }


            }
        });


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Signup.class));
            }
        });


    }

    public void GetUUID() {
        int permissionCheck = ContextCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WAKE_LOCK, Manifest.permission.CHANGE_NETWORK_STATE}, REQuest_Read_Phone_State);
        } else {

            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            uuidobe = tManager.getDeviceId();

            //this will give UUID Number
            TelephonyManager teleManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            String tmSerial = teleManager.getSimSerialNumber();
            String tmDeviceId = teleManager.getDeviceId();
            String androidId = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            if (tmSerial == null) tmSerial = "1";
            if (tmDeviceId == null) tmDeviceId = "1";
            if (androidId == null) androidId = "1";
            UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDeviceId.hashCode() << 32) | tmSerial.hashCode());
            uuid = deviceUuid.toString();

            Log.e(TAG, "fist uuid number " + uuidobe + " second UUID number " + uuid);

        }

    }


    public void LoginUser(final ModelCheckUser modelCheckUser) {
        progress_ll.setVisibility(View.VISIBLE);
        //mProgress.show(); @05_10_2017
        // materialDialog.show();
        final String mURL = UtillClasses.BaseUrl + "/check_user";
        // final String mURL = "http://www.tenderilive.com/xmlrpcs/mobile/check_user";

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //pass is set for this ask for that if requried

                Log.e(TAG, response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("statusCode") == 200) {

                        if (!sessionManager.GetUserLastLogedEmail().equals(modelCheckUser.getEmail())) {
                            sessionManager.GetLogout();
                            DataBaseHalper dataBaseHalper = new DataBaseHalper(Login.this);
                            dataBaseHalper.getDeleteAllTable();
                        }


                       /* try {
                            Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/


                        JSONObject jsondata = jsonObject.getJSONObject("data");
                        String signature = jsondata.optString("signature");
                        int statusUser = jsondata.optInt("statusUser");
                        language = jsondata.optString("languageUser");
                        wizardUser = jsondata.optInt("wizardUser");
                        WelcomeActivity.welcomeLang = language;

                        sessionManager.SetSessionInfo(signature, statusUser, modelCheckUser.getEmail());

                        Map<String, String> mapDeviceInfo = new HashMap<String, String>();
                        mapDeviceInfo.put(SessionManager.UUID, modelCheckUser.getUuid());
                        mapDeviceInfo.put(SessionManager.MODEL, modelCheckUser.getMobModel());
                        mapDeviceInfo.put(SessionManager.PLATFORM, modelCheckUser.getMobPlatform());
                        mapDeviceInfo.put(SessionManager.PLATFORM_VER, modelCheckUser.getMobPlatVer());
                        mapDeviceInfo.put(SessionManager.DeviceNmae, modelCheckUser.getMobDevicName());
                        mapDeviceInfo.put(SessionManager.App_Ver, modelCheckUser.getAppVer());
                        mapDeviceInfo.put(SessionManager.User_lang, Locale.getDefault().getDisplayLanguage());
                        Log.e(TAG, "local default language " + Locale.getDefault().getDisplayLanguage());
                        mapDeviceInfo.put(SessionManager.EMAIL, modelCheckUser.getEmail());
                        mapDeviceInfo.put(SessionManager.SIGNATURE, signature);
                        sessionManager.SetDeviceINfo(mapDeviceInfo);


                        //setup Firebase Analytics
                        String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                        bundle.putString(GlobalDataAcess.Class_name, "Login_class");
                        bundle.putString(GlobalDataAcess.Time_Date, date_time);
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                        //GetUserTags(mapDeviceInfo);
                        //temp Datais
                        //mProgress.dismiss();


                        introSlider();

                        if (Signup.signLang) {

                            language = Signup.selectetLanguage;

                        }


                        switch (language) {

                            case "english":

                                lang = "en";
                                sessionManager.setUserLang("english");
                                sessionManager.setApplanguage(1, "en");
                                sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_english));

                                break;


                            case "serbian":
                                lang = "sr";
                                sessionManager.setUserLang("serbian");
                                sessionManager.setApplanguage(2, "sr");
                                sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                                //sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));


                                break;

                            case "bosnian":
                                lang = "bs";
                                sessionManager.setUserLang("bosnian");
                                sessionManager.setApplanguage(3, "bs");
                                sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                                // sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));


                                break;


                            default:
                                lang = "sr";
                                sessionManager.setUserLang("serbian");
                                sessionManager.setApplanguage(2, "sr");
                                sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                                // sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));
                                break;


                        }

                        Log.e(TAG, " value of the language is " + lang);
                        Locale locale = new Locale(lang);
                        locale.setDefault(locale);
                        Configuration configuration = new Configuration();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

                            Resources resources = getResources();
                            configuration.setLocale(locale);
                            getApplication().createConfigurationContext(configuration);

                        } else {
                            configuration.locale = locale;
                            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                        }


                        AppController.getmInstance().onConfigurationChanged(configuration);
                        sessionManager.setAppLangChange(true);


                        if (dataLoaded) {

                            progress_ll.setVisibility(View.GONE);
                            Intent intent = new Intent(Login.this, WelcomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);


                        } else {


                            LoginUser(modelCheckUser);
                        }

                    } else {
                        // mProgress.hide();
                        progress_ll.setVisibility(View.GONE);
                        // materialDialog.hide();
                        if (phone_et != null)
                            Snackbar.make(phone_et, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG).show();
                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(" error geting responce ", " sever error info " + error.getMessage());

            }
        }) {

            /* @Override
             protected Map<String, String> getParams() throws AuthFailureError {
                 Map<String,String> mapheader  = new HashMap<>();

                 mapheader.put("x-api-ver","1.0");
                 //mapheader.put("x-api-key","a7d335c1cb24c90eb578e8eff51948eb");
                 mapheader.put("x-email","istojanovic@gmail.com");
                 mapheader.put("x-password" ,"2d64e46ac5f027477c28c5d15cd689c0" );
                 mapheader.put("x-mobile-number" ,"381641204591" );
                 mapheader.put("x-uuid" ,"uuid" );
                 mapheader.put("x-model" ,"SMN9005" );
                 mapheader.put("x-platform" ," Androi" );
                 mapheader.put("x-platform-ver" ,"5.0.1" );
                 mapheader.put("x-device-name" ,"Note3" );
                 //mapheader.put("Content-Type" ,"multipart/form-data");

                 return mapheader;
             }
 */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();
                /*mapheader.put("x-app-ver","1.0");
                mapheader.put("x-api-key","a7d335c1cb24c90eb578e8eff51948eb");
                mapheader.put("x-email","istojanovic@gmail.com");
                mapheader.put("x-password" ,"2d64e46ac5f027477c28c5d15cd689c0" );
                mapheader.put("x-mobile-number" ,"381641204591" );
                mapheader.put("x-uuid" ,"uuid" );
                mapheader.put("x-model" ,"SMN9005" );
                mapheader.put("x-platform" ," Androi" );
                mapheader.put("x-platform-ver" ,"5.0.1" );
                mapheader.put("x-device-name" ,"Note3" );
                mapheader.put("Content-Type" ,"multipart/form-data");
*/
                mapheader.put(UtillClasses.x_api_ver, UtillClasses.x_api_ver_Value);
                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, modelCheckUser.getEmail());
                mapheader.put(UtillClasses.x_password, modelCheckUser.getPassword());
                //mapheader.put(UtillClasses.x_password ,"2d64e46ac5f027477c28c5d15cd689c0" );
                mapheader.put(UtillClasses.x_mobile_no, modelCheckUser.getMobile());
                mapheader.put(UtillClasses.x_uuid, modelCheckUser.getUuid());
                mapheader.put(UtillClasses.x_model, modelCheckUser.getMobModel());
                mapheader.put(UtillClasses.x_platform, modelCheckUser.getMobPlatform());
                mapheader.put(UtillClasses.x_platform_ver, modelCheckUser.getMobPlatVer());
                mapheader.put(UtillClasses.x_device_name, modelCheckUser.getMobDevicName());
                mapheader.put(UtillClasses.x_app_ver, modelCheckUser.getAppVer());

                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                Log.e(TAG, "login reuest is " + mapheader.toString());
                return mapheader;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    private void introSlider() {


        final Map<String, String> deviceInfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "get_wizard";


        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e(TAG, "Check IntroSlider Data" + response);


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {

                                JSONArray array = jsonObject.getJSONArray("data");
                                WelcomeActivity.jsonArray = array;
                                dataLoaded = true;

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e(TAG, "Data " + error.getMessage());


                      /*  try {
                            Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/


                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceInfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceInfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceInfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceInfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceInfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceInfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceInfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceInfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceInfoMap.get(SessionManager.User_lang));
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice count header is " + mapheader.toString());
                return mapheader;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQuest_Read_Phone_State:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //this only give IMEI number
                    TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    @SuppressLint("MissingPermission") String Uuid = tManager.getDeviceId();

                    //this will give UUID Number
                    TelephonyManager teleManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                    @SuppressLint("MissingPermission") String tmSerial = teleManager.getSimSerialNumber();
                    @SuppressLint("MissingPermission") String tmDeviceId = teleManager.getDeviceId();
                    String androidId = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
                    if (tmSerial == null) tmSerial = "1";
                    if (tmDeviceId == null) tmDeviceId = "1";
                    if (androidId == null) androidId = "1";
                    UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDeviceId.hashCode() << 32) | tmSerial.hashCode());
                    uuid = deviceUuid.toString();

                    Log.e(TAG, "fist uuid number " + Uuid + " second UUID number " + uuid);
                    // return uniqueId;
                }
                break;
            default:
                break;
        }
    }


    public void tempLogin(ModelCheckUser modelCheckUser) {

        String tempSignature = GlobalDataAcess.DefaultAPiSig;
        sessionManager.SetSessionInfo(tempSignature, 0, "istojanovic@gmail.com");
        Map<String, String> mapDeviceInfo = new HashMap<String, String>();
        mapDeviceInfo.put(SessionManager.UUID, modelCheckUser.getUuid());
        mapDeviceInfo.put(SessionManager.MODEL, modelCheckUser.getMobModel());
        mapDeviceInfo.put(SessionManager.PLATFORM, modelCheckUser.getMobPlatform());
        mapDeviceInfo.put(SessionManager.PLATFORM_VER, modelCheckUser.getMobPlatVer());
        mapDeviceInfo.put(SessionManager.DeviceNmae, modelCheckUser.getMobDevicName());
        mapDeviceInfo.put(SessionManager.App_Ver, modelCheckUser.getAppVer());
        mapDeviceInfo.put(SessionManager.User_lang, Locale.getDefault().getDisplayLanguage());
        mapDeviceInfo.put(SessionManager.EMAIL, modelCheckUser.getEmail());
        mapDeviceInfo.put(SessionManager.SIGNATURE, tempSignature);
        sessionManager.SetDeviceINfo(mapDeviceInfo);

    }

    @Override
    public void CreateDemoAcc(String email, String pass, String Phone) {
        email_et.setText(email);
        pass_et.setText(pass);
        phone_et.setText(Phone);
    }


}
