package com.tenderbooking.Services;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.FragmentTender;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.DrawerItemClicked;
import com.tenderbooking.Model.ModelAdviceAnsware;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.Model.ModelUserFilters;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;
import com.tenderbooking.Splash;
import com.tenderbooking.Volley.AppController;
import com.tenderbooking.event_bus.HomeCallEvent;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledExecutorService;


public class NotificaitonServices extends Service {


    public static String TAG = NotificaitonServices.class.getSimpleName();
    public AlarmManager alarmManager;
    public SessionManager sessionManager;
    DataBaseHalper dataBaseHalper;
    Map<String, String> deviceinfoMap;
    ScheduledExecutorService scheduledExecutorService;
    int array_size = 0;
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        //HandlerThread thread = new HandlerThread("ServiceStartArguments",
        //       Process.THREAD_PRIORITY_BACKGROUND);
        // thread.start();
        //try {
        //  thread.sleep(10000);
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        //}
//
//        // Get the HandlerThread's Looper and use it for our Handler
        // mServiceLooper = thread.getLooper();
        //  mServiceHandler = new ServiceHandler(mServiceLooper);


        alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());
        deviceinfoMap = sessionManager.GetDeviceInfo();

        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

//        sessionManager = new SessionManager(getApplicationContext());
//        if (alarmManager ==null);
//        alarmManager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
//
//        Intent myIntent = new Intent(GlobalDataAcess.user_Broadcast);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,myIntent,0);
//        alarmManager.cancel(pendingIntent);
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,0,1000*60*2,pendingIntent);


        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
      /*  Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);*/

        // If we get killed, after returning from here, restart
        Log.e(TAG, "service start ");

        if (intent.hasExtra(GlobalDataAcess.Notification_type) && (intent.getIntExtra(GlobalDataAcess.Notification_type, 0) == 1))
            ;
        { //  semicolon is placed here
            getCheckAdviceQueNumber(getApplicationContext());
            getCheckAdviceAnsware(getApplicationContext());
            // getAdviceAnsware(getApplicationContext());
            SendSyncAction(getApplicationContext());

        }


//         scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
//
//        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
//            @Override
//            public void run() {
//                getCheckAdviceQueNumber(getApplicationContext());
//                getCheckAdviceAnsware(getApplicationContext());
//                getAdviceAnsware(getApplicationContext());
//            }
//        },0,60*1000*2, TimeUnit.SECONDS);


//                                    Runnable runnable = new Runnable() { // thread to run after specific time dudration.
//                                        @Override
//                                        public void run() {
//
//                                            new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    //when data array is empty
//                                                    getCheckAdviceQueNumber(getApplicationContext());
//                                                    getCheckAdviceAnsware(getApplicationContext());
//                                                   // getAdviceAnsware(getApplicationContext());
//                                                }
//                                            },60*1000*2);
//                                        }
//                                    };
//                                    runnable.run();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        if (scheduledExecutorService != null) {
            if (!scheduledExecutorService.isShutdown()) {
                scheduledExecutorService.shutdown();
            }
            //  startService(new Intent(this,NotificaitonServices.class));
            Log.e(TAG, "service stop");
            Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();

        }
    }

    public synchronized void getAdviceAnsware(final Context mContext) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        final String mURL = UtillClasses.BaseUrl + "check_advice_answers";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " getdata  service at responce  advice answare " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getJSONArray("data").length() > 0) {
                                    DataBaseHalper dataBaseHalper = new DataBaseHalper(mContext);
                                    Log.e(TAG, "sercie at advice answare reviced");
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int dataSize = jsonArray.length();

                                    for (int i = 0; dataSize > i; i++) {
                                        JSONObject jsondata = jsonArray.getJSONObject(i);
                                        ModelAdviceAnsware modelAdviceAnsware = new ModelAdviceAnsware();

                                        modelAdviceAnsware.setIdQuestion(jsondata.optString("idQuestion"));
                                        modelAdviceAnsware.setDetialsAnsware(jsondata.optString("detailsAnswer"));
                                        modelAdviceAnsware.setTitleQuestion(jsondata.optString("titleQuestion"));
                                        // dataBaseHalper.Insert_adviceanswer(modelAdviceAnsware);
                                        dataBaseHalper.updateStatusAdvice(modelAdviceAnsware.getIdQuestion(), "user", "4");
                                        Log.e(TAG, "sercie at advice answare question id" + jsondata.optString("idQuestion"));
                                        //  showNotification(mContext,modelAdviceAnsware.getTitleQuestion());
                                    }


                                    //Log.e(TAG)
                                    // int data = jsonObject.getInt("data");

//                                    Intent intent = new Intent(mContext , Home.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                                    PendingIntent pendingIntent = PendingIntent.getActivity(mContext,0,intent,PendingIntent.FLAG_ONE_SHOT);
//
//                                    Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.app_icon);
//
//                                    android.support.v4.app.NotificationCompat.Builder mBuilder =
//                                            new NotificationCompat.Builder(mContext)
//                                                    .setContentTitle(mContext.getResources().getString(R.string.question_ans_arive))
//                                                    .setStyle(new NotificationCompat.BigTextStyle())
//                                                    .setAutoCancel(true)
//                                                    .setContentText(jsonArray.getJSONObject(jsonArray.length()-1).optString("titleQuestion"))
//                                                    .setSmallIcon(R.drawable.status_bar_icon)
//                                                    .setLargeIcon(icon)
//                                                    .setContentIntent(pendingIntent);
//
//                                    Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//                                    if (sessionManager.GetNotiSound()){
//                                        mBuilder.setSound(alarmsound);
//                                    }
//                                    Log.e(TAG,"Notification from id advice answer thorugh last time check ");
//                                    if (sessionManager.GetNotiEnableTimeInterval()) {
//                                        Log.e(TAG,"notification enable ");
//                                        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//                                        notificationManager.notify(0, mBuilder.build());
//                                    }else
//                                    {
//
//                                        Log.e(TAG,"notification disable ");
//                                        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");
//
//                                        String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
//                                        String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();
//
//                                        String current_session = time_format.format(new Date());
//
//                                    /*Date startTime = time_format.parse("23:00");
//                                    Date stopTime =  time_format.parse("07:00");*/
//
//                                        Date startTime = time_format.parse(start_time_sesssion);
//                                        Date stopTime =  time_format.parse(stop_time_sesssion);
//
//
//                                        Date time = new Date();
//
//                                        if(GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion+":00",stop_time_sesssion+":00",current_session) ) {
//                                            System.out.println("alarm enable");
//                                            Log.e(TAG,"alarm is enable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                        }else{
//                                            Log.e(TAG,"alarm is disable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                            int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
//                                            NotificationManager notificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                                            notificationManager.notify(m, mBuilder.build());
//                                        }
//
//                                    }
                                  /*
                                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                                    notificationManager.notify(0,mBuilder.build());*/


                                    // String date_time = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss").format(new java.util.Date());
                                    // sessionManager.setLastCheckService(date_time);


                                } else {
                                }
                            } else {

                            }

                        } catch (JSONException e) {
                            //progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {


                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                mapFooder.put(UtillClasses.lastCheck, sessionManager.GetlastCheckAdvAns());
                Log.e(TAG, "service advice answare lastcheck data : " + sessionManager.GetlastCheckAdvAns());
                // mapFooder.put(UtillClasses.lastCheck,"2016-01-01 12:00:00");


                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public synchronized void getCheckAdviceAnsware(final Context context) {
        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final ArrayList<ModelAdviceAnsware> modelAdviceAnswareList = new ArrayList<>();

        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(context);

        final String question_list = dataBaseHalper.getUnAnsweredAdvice();

        if (!TextUtils.isEmpty(question_list)) {
            final String mURL = UtillClasses.BaseUrl + "check_advice_id_answers";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e(TAG, "service error url " + mURL + " question list  " + question_list + " responce : " + response);
                           /* try {
                                Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " question list  " + question_list + " responce : " + response);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }*/

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getInt("statusCode") == 200) {

                                    if (jsonObject.getJSONArray("data").length() > 0) {

                                        //Log.e(TAG, "sercie at advice answare by id reviced");
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                                        int dataSize = jsonArray.length();

                                        for (int i = 0; dataSize > i; i++) {
                                            JSONObject jsondata = jsonArray.getJSONObject(i);
                                            ModelAdviceAnsware modelAdviceAnsware = new ModelAdviceAnsware();

                                            modelAdviceAnsware.setIdQuestion(jsondata.optString("idQuestion"));
                                            modelAdviceAnsware.setDetialsAnsware(jsondata.optString("detailsAnswer"));
                                            modelAdviceAnsware.setTitleQuestion(jsondata.optString("titleQuestion"));
                                            dataBaseHalper.Insert_adviceanswer(modelAdviceAnsware);
                                            dataBaseHalper.updateStatusAdvice(modelAdviceAnsware.getIdQuestion(), "user", "4");
                                            Log.e(TAG, "sercie at advice answare question id" + jsondata.optString("idQuestion"));
                                            modelAdviceAnswareList.add(modelAdviceAnsware);

                                            showNotification(context, modelAdviceAnsware.getTitleQuestion(), context.getResources().getString(R.string.question_ans_arive));
                                            if (i + 1 == dataSize) {
                                                EventBus.getDefault().post(modelAdviceAnswareList);
                                                Log.e(TAG, "advice answer found for " + modelAdviceAnswareList.size());
                                            }
                                        }
//                                        //start notification
//                                        Intent intent = new Intent(context, Home.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                                        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_ONE_SHOT);
//
//                                        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
//
//                                        android.support.v4.app.NotificationCompat.Builder mBuilder =
//                                                new NotificationCompat.Builder(context)
//                                                        .setContentTitle(context.getResources().getString(R.string.question_ans_arive))
//                                                        .setStyle(new NotificationCompat.BigTextStyle())
//                                                        .setAutoCancel(true)
//                                                        .setContentText(jsonArray.getJSONObject(jsonArray.length()-1).optString("titleQuestion"))
//                                                        .setSmallIcon(R.drawable.status_bar_icon)
//                                                        .setLargeIcon(icon)
//                                                        .setContentIntent(pendingIntent);
//
//                                        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//                                        if (sessionManager.GetNotiSound()){
//                                            mBuilder.setSound(alarmsound);
//                                        }
//                                        Log.e(TAG,"Notification from id advice answer thorugh question id's");
//                                        if (sessionManager.GetNotiEnableTimeInterval()) {
//                                            Log.e(TAG,"notification enable ");
//                                            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//                                            notificationManager.notify(0, mBuilder.build());
//                                        }else
//                                        {
//
//                                            Log.e(TAG,"notification disable ");
//                                            SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");
//
//                                            String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
//                                            String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();
//
//                                            String current_session = time_format.format(new Date());
//
//                                    /*Date startTime = time_format.parse("23:00");
//                                    Date stopTime =  time_format.parse("07:00");*/
//
//                                            Date startTime = time_format.parse(start_time_sesssion);
//                                            Date stopTime =  time_format.parse(stop_time_sesssion);
//
//
//                                            Date time = new Date();
//
//                                            if(GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion+":00",stop_time_sesssion+":00",current_session) ) {
//                                                System.out.println("alarm enable");
//                                                Log.e(TAG,"alarm is enable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                            }else{
//                                                Log.e(TAG,"alarm is disable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                                int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
//                                                NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                                                notificationManager.notify(m, mBuilder.build());
//                                            }
//
//                                        }

                                        //Log.e(TAG)
                                        // int data = jsonObject.getInt("data");


                                  /*
                                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                                    notificationManager.notify(0,mBuilder.build());*/


                                        // String date_time = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss").format(new java.util.Date());
                                        // sessionManager.setLastCheckService(date_time);


                                    } else {
//                                        Runnable runnable = new Runnable() {
//                                            @Override
//                                            public void run() {
//
//                                                new Handler().postDelayed(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        //when data array is empty
//                                                        getCheckAdviceAnsware(context);
//
//                                                    }
//                                                }, 60000);
//                                            }
//                                        };
//                                        runnable.run();
                                    }
                                } else {

                                }

                            } catch (JSONException e) {
                                //progressDialog.hide();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "service error url " + mURL + " question list  " + question_list + " error msg " + error.getMessage() + " caouse " + error.getCause());
                   /* try {
                        Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " question list  " + question_list + " error msg " + error.getMessage() + " caouse " + error.getCause());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        getCheckAdviceAnsware();
//
//                                    }
//                                }, 60000);
//                            }
//                        };
//                        runnable.run();
                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {


                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> mapheader = new HashMap<String, String>();


                    mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                    mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                    mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                    mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                    mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                    mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                    mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                    mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                    mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                    mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                    //mapheader.put(UtillClasses.x_user_language,"english");
                    //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                    mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                    return mapheader;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> mapFooder = new HashMap<>();

                    mapFooder.put(UtillClasses.IdList, question_list);
                    // mapFooder.put(UtillClasses.lastCheck,"2016-01-01 12:00:00");


                    return mapFooder;
                }
            };
            //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(15 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
            stringRequest.setShouldCache(false);
            AppController.getmInstance().addToRequestQueue(stringRequest);
        }

    }

    public synchronized void getCheckAdviceQueNumber(final Context context) {

        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());
        deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "check_advices_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        setlastcheck();
                        Log.e(TAG, "check advice number response is " + response);
/*

                        try {
                            Logger.getInstance().createFileOnDevice(true, " service url:" + mURL + " last check " + dataBaseHalper.getLastAdviceAssignedDate() + "  responce : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {

                                if (jsonObject.getInt("data") > 0) {
                                    //get advice list
                                    Log.e(TAG, "data is more then expected is :" + jsonObject.getInt("data"));
                                    showNotification(context, String.format(context.getResources().getString(R.string.you_have_new_advice), jsonObject.getString("data")), context.getResources().getString(R.string.tenderlive_notification));
                                    getAdviceList(context);

                                } else {


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error advice new count " + error.getMessage());
               /* try {
                    Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " last check " + dataBaseHalper.getLastAdviceAssignedDate() + " error msg " + error.getMessage() + " caouse " + error.getCause());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice count header is " + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();

                //String lastcheck = sessionManager.getAdviceLastCheck();
                String lastcheck = dataBaseHalper.getLastAdviceAssignedDate();
                //String lastcheck = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                // String lastcheck ="2017-01-01 03:19:14";
                Log.e(TAG, "last check for the advice is " + lastcheck);

                if (!lastcheck.equals("0000-00-00 00:00:00"))
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public synchronized void getAdviceList(final Context context) {
        final String mURL = UtillClasses.BaseUrl + "get_advices_list";

        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());
        deviceinfoMap = sessionManager.GetDeviceInfo();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                /*try {
                    Logger.getInstance().createFileOnDevice(true, " service url:" + mURL + " last check " + dataBaseHalper.getLastAdviceAssignedDate() + "  responce : " + response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
*/
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("statusCode") == 200) {
                        //setlastcheck();
                        ArrayList<ModelAdviceList> modelAdviceListArrayList = new ArrayList<>();

                        sessionManager.setAdviceFirstTime(false);
                        if (!jsonObject.isNull("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            int size = jsonArray.length();

                            ArrayList<ModelAdviceList> modelAdviceListArrayList1 = new ArrayList<>();
                            modelAdviceListArrayList1.clear();
                            Log.e(TAG, "size of advice number is :" + size);
                            for (int i = 0; i < size; i++) {
                                JSONObject jsondata = jsonArray.getJSONObject(i);
                                ModelAdviceList modelAdviceList = new ModelAdviceList();
                                modelAdviceList.setTypeAdvice(jsondata.optString("typeAdvice"));
                                //  Log.e(TAG,"type of advice recived is "+jsondata.optString("typeAdvice"));
                                modelAdviceList.setIdAdvice(jsondata.optString("idAdvice"));
                                modelAdviceList.setIdTheme(jsondata.optInt("idTheme"));
                                modelAdviceList.setNameTheme(jsondata.optString("nameTheme"));
                                modelAdviceList.setCountryFlagAdvice(jsondata.optString("countryFlagAdvice"));
                                modelAdviceList.setSourceAdvice(jsondata.optString("sourceAdvice"));
                                modelAdviceList.setTitleAdvice(jsondata.optString("titleAdvice"));
                                modelAdviceList.setDocumentAdvice(jsondata.optString("documentAdvice"));
                                modelAdviceList.setUpdatedAdvice(jsondata.optString("updatedAdvice"));
                                modelAdviceList.setPublicatedAdvice(jsondata.optString("publicatedAdvice"));
                                modelAdviceList.setHaveRateAdvice(jsondata.optInt("haveRateAdvice"));
                                modelAdviceList.setStatusAdvice(jsondata.optString("statusAdvice"));
                                modelAdviceList.setCountryAdvice(jsondata.optString("countryAdvice"));
                                //add model in the arraylist or update the list


                                // modelAdviceListList.add(modelAdviceList);
                                if (modelAdviceList.getTypeAdvice().equals("user")) {
                                    Log.e(TAG, "user data respnce is " + jsondata.toString());
                                    // modelAdviceList.setStatusAdvice("2");
                                    //  dataBaseHalper.InsertAdvicdListUser(modelAdviceList);
                                    modelAdviceListArrayList.add(modelAdviceList);
                                    //showNotification(context,modelAdviceList.getTitleAdvice(),context.getResources().getString(R.string.new_quest_arrive));
                                }
                                // dataBaseHalper.InsertAdvicdList(modelAdviceList);
                                // Log.e(TAG,"Insetion for the advice number is :"+i);

                                modelAdviceListArrayList1.add(modelAdviceList);

                                // setDatafromDatabase("user",-1);
                                if (i + 1 == size) {
                                    dataBaseHalper.InsertAdvicdList(modelAdviceListArrayList1);
                                    // EventBus.getDefault().post(modelAdviceListArrayList);
                                    Log.e(TAG, "post advice list from advice list ");
                                }
                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " volley responce error  reason is " + error.getMessage());
                //getAdviceList();
             /*   try {
                    Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " last check " + dataBaseHalper.getLastAdviceAssignedDate() + " error msg " + error.getMessage() + " caouse " + error.getCause());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice list header data" + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();


                //  String lastcheck = sessionManager.getAdviceLastCheckGMT();
                if (sessionManager.getAdviceFirstTime()) { // remove due to check and remove on destroy condition
                    //  mapFooder.put(UtillClasses.lastCheck, "0000-00-00 00:00:00");
                } else if (!dataBaseHalper.getLastAdviceAssignedDate().equals("0000-00-00 00:00:00")) {
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());
                }
                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void setlastcheck() {
        String simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

        SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

        String GMtDate = simpleDateFormatGMT.format(new Date());
        sessionManager.setAdviceLastCheck(simpleDateFormat, GMtDate);


    }

    public void showNotification(Context mContext, String message, String title) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Channel", "tender", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }

        }

        Intent intent = new Intent(mContext, Splash.class);
        intent.putExtra(GlobalDataAcess.Notification_type, true);
        intent.putExtra(GlobalDataAcess.Menu_option, GlobalDataAcess.group_selected);
        intent.putExtra(GlobalDataAcess.MenuOptionAdvice, GlobalDataAcess.group_select_advice);
        intent.putExtra(GlobalDataAcess.MenuOptionAdviceMain, GlobalDataAcess.group_select_advice_main);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.app_icon);

        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext, "Channel")
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle())
                        .setAutoCancel(true)
                        .setContentText(message)
                        //.setSubText(message)
                        .setSmallIcon(R.drawable.status_bar_icon)
                        .setLargeIcon(icon)
                        .setContentIntent(pendingIntent);

        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if (sessionManager.GetNotiSound()) {
            mBuilder.setSound(alarmsound);
        }
        int m = (int) ((new Date().getTime()));
        Log.e(TAG, "Notification from id advice answer thorugh last time check ");
        if (sessionManager.GetNotiEnableTimeInterval()) {
            Log.e(TAG, "notification enable ");
            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, mBuilder.build());
        } else {

            Log.e(TAG, "notification disable ");
            SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");

            String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
            String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();

            String current_session = time_format.format(new Date());

                                    /*Date startTime = time_format.parse("23:00");
                                    Date stopTime =  time_format.parse("07:00");*/

            //   Date startTime = time_format.parse(start_time_sesssion);
            //  Date stopTime =  time_format.parse(stop_time_sesssion);


            Date time = new Date();

            try {
                if (GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion + ":00", stop_time_sesssion + ":00", current_session)) {
                    System.out.println("alarm enable");
                    Log.e(TAG, "alarm is enable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                } else {
                    Log.e(TAG, "alarm is disable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                    // int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                    NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(0, mBuilder.build());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    public void SendSyncAction(final Context mContext) {
        final String mURL = UtillClasses.BaseUrl + "get_tender_actions";

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String message = " synchronization sucess , url :" + mURL + " action type " + ", responce: " + response.toString();
                       /* try {
                            Logger.getInstance().createFileOnDevice(true, message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        SyncData syncData = new SyncData(response);
                        syncData.execute();
                        // action on response
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            if (jsonObject.getInt("statusCode")==200) {
//                                JSONArray jsonArray = jsonObject.getJSONArray("data");
//
//                                int array_size = jsonArray.length();
//
//                                for (int i = 0;i < array_size ;i++){
//                                    JSONObject jsonData = jsonArray.getJSONObject(i);
//                                    switch (jsonData.optString(""))
//                                }
//
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mProgressDialog.hide();
                String message = " synchronization failed , url :" + mURL + ", error: " + error.getCause() + " error message " + error.getMessage();
              /*  try {
                    Logger.getInstance().createFileOnDevice(true, message);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();


                // if (ask_adv_spinner!= null)
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                } else if (error instanceof AuthFailureError) {
                    //
                } else if (error instanceof ServerError) {

                }
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

//                mapFooder.put(UtillClasses.Action_ActionType,actionType);
//                mapFooder.put(UtillClasses.Action_ActionDetails,action);

                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void getSyncronizeTender(String tenderId, String stauts) {
        //String statusTender = dataBaseHalper.getTenderStatus(tenderId);


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
        sessionManager.setIsAppKill(true);
        // sessionManager.setSelectedQwery("");
    }

    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }


        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    public class SyncData extends AsyncTask<Void, Void, Void> {

        String response;

        public SyncData(String response) {
            this.response = response;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getInt("statusCode") == 200) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    array_size = jsonArray.length();
                    if (array_size > 0)
                        for (int i = 0; i < array_size; i++) {
                            JSONObject jsonData = jsonArray.getJSONObject(i);
                            String actionType = jsonData.optString("actionDetails");
                            String[] actionTypeArray = actionType.split("\\|");
//                        long statusTender = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));

                            switch (jsonData.optString("actionType")) {

                                case UtillClasses.Action_tender_read:
                                    dataBaseHalper.SetReadedStatus(jsonData.optString("actionDetails"), 1, -1);
                                    EventBus.getDefault().post(1);
                                    break;
                                case UtillClasses.Action_tender_Unread:
                                    dataBaseHalper.SetReadedStatus(jsonData.optString("actionDetails"), 0, -1);
                                    EventBus.getDefault().post(1);
                                    break;
                                case UtillClasses.Action_tender_delete:
                                    long statusTender = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    if (((statusTender) & 2147483648l) != 2147483648l) {
                                        long action = (statusTender) | 2147483648l;
                                        getSyncronizeTender(actionTypeArray[0], String.valueOf(action));
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tender_undelete:
                                    long statusTender_1 = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    if (((statusTender_1) & 2147483648l) == 2147483648l) {
                                        long action = (statusTender_1) & 2147483647l;
                                        getSyncronizeTender(actionTypeArray[0], String.valueOf(action));
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tender_flag_set:
                                    long statusTender_2 = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    if ((statusTender_2 & 8192l) != 8192l) {
                                        long stautsData = statusTender_2 | 8192;
                                        dataBaseHalper.SetFlagStatus(actionTypeArray[0], String.valueOf(stautsData));
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tender_flag_unset:
                                    long statusTender_3 = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    long unset_flat_val = 4294967295l - 8192l;
                                    long flag_deSelect_value = statusTender_3 & unset_flat_val;
                                    dataBaseHalper.SetFlagStatus(actionTypeArray[0], String.valueOf(flag_deSelect_value));
                                    EventBus.getDefault().post(1);
                                    break;
                                case UtillClasses.Action_tender_tag_set:
                                    String tenderTag = dataBaseHalper.getTenderTag(actionTypeArray[0]);
                                    if ((Long.parseLong(tenderTag) & Long.parseLong(actionTypeArray[1])) != Long.parseLong(actionTypeArray[1])) {
                                        long tagSet = (Long.parseLong(tenderTag) | Long.parseLong(actionTypeArray[1]));
                                        dataBaseHalper.UpdateTagList(String.valueOf(tagSet), actionTypeArray[0]);
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tender_tag_unset:
                                    String tenderTag1 = dataBaseHalper.getTenderTag(actionTypeArray[0]);
                                    if ((Long.parseLong(tenderTag1) & Long.parseLong(actionTypeArray[1])) == Long.parseLong(actionTypeArray[1])) {
                                        long tagSet = (Long.parseLong(tenderTag1) - Long.parseLong(actionTypeArray[1]));
                                        dataBaseHalper.UpdateTagList(String.valueOf(tagSet), actionTypeArray[0]);
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tender_spam_set:
                                    long statusTender_4 = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    if ((!((statusTender_4 & Long.parseLong("2147483648")) > 0)) && (((statusTender_4) & Long.parseLong("16384")) != 16384l)) {
                                        String spam_set = String.valueOf(statusTender_4 | 16384l);
                                        dataBaseHalper.SetFlagStatus(actionTypeArray[0], spam_set); // only used flag to update status data
                                    }
                                    EventBus.getDefault().post(1);
                                    break;
                                case UtillClasses.Action_tender_spam_unset:
                                    // data not avalible
                                    long statusTender_5 = Long.parseLong(dataBaseHalper.getTenderStatus(actionTypeArray[0]));
                                    if ((!((statusTender_5 & Long.parseLong("2147483648")) > 0)) && (((statusTender_5) & Long.parseLong("16384")) == 16384l)) {
                                        String spam_unset = String.valueOf(statusTender_5 - 16384l);
                                        dataBaseHalper.SetFlagStatus(actionTypeArray[0], spam_unset); // only used flag to update status data
                                        EventBus.getDefault().post(1);
                                    }
                                    break;
                                case UtillClasses.Action_tag_add:
                                    ModelUserTag modeluserTag = new ModelUserTag();
                                    modeluserTag.setTagIndex(actionTypeArray[0]);
                                    modeluserTag.setTagText(actionTypeArray[1]);
                                    modeluserTag.setColorId(actionTypeArray[2]);
                                    modeluserTag.setColorCSS(actionTypeArray[3]);
                                    modeluserTag.setColorName(actionTypeArray[4]);

                                    if (Home.modelUserTagArrayList != null) {
                                        Home.modelUserTagArrayList.add(modeluserTag);
                                        //  Home.adapter.notifyDataSetChanged();
                                        HomeCallEvent homeCallEvent = new HomeCallEvent();
                                        homeCallEvent.setStart(true);
                                        EventBus.getDefault().post(homeCallEvent);
                                    }
                                    break;
                                case UtillClasses.Action_tag_update:
                                    ModelUserTag modeluserTag_updae = new ModelUserTag();
                                    modeluserTag_updae.setTagIndex(actionTypeArray[0]);
                                    modeluserTag_updae.setTagText(actionTypeArray[1]);
                                    modeluserTag_updae.setColorId(actionTypeArray[2]);
                                    modeluserTag_updae.setColorCSS(actionTypeArray[3]);
                                    modeluserTag_updae.setColorName(actionTypeArray[4]);

                                    if (Home.modelUserTagArrayList != null) {
                                        for (int k = 0; k < Home.modelUserTagArrayList.size(); k++) {
                                            if (Home.modelUserTagArrayList.get(k).getTagIndex().equals(modeluserTag_updae.getTagIndex())) {
                                                Home.modelUserTagArrayList.get(k).setTagText(modeluserTag_updae.getTagText());
                                                Home.modelUserTagArrayList.get(k).setColorName(modeluserTag_updae.getColorName());
                                                Home.modelUserTagArrayList.get(k).setColorCSS(modeluserTag_updae.getColorCSS());
                                                Home.modelUserTagArrayList.get(k).setColorId(modeluserTag_updae.getColorId());
//                                        Home.modelUserTagArrayList.get(k).
                                                // Home.adapter.notifyDataSetChanged();
                                                HomeCallEvent homeCallEvent = new HomeCallEvent();
                                                homeCallEvent.setStart(true);
                                                EventBus.getDefault().post(homeCallEvent);
                                            }
                                        }
                                    }

                                    break;
                                case UtillClasses.Action_tag_del:

                                    if (Home.modelUserTagArrayList != null) {

                                        for (int j = 0; j < Home.modelUserTagArrayList.size(); j++) {
                                            if (Home.modelUserTagArrayList.get(j).getTagIndex().equals(actionTypeArray[0])) {
                                                Home.modelUserTagArrayList.remove(j);
                                                HomeCallEvent homeCallEvent = new HomeCallEvent();
                                                homeCallEvent.setStart(true);
                                                EventBus.getDefault().post(homeCallEvent);
//                                                for (int k = j;k<Home.modelUserFiltersArrayList.size();k++){
//                                                    int tagIndex_incremented = Integer.parseInt(Home.modelUserTagArrayList.get(k).getTagIndex())+ 1;
//
//                                                    Home.modelUserTagArrayList.get(k).setTagIndex( String.valueOf(tagIndex_incremented) );
//                                                }
                                            }
                                        }
                                    }

                                    break;
                                case UtillClasses.Action_filter_add:
                                    ModelUserFilters modelUserfilter_add = new ModelUserFilters();
                                    modelUserfilter_add.setFilterType(actionTypeArray[0]);
                                    modelUserfilter_add.setFilterId(actionTypeArray[1]);
                                    modelUserfilter_add.setFilterName(actionTypeArray[2]);
                                    modelUserfilter_add.setFilterAll(actionTypeArray[3]);

                                    if (Home.modelUserFiltersArrayList != null) {
                                        Home.modelUserFiltersArrayList.add(modelUserfilter_add);
                                        HomeCallEvent homeCallEvent = new HomeCallEvent();
                                        homeCallEvent.setStart(true);
                                        EventBus.getDefault().post(homeCallEvent);
                                    }
                                    break;
                                case UtillClasses.Action_filter_update:

                                    if (Home.modelUserFiltersArrayList != null) {

                                        for (int k = 0; k < Home.modelUserFiltersArrayList.size(); k++) {
                                            if (Home.modelUserFiltersArrayList.get(k).getFilterId().equals(actionTypeArray[1])) {
                                                Home.modelUserFiltersArrayList.get(k).setFilterType(actionTypeArray[0]);
                                                Home.modelUserFiltersArrayList.get(k).setFilterName(actionTypeArray[2]);
                                                Home.modelUserFiltersArrayList.get(k).setFilterAll(actionTypeArray[3]);
                                                //   Home.adapter.notifyDataSetChanged();

                                                HomeCallEvent homeCallEvent = new HomeCallEvent();
                                                homeCallEvent.setStart(true);
                                                EventBus.getDefault().post(homeCallEvent);
                                            }
                                        }
                                    }

                                    break;
                                case UtillClasses.Action_filter_del:
                                    if (Home.modelUserFiltersArrayList != null) {

                                        for (int k = 0; k < Home.modelUserFiltersArrayList.size(); k++) {
                                            if (Home.modelUserFiltersArrayList.get(k).getFilterId().equals(actionTypeArray[1])) {
                                                Home.modelUserFiltersArrayList.remove(k);
                                                //Home.adapter.notifyDataSetChanged();
                                                HomeCallEvent homeCallEvent = new HomeCallEvent();
                                                homeCallEvent.setStart(true);
                                                EventBus.getDefault().post(homeCallEvent);
                                            }
                                        }
                                    }
                                    break;
                            }

                            if (i + 1 == array_size) {

//                                // for home Activity
//                                if (Home.adapter != null) {
//                                    Home.adapter.notifyDataSetChanged();
//                                }
//                                // for fragment tender
//                                DrawerItemClicked drawerItemClicked = new FragmentTender();
//                                drawerItemClicked.GetDrawerItemClicked(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
//
//
//                                if (EventBus.getDefault().isRegistered(FragmentTender.class)) {
//                                    EventBus.getDefault().post(2);
//                                }
//                                if (EventBus.getDefault().isRegistered(Home.class)) {
//                                    HomeCallEvent homeCallEvent = new HomeCallEvent();
//                                    homeCallEvent.setStart(true);
//                                    EventBus.getDefault().postSticky(homeCallEvent);
//                                }
                            }
                        }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
             /*   try {
                    Logger.getInstance().createFileOnDevice(true, " catch part of sync data from server ");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }*/

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            // for home Activity
//            if (Home.adapter != null) {
//                Home.adapter.notifyDataSetChanged();
//
//            }
            if (array_size > 0) {

                if (Home.handler != null) {

                    Home.handler.sendEmptyMessage(0);
                }

                // for fragment tender
                DrawerItemClicked drawerItemClicked = new FragmentTender();
                drawerItemClicked.GetDrawerItemClicked(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);


                if (EventBus.getDefault().isRegistered(FragmentTender.class)) {
                    EventBus.getDefault().post(2);
                }
                if (EventBus.getDefault().isRegistered(Home.class)) {
                    HomeCallEvent homeCallEvent = new HomeCallEvent();
                    homeCallEvent.setStart(true);
                    EventBus.getDefault().postSticky(homeCallEvent);
                }
            }
        }
    }
}