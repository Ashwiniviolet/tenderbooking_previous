package com.tenderbooking.Services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.NukeSSLCerts;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.R;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class MYBrodcastReciver extends WakefulBroadcastReceiver {
    public static String TAG = MYBrodcastReciver.class.getSimpleName();

    SessionManager sessionManager = null;
    public DataBaseHalper dataBaseHalper;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (sessionManager == null) {
            sessionManager = new SessionManager(context);
        }

        int alarmType = intent.getIntExtra(UtillClasses.AlarmType, 3);
        Log.e(TAG, "Alarm type value is " + alarmType);

        if (alarmType == 1 && sessionManager.GetNotiEnable()) {


            getTenderNumber(context);

         /*   Intent syncIntent = new Intent();
            syncIntent.putExtra(GlobalDataAcess.TypeService, 2);
            syncIntent.setClass(context, NotiIntentService.class);
            Log.e(TAG, " hit notification");

            startWakefulService(context, syncIntent);
*/
            //            Toast.makeText(context, "notification is called", Toast.LENGTH_SHORT).show();
            /*try {
                Logger.getInstance().createFileOnDevice(true, "notification is called");
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        } else if (alarmType == 2) {
            Log.e(TAG, " hit chache clear");

            DataBaseHalper dataBaseHalper = new DataBaseHalper(context);
            dataBaseHalper.getDeleteAllTable();

        } else if (alarmType == 3) {

           /* Intent syncIntent = new Intent();
            syncIntent.setClass(context, com.tenderbooking.Services.NotificaitonServices.class);
            Log.e(TAG, " hit notification Notification");
            startWakefulService(context, syncIntent);*/
        }
    }


    public void getTenderNumber(final Context context) {

        NukeSSLCerts.nuke();

        if (sessionManager == null)
            sessionManager = new SessionManager(context.getApplicationContext());
        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(context.getApplicationContext());

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        final String mURL = UtillClasses.BaseUrl + "check_tenders_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " getdata service at responce " + response);

                        /*try {
                            Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " responce : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        // showdemoNotification(); // demo notificaton check
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {

                                if (jsonObject.getInt("data") > 0) {
                                    //previouesData = sessionManager.GetNotiTenderCount();

                                    //Log.e(TAG)
                                    int data = jsonObject.getInt("data");


                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                        NotificationChannel channel = new NotificationChannel("Channel", "tender", NotificationManager.IMPORTANCE_DEFAULT);
                                        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                                        if (notificationManager != null) {
                                            notificationManager.createNotificationChannel(channel);
                                        }

                                    }
                                    Intent intent = new Intent(context.getApplicationContext(), Home.class);
                                    intent.putExtra(GlobalDataAcess.Menu_option, GlobalDataAcess.group_selected);
                                    intent.putExtra(GlobalDataAcess.MenuOptionAdvice, GlobalDataAcess.group_select_advice);
                                    intent.putExtra(GlobalDataAcess.MenuOptionAdviceMain, GlobalDataAcess.group_select_advice_main);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                                    PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
                                    Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
                                    android.support.v4.app.NotificationCompat.Builder mBuilder =
                                            new NotificationCompat.Builder(context.getApplicationContext(), "Channel")
                                                    .setContentTitle(context.getResources().getString(R.string.tenderi__live_noti))
                                                    .setStyle(new NotificationCompat.BigTextStyle())
                                                    .setContentText(context.getResources().getString(R.string.you_have_new_tenders) + " ( " + data + " )")
                                                    .setSubText(context.getResources().getString(R.string.you_have_new_tenders) + " ( " + data + " )")
                                                    /* .setContentText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
                                                     .setSubText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
                                                    */.setSmallIcon(R.drawable.status_bar_icon)
                                                    .setAutoCancel(true)
                                                    .setLargeIcon(icon)
                                                    .setContentIntent(pendingIntent);

                                    Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                                    if (sessionManager.GetNotiSound()) {
                                        mBuilder.setSound(alarmsound);
                                    }

                                    if (sessionManager.GetNotiEnableTimeInterval()) {
                                        Log.e(TAG, "notification enable ");
                                        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                        notificationManager.notify(1, mBuilder.build());
                                    } else {

                                        Log.e(TAG, "notification disable ");
                                        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");

                                        String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
                                        String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();

                                        String current_session = time_format.format(new Date());

                                            /*Date startTime = time_format.parse("23:00");
                                            Date stopTime =  time_format.parse("07:00");*/

                                        /*Date startTime = time_format.parse(start_time_sesssion);
                                        Date stopTime = time_format.parse(stop_time_sesssion);


                                        Date time = new Date();*/

                                        if (GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion + ":00", stop_time_sesssion + ":00", current_session)) {
                                            System.out.println("alarm enable");
                                            Log.e(TAG, "alarm is enable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                                        } else {
                                            Log.e(TAG, "alarm is disable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                                            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                            notificationManager.notify(1, mBuilder.build());
                                        }

                                    }

                                       /* String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                                        sessionManager.setLastCheckService(date_time);*/


                                } else {

                                    //  Snackbar.make(mRecyclerView," Not new List Avalible ",Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                            }

                        } catch (JSONException e) {
                            //progressDialog.hide();
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               /* try {
                    Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " caouse : " + error.getCause());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e(TAG, " tender list service on error " + error.getMessage());
                    // GetTenderNumber();

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        GetTenderNumber();
//
//                                    }
//                                },60000);
//                            }
//                        };
//                        runnable.run();


                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        GetTenderNumber();
//
//                                    }
//                                },60000);
//                            }
//                        };
//                        runnable.run();
                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();
                DataBaseHalper dataBaseHalper_one = new DataBaseHalper(context.getApplicationContext());
                // mapFooder.put(UtillClasses.lastCheck,sessionManager.GetLastCheckServie());
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper_one.getLastTenderAssignedDate());


                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }


}