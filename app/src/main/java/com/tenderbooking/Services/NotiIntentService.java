package com.tenderbooking.Services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.Model.MessageEvent;
import com.tenderbooking.Model.ModelTenderList;
import com.tenderbooking.R;
import com.tenderbooking.Volley.AppController;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class NotiIntentService extends IntentService {


    public static final String TAG = NotiIntentService.class.getSimpleName();
    public static int previouesData = 0;
    public SessionManager sessionManager;
    public DataBaseHalper dataBaseHalper;
    public boolean isrun = true;
    /*
     *
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */


    public NotiIntentService() {
        super("name");
    }

    public NotiIntentService(String name) {
        super(name);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sessionManager != null) {
            sessionManager = null;
        }
        if (dataBaseHalper != null) {
            dataBaseHalper = null;
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            sessionManager = new SessionManager(getApplicationContext());
            dataBaseHalper = new DataBaseHalper(getApplicationContext());
            MYBrodcastReciver.completeWakefulIntent(intent);
            Log.e(TAG, " hit intent service in the this section ");

            int typeService = intent.getIntExtra(GlobalDataAcess.TypeService, 3);

            Log.e(TAG, "servcie type " + typeService);

            if (typeService == 1) {

                Log.e(TAG, "advice service intent in while");
                //getAdviceAnsware();


            }
            if (typeService == 2) {
                GetTenderNumber();
            }
        } else {
            Log.e(TAG, "intent is empty ");
        }
    }

    public void GetTenderNumber() {
        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());
        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(getApplicationContext());

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        final String mURL = UtillClasses.BaseUrl + "check_tenders_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " getdata service at responce " + response);

                        /*try {
                            Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " responce : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        // showdemoNotification(); // demo notificaton check
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {

                                if (jsonObject.getInt("data") > 0) {
                                    //previouesData = sessionManager.GetNotiTenderCount();

                                    //Log.e(TAG)
                                    int data = jsonObject.getInt("data");
                                    GetData();

                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                        NotificationChannel channel = new NotificationChannel("Channel", "tender", NotificationManager.IMPORTANCE_DEFAULT);
                                        NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
                                        if (notificationManager != null) {
                                            notificationManager.createNotificationChannel(channel);
                                        }

                                    }

                                    Intent intent = new Intent(getApplicationContext(), Home.class);
                                    intent.putExtra(GlobalDataAcess.Menu_option, GlobalDataAcess.group_selected);
                                    intent.putExtra(GlobalDataAcess.MenuOptionAdvice, GlobalDataAcess.group_select_advice);
                                    intent.putExtra(GlobalDataAcess.MenuOptionAdviceMain, GlobalDataAcess.group_select_advice_main);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
                                    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);
                                    android.support.v4.app.NotificationCompat.Builder mBuilder =
                                            new NotificationCompat.Builder(getApplicationContext(), "Channel")
                                                    .setContentTitle(getResources().getString(R.string.tenderi__live_noti))
                                                    .setStyle(new NotificationCompat.BigTextStyle())
                                                    .setContentText(getResources().getString(R.string.you_have_new_tenders) + " ( " + data + " )")
                                                    .setSubText(getResources().getString(R.string.you_have_new_tenders) + " ( " + data + " )")
                                                    /* .setContentText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
                                                     .setSubText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
                                                    */.setSmallIcon(R.drawable.status_bar_icon)
                                                    .setAutoCancel(true)
                                                    .setLargeIcon(icon)
                                                    .setContentIntent(pendingIntent);

                                    Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                                    if (sessionManager.GetNotiSound()) {
                                        mBuilder.setSound(alarmsound);
                                    }

                                    if (sessionManager.GetNotiEnableTimeInterval()) {
                                        Log.e(TAG, "notification enable ");
                                        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                        notificationManager.notify(1, mBuilder.build());
                                    } else {

                                        Log.e(TAG, "notification disable ");
                                        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");

                                        String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
                                        String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();

                                        String current_session = time_format.format(new Date());

                                            /*Date startTime = time_format.parse("23:00");
                                            Date stopTime =  time_format.parse("07:00");*/

                                        /*Date startTime = time_format.parse(start_time_sesssion);
                                        Date stopTime = time_format.parse(stop_time_sesssion);


                                        Date time = new Date();*/

                                        if (GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion + ":00", stop_time_sesssion + ":00", current_session)) {
                                            System.out.println("alarm enable");
                                            Log.e(TAG, "alarm is enable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                                        } else {
                                            Log.e(TAG, "alarm is disable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                            notificationManager.notify(1, mBuilder.build());
                                        }

                                    }

                                       /* String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                                        sessionManager.setLastCheckService(date_time);*/


                                } else {

                                    //  Snackbar.make(mRecyclerView," Not new List Avalible ",Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                            }

                        } catch (JSONException e) {
                            //progressDialog.hide();
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               /* try {
                    Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " caouse : " + error.getCause());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e(TAG, " tender list service on error " + error.getMessage());
                    // GetTenderNumber();

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        GetTenderNumber();
//
//                                    }
//                                },60000);
//                            }
//                        };
//                        runnable.run();


                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        GetTenderNumber();
//
//                                    }
//                                },60000);
//                            }
//                        };
//                        runnable.run();
                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();
                DataBaseHalper dataBaseHalper_one = new DataBaseHalper(getApplicationContext());
                // mapFooder.put(UtillClasses.lastCheck,sessionManager.GetLastCheckServie());
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper_one.getLastTenderAssignedDate());
              /*  try {
                    Logger.getInstance().createFileOnDevice(true, "service tender count last check " + dataBaseHalper_one.getLastTenderAssignedDate());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                // Log.e(TAG, " Header time and date for tender sevice "+sessionManager.GetLastCheckServie());
                // mapFooder.put(UtillClasses.lastCheck,"0000-00-00 00:00:00");
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                        /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                        Log.e(TAG, " Header time and date "+date_time);
                        sessionManager.setLastCheck(date_time);*/
                dataBaseHalper_one = null;

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void GetData() {
        if (sessionManager == null)
            sessionManager = new SessionManager(getApplicationContext());
        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(getApplicationContext());


        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        //final String mURL = UtillClasses.BaseUrl+"/get_tenders_list";
        final String mURL = "https://www.tenderilive.com/xmlrpcs/mobile/get_tenders_list";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        DataBaseHalper dataBaseHalper = new DataBaseHalper(getApplicationContext());
                        //this line is temp
                        // sessionManager.SetTenderList(response);

                      /*  try {
                            Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " responce : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {


                                //  String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                                //String date_time = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss").format(new java.util.Date());
                                // Log.e(TAG, " Header time and date "+GlobalDataAcess.ConvertedDate(date_time));
                                //sessionManager.setLastCheck(date_time);
                                //String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());

                                //sessionManager.setLastCheckService(date_time);
                                sessionManager.setLastCheckService(GlobalDataAcess.getGMTTime());

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                int size = jsonArray.length();
                                Log.e(TAG, " tender list responce SIZE TOTAL " + size);

                                for (int i = 0; i < size; i++) {

                                    JSONObject jsondata = jsonArray.getJSONObject(i);
                                    ModelTenderList modelTenderList = new ModelTenderList();
                                    modelTenderList.setAmountCurrency(jsondata.optString("amountCurrency"));
                                    modelTenderList.setAmountTender(jsondata.optDouble("amountTender"));
                                    modelTenderList.setAssignedTender(jsondata.optString("assignedTender"));
                                    modelTenderList.setCategoriesTender(jsondata.optString("categoriesTender"));
                                    modelTenderList.setCityBuyer(jsondata.optString("cityBuyer"));
                                    modelTenderList.setCityTender(jsondata.optString("cityBuyerID"));
                                    modelTenderList.setCountryFlagTender(jsondata.optString("countryFlagTender"));
                                    modelTenderList.setCountryTender(jsondata.optString("countryTender"));
                                    modelTenderList.setDateDocumentation(jsondata.optString("dateDocumentation"));
                                    modelTenderList.setDateStart(jsondata.optString("dateStart"));
                                    modelTenderList.setDateStop(jsondata.optString("dateStop"));
                                    modelTenderList.setDocumentationCurrency(jsondata.optString("documentationCurrency"));
                                    modelTenderList.setDocumentationPrice(jsondata.optDouble("documentationPrice"));
                                    modelTenderList.setIdAwardCriteria(jsondata.optString("idAwardCriteria"));
                                    modelTenderList.setIdBuyer(jsondata.optString("idBuyer"));
                                    modelTenderList.setIdContract(jsondata.optString("idContract"));
                                    modelTenderList.setIdDocument(jsondata.optString("idDocument"));
                                    modelTenderList.setIdNUTS(jsondata.optString("idNUTS"));
                                    modelTenderList.setIdProcedure(jsondata.optString("idProcedure"));
                                    modelTenderList.setIdTender(jsondata.optString("idTender"));
                                    modelTenderList.setMainLanguage(jsondata.optString("mainLanguage"));
                                    modelTenderList.setNameAwardCriteria(jsondata.optString("nameAwardCriteria"));
                                    modelTenderList.setNameBuyer(jsondata.optString("nameBuyer"));
                                    modelTenderList.setNameContract(jsondata.optString("nameContract"));
                                    modelTenderList.setNameDocument(jsondata.optString("nameDocument"));
                                    modelTenderList.setNameProcedure(jsondata.optString("nameProcedure"));
                                    modelTenderList.setOptionsTender(jsondata.optString("optionsTender"));
                                    modelTenderList.setSourcetender(jsondata.optString("sourceTender"));
                                    modelTenderList.setStatusTender(jsondata.optString("statusTender"));
                                    modelTenderList.setTagTender(jsondata.optString("tagTender"));
                                    modelTenderList.setTitleTender(jsondata.optString("titleTender"));
                                    modelTenderList.setTypeTender(jsondata.optString("typeTender"));
                                    modelTenderList.setUrlTender(jsondata.optString("urlTender"));


                                    // / arrayTenderList.add(modelTenderList);
                                    // filteredArraylist.add(modelTenderList);
                                    // temp data

                                    dataBaseHalper.InsertTenderList(modelTenderList);
                                    // adapter.notifyDataSetChanged();

                                    if (i + 1 == size) {
                                        //data is complete downloaded
                                        EventBus.getDefault().post(new MessageEvent("hello one"));
                                    }

                                }
                                // Log.e(TAG,"asysn task called");
                              /* GetingTenderList getingTenderList = new GetingTenderList();
                                getingTenderList.execute();*/

                                //dataBaseHalper.GetTenderList();
                               /* try {
                                    GetFilteredData(0,"");
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "unexpected error Try Againg!", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.hide();*/


                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e(TAG ," tender list responce Error "+error.getMessage()+" cause "+error.getCause());
                // progressDialog.hide();

              /*  try {
                    Logger.getInstance().createFileOnDevice(true, "service error url " + mURL + " last check  " + " caouse : " + error.getCause());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    // GetData();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {


                } else if (error instanceof NetworkError) {


                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<>();
                DataBaseHalper dataBaseHalper_one = new DataBaseHalper(getApplicationContext());
               /* try {
                    Logger.getInstance().createFileOnDevice(true, "service tender count last check " + dataBaseHalper_one.getLastTenderAssignedDate());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                if (!dataBaseHalper.getLastTenderAssignedDate().equals("0000-00-00 00:00:00"))
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper_one.getLastTenderAssignedDate());

                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                return mapFooder;
            }

           /* @Override
            public String getBodyContentType() {

                return "application/x-www-form-urlencoded";
            }*/

            /**
             * 设置Volley网络请求的编码方式。。。。
             */
           /* @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }*/
           /*@Override
            protected String getParamsEncoding() {
                Map<String,String> mapFooder = new HashMap<>();
                String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
               String dateheader = "lastCheck:"+date_time;

                *//* // encode data on your side using BASE64
                byte[]   bytesEncoded = Base64.encodeBase64(dateheader.getBytes());
                System.out.println("ecncoded value is " + new String(bytesEncoded ));*//*

                // Sending side
                byte[] data = new byte[0];
                try {
                    data = dateheader.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
              String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                return base64;\
            }*/
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
        GlobalDataAcess.titleString = getResources().getString(R.string.active);

        GlobalDataAcess.group_selected = -1;
        GlobalDataAcess.group_select_advice = -1;
        GlobalDataAcess.group_select_advice_main = -1;
        // do change for application advice menu
        GlobalDataAcess.default_string = null;
        GlobalDataAcess.default_int = -2;
    }


//    public void showdemoNotification() {
//
//
//        String demo = "demo";
//
//        Intent intent = new Intent(getApplicationContext(), Home.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);
//        android.support.v4.app.NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(getApplicationContext())
//                        .setContentTitle(getResources().getString(R.string.tenderi__live_noti))
//                        .setStyle(new NotificationCompat.BigTextStyle())
////                        .setContentText(getResources().getString(R.string.you_have_new_tenders) + demo)
//                        .setSubText(getResources().getString(R.string.you_have_new_tenders)+demo)
//                                               /* .setContentText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
//                                                .setSubText(newdata==1?getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tender):getResources().getString(R.string.you_have)+newdata+getResources().getString(R.string.new_tenders))
//                                               */
//                        .setSmallIcon(R.drawable.status_bar_icon)
//                        .setAutoCancel(true)
//                        .setLargeIcon(icon)
//                        .setContentIntent(pendingIntent);
//
//        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
////        if (sessionManager.GetNotiSound()){
////            mBuilder.setSound(alarmsound);
////        }
//
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, mBuilder.build());
//
//
//                                       /* String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
//                                        sessionManager.setLastCheckService(date_time);*/
//
//
//    }

}