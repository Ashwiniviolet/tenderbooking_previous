package com.tenderbooking;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tenderbooking.HelperClasses.SessionManager;

import java.util.LinkedList;
import java.util.List;


public class FontSettings extends AppCompatActivity {

    AlertDialog mAlertDialog;
    AlertDialog.Builder builder;
    RadioGroup radioGroupTExt;
    RadioButton radioTiny, radiosmall, radioMedium, radioLarge, radioHuge;
    SessionManager sessionManager;
    Button textsize_cancelBtn;
    // ListView lv;
    List<String[]> colorList;
    RecyclerView mRecycleview;
    TextSizeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_font_settings);
        sessionManager = new SessionManager(FontSettings.this);
        mRecycleview = (RecyclerView) findViewById(R.id.textsize_recyclerview);


        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Font Settings");
        colorList = new LinkedList<String[]>();
        colorList.add(new String[]{"Tender List", sessionManager.GetTenderSizeString()});
        colorList.add(new String[]{"Advices List", sessionManager.GetAdviceSizeString()});
        colorList.add(new String[]{"Help List", sessionManager.GetHelpSizeString()});
        colorList.add(new String[]{"Content List", sessionManager.GetContentSizeString()});

        adapter = new TextSizeAdapter(colorList);


        mRecycleview.setHasFixedSize(true);
        mRecycleview.setLayoutManager(new LinearLayoutManager(FontSettings.this));
        mRecycleview.setAdapter(adapter);

        mRecycleview.addOnItemTouchListener(new RecycerItemClickListener(FontSettings.this, new RecycerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int size = 1;
                if (position == 0) {
                    switch (sessionManager.GetTenderSize()) {
                        case 12:
                            size = 1;
                            break;

                        case 15:
                            size = 2;
                            break;


                        case 20:
                            size = 3;
                            break;

                        case 25:
                            size = 4;
                            break;

                        case 30:
                            size = 5;
                            break;
                    }
                } else if (position == 1) {
                    switch (sessionManager.GetAdviceSize()) {
                        case 12:
                            size = 1;
                            break;

                        case 15:
                            size = 2;
                            break;


                        case 20:
                            size = 3;
                            break;

                        case 25:
                            size = 4;
                            break;

                        case 30:
                            size = 5;
                            break;
                    }

                } else if (position == 2) {
                    switch (sessionManager.GetHelpSize()) {
                        case 12:
                            size = 1;
                            break;

                        case 15:
                            size = 2;
                            break;


                        case 20:
                            size = 3;
                            break;

                        case 25:
                            size = 4;
                            break;

                        case 30:
                            size = 5;
                            break;
                    }
                } else if (position == 3) {
                    switch (sessionManager.GetContentSize()) {
                        case 12:
                            size = 1;
                            break;

                        case 15:
                            size = 2;
                            break;


                        case 20:
                            size = 3;
                            break;

                        case 25:
                            size = 4;
                            break;

                        case 30:
                            size = 5;
                            break;
                    }
                }


                showRadioButtonDialog(position, size);
            }
        }));

        // Note - we're specifying android.R.id.text1 as a param, but it's ignored
        // because we override getView(). That param usually tells ArrayAdapter
        // where to find the one TextView entity in a complex layout.
        // If our layout was a simple TextView (like android.R.layout.simple_list_item_1),
        // we wouldn't need that param.

      /* lv=(ListView)findViewById(android.R.id.list);
        lv.setAdapter(new ArrayAdapter<String[]>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                colorList) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // Must always return just a View.
                View view = super.getView(position, convertView, parent);

                // If you look at the android.R.layout.simple_list_item_2 source, you'll see
                // it's a TwoLineListItem with 2 TextViews - text1 and text2.
                //TwoLineListItem listItem = (TwoLineListItem) view;
                String[] entry = colorList.get(position);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(entry[0]);
                text2.setText(entry[1]);
                return view;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showRadioButtonDialog(position);
            }
        });*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {


            this.finish();

            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showRadioButtonDialog(final int position, final int size) {

     /*   // custom dialog
        final Dialog dialog = new Dialog(this);

       dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setLayout(500,300);

       dialog.setContentView(R.layout.radiobutton_dialog);



        dialog.show();*/

        RadioGroup rg = (RadioGroup) new RadioGroup(this);


        builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.textsize_radio_button, null, false);
        radioGroupTExt = (RadioGroup) view.findViewById(R.id.radioGroupTExt);
        radioTiny = (RadioButton) view.findViewById(R.id.radioTiny);
        radiosmall = (RadioButton) view.findViewById(R.id.radiosmall);
        radioMedium = (RadioButton) view.findViewById(R.id.radioMedium);
        radioLarge = (RadioButton) view.findViewById(R.id.radioLarge);
        radioHuge = (RadioButton) view.findViewById(R.id.radioHuge);
        textsize_cancelBtn = (Button) view.findViewById(R.id.textsize_cancelBtn);


        switch (size) {
            case 1:
                radioTiny.setChecked(true);
                break;
            case 2:
                radiosmall.setChecked(true);
                break;
            case 3:
                radioMedium.setChecked(true);
                break;
            case 4:
                radioLarge.setChecked(true);
                break;
            case 5:
                radioHuge.setChecked(true);
                break;
        }


        textsize_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
            }
        });
        //1 for tenderlist , 2= advice_list, 3= help List 4 = content list
        radioGroupTExt.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {

                    case R.id.radioTiny:
                        if (position == 0) {
                            sessionManager.SetTenderSize(12, "Tiny");
                            colorList.set(0, new String[]{"Tender List", "Tiny"});
                            //mAlertDialog.dismiss();
                        } else if (position == 1) {
                            sessionManager.SetAdviceSize(12, "Tiny");
                            //mAlertDialog.dismiss();
                        } else if (position == 2) {
                            sessionManager.SetHelpSize(12, "Tiny");
                            //mAlertDialog.dismiss();
                        } else if (position == 3) {
                            sessionManager.SetContentSize(12, "Tiny");
                            //mAlertDialog.dismiss();
                        }
                        // mAlertDialog.dismiss();
                        // lv.notifyAll();
                        break;

                    case R.id.radiosmall:
                        if (position == 0) {
                            sessionManager.SetTenderSize(15, "Small");
                            colorList.set(0, new String[]{"Tender List", "Small"});
                            //mAlertDialog.dismiss();
                        } else if (position == 1) {
                            colorList.set(1, new String[]{"Advices List", "Small"});
                            sessionManager.SetAdviceSize(15, "Small");
                            //mAlertDialog.dismiss();
                        } else if (position == 2) {
                            sessionManager.SetHelpSize(15, "Small");
                            colorList.set(2, new String[]{"Help List", "Small"});
                            //mAlertDialog.dismiss();
                        } else if (position == 3) {
                            sessionManager.SetContentSize(15, "Small");
                            //mAlertDialog.dismiss();
                            colorList.set(3, new String[]{"Content List", "Small"});
                        }
                        //  mAlertDialog.dismiss();
                        // lv.notifyAll();
                        break;

                    case R.id.radioMedium:

                        if (position == 0) {
                            sessionManager.SetTenderSize(20, "Medium");
                            colorList.set(0, new String[]{"Tender List", "Medium"});
                            //mAlertDialog.dismiss();
                        } else if (position == 1) {
                            sessionManager.SetAdviceSize(20, "Medium");
                            colorList.set(1, new String[]{"Advices List", "Medium"});
                            //mAlertDialog.dismiss();
                        } else if (position == 2) {
                            sessionManager.SetHelpSize(20, "Medium");
                            colorList.set(2, new String[]{"Help List", "Medium"});
                            //mAlertDialog.dismiss();
                        } else if (position == 3) {
                            sessionManager.SetContentSize(20, "Medium");
                            colorList.set(3, new String[]{"Content List", "Medium"});
                            //mAlertDialog.dismiss();
                        }
                        // mAlertDialog.dismiss();
                        // lv.notifyAll();
                        break;

                    case R.id.radioLarge:
                        if (position == 0) {
                            colorList.set(0, new String[]{"Tender List", "Large"});
                            sessionManager.SetTenderSize(25, "Large");

                            //mAlertDialog.dismiss();
                        } else if (position == 1) {
                            colorList.set(1, new String[]{"Advices List", "Large"});
                            sessionManager.SetAdviceSize(25, "Large");
                            //mAlertDialog.dismiss();
                        } else if (position == 2) {
                            sessionManager.SetHelpSize(25, "Large");
                            colorList.set(2, new String[]{"Help List", "Large"});
                            //mAlertDialog.dismiss();
                        } else if (position == 3) {
                            sessionManager.SetContentSize(25, "Large");
                            //mAlertDialog.dismiss();
                            colorList.set(3, new String[]{"Content List", "Large"});
                        }
                        // mAlertDialog.dismiss();
                        //  lv.notifyAll();
                        break;

                    case R.id.radioHuge:
                        if (position == 0) {
                            sessionManager.SetTenderSize(30, "Huge");
                            colorList.set(0, new String[]{"Tender List", "Huge"});
                            //mAlertDialog.dismiss();
                        } else if (position == 1) {
                            sessionManager.SetAdviceSize(30, "Huge");
                            colorList.set(1, new String[]{"Advices List", "Huge"});
                            //mAlertDialog.dismiss();
                        } else if (position == 2) {
                            sessionManager.SetHelpSize(30, "Huge");
                            colorList.set(2, new String[]{"Help List", "Huge"});
                            //mAlertDialog.dismiss();
                        } else if (position == 3) {
                            sessionManager.SetContentSize(30, "Huge");
                            colorList.set(3, new String[]{"Content List", "Huge"});
                            //mAlertDialog.dismiss();
                        }

                        // lv.notifyAll();
                        break;

                }
                mAlertDialog.dismiss();
                adapter.notifyDataSetChanged();

                /*Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mAlertDialog.dismiss();
                               adapter.notifyDataSetChanged();
                            }
                        },2500);
                    }
                };
                runnable.run();*/
            }
        });

        mAlertDialog.setView(view);


        mAlertDialog.show();
    }

    public class TextSizeAdapter extends RecyclerView.Adapter<TextSizeAdapter.MYViewholder> {

        List<String[]> listString;

        public TextSizeAdapter(List<String[]> list) {
            this.listString = list;
        }

        @Override
        public MYViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(FontSettings.this).inflate(android.R.layout.simple_list_item_2, parent, false);

            return new MYViewholder(view);
        }

        @Override
        public void onBindViewHolder(MYViewholder holder, int position) {
            String[] entry = colorList.get(position);
            holder.text1.setText(entry[0]);
            holder.text2.setText(entry[1]);
        }

        @Override
        public int getItemCount() {
            return listString.size();
        }

        public class MYViewholder extends RecyclerView.ViewHolder {
            TextView text1, text2;

            public MYViewholder(View itemView) {
                super(itemView);
                text1 = (TextView) itemView.findViewById(android.R.id.text1);
                text2 = (TextView) itemView.findViewById(android.R.id.text2);
            }
        }
    }
}
