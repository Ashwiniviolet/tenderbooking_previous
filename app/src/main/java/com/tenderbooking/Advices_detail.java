package com.tenderbooking;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.MainTab.ExpandableListAdapter;
import com.tenderbooking.Model.ModelAdviceDetials;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Advices_detail extends AppCompatActivity {
    ExpandableListView mExpandableLIstview;
    ExpandableListAdapter adapter;
    LinkedHashMap<String, List<String>> mExpandableListData;
    List<String> mExpandableListTitle;
    Toolbar mToolbar;
    DrawerLayout drawer;
    Map<String, String> deviceinfoMap;
    SessionManager sessionManager;
    ModelAdviceDetials modelAdviceDetials;
    DataBaseHalper dataBaseHalper;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(Advices_detail.this);
        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        String lang = sessionManager.getAppLangName();
        UtillClasses.langChange(lang, this);
        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small_Noactionbar);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium_Noactionbar);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large_Noactionbar);
                break;
        }

        setContentView(R.layout.activity_advices_detail);
        mExpandableLIstview = (ExpandableListView) findViewById(R.id.expan_listview_adv);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_advice);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_advice);
        setSupportActionBar(mToolbar);
        //sessionManager = new SessionManager(Advices_detail.this);
        dataBaseHalper = new DataBaseHalper(Advices_detail.this);
        deviceinfoMap = sessionManager.GetDeviceInfo();

//        mExpandableListData = ExpanAdviDrawData.getData(this);
//        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
//        adapter = new ExpandableListAdapter(Advices_detail.this,mExpandableListTitle,mExpandableListData);
//        mExpandableLIstview.setAdapter(adapter);
//        mExpandableLIstview.setDividerHeight(1);


        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawerToggle.syncState();

        drawer.addDrawerListener(mDrawerToggle);
        /*
        ActionBar ab=getSupportActionBar();
        ab.setElevation(0);
        drawer.addDrawerListener(mDrawerToggle);*/

        Intent intent = getIntent();
        String typeAdvice = intent.getStringExtra(GlobalDataAcess.Ask_advice_typeAdvice);
        String idAdvice = intent.getStringExtra(GlobalDataAcess.Ask_advice_idAdvice);

       /* if (idAdvice!=null)
        if (dataBaseHalper.checkidAdviceAval(idAdvice)){
            modelAdviceDetials = dataBaseHalper.getAdviceDetails(idAdvice);
        }else{
            getAdviceDetails(typeAdvice,idAdvice);
        }*/

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void getAdviceDetails(final String typeAdvices, final String idAdvices) {
        final String mURL = UtillClasses.BaseUrl + "get_advice_details";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {
                                    JSONObject jsondata = jsonObject.getJSONObject("data");
                                    modelAdviceDetials.setIdAdvice(jsondata.optString("idAdvice"));
                                    modelAdviceDetials.setIdTender(jsondata.optString("idTender"));
                                    modelAdviceDetials.setTitleAdvice(jsondata.optString("titleAdvice"));
                                    modelAdviceDetials.setQuestionAdvice(jsondata.optString("questionAdvice"));
                                    modelAdviceDetials.setTextAdvice(jsondata.optString("textAdvice"));
                                    dataBaseHalper.InsertAdviceDetails(modelAdviceDetials);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<String, String>();
                mapFooder.put(UtillClasses.IdAdvice, idAdvices);
                mapFooder.put(UtillClasses.TypeAdvice, typeAdvices);
                return mapFooder;
            }
        };
    }
}
