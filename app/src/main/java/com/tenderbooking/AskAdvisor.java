package com.tenderbooking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.Services.NotificaitonServices;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class AskAdvisor extends AppCompatActivity {

    public static final String TAG = AskAdvisor.class.getSimpleName();
    public ActionBar actionBar;
    List<String> listcountry;
    List<String> listCountrycode;
    String selectedCountryId = "";
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapterSpinner;
    String tenderId;
    DataBaseHalper dataBaseHalper;
    private EditText ask_adv_qText, ask_adv_qTitle, ask_adv_tenderId;
    private CheckBox Ask_adv_checkBoc;
    private Spinner ask_adv_spinner;
    private Button ask_adv_sendBtn;
    private SessionManager sessionManager;
    //firebase
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(AskAdvisor.this);
        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large);
                break;
        }

        setContentView(R.layout.activity_ask_advisor);
        ActionBar ab = getSupportActionBar();
        //  sessionManager = new SessionManager(this);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(getResources().getString(R.string.Ask_advisor));


        mProgressDialog = new ProgressDialog(AskAdvisor.this);
        //mProgressDialog.setMessage("sending");
        mProgressDialog.setMessage(getResources().getString(R.string.sending));
        mProgressDialog.setCancelable(false);

        ask_adv_qText = (EditText) findViewById(R.id.ask_adv_qText);
        ask_adv_qTitle = (EditText) findViewById(R.id.ask_adv_qTitle);
        ask_adv_tenderId = (EditText) findViewById(R.id.ask_adv_tenderId);
        Ask_adv_checkBoc = (CheckBox) findViewById(R.id.Ask_adv_checkBoc);
        ask_adv_sendBtn = (Button) findViewById(R.id.ask_adv_sendBtn);
        ask_adv_spinner = (Spinner) findViewById(R.id.ask_adv_spinner);

        Ask_adv_checkBoc.setEnabled(false);
        ask_adv_tenderId.setEnabled(false);

        Intent intent = getIntent();
        final int isTenidAval = intent.getIntExtra(GlobalDataAcess.Ask_advice_tendAval, 0);
        tenderId = String.valueOf(intent.getIntExtra(GlobalDataAcess.Ask_advice_tenderId, 0));
        final String countryId = String.valueOf(intent.getIntExtra(GlobalDataAcess.Ask_advice_countryId, 0));

        //firebase Analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setUserId(sessionManager.GetUserEmail());
        firebaseAnalytics.setCurrentScreen(this, "Ask Advisor", "Ask Advisor");
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);


        listcountry = new ArrayList<>();
        listcountry.add("Srbija");
        //listcountry.add("Crna Gora");
        listcountry.add(getString(R.string.bosnia_and_herzengovina));
        // listcountry.add("Croatia");


        if (isTenidAval == 0) {
            adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listcountry);
            tenderId = "0";

        } else {
            List<String> tempList = new ArrayList<>();
            tempList.add(countryId);
            adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tempList);
        }

        ask_adv_spinner.setAdapter(adapterSpinner);

        if (isTenidAval == 0) {
            Ask_adv_checkBoc.setChecked(false);
            ask_adv_tenderId.setText("0");
            ask_adv_tenderId.setVisibility(View.GONE);


        } else {
            Ask_adv_checkBoc.setVisibility(View.GONE);
            ask_adv_tenderId.setVisibility(View.GONE);
            ask_adv_spinner.setVisibility(View.GONE);
            Ask_adv_checkBoc.setChecked(true);
            ask_adv_tenderId.setText(tenderId);
            ask_adv_spinner.setSelection(0);
            ask_adv_spinner.setEnabled(false);
        }


        ask_adv_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                switch (ask_adv_spinner.getSelectedItemPosition()) {
                    case 0:
                        selectedCountryId = "00100";
                        break;
                    case 1:
                        selectedCountryId = "00300";//for bosnia and herzegovina
                        // selectedCountryId = "00200";
                        break;

                    case 2:
                        // selectedCountryId = "00300";
                        break;

                    case 3:
                        //selectedCountryId= "00400";
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCountryId = "0";
            }
        });
        /*int id=getIntent().getIntExtra("tenderid",0);

       // Spinner spinner=(Spinner)findViewById(R.id.spinner);
        if(id>0)
        {
            spinner.setEnabled(false);
        }*/

        /*Ask_adv_checkBoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    ask_adv_tenderId.setEnabled(true);
                    ask_adv_tenderId.requestFocus();
                }else{
                    ask_adv_tenderId.setEnabled(false);
                    ask_adv_tenderId.setText("");
                }
            }
        });*/

        ask_adv_sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!true) {
                    if (ask_adv_spinner != null) {
                        Snackbar.make(ask_adv_spinner, getResources().getString(R.string.please_select_country), Snackbar.LENGTH_LONG).show();
                    }
                } else if (ask_adv_qTitle.getText().toString().trim().length() == 0) {
                    ask_adv_qTitle.setError(getResources().getString(R.string.please_enter_title));
                    ask_adv_qTitle.requestFocus();
                } else if (ask_adv_qText.getText().toString().trim().length() == 0) {
                    ask_adv_qText.setError(getResources().getString(R.string.please_enter_question));
                    ask_adv_qText.requestFocus();
                } else if (isTenidAval == 0 && selectedCountryId.equals("0")) {
                    if (ask_adv_spinner != null) {
                        Snackbar.make(ask_adv_spinner, getResources().getString(R.string.select_country_first), Snackbar.LENGTH_SHORT).show();
                    }
                } else {

                    mProgressDialog.show();
                    GlobalDataAcess.hideSoftKeyboard(AskAdvisor.this);
                    Map<String, String> mapAdvice = new HashMap<String, String>();
                    //String tenderID = "";

                  /*  if (Ask_adv_checkBoc.isSelected()){
                        tenderID = ask_adv_tenderId.getText().toString().trim();
                    }
                    else
                        tenderID = "0";*/

                    String typeAdvice = "user";
                    // selected country id
                    //String countryAdvice = ask_adv_spinner.getSelectedItemPosition()+"";
                    String countryAdvice;
                    if (isTenidAval == 0) {
                        countryAdvice = selectedCountryId;
                    } else {
                        countryAdvice = countryId;
                    }


                    String titleQuestion = ask_adv_qTitle.getText().toString().trim();
                    String textQuestion = ask_adv_qText.getText().toString().trim();

                    mapAdvice.put(UtillClasses.TypeAdvice, typeAdvice);
                    mapAdvice.put(UtillClasses.idtender, tenderId);
                    mapAdvice.put(UtillClasses.CountryAdvice, countryAdvice);
                    //mapAdvice.put(UtillClasses.CountryAdvice,"17700");
                    mapAdvice.put(UtillClasses.TitleQuestion, titleQuestion);
                    mapAdvice.put(UtillClasses.DetailsQuestion, textQuestion);
                    SendAdviceQuestion(mapAdvice);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {


            this.finish();

            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void SendAdviceQuestion(final Map<String, String> mapAdviceQues) {

        //final String mURL = UtillClasses.BaseUrl+"send_advice_question";

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "send_advice_question";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
                                    //firebase
                                    Bundle bundle = new Bundle();
                                    //bundle.putString();

                                   /* try {
                                        Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/

                                    // call send
                                    int questionId = jsonObject.optInt("data");
                                    // editing in server

                                    //String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());

                                    SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

                                    String GMtDate = simpleDateFormatGMT.format(new Date());

                                    ModelAdviceList modelAdviceList = new ModelAdviceList();
                                    modelAdviceList.setIdAdvice(jsonObject.optString("data"));
                                    modelAdviceList.setTitleAdvice(mapAdviceQues.get(UtillClasses.TitleQuestion));
                                    modelAdviceList.setTypeAdvice("user");
                                    modelAdviceList.setStatusAdvice("1");
                                    modelAdviceList.setHaveRateAdvice(1);

                                    if (mapAdviceQues.get(UtillClasses.CountryAdvice).equals("00100")) {
                                        modelAdviceList.setCountryFlagAdvice("rs");
                                    } else if (mapAdviceQues.get(UtillClasses.CountryAdvice).equals("00300")) {
                                        modelAdviceList.setCountryFlagAdvice("ba");
                                    }

                                    //temp do on 10-08 laxmikant chhipa
//                                    dataBaseHalper = new DataBaseHalper(AskAdvisor.this);
//                                    dataBaseHalper.InsertAdvicdListUser(modelAdviceList);
//                                    Fragment_advice.modelAdviceListList.add(0,modelAdviceList);
//
                                    //temp do on 10-08 laxmikant chhipa

                                    sessionManager.SetLastCheckAdvAns(GMtDate);
                                    Toast.makeText(AskAdvisor.this, getResources().getString(R.string.send_suces), Toast.LENGTH_SHORT).show();// Intent serviceIntent = new Intent(AskAdvisor.this, NotiIntentService.class);
                                    // serviceIntent.putExtra(GlobalDataAcess.TypeService,1);
                                    //  startService(serviceIntent);

                                    Intent intent = new Intent(AskAdvisor.this, NotificaitonServices.class);
                                    intent.putExtra(GlobalDataAcess.Notification_type, 1);
                                    startService(intent);

                                    mProgressDialog.hide();
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProgressDialog.hide();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.hide();
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();

                /*try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                if (ask_adv_spinner != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        SendAdviceQuestion(mapAdviceQues);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {
                        //
                    } else if (error instanceof ServerError) {
                        //
                        Snackbar snackbar = Snackbar
                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        SendAdviceQuestion(mapAdviceQues);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {
                        //
                        Snackbar snackbar = Snackbar
                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        SendAdviceQuestion(mapAdviceQues);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof ParseError) {
                        //
                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                mapFooder.put(UtillClasses.TypeAdvice, mapAdviceQues.get(UtillClasses.TypeAdvice));
                mapFooder.put(UtillClasses.idtender, mapAdviceQues.get(UtillClasses.idtender));
                mapFooder.put(UtillClasses.CountryAdvice, mapAdviceQues.get(UtillClasses.CountryAdvice));
                mapFooder.put(UtillClasses.TitleQuestion, mapAdviceQues.get(UtillClasses.TitleQuestion));
                mapFooder.put(UtillClasses.DetailsQuestion, mapAdviceQues.get(UtillClasses.DetailsQuestion));

                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }
}
