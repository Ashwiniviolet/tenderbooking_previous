package com.tenderbooking;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.HelperClasses.VersionChecker;
import com.tenderbooking.Model.Model_about_page;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class About extends AppCompatActivity {

    public static final String TAG = About.class.getSimpleName();
    public ProgressDialog mProgressDialog;
    public AlertDialog.Builder mAlertBuilder;
    LinearLayout about_aboutUs, about_treamnd_cond, about_pri_policy, about_check_for_update, about_check_App_status;
    TextView about_version_code, about_Build_value;
    SessionManager sessionManager;
    private AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(About.this);

        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large);
                break;
        }

        setContentView(R.layout.activity_about);

        ActionBar ab = getSupportActionBar();
        //  sessionManager = new SessionManager(this);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(getResources().getString(R.string.about));

        mProgressDialog = new ProgressDialog(About.this);
        mProgressDialog.setMessage(getResources().getString(R.string.loading));
        mProgressDialog.setCancelable(false);


        about_aboutUs = (LinearLayout) findViewById(R.id.about_aboutUs_data);
        about_treamnd_cond = (LinearLayout) findViewById(R.id.about_treamnd_cond);
        about_pri_policy = (LinearLayout) findViewById(R.id.about_pri_policy);
        about_check_for_update = (LinearLayout) findViewById(R.id.about_check_for_update);
        about_check_App_status = (LinearLayout) findViewById(R.id.about_check_App_status);
        about_version_code = (TextView) findViewById(R.id.about_version_code);
        about_Build_value = (TextView) findViewById(R.id.about_Build_value);

        //sessionManager = new SessionManager(About.this);

        about_aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalDataAcess.hasConnection(About.this)) {
                    GetAboutPage("about", 1);
                    mProgressDialog.show();
                } else {
                    GlobalDataAcess.apiDialog(About.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                }
                Log.e(TAG, " click on About us btn");
            }
        });

        about_check_App_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(About.this, R.style.MyDialogTheme);
                builder.setCancelable(true);
                builder.setTitle(getResources().getString(R.string.check_app_status));
                builder.setMessage(getResources().getString(R.string.status_ok));
                builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create();
                builder.show();

            }
        });

        about_treamnd_cond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalDataAcess.hasConnection(About.this)) {
                    mProgressDialog.show();
                    GetAboutPage("tc", 2);
                } else {
                    GlobalDataAcess.apiDialog(About.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                }
            }
        });

        about_pri_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalDataAcess.hasConnection(About.this)) {
                    mProgressDialog.show();
                    GetAboutPage("pp", 3);
                } else {
                    GlobalDataAcess.apiDialog(About.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                }
            }
        });
        about_check_for_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VersionChecker versionChecker = new VersionChecker();
                try {
                    mProgressDialog.show();
                    String VersionName = BuildConfig.VERSION_NAME;
                    // String latestVersion = versionChecker.execute().get();
                    if (VersionName != versionChecker.execute().get()) {
                        Log.e(TAG, "version of the applicaiton is different:");
                        mProgressDialog.hide();
                        if (mAlertBuilder == null)
                            mAlertBuilder = new AlertDialog.Builder(About.this, R.style.TintTheme);
                        mAlertBuilder.setCancelable(true);
                        mAlertBuilder.setTitle(getResources().getString(R.string.update_avalible));
                        mAlertBuilder.setMessage(getResources().getString(R.string.update_aval_try));
                        mAlertBuilder.setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    About.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Splash.PackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    About.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + Splash.PackageName)));
                                }
                                dialog.dismiss();

                            }
                        });
                        mAlertBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });
                        mAlertBuilder.create();
                        mAlertBuilder.show();

                    } else {
                        mProgressDialog.hide();
                        GlobalDataAcess.apiDialog(About.this, getResources().getString(R.string.updated_version), getResources().getString(R.string.already_lateast));
                        Log.e(TAG, "version of the applicaiton is Same:");
                    }
                    //  Log.e(TAG,"version of the applicaiton is :"+versionChecker.execute().get());
                } catch (InterruptedException e) {
                    mProgressDialog.hide();
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    mProgressDialog.hide();
                    e.printStackTrace();
                } finally {

                }
            }
        });

        String[] mobileArray = {"Credits", "Terms & Conditions", "Check For Updates", "Check For App Status"};


        final List<String[]> colorList = new LinkedList<String[]>();

        colorList.add(new String[]{"Version", "1.1.1.6"});
        colorList.add(new String[]{"Build", "7561P"});

        // Note - we're specifying android.R.id.text1 as a param, but it's ignored
        // because we override getView(). That param usually tells ArrayAdapter
        // where to find the one TextView entity in a complex layout.
        // If our layout was a simple TextView (like android.R.layout.simple_list_item_1),
        // we wouldn't need that param.

        ListView lv = (ListView) findViewById(android.R.id.list);
        lv.setAdapter(new ArrayAdapter<String[]>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                colorList) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // Must always return just a View.
                View view = super.getView(position, convertView, parent);

                // If you look at the android.R.layout.simple_list_item_2 source, you'll see
                // it's a TwoLineListItem with 2 TextViews - text1 and text2.
                //TwoLineListItem listItem = (TwoLineListItem) view;
                String[] entry = colorList.get(position);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(entry[0]);
                text2.setText(entry[1]);
                return view;
            }
        });

        ArrayAdapter adapterr = new ArrayAdapter<String>(this,
                R.layout.listview, mobileArray);

        ListView listView = (ListView) findViewById(R.id.listViewBottom);
        listView.setAdapter(adapterr);

//        ActionBar ab=getSupportActionBar();
//        ab.setDisplayHomeAsUpEnabled(true);
//        ab.setTitle("About");

        /**/
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        about_version_code.setText(deviceinfoMap.get(SessionManager.App_Ver));

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        if (pInfo != null)
            about_Build_value.setText(version);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {


            this.finish();

            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void GetAboutPage(final String typePage, final int type) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "get_about_page";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " About Us Data " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {

                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    Model_about_page model_about_page = new Model_about_page();
                                    model_about_page.setTitlepage(jsonObjectData.optString("titlePage"));
                                    model_about_page.setTextpage(jsonObjectData.optString("textPage"));

                                    getAboutPageResult(model_about_page, type);

                                } else {
                                    mProgressDialog.hide();
                                    //  progressDialog.hide();
                                    if (about_aboutUs != null) {
                                        Snackbar.make(about_aboutUs, " Something went wrong! Try Again ", Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                mProgressDialog.hide();
                                // progressDialog.hide();
                                String messages = jsonObject.optString("message");
                                if (about_aboutUs != null) {
                                    Snackbar.make(about_aboutUs, "" + messages, Snackbar.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException e) {
                            // progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.hide();
                //Snackbar.make(about_aboutUs,"  "+error.getMessage(),Snackbar.LENGTH_SHORT).show();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(about_aboutUs, "Something Went Wrong! Try Again Later", Snackbar.LENGTH_LONG)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetAboutPage(typePage, type);
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {
                    //
                } else if (error instanceof ServerError) {
                    //
                    Snackbar snackbar = Snackbar
                            .make(about_aboutUs, "Something Went Wrong! Try Again Later", Snackbar.LENGTH_LONG)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetAboutPage(typePage, type);
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {
                    //
                } else if (error instanceof ParseError) {
                    //
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                //mapFooder.put(UtillClasses.lastCheck,sessionManager.getLastCheck());
                mapFooder.put(UtillClasses.Typepage, typePage + "");
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void getAboutPageResult(Model_about_page model_about_page, int typeuser) {

        mProgressDialog.hide();
        if (typeuser == 1) ;
        {
            Intent intent = new Intent(this, PrivacyPolicy.class);
            intent.putExtra(GlobalDataAcess.About_title, model_about_page.getTitlepage());
            intent.putExtra(GlobalDataAcess.About_text, model_about_page.getTextpage());
            intent.putExtra(GlobalDataAcess.About_usertype, typeuser);
            startActivity(intent);
        }

    }

    public void loadBatterySection() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        // registerReceiver(,intentFilter);
    }
}
