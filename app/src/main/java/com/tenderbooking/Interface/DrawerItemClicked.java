package com.tenderbooking.Interface;

/**
 * Created by laxmikant bolya on 19-03-2017.
 */

public interface DrawerItemClicked {

    public void GetDrawerItemClicked(int mainItem, String subItem);
}
