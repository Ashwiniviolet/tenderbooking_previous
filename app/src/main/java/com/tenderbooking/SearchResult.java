package com.tenderbooking;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Model.ModelSearchTenderList;
import com.tenderbooking.Volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SearchResult extends AppCompatActivity {

    public CoordinatorLayout coordinatorLayout;
    public SessionManager sessionManager;
    public String TAG = "Search Data ";
    ArrayList<ModelSearchTenderList> arrayList = new ArrayList<>();
    ListView listView;
    LinearLayout linearLayout;
    android.support.v7.app.ActionBar actionBar;
    SearchView searchView;
    private boolean tracker = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);


        linearLayout = (LinearLayout) findViewById(R.id.prog_bar);
        listView = (ListView) findViewById(R.id.search_list);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        handleIntent(getIntent());


    }


    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }


    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            doMySearch(query);

        }
    }

    private void doMySearch(String query) {

        //set recent word as query

        if (tracker == true) {
            searchView.setQuery(query, false);

        }
        linearLayout.setVisibility(View.VISIBLE);
        getTenderByKeyword(query);


    }

    private void getTenderByKeyword(final String query) {


        sessionManager = new SessionManager(getApplicationContext());
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = "https://www.tenderilive.com/xmlrpcs/mobile/get_tenders_by_keywords";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {


                        //FragmentTender.TenderListDBStore tenderListDBStore = new FragmentTender.TenderListDBStore(response);
                        //tenderListDBStore.execute();
                        JSONObject jsonObject;
                        try {
                            jsonObject = new JSONObject(response);

                            Log.e(TAG, response);
                            if (jsonObject.getInt("statusCode") == 200) {

                                //JSONArray array = jsonObject.getJSONArray("data");

                                TenderSearchListDBStore tenderSearchListDBStore = new TenderSearchListDBStore(response);
                                tenderSearchListDBStore.execute();

                                Log.e(TAG, response);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


               /* try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getTenderByKeyword(query);
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getTenderByKeyword(query);
                                }
                            });


// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getTenderByKeyword(query);
                                }
                            });


// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // return super.getParams();
                Map<String, String> mapFooder = new HashMap<>();
                mapFooder.put(UtillClasses.Keywords, query);
                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_desc);
                return mapFooder;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 100, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);


    }

    public void setadapter() {
        //clear focus
        searchView.clearFocus();

        listView.setAdapter(new SearchAdapter(this, arrayList));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        this.searchView = searchView;
        searchView.setActivated(true);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.onActionViewExpanded();
        searchView.setIconified(false);


        tracker = true;


        // searchView.setIconifiedByDefault(true);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public class TenderSearchListDBStore extends AsyncTask<String, Void, String> {

        String response = null;

        TenderSearchListDBStore(String response) {

            this.response = response;
        }

        @Override
        protected String doInBackground(String... params) {
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("statusCode") == 200) {


                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        int size = jsonArray.length();
                        arrayList.clear();

                        Log.e(TAG, " tender list responce SIZE TOTAL " + size);

                        for (int i = 0; i < size; i++) {

                            JSONObject jsondata = jsonArray.getJSONObject(i);

                            ModelSearchTenderList modelSearchTenderList = new ModelSearchTenderList();
                            modelSearchTenderList.setAmountCurrency(jsondata.optString("amountCurrency"));
                            modelSearchTenderList.setAmountTender(jsondata.optDouble("amountTender"));
                            modelSearchTenderList.setAssignedTender(jsondata.optString("assignedTender"));
                            modelSearchTenderList.setCategoriesTender(jsondata.optString("categoriesTender"));
                            modelSearchTenderList.setCityBuyer(jsondata.optString("cityBuyer"));
                            modelSearchTenderList.setCityTender(jsondata.optString("cityBuyerID"));
                            modelSearchTenderList.setCountryFlagTender(jsondata.optString("countryFlagTender"));
                            modelSearchTenderList.setCountryTender(jsondata.optString("countryTender"));
                            modelSearchTenderList.setDateDocumentation(jsondata.optString("dateDocumentation"));
                            modelSearchTenderList.setDateStart(jsondata.optString("dateStart"));
                            modelSearchTenderList.setDateStop(jsondata.optString("dateStop"));
                            modelSearchTenderList.setDocumentationCurrency(jsondata.optString("documentationCurrency"));
                            modelSearchTenderList.setDocumentationPrice(jsondata.optDouble("documentationPrice"));
                            modelSearchTenderList.setIdAwardCriteria(jsondata.optString("idAwardCriteria"));
                            modelSearchTenderList.setIdBuyer(jsondata.optString("idBuyer"));
                            modelSearchTenderList.setIdContract(jsondata.optString("idContract"));
                            modelSearchTenderList.setIdDocument(jsondata.optString("idDocument"));
                            modelSearchTenderList.setIdNUTS(jsondata.optString("idNUTS"));
                            modelSearchTenderList.setIdProcedure(jsondata.optString("idProcedure"));
                            modelSearchTenderList.setIdTender(jsondata.optString("idTender"));
                            modelSearchTenderList.setMainLanguage(jsondata.optString("mainLanguage"));
                            modelSearchTenderList.setNameAwardCriteria(jsondata.optString("nameAwardCriteria"));
                            modelSearchTenderList.setNameBuyer(jsondata.optString("nameBuyer"));
                            modelSearchTenderList.setNameContract(jsondata.optString("nameContract"));
                            modelSearchTenderList.setNameDocument(jsondata.optString("nameDocument"));
                            modelSearchTenderList.setNameProcedure(jsondata.optString("nameProcedure"));
                            modelSearchTenderList.setOptionsTender(jsondata.optString("optionsTender"));
                            modelSearchTenderList.setSourcetender(jsondata.optString("sourceTender"));
                            modelSearchTenderList.setStatusTender(jsondata.optString("statusTender"));
                            modelSearchTenderList.setTagTender(jsondata.optString("tagTender"));
                            modelSearchTenderList.setTitleTender(jsondata.optString("titleTender"));
                            modelSearchTenderList.setTypeTender(jsondata.optString("typeTender"));
                            modelSearchTenderList.setUrlTender(jsondata.optString("urlTender"));


                            arrayList.add(modelSearchTenderList);


                            if (i + 1 == size) {
                                return "";
                            }
                        }
                    } else {

                        Snackbar.make(coordinatorLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {

            Log.e(TAG, Integer.toString(arrayList.size()));
            super.onPostExecute(s);
            linearLayout.setVisibility(View.GONE);
            setadapter();


        }
    }
}
