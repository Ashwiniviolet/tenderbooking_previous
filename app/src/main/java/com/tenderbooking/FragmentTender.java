package com.tenderbooking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.PagginationScrollListener;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Interface.DrawerItemClicked;
import com.tenderbooking.Interface.InterPagTenderlist;
import com.tenderbooking.Interface.InterfaceTendListClick;
import com.tenderbooking.MainTab.MyRecycleviewAdapter;
import com.tenderbooking.MainTab.RecyclerviewDevider;
import com.tenderbooking.Model.MessageEvent;
import com.tenderbooking.Model.ModelTenderList;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.Services.NotificaitonServices;
import com.tenderbooking.Volley.AppController;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class FragmentTender extends Fragment implements DrawerItemClicked, InterfaceTendListClick {

    public static final String TAG = FragmentTender.class.getSimpleName();

    SharedPreferences preferences;

    boolean loadAgain = false;
    private static final int PAGE_START = 0;
    public static MyRecycleviewAdapter adapter;
    public static ArrayList<ModelTenderList> arrayTenderList;
    public static ArrayList<ModelTenderList> filteredArraylist;
    public static String tenderId = "0";

    public static DataBaseHalper dataBaseHalper;
    public static int totalPages = 0;
    public static FlexboxLayoutManager flexboxLayoutManager;
    // If current page is the last page (Pagination will stop after this page load)
    public static boolean isLastPage = false;
    // indicates the current page which Pagination is fetching.
    public static int currentPage = PAGE_START;
    public RecyclerView mRecyclerView;
    public ProgressDialog progressDialog;
    public int defaultMainItem = -1;
    public String defaullSubItem = null;
    public View view;
    public Home home = new Home();
    ImageView scroll_up;
    TextView main_updateStatus;
    ArrayList<ModelUserTag> modelUserTagArrayList;
    private SessionManager sessionManager;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout progress_ll;
    private TextView loadingText;
    private boolean test;
    private TextView tender_not_found;
    private SwipeRefreshLayout swipeRefreshLayout;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;
    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES = 3;
    private String selectedQuery = "";
    // ( (currentpage * 25)+1 )  currentPage+* 25
    private String qwery = "";


    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.tender_recy_lay, container, false);
        dataBaseHalper = new DataBaseHalper(getActivity());
        sessionManager = new SessionManager(getActivity());
        arrayTenderList = new ArrayList<>();
        filteredArraylist = new ArrayList<>();


        //userdefined tag

        modelUserTagArrayList = new ArrayList<>();


        scroll_up = (ImageView) view.findViewById(R.id.scroll_up);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.main_recyclerview);
        main_updateStatus = (TextView) view.findViewById(R.id.main_updateStatus);
        tender_not_found = (TextView) view.findViewById(R.id.tender_not_found);
        loadingText = (TextView) view.findViewById(R.id.login_progressDialog);
        progress_ll = (LinearLayout) view.findViewById(R.id.progress_ll);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.tender_swipe_layout);

        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        progress_ll.setVisibility(View.VISIBLE);
        //flexbox layout details


        if (sessionManager.getTenderFirstTime()) {

            loadingText.setText(getResources().getString(R.string.first_loading));

        } else {

            loadingText.setText(getResources().getString(R.string.loading));
        }
        flexboxLayoutManager = new FlexboxLayoutManager();


        //adapter = new MyRecycleviewAdapter(getActivity(),arrayTenderList);
        int textsize = sessionManager.GetTenderSize();
        adapter = new MyRecycleviewAdapter(getActivity(), filteredArraylist, Home.modelUserTagArrayList, flexboxLayoutManager, textsize);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addItemDecoration(new RecyclerviewDevider(3));
        mRecyclerView.addOnItemTouchListener(new RecycerItemClickListener(getActivity(), new RecycerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position != RecyclerView.NO_POSITION) {

                    if (view.getId() == R.id.recy_custlaymain) {

                    } else if (view.getId() == R.id.swaplay_DeleteLL) {
                        Log.e(TAG, "Deleted data clicked  layout clicked");
                    }

                }
            }
        }));

        mRecyclerView.setOnScrollListener(new PagginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (filteredArraylist.size() >= 15) {
                    isLoading = true;
                    currentPage += 1;


                    if (adapter != null) {

                        try {
                            adapter.addLoadingFooter();
                        } catch (Exception e) {

                        }
                    }
//                getPaggingTenderList(selectedQuery);
//                    getPaggingTenderList(AppController.sessionManager.getSelectedQwery(),((currentPage *25)+1));
                    getPaggingTenderList(AppController.sessionManager.getSelectedQwery(), (currentPage * 25)); // by laxmikant on 14/12/2017
                }
            }

            @Override
            public int getTotalPageCount() {

                return totalPages;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }


            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        scroll_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToPosition(0);
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (GlobalDataAcess.hasConnection(getActivity())) {
                    swipeRefreshLayout.setRefreshing(true);


                    Intent intent = new Intent(getActivity(), NotificaitonServices.class);
                    intent.putExtra(GlobalDataAcess.Notification_type, 1);
                    getActivity().startService(intent);


                    GetTenderNumber();
                    GlobalDataAcess.resetAlarm(getActivity(), sessionManager.GetNotiTimeInterval());

                } else {
                    GlobalDataAcess.apiDialog(getActivity(), getActivity().getResources().getString(R.string.network_error), getActivity().getResources().getString(R.string.error_msg));
                }
            }
        });


        if (GlobalDataAcess.hasConnection(getActivity())) {
            GetTenderNumber();
        } else {
            GlobalDataAcess.apiDialog(getActivity(), getActivity().getResources().getString(R.string.network_error), getActivity().getResources().getString(R.string.error_msg));
        }

        getTagData();
        return view;
    }


    public void GetData() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        //final String mURL = UtillClasses.BaseUrl+"/get_tenders_list";
        final String mURL = "https://www.tenderilive.com/xmlrpcs/mobile/get_tenders_list";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        swipeRefreshLayout.setRefreshing(false);
                        sessionManager.setLastCheckService(GlobalDataAcess.getGMTTime());
                        TenderListDBStore tenderListDBStore = new TenderListDBStore(response);
                        tenderListDBStore.execute();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progress_ll.setVisibility(View.GONE);


                try {
                    if (GlobalDataAcess.mainValue == -1 && GlobalDataAcess.subValue == null) {
                        GetFilteredData(0, "");
                    } else {
                        GetFilteredData(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();

                }

                if (mRecyclerView != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetData();
                                    }
                                });


                        snackbar.setActionTextColor(Color.RED);


                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetData();
                                    }
                                });


                        snackbar.setActionTextColor(Color.RED);


                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetData();
                                    }
                                });


// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof ParseError) {

                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));

                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("For head", String.valueOf(mapheader));
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // return super.getParams();
                Map<String, String> mapFooder = new HashMap<>();
                if (sessionManager.getTenderFirstTime()) {
                    mapFooder.put(UtillClasses.lastCheck, "0000-00-00 00:00:00");
                } else
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastTenderAssignedDate());

                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                Log.e("For prams", String.valueOf(mapFooder));
                Log.e(TAG, "tender list last check " + sessionManager.GetLastCheckServie());

                return mapFooder;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void GetTenderNumber() {

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        final String mURL = UtillClasses.BaseUrl + "check_tenders_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isAdded() && getActivity() != null) {

                            Log.e(TAG, " tender list count responce " + response);

                            String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault()).format(new java.util.Date());

                            sessionManager.setLastCheck(date_time);
                            main_updateStatus.setText(getResources().getString(R.string.Last_updated_on) + " " + new SimpleDateFormat("dd.MM.yyyy. HH:mm", Locale.getDefault()).format(Calendar.getInstance().getTime()));


                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getInt("statusCode") == 200) {


                                    loadAgain = jsonObject.getInt("data") == 500;
                                    if (jsonObject.getInt("data") > 0) {

                                        GetData();


                                    } else {
                                        swipeRefreshLayout.setRefreshing(false);
                                        try {
                                            if (GlobalDataAcess.mainValue == -1 && GlobalDataAcess.subValue == null) {
                                                GetFilteredData(0, "");
                                            } else {
                                                GetFilteredData(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            //  Toast.makeText(getActivity(),"", Toast.LENGTH_SHORT).show();
                                        }

                                        {

                                            sessionManager.setLastCheckService(GlobalDataAcess.getGMTTime());
                                            ////            adapter.notifyDataSetChanged(); ;
                                            Log.e(TAG, "data come from the database when new data 0 and size of array " + filteredArraylist.size());
                                            main_updateStatus.setText(getResources().getString(R.string.Last_updated_on) + " " + new SimpleDateFormat("dd.MM.yyyy. HH:mm", Locale.getDefault()).format(Calendar.getInstance().getTime()));
//                                        progressDialog.hide();
                                            progress_ll.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipeRefreshLayout.setRefreshing(false);
//                                progressDialog.hide();
                                    progress_ll.setVisibility(View.GONE);
                                    String messages = jsonObject.optString("message");
                                    Log.e(TAG, "tender number count error and message is " + messages);
                                    if (mRecyclerView != null) ;
                                    //  Snackbar.make(mRecyclerView,""+messages,Snackbar.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                           /* try {
                                GetFilteredData(0,"");
                            } catch (ParseException e2) {
                                e2.printStackTrace();
                                Toast.makeText(getActivity(), "unexpected error Try Againg!", Toast.LENGTH_SHORT).show();
                            }*/
                                swipeRefreshLayout.setRefreshing(false);
//                            progressDialog.hide();
                                progress_ll.setVisibility(View.GONE);
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*try {
                    GetFilteredData(0,"");
                } catch (ParseException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "unexpected error Try Againg!", Toast.LENGTH_SHORT).show();
                }*/
                swipeRefreshLayout.setRefreshing(false);
                //  progressDialog.hide();
                progress_ll.setVisibility(View.GONE);

              /*  try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                try {  // default data is there
                    if (GlobalDataAcess.mainValue == -1 && GlobalDataAcess.subValue == null) {
                        GetFilteredData(0, "");
                    } else {
                        GetFilteredData(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    //  Toast.makeText(getActivity(),"", Toast.LENGTH_SHORT).show();
                }


                if (mRecyclerView != null) {
                    //Snackbar.make(mRecyclerView,"  "+error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(mRecyclerView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetTenderNumber();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetTenderNumber();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                // mapFooder.put(UtillClasses.lastCheck,sessionManager.getLastCheck());
                if (sessionManager.getTenderFirstTime()) {
                    mapFooder.put(UtillClasses.lastCheck, "0000-00-00 00:00:00");
                } else
                    // mapFooder.put(UtillClasses.lastCheck,sessionManager.GetLastCheckServie());
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastTenderAssignedDate());

                Log.e(TAG, "tender list count time and date = " + sessionManager.getLastCheck());
                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void GetDrawerItemClicked(int mainItem, String subItem) {


        try {

            GetFilteredData(mainItem, subItem);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnMessageEvent(MessageEvent event) {
        // Log.e(TAG,"on message event clicked");
        if (GlobalDataAcess.mainValue == -1 && GlobalDataAcess.subValue == null) {
            //  Log.e(TAG,"on message event with defalut main or sub value ");
            try {

                GetFilteredData(0, "");
                //  progressDialog.show();on
            } catch (ParseException e) {
                Log.e(TAG, "on message default main or sub catch block");
                e.printStackTrace();

            }
        } else {
            Log.e(TAG, "on message event with main or sub value ");
            try {
                GetFilteredData(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
                //  progressDialog.show();
            } catch (ParseException e) {
                Log.e(TAG, "on message  main or sub catch block");
                e.printStackTrace();
            }
        }

    }

    public void GetFilteredData(int mainItem, String sunItem) throws ParseException {


        // Log.e(TAG,"total entries in tender list :"+dataBaseHalper.GetTenderListCount());
        if (sunItem == null)
            return;

        GlobalDataAcess.mainValue = mainItem;
        GlobalDataAcess.subValue = sunItem;

        defaultMainItem = mainItem;
        defaullSubItem = sunItem;
        currentPage = 0;
        arrayTenderList.clear();
        isLastPage = false;

        //adapter.notifyDataSetChanged(); ;
        //for handleing the list for multiple time
//        if (dataBaseHalper.GetTenderList()) { // hide currently we do get data in format of 25
        if (true) {

            // check for the tender folder is empty or not
//            if (filteredArraylist!= null && filteredArraylist.size() ==0){
//                mRecyclerView.setVisibility(View.GONE);
//                tender_not_found.setVisibility(View.VISIBLE);
//                return;
//            }else{
//                mRecyclerView.setVisibility(View.VISIBLE);
//                tender_not_found.setVisibility(View.GONE);
//            }

            arrayTenderList.addAll(filteredArraylist);
            // swipeRefreshLayout.setRefreshing(true);
            Log.e(TAG, "filtered array list size is :" + filteredArraylist.size() + " value of mainiem is " + mainItem + " value of the sub item is :" + sunItem);
            filteredArraylist.clear();
            adapter.notifyDataSetChanged();
            ;

            if (mainItem == 0) {//for active

//                Date currentdate = new Date();
//
//
//                Log.e(TAG, "tenderlist is created in active");
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && ((currentdate.after(startingDate) && (currentdate.before(endingDate))) || stopDate.equals("0000-00-00 00:00:00")) && (!arrayTenderList.get(i).getOptionsTender().equals("8192")) && (!arrayTenderList.get(i).getStatusTender().equals("1024"))) {
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && ((currentdate.after(startingDate) && (currentdate.before(endingDate))) || stopDate.equals("0000-00-00 00:00:00")) && (!((Long.parseLong(arrayTenderList.get(i).getOptionsTender()) & Long.parseLong("8192")) > 0)) &&  /*(!arrayTenderList.get(i).getStatusTender().equals("1024"))*/  (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("1024")) > 0))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }

//                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( (current_date between tenderList_table.date_start and tenderList_table.date_stop) or (tenderList_table.date_start ='0000-00-00 00:00:00')) and (tenderList_table.options_tender & 8192 <> 8192) and (tenderList_table.status_tender & 1024 <> 1024 ) and (tenderList_table.id_document <> 'L' ) order by tenderList_table.assigned_tender desc ";
                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 =0 ) and (tenderList_table.status_tender & 16384 =0) and (tenderList_table.status_tender & 1024 =0 )and (tenderList_table.options_tender & 8192 =0) and (tenderList_table.date_start < date(tenderList_table.date_stop) or date_stop = '0000-00-00 00:00:00')and(tenderList_table.current_datetime < date_stop OR date_stop = '0000-00-00 00:00:00')and (tenderList_table.id_document != 'L' ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);

            } else if (mainItem == 1) {//for Secondary

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && ((currentdate.after(startingDate) && (currentdate.before(endingDate))) || stopDate.equals("0000-00-00 00:00:00")) && (!arrayTenderList.get(i).getOptionsTender().equals("8192")) && (arrayTenderList.get(i).getStatusTender().equals("1024"))) {
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && ((currentdate.after(startingDate) && (currentdate.before(endingDate))) || stopDate.equals("0000-00-00 00:00:00")) && (!((Long.parseLong(arrayTenderList.get(i).getOptionsTender()) & Long.parseLong("8192")) > 0)) && (((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("1024")) > 0))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }

                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 =0 ) and (tenderList_table.status_tender & 16384 =0) and (tenderList_table.status_tender & 1024 > 0 )and (tenderList_table.options_tender & 8192 =0) and (tenderList_table.date_start < date(tenderList_table.date_stop) or date_stop = '0000-00-00 00:00:00')and(tenderList_table.current_datetime < date_stop OR date_stop = '0000-00-00 00:00:00')and (tenderList_table.id_document != 'L' ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 2) {//for have Value

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && arrayTenderList.get(i).getAmountTender() > 0) {
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && arrayTenderList.get(i).getAmountTender() > 0) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }

                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( tenderList_table.amount_tender > 0.0 ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                ////            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 3) {//for with Flag

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    //  if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && arrayTenderList.get(i).getStatusTender().equals("8192")) {
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && (((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("8192")) > 0))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }

                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( tenderList_table.status_tender & 8192 = 8192 ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                ////            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 4) {//for hot

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
////                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
////
////                    String startDate = arrayTenderList.get(i).getDateStart();
////                    String stopDate = arrayTenderList.get(i).getDateStop();
////
////                    Date startingDate = date.parse(startDate);
////                    Date endingDate = date.parse(stopDate);
////
////
////                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && arrayTenderList.get(i).getOptionsTender().equals("65536")) {
////                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && (((Long.parseLong(arrayTenderList.get(i).getOptionsTender()) & Long.parseLong("65536")) > 0))) {
////                        filteredArraylist.add(arrayTenderList.get(i));
////                        //            adapter.notifyDataSetChanged(); ;
////
////                    }
////                    if (i + 1 == arraySize) {
////                        goToFirstPosition();
////                    }
//
//
//
//                }
                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( tenderList_table.options_tender & 65536 = 65536 ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);

                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 5) {//for Procurement plan

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                  /*  SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);*/
//
//
//                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && arrayTenderList.get(i).getIdDocument().equals("A")) {
//                    if (!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) &
//                            Long.parseLong("2147483648")) > 0) && !((Long.parseLong(arrayTenderList.get(i).getStatusTender())
//                            & Long.parseLong("16384")) > 0) &&
//                            arrayTenderList.get(i).getIdDocument().equals("A")) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }
                qwery = "select * from tenderList_table  where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( tenderList_table.id_document  = 'A' ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                //  //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 6) {//for spam
//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }
                qwery = "select * from tenderList_table  where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 = 16384)  order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 7) {//for Expired

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                int count = 0;
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    // if ((!arrayTenderList.get(i).getStatusTender().equals("2147483648")) && (!arrayTenderList.get(i).getStatusTender().equals("16384")) && ( ((currentdate.before(endingDate))) || (!stopDate.equals("0000-00-00 00:00:00")) ) && (arrayTenderList.get(i).getOptionsTender().equals("8192"))) {
//                    // if ((! (  (Long.parseLong(arrayTenderList.get(i).getStatusTender().trim())  & Long.parseLong("2147483648")  ) > 0 ) )  && (! ( ( Long.parseLong( arrayTenderList.get(i).getStatusTender())  & Long.parseLong("16384"))  >0)) && ( ((currentdate.before(endingDate))) || (!stopDate.equals("0000-00-00 00:00:00")) ) &&  (( ( Long.parseLong( arrayTenderList.get(i).getOptionsTender())  & Long.parseLong("8192"))  >0))) {
//                    if ((!((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0)) && (!((Long.parseLong(arrayTenderList.get(i).getStatusTender()) & Long.parseLong("16384")) > 0)) && ((((currentdate.after(endingDate))) && (!stopDate.equals("0000-00-00 00:00:00"))) || (((Long.parseLong(arrayTenderList.get(i).getOptionsTender()) & Long.parseLong("8192")) > 0)))) {
//                        //  Log.e(TAG,"option tender value is "+arrayTenderList.get(i).getOptionsTender()+" status tender value is "+arrayTenderList.get(i).getStatusTender()+" and value of the tender is "+arrayTenderList.get(i).getIdTender()+"  at position "+(count++)+" predate check value "+currentdate.after(endingDate)+" end date check details "+(!stopDate.equals("0000-00-00 00:00:00"))+ " stop time value "+stopDate+" current date value "+currentdate+" ending date "+endingDate);
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }


//                qwery = "select * from tenderList_table  where (tenderList_table.status_tender & 2147483648 <> 2147483648 ) and (tenderList_table.status_tender & 16384 <> 16384) and ( ( (current_date > tenderList_table.date_stop) and (tenderList_table.date_stop <> '0000-00-00 00:00:00') ) or (tenderList_table.options_tender & 8192 = 8192) ) order by tenderList_table.assigned_tender desc";
                qwery = "select * from tenderList_table where (tenderList_table.status_tender & 2147483648 =0 ) and (tenderList_table.status_tender & 16384 =0) and (tenderList_table.date_start = date(tenderList_table.date_stop) or (tenderList_table.current_datetime > date_stop and date_stop != '0000-00-00 00:00:00') or(tenderList_table.options_tender & 8192 >0) or tenderList_table.id_document = 'A' ) order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == 8) {//for deleted

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                    String startDate = arrayTenderList.get(i).getDateStart();
//                    String stopDate = arrayTenderList.get(i).getDateStop();
//
//                    Date startingDate = date.parse(startDate);
//                    Date endingDate = date.parse(stopDate);
//
//
//                    if ((((Long.parseLong(arrayTenderList.get(i).getStatusTender().trim()) & Long.parseLong("2147483648")) > 0))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }
                qwery = "select * from tenderList_table  where (tenderList_table.status_tender & 2147483648 = 2147483648 )  order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);


                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
//            } else if (mainItem == 9) {//for Tag
            } else if (mainItem == 10) {//for Tag

//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//               /* SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                String startDate = arrayTenderList.get(i).getDateStart();
//                String stopDate = arrayTenderList.get(i).getDateStop();
//
//                Date startingDate = date.parse(startDate);
//                Date endingDate = date.parse(stopDate);
//*/
//
//                    //if ((arrayTenderList.get(i).getTagTender().equals(sunItem))) {
//                    if ((Long.parseLong(arrayTenderList.get(i).getTagTender()) & Long.parseLong(sunItem)) > 0) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }

                qwery = "select * from tenderList_table where (tenderList_table.tag_tender & " + sunItem + " = " + sunItem + ") order by tenderList_table.assigned_tender desc ";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);
                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
//            } else if (mainItem == 10) {//for categories  for this  in web app the take only 3 digits
            } else if (mainItem == 11) {//for categories  for this  in web app the take only 3 digits
                String category_from_navi = sunItem.toString().substring(0, 3);

                //three cate string
                String category_for_three = sunItem.toString().toString().substring(0, 3) + "00000";

//                        if(seperated_cate[j].toString().equals(category_for_three)){
//                            Log.e(TAG,"cate filter three text tender id ="+arrayTenderList.get(i).getIdTender()+" list cat is  "+seperated_cate[j]+ " compair with "+category_for_three);
//                        }
                //two cate String
                String category_for_two = sunItem.toString().toString().substring(0, 2) + "000000";


//                int countPosition = 0;
//                try {
//
//                    int arraySize = arrayTenderList.size();
//                    for (int i = 0; i < arraySize; i++) {
//
//                        // first of all  seperateing het cateforiy names
//
//                        //  Log.e(TAG," country tender name is :"+arrayTenderList.get(i).getCategoriesTender()+" and filter category name is  :"+sunItem +" for tender id is "+arrayTenderList.get(i).getIdTender());
//
//                        String[] seperated_cate = arrayTenderList.get(i).getCategoriesTender().toString().split(",");
//
//                        int size_array = seperated_cate.length;
//
//                        for (int j = 0; j < size_array; j++) {
//                            //String category_from_list = arrayTenderList.get(i).getCategoriesTender().toString().substring(0,4);
//                        /*String category_from_list = seperated_cate[j].toString().substring(0, 4);
//                        String category_from_navi = sunItem.toString().substring(0, 4);*/
//
//                            String category_from_list = seperated_cate[j].toString().substring(0, 3);
//                            String category_from_navi = sunItem.toString().substring(0, 3);
//
//                            //three cate string
//                            String category_for_three = sunItem.toString().toString().substring(0, 3) + "00000";
//
////                        if(seperated_cate[j].toString().equals(category_for_three)){
////                            Log.e(TAG,"cate filter three text tender id ="+arrayTenderList.get(i).getIdTender()+" list cat is  "+seperated_cate[j]+ " compair with "+category_for_three);
////                        }
//                            //two cate String
//                            String category_for_two = sunItem.toString().toString().substring(0, 2) + "000000";
////                        if(seperated_cate[j].toString().equals(category_for_two)){
////                            Log.e(TAG,"cate filter two text tender id ="+arrayTenderList.get(i).getIdTender()+" list cat is  "+seperated_cate[j]+ " compair with "+category_for_two);
////                        }
//                            // Log.e(TAG, " Category tender name is :"+"Tender id is "+ arrayTenderList.get(i).getIdTender()+" and cate from list "+ category_from_list + " and filter category name is  :" + category_from_navi + " cate three = "+category_for_three+" cate two = "+category_for_two+" value of cate is "+seperated_cate[j].toString()+ "");
//                            //if ((arrayTenderList.get(i).getCategoriesTender().contains(sunItem))) {
//                            if ((category_from_list.equals(category_from_navi)) || (seperated_cate[j].toString().equals(category_for_three)) || (seperated_cate[j].toString().equals(category_for_two))) {
//                                //Log.e(TAG,"categories list for cate "+sunItem+" tender id = "+arrayTenderList.get(i).getIdTender()+"cate from list "+seperated_cate[j].toString()+ " cat from list "+category_from_list+" cat to comp "+category_from_navi+" at position "+countPosition);
//                                // Log.e(TAG,"categories list for cate "+sunItem+" tender id = "+arrayTenderList.get(i).getIdTender()+"cate from list "+arrayTenderList.get(i).getCategoriesTender()+ " at position "+countPosition );
//                                filteredArraylist.add(arrayTenderList.get(i));
//                                //            adapter.notifyDataSetChanged(); ;
//                                countPosition++;
//                                break;
//                            }
//                        }
//
//                        if (i + 1 == arraySize) {
//                            goToFirstPosition();
//                        }
//                    }
//                    // //            adapter.notifyDataSetChanged(); ;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e(TAG, "value of the posion is " + countPosition);
//                }

                qwery = "select * from tenderList_table where (tenderList_table.categories_tender  like '" + category_from_navi + "_____')or (tenderList_table.categories_tender  like '%," + category_from_navi + "_____') or (tenderList_table.categories_tender  like '" + category_from_navi + "_____,%') or (tenderList_table.categories_tender  like '%," + category_from_navi + "_____,%') or (tenderList_table.categories_tender = '" + category_for_three + "')or (tenderList_table.categories_tender = '%," + category_for_three + "') or (tenderList_table.categories_tender = '" + category_for_three + ",%') or (tenderList_table.categories_tender = '%," + category_for_three + ",%') or (tenderList_table.categories_tender = '" + category_for_two + "') or (tenderList_table.categories_tender = '%," + category_for_two + "') or (tenderList_table.categories_tender = '" + category_for_two + ",%') or (tenderList_table.categories_tender = '%," + category_for_two + ",%') order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);
                AppController.sessionManager.setSelectedQwery(qwery);
                //} else if (mainItem == 11) {//for Countries
            } else if (mainItem == 12) {//for Countries
                String country_from_navi = sunItem.toString().substring(0, 3);
//                Date currentdate = new Date();
//
//
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//               /* SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                String startDate = arrayTenderList.get(i).getDateStart();
//                String stopDate = arrayTenderList.get(i).getDateStop();
//
//                Date startingDate = date.parse(startDate);
//                Date endingDate = date.parse(stopDate);
//                */
//                    String country_from_list = arrayTenderList.get(i).getCountryTender().toString().substring(0, 3);
//                    String country_from_navi = sunItem.toString().substring(0, 3);
//                    //   Log.e(TAG," country tender name is :"+country_from_list+" and filter category name is  :"+country_from_navi);
//
//                    // if ((arrayTenderList.get(i).getCountryTender().equals(sunItem))) {
//                    if (country_from_list.equals(country_from_navi)) {
//                        //Log.e(TAG," country tender name is :"+arrayTenderList.get(i).getCountryTender()+" and filter category name is  :"+sunItem);
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }


                qwery = "select * from tenderList_table where (tenderList_table.country_tender  like '" + country_from_navi + "__') order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);
                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == (12 + 1)) {//for Cities

//                Date currentdate = new Date();
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//               /* SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                String startDate = arrayTenderList.get(i).getDateStart();
//                String stopDate = arrayTenderList.get(i).getDateStop();
//
//                Date startingDate = date.parse(startDate);
//                Date endingDate = date.parse(stopDate);
//*/
//              /* if (arrayTenderList.get(i).getIdTender().trim().equals("20170000000038705")){
//                   Log.e(TAG,arrayTenderList.get(i).toString());
//               }*/
//
//                    // Log.e(TAG,"city filter"+" tender id = "+arrayTenderList.get(i).getIdTender()+" city tender "+arrayTenderList.get(i).getCityTender()+" and city compaired value is "+sunItem);
//                    if ((arrayTenderList.get(i).getCityTender().equals(sunItem))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//                    }
//
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }
                qwery = "select * from tenderList_table where (tenderList_table.city_tender  = " + sunItem + ") order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);
                AppController.sessionManager.setSelectedQwery(qwery);
                // //            adapter.notifyDataSetChanged(); ;
            } else if (mainItem == (13 + 1)) {//for Buyer

//                Date currentdate = new Date();
//                int arraySize = arrayTenderList.size();
//                for (int i = 0; i < arraySize; i++) {
//               /* SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//
//                String startDate = arrayTenderList.get(i).getDateStart();
//                String stopDate = arrayTenderList.get(i).getDateStop();
//
//                Date startingDate = date.parse(startDate);
//                Date endingDate = date.parse(stopDate);
//*/
//                    //Log.e(TAG,"id tender is "+arrayTenderList.get(i).getIdTender()+" id buyer is "+arrayTenderList.get(i).getIdBuyer()+" and compair with "+sunItem);
//                    if ((arrayTenderList.get(i).getIdBuyer().equals(sunItem))) {
//                        filteredArraylist.add(arrayTenderList.get(i));
//                        //            adapter.notifyDataSetChanged(); ;
//
//                    }
//                    if (i + 1 == arraySize) {
//                        goToFirstPosition();
//                    }
//                }
                // //            adapter.notifyDataSetChanged(); ;

                qwery = "select * from tenderList_table where (tenderList_table.id_buyer  = " + sunItem + ") order by tenderList_table.assigned_tender desc";
                getPaggingTenderList(qwery, 0);
                getpaggingTenCount(qwery);
                AppController.sessionManager.setSelectedQwery(qwery);
            }
            // progressDialog.hide();

//            try{
//
//                new java.util.Timer().schedule(
//                        new java.util.TimerTask() {
//                            @Override
//                            public void run() {
//                                // your code here
//                                if (mRecyclerView != null) {
//                                    //mRecyclerView.smoothScrollToPosition(0);
//                                    mRecyclerView.scrollToPosition(0);
//                                }
//                            }
//                        },
//                        1000
//                );
//            }catch (Exception e){
//                Log.e(TAG,"smooth scroll to the first position is Successfully done");
//                e.printStackTrace();
//                Log.e(TAG,"smooth scroll to the first position is crash");
//            }

        }


    }

    private void getpaggingTenCount(String qwery) {
        totalPages = dataBaseHalper.getPaggingDataCount(qwery);
    }


    public void goToFirstPosition() {
        //   scrollToPosition(0);
        if (mRecyclerView != null) {
            Log.e(TAG, "recyclerview scroll successfully done");

            final FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager();
            //adapter = new MyRecycleviewAdapter(getActivity(),arrayTenderList);
            int textsize = sessionManager.GetTenderSize();
            adapter = new MyRecycleviewAdapter(getActivity(), filteredArraylist, Home.modelUserTagArrayList, flexboxLayoutManager, textsize);

            mRecyclerView.setAdapter(adapter);
            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
//            //            adapter.notifyDataSetChanged(); ;


        } else {

            Log.e(TAG, "recyclerview scroll Crashes");
            adapter.notifyDataSetChanged();

            //  adapter.notifyItemRangeChanged(0,adapter.getItemCount());
        }
        //  swipeRefreshLayout.setRefreshing(false);
    }

    public void getTagData() {

        String response = sessionManager.getTempTag();
        // String  response ="{\"data\":[{\"tagIndex\":1,\"tagText\":\"test yellow\",\"colorID\":1,\"colorCSS\":\"f7e298\",\"colorName\":\"yellow\"},{\"tagIndex\":2,\"tagText\":\"test red\",\"colorID\":3,\"colorCSS\":\"fba3a1\",\"colorName\":\"red\"},{\"tagIndex\":4,\"tagText\":\"test orange\",\"colorID\":1,\"colorCSS\":\"fba3a1\",\"colorName\":\"orange\"},{\"tagIndex\":8,\"tagText\":\"test blue\",\"colorID\":1,\"colorCSS\":\"f1afec\",\"colorName\":\"blue\"},{\"tagIndex\":16,\"tagText\":\"test yellow\",\"colorID\":1,\"colorCSS\":\"f7e298\",\"colorName\":\"yellow\"}],\"statusCode\":\"200\",\"message\":\"Ok\"}";
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("statusCode") == 200) {
                modelUserTagArrayList.clear();
                if (!jsonObject.isNull("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    int size = jsonArray.length();
                    for (int i = 0; i < size; i++) {
                        JSONObject jsonData = jsonArray.getJSONObject(i);
                        ModelUserTag modelUserTag = new ModelUserTag();
                        modelUserTag.setTagIndex(jsonData.optString("tagIndex"));
                        modelUserTag.setTagText(jsonData.optString("tagText"));
                        modelUserTag.setColorId(jsonData.optString("colorID"));
                        modelUserTag.setColorCSS(jsonData.optString("colorCSS"));
                        modelUserTag.setColorName(jsonData.optString("colorName"));
                        modelUserTagArrayList.add(modelUserTag);
                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void TenderlistClickHandle(int type, int position) {
        //1 = set flag, 2 = set tag , 3 =set as unread ,4 = deletee
        if (position != RecyclerView.NO_POSITION) {
            if (type == 1) {

            } else if (type == 2) {
                /*//show tag dilaog

                mAlertDialog =  build.create();

                modelTagArrayList = new ArrayList<>();

                for(int i = 0;i<5 ;i++){
                    ModelTag modelTag = new ModelTag();
                    modelTag.setTagName("number"+i);
                    modelTagArrayList.add(modelTag);
                }
                *//* LayoutInflater layoutInflater= (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.tenderlist_tag_recylay,null,false);*//*

                mRecyclerviewTag = (RecyclerView)view_alert.findViewById(R.id.tender_list_tag_recy);
                setTagCanBtn = (Button)view_alert.findViewById(R.id.tender_list_tag_canBtn);
                setTagUpdBtn = (Button)view_alert.findViewById(R.id.tender_list_tag_updBtn);

                mRecyclerviewTag.setHasFixedSize(true);
                mRecyclerviewTag.setLayoutManager(new LinearLayoutManager(getActivity()));

                adapterTag = new TenderListSetTagAdapter(getActivity(),modelTagArrayList);

                mRecyclerviewTag.setAdapter(adapterTag);

                setTagCanBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                    }
                });

                setTagUpdBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                    }
                });


                mAlertDialog.setView(view_alert);*/

                //mAlertDialog.show();

            } else if (type == 4) {
                filteredArraylist.remove(position);

                //            adapter.notifyDataSetChanged(); ;
            } else if (type == 3) {
                filteredArraylist.get(position).setReaded(false);
                //            adapter.notifyDataSetChanged(); ;
            }
        }
    }


    private void scrollToPosition(int i) {

        mRecyclerView.scrollToPosition(i);
        if (i == 0) {
            linearLayoutManager.scrollToPositionWithOffset(0, 0);
            //mScrollView.fullScroll(View.FOCUS_UP);
        }
    }


    public void getPaggingTenderList(String qwery, int lower_point) {
//        dataBaseHalper.getPaddingTenderList(qwery, startPosition, endPosition, currentPage, new InterPagTenderlist() {
        selectedQuery = qwery;


//        dataBaseHalper.getPaddingTenderList(qwery, ((currentPage *25)+1), ((currentPage+1) *25), currentPage, new InterPagTenderlist() {
        dataBaseHalper.getPaddingTenderList(qwery, lower_point, ((currentPage + 1) * 25), currentPage, new InterPagTenderlist() {
            @Override
            public void getTenderlist(Object object) {
                if (adapter != null) {
                    adapter.removeLoadingFooter();
                }
                if (object instanceof Integer) {
                } else {
                    if (((ArrayList<ModelTenderList>) object).size() > 0) {
                        filteredArraylist.addAll(((ArrayList<ModelTenderList>) object));
                        adapter.notifyDataSetChanged();
                    }
                }


                isLoading = false;
                if (currentPage == totalPages) {
                    isLastPage = true;
                }


            }
        });
    }

    public class TenderListDBStore extends AsyncTask<String, Void, String> {

        String response = null;

        public TenderListDBStore(String response) {
            this.response = response;
        }

        @Override
        protected String doInBackground(String... params) {
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("statusCode") == 200) {

                        Log.e(TAG, response);


                        sessionManager.setTenderFirstTime(false);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        int size = jsonArray.length();
                        Log.e(TAG, " tender list responce SIZE TOTAL " + size);

                        for (int i = 0; i < size; i++) {

                            JSONObject jsondata = jsonArray.getJSONObject(i);
                            ModelTenderList modelTenderList = new ModelTenderList();
                            modelTenderList.setAmountCurrency(jsondata.optString("amountCurrency"));
                            modelTenderList.setAmountTender(jsondata.optDouble("amountTender"));
                            modelTenderList.setAssignedTender(jsondata.optString("assignedTender"));
                            modelTenderList.setCategoriesTender(jsondata.optString("categoriesTender"));
                            modelTenderList.setCityBuyer(jsondata.optString("cityBuyer"));
                            modelTenderList.setCityTender(jsondata.optString("cityBuyerID"));
                            modelTenderList.setCountryFlagTender(jsondata.optString("countryFlagTender"));
                            modelTenderList.setCountryTender(jsondata.optString("countryTender"));
                            modelTenderList.setDateDocumentation(jsondata.optString("dateDocumentation"));
                            modelTenderList.setDateStart(jsondata.optString("dateStart"));
                            modelTenderList.setDateStop(jsondata.optString("dateStop"));
                            modelTenderList.setDocumentationCurrency(jsondata.optString("documentationCurrency"));
                            modelTenderList.setDocumentationPrice(jsondata.optDouble("documentationPrice"));
                            modelTenderList.setIdAwardCriteria(jsondata.optString("idAwardCriteria"));
                            modelTenderList.setIdBuyer(jsondata.optString("idBuyer"));
                            modelTenderList.setIdContract(jsondata.optString("idContract"));
                            modelTenderList.setIdDocument(jsondata.optString("idDocument"));
                            modelTenderList.setIdNUTS(jsondata.optString("idNUTS"));
                            modelTenderList.setIdProcedure(jsondata.optString("idProcedure"));
                            modelTenderList.setIdTender(jsondata.optString("idTender"));
                            modelTenderList.setMainLanguage(jsondata.optString("mainLanguage"));
                            modelTenderList.setNameAwardCriteria(jsondata.optString("nameAwardCriteria"));
                            modelTenderList.setNameBuyer(jsondata.optString("nameBuyer"));
                            modelTenderList.setNameContract(jsondata.optString("nameContract"));
                            modelTenderList.setNameDocument(jsondata.optString("nameDocument"));
                            modelTenderList.setNameProcedure(jsondata.optString("nameProcedure"));
                            modelTenderList.setOptionsTender(jsondata.optString("optionsTender"));
                            modelTenderList.setSourcetender(jsondata.optString("sourceTender"));
                            modelTenderList.setStatusTender(jsondata.optString("statusTender"));
                            modelTenderList.setTagTender(jsondata.optString("tagTender"));
                            modelTenderList.setTitleTender(jsondata.optString("titleTender"));
                            modelTenderList.setTypeTender(jsondata.optString("typeTender"));
                            modelTenderList.setUrlTender(jsondata.optString("urlTender"));
                            dataBaseHalper.InsertTenderList(modelTenderList);

                            if (i + 1 == size) {
                                return "";

                            }

                        }
                        dataBaseHalper.close();

                    } else {
//
                        if (mRecyclerView != null) {
                            Snackbar.make(mRecyclerView, getActivity().getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {


//
                    e.printStackTrace();
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (GlobalDataAcess.mainValue == -1 && GlobalDataAcess.subValue == null) {
                    GetFilteredData(0, "");
                } else {
                    GetFilteredData(GlobalDataAcess.mainValue, GlobalDataAcess.subValue);
                }
            } catch (ParseException e) {
                e.printStackTrace();

            }


            progress_ll.setVisibility(View.GONE);

            if (loadAgain) {
                GetTenderNumber();
            } else {
                if (preferences.getBoolean("isFirst", true)) {
                    if (getActivity() != null) {
                        ((Home) getActivity()).setAppLangDialog(sessionManager.getAppLanguage());
                    }
                }
            }
        }

    }

}
