package com.tenderbooking.event_bus;

/**
 * Created by namok on 03-10-2017.
 */

public class HomeCallEvent {
    boolean isStart = false;

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }
}
