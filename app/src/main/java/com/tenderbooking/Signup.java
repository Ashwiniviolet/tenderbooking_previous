package com.tenderbooking;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Interface.InterfaceDemoAccount;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Signup extends AppCompatActivity {

    public static String selectetLanguage = "english";
    public static boolean signLang = false;
    Spinner signup_country, signup_language;
    EditText firstNameEt, lastNameEt, signUp_emailEt, signUp_phoneEt;
    String firstName, lastname, email, phone;
    Button signupbtn;
    ArrayAdapter<String> adapterSpinner;
    ArrayAdapter<String> adapterlanguage;
    List<String> listcountry;
    List<String> listlanguage;
    String selectedCountryId;
    RadioGroup signup_radioGrp;
    RadioButton signup_maleRadio, signup_femaleRadio;
    int gender = 1;
    Map<String, String> mapFooder;
    SessionManager sessionManager;
    ProgressDialog mProgressDialog;
    private LinearLayout progress_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mapFooder = new HashMap<>();
        Initialization();
        sessionManager = new SessionManager(Signup.this);
        progress_ll = (LinearLayout) findViewById(R.id.progress_ll);

        mProgressDialog = new ProgressDialog(Signup.this);
        mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
        mProgressDialog.setCancelable(false);
        signup_maleRadio.setChecked(true);


        listcountry = new ArrayList<>();
        listcountry.add("Srbija");
        // listcountry.add("Crna Gora");
        listcountry.add("Bosna i Hercegovina");
        listcountry.add("UK");
        // listcountry.add("tCroatia");

        //language list
       /* listlanguage = new ArrayList<>();
        listlanguage.add("UK");
        listlanguage.add("Srbija");
        listlanguage.add("Bosna i Hercegovina");
*/
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Kreirajte demo nalog");

        //adapter country
        adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listcountry);

        //adapter language
        // adapterlanguage = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,listlanguage);

        signup_country.setAdapter(adapterSpinner);

        // signup_language.setAdapter(adapterlanguage);

        signup_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (signup_country.getSelectedItemPosition()) {
                    case 0:
                        selectedCountryId = "00100";
                        selectetLanguage = "serbian";
                        break;

                    case 1:
                        selectedCountryId = "00300";
                        selectetLanguage = "bosnian";
                        break;

                    case 2:
                        selectedCountryId = "22400";
                        selectetLanguage = "english";
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCountryId = "0";
            }
        });
/*
        signup_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectetLanguage = listlanguage.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        signup_radioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.signup_maleRadio) {
                    gender = 1;

                } else if (checkedId == R.id.signup_femaleRadio) {
                    gender = 2;
                }
            }
        });


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstNameEt.getText().toString().trim().length() == 0) {
                    firstNameEt.setError(getResources().getString(R.string.first_name));
                    firstNameEt.requestFocus();
                } else if (lastNameEt.getText().toString().trim().length() == 0) {
                    lastNameEt.setError(getResources().getString(R.string.last_name));
                    lastNameEt.requestFocus();
                } else if (!GlobalDataAcess.isValidEmail(signUp_emailEt.getText().toString().trim())) {
                    signUp_emailEt.setError(getResources().getString(R.string.email_address));
                    signUp_emailEt.requestFocus();
                } else if (signUp_phoneEt.getText().toString().trim().length() == 0) {
                    signUp_phoneEt.setError(getResources().getString(R.string.phone_number));
                    signUp_phoneEt.requestFocus();
                } else {
                    GlobalDataAcess.hideSoftKeyboard(Signup.this);
                    firstName = firstNameEt.getText().toString().trim();
                    lastname = lastNameEt.getText().toString().trim();
                    email = signUp_emailEt.getText().toString().trim();
                    phone = signUp_phoneEt.getText().toString().trim();

                    mapFooder.put(UtillClasses.Demo_firstname, firstName);
                    mapFooder.put(UtillClasses.Demo_lastName, lastname);
                    mapFooder.put(UtillClasses.Demo_gender, gender + "");
                    mapFooder.put(UtillClasses.Demo_email, email);
                    mapFooder.put(UtillClasses.Demo_mobile, phone);
                    mapFooder.put(UtillClasses.Demo_Country, selectedCountryId);

                    demoLogin(mapFooder);

                }
            }
        });

    }

    public void Initialization() {
        signup_country = (Spinner) findViewById(R.id.signup_country);
        //signup_language = (Spinner)findViewById(R.id.signup_language);
        firstNameEt = (EditText) findViewById(R.id.firstName);
        lastNameEt = (EditText) findViewById(R.id.lastName);
        signUp_emailEt = (EditText) findViewById(R.id.signUp_email);
        signUp_phoneEt = (EditText) findViewById(R.id.signUp_phone);
        signupbtn = (Button) findViewById(R.id.signupbtn);
        signup_radioGrp = (RadioGroup) findViewById(R.id.signup_radioGrp);
        signup_maleRadio = (RadioButton) findViewById(R.id.signup_maleRadio);
        signup_femaleRadio = (RadioButton) findViewById(R.id.signup_femaleRadio);
    }

    public void demoLogin(final Map<String, String> map) {

//        mProgressDialog.show();
        progress_ll.setVisibility(View.VISIBLE);
        final String mURL = UtillClasses.BaseUrl + "create_demo_account";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonobject = new JSONObject(response);

//                            mProgressDialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            if (jsonobject.getInt("statusCode") == 200) {
                                if (!jsonobject.isNull("data")) {
                                    String username = jsonobject.getJSONObject("data").optString("user");
                                    String password = jsonobject.getJSONObject("data").optString("password");
                                    String mobileno = map.get(UtillClasses.Demo_mobile);
                                    Login.wizardUser = jsonobject.optInt("wizardUser");

                                    sessionManager.setUserLang(selectetLanguage);
                                    InterfaceDemoAccount interfaceDemo = new Login();
                                    interfaceDemo.CreateDemoAcc(username, password, mobileno);
                                    signLang = true;
                                    finish();

                                }
                            } else {
                                String message = jsonobject.optString("message");
                                if (lastNameEt != null)
                                    Snackbar.make(lastNameEt, message, Snackbar.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress_ll.setVisibility(View.GONE);
//              mProgressDialog.hide();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapHeader = new HashMap<>();
                mapHeader.put("Content-Type", "application/x-www-form-urlencoded");
                //mapHeader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                return mapHeader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            this.finish();
            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

}
