package com.tenderbooking.Reciver;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Model.ModelAdviceAnsware;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.R;
import com.tenderbooking.Services.NotiIntentService;
import com.tenderbooking.Splash;
import com.tenderbooking.Volley.AppController;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by namok on 02-08-2017.
 */

public class BookTrackReciver extends BroadcastReceiver {

    public static final String TAG = BookTrackReciver.class.getSimpleName();
    public static long last_run;
    public SessionManager sessionManager;
    DataBaseHalper dataBaseHalper;
    Map<String, String> deviceinfoMap;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent.getAction().equals(GlobalDataAcess.user_Broadcast)) {

//            if ( (System.currentTimeMillis() -last_run) > 10000){
//                Log.e(TAG,"reciver called with diff "+(System.currentTimeMillis() -last_run));
//                last_run = System.currentTimeMillis();
//                Intent intent1 = new Intent(context, NotificaitonServices.class);
//                context.startService(intent1);
//            }
//            Log.e(TAG,"boot time reciver application on ");
//            if (sessionManager==null)
//            sessionManager = new SessionManager(context);
//            deviceinfoMap = sessionManager.GetDeviceInfo();
//            getCheckAdviceAnsware(context);
//            getAdviceAnsware(context);
//            getCheckAdviceQueNumber(context);
//            if (dataBaseHalper ==null)
//                dataBaseHalper = new DataBaseHalper(context);

        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.e(TAG, "boot time application on ");
            Intent intent1 = new Intent(context, com.tenderbooking.Services.NotificaitonServices.class);
            context.startService(intent1);

            Intent intent2 = new Intent(context, NotiIntentService.class);
            context.startService(intent2);
        }
    }

    public synchronized void getAdviceAnsware(final Context mContext) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        final String mURL = UtillClasses.BaseUrl + "check_advice_answers";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " getdata  service at responce  advice answare " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getJSONArray("data").length() > 0) {
                                    DataBaseHalper dataBaseHalper = new DataBaseHalper(mContext);
                                    Log.e(TAG, "sercie at advice answare reviced");
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int dataSize = jsonArray.length();

                                    for (int i = 0; dataSize > i; i++) {
                                        JSONObject jsondata = jsonArray.getJSONObject(i);
                                        ModelAdviceAnsware modelAdviceAnsware = new ModelAdviceAnsware();

                                        modelAdviceAnsware.setIdQuestion(jsondata.optString("idQuestion"));
                                        modelAdviceAnsware.setDetialsAnsware(jsondata.optString("detailsAnswer"));
                                        modelAdviceAnsware.setTitleQuestion(jsondata.optString("titleQuestion"));
                                        // dataBaseHalper.Insert_adviceanswer(modelAdviceAnsware);
                                        dataBaseHalper.updateStatusAdvice(modelAdviceAnsware.getIdQuestion(), "user", "4");
                                        Log.e(TAG, "sercie at advice answare question id" + jsondata.optString("idQuestion"));
                                        //  showNotification(mContext,modelAdviceAnsware.getTitleQuestion());
                                    }


                                    //Log.e(TAG)
                                    // int data = jsonObject.getInt("data");

//                                    Intent intent = new Intent(mContext , Home.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                                    PendingIntent pendingIntent = PendingIntent.getActivity(mContext,0,intent,PendingIntent.FLAG_ONE_SHOT);
//
//                                    Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.app_icon);
//
//                                    android.support.v4.app.NotificationCompat.Builder mBuilder =
//                                            new NotificationCompat.Builder(mContext)
//                                                    .setContentTitle(mContext.getResources().getString(R.string.question_ans_arive))
//                                                    .setStyle(new NotificationCompat.BigTextStyle())
//                                                    .setAutoCancel(true)
//                                                    .setContentText(jsonArray.getJSONObject(jsonArray.length()-1).optString("titleQuestion"))
//                                                    .setSmallIcon(R.drawable.status_bar_icon)
//                                                    .setLargeIcon(icon)
//                                                    .setContentIntent(pendingIntent);
//
//                                    Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//                                    if (sessionManager.GetNotiSound()){
//                                        mBuilder.setSound(alarmsound);
//                                    }
//                                    Log.e(TAG,"Notification from id advice answer thorugh last time check ");
//                                    if (sessionManager.GetNotiEnableTimeInterval()) {
//                                        Log.e(TAG,"notification enable ");
//                                        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//                                        notificationManager.notify(0, mBuilder.build());
//                                    }else
//                                    {
//
//                                        Log.e(TAG,"notification disable ");
//                                        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");
//
//                                        String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
//                                        String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();
//
//                                        String current_session = time_format.format(new Date());
//
//                                    /*Date startTime = time_format.parse("23:00");
//                                    Date stopTime =  time_format.parse("07:00");*/
//
//                                        Date startTime = time_format.parse(start_time_sesssion);
//                                        Date stopTime =  time_format.parse(stop_time_sesssion);
//
//
//                                        Date time = new Date();
//
//                                        if(GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion+":00",stop_time_sesssion+":00",current_session) ) {
//                                            System.out.println("alarm enable");
//                                            Log.e(TAG,"alarm is enable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                        }else{
//                                            Log.e(TAG,"alarm is disable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                            int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
//                                            NotificationManager notificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                                            notificationManager.notify(m, mBuilder.build());
//                                        }
//
//                                    }
                                  /*
                                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                                    notificationManager.notify(0,mBuilder.build());*/


                                    // String date_time = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss").format(new java.util.Date());
                                    // sessionManager.setLastCheckService(date_time);


                                } else {
                                }
                            } else {

                            }

                        } catch (JSONException e) {
                            //progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {


                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                mapFooder.put(UtillClasses.lastCheck, sessionManager.GetlastCheckAdvAns());
                Log.e(TAG, "service advice answare lastcheck data : " + sessionManager.GetlastCheckAdvAns());
                // mapFooder.put(UtillClasses.lastCheck,"2016-01-01 12:00:00");


                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public synchronized void getCheckAdviceAnsware(final Context context) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final ArrayList<ModelAdviceAnsware> modelAdviceAnswareList = new ArrayList<>();

        if (dataBaseHalper == null)
            dataBaseHalper = new DataBaseHalper(context);

        final String question_list = dataBaseHalper.getUnAnsweredAdvice();

        if (!TextUtils.isEmpty(question_list)) {
            final String mURL = UtillClasses.BaseUrl + "check_advice_id_answers";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getInt("statusCode") == 200) {

                                    if (jsonObject.getJSONArray("data").length() > 0) {

                                        //Log.e(TAG, "sercie at advice answare by id reviced");
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                                        int dataSize = jsonArray.length();

                                        for (int i = 0; dataSize > i; i++) {
                                            JSONObject jsondata = jsonArray.getJSONObject(i);
                                            ModelAdviceAnsware modelAdviceAnsware = new ModelAdviceAnsware();

                                            modelAdviceAnsware.setIdQuestion(jsondata.optString("idQuestion"));
                                            modelAdviceAnsware.setDetialsAnsware(jsondata.optString("detailsAnswer"));
                                            modelAdviceAnsware.setTitleQuestion(jsondata.optString("titleQuestion"));
                                            dataBaseHalper.Insert_adviceanswer(modelAdviceAnsware);
                                            dataBaseHalper.updateStatusAdvice(modelAdviceAnsware.getIdQuestion(), "user", "4");
                                            Log.e(TAG, "sercie at advice answare question id" + jsondata.optString("idQuestion"));
                                            modelAdviceAnswareList.add(modelAdviceAnsware);

                                            showNotification(context, modelAdviceAnsware.getTitleQuestion(), context.getResources().getString(R.string.question_ans_arive));
                                            if (i + 1 == dataSize) {
                                                EventBus.getDefault().post(modelAdviceAnswareList);
                                                Log.e(TAG, "advice answer found for " + modelAdviceAnswareList.size());
                                            }
                                        }
//                                        //start notification
//                                        Intent intent = new Intent(context, Home.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                                        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_ONE_SHOT);
//
//                                        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
//
//                                        android.support.v4.app.NotificationCompat.Builder mBuilder =
//                                                new NotificationCompat.Builder(context)
//                                                        .setContentTitle(context.getResources().getString(R.string.question_ans_arive))
//                                                        .setStyle(new NotificationCompat.BigTextStyle())
//                                                        .setAutoCancel(true)
//                                                        .setContentText(jsonArray.getJSONObject(jsonArray.length()-1).optString("titleQuestion"))
//                                                        .setSmallIcon(R.drawable.status_bar_icon)
//                                                        .setLargeIcon(icon)
//                                                        .setContentIntent(pendingIntent);
//
//                                        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//                                        if (sessionManager.GetNotiSound()){
//                                            mBuilder.setSound(alarmsound);
//                                        }
//                                        Log.e(TAG,"Notification from id advice answer thorugh question id's");
//                                        if (sessionManager.GetNotiEnableTimeInterval()) {
//                                            Log.e(TAG,"notification enable ");
//                                            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//                                            notificationManager.notify(0, mBuilder.build());
//                                        }else
//                                        {
//
//                                            Log.e(TAG,"notification disable ");
//                                            SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");
//
//                                            String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
//                                            String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();
//
//                                            String current_session = time_format.format(new Date());
//
//                                    /*Date startTime = time_format.parse("23:00");
//                                    Date stopTime =  time_format.parse("07:00");*/
//
//                                            Date startTime = time_format.parse(start_time_sesssion);
//                                            Date stopTime =  time_format.parse(stop_time_sesssion);
//
//
//                                            Date time = new Date();
//
//                                            if(GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion+":00",stop_time_sesssion+":00",current_session) ) {
//                                                System.out.println("alarm enable");
//                                                Log.e(TAG,"alarm is enable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                            }else{
//                                                Log.e(TAG,"alarm is disable with "+start_time_sesssion+":00"+stop_time_sesssion+":00"+current_session);
//                                                int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
//                                                NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                                                notificationManager.notify(m, mBuilder.build());
//                                            }
//
//                                        }

                                        //Log.e(TAG)
                                        // int data = jsonObject.getInt("data");


                                  /*
                                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                                    notificationManager.notify(0,mBuilder.build());*/


                                        // String date_time = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss").format(new java.util.Date());
                                        // sessionManager.setLastCheckService(date_time);


                                    } else {
//                                        Runnable runnable = new Runnable() {
//                                            @Override
//                                            public void run() {
//
//                                                new Handler().postDelayed(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        //when data array is empty
//                                                        getCheckAdviceAnsware(context);
//
//                                                    }
//                                                }, 60000);
//                                            }
//                                        };
//                                        runnable.run();
                                    }
                                } else {

                                }

                            } catch (JSONException e) {
                                //progressDialog.hide();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //when data array is empty
//                                        getCheckAdviceAnsware();
//
//                                    }
//                                }, 60000);
//                            }
//                        };
//                        runnable.run();
                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {


                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> mapheader = new HashMap<String, String>();


                    mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                    mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                    mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                    mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                    mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                    mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                    mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                    mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                    mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                    mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                    //mapheader.put(UtillClasses.x_user_language,"english");
                    //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                    mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                    return mapheader;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> mapFooder = new HashMap<>();

                    mapFooder.put(UtillClasses.IdList, question_list);
                    // mapFooder.put(UtillClasses.lastCheck,"2016-01-01 12:00:00");


                    return mapFooder;
                }
            };
            //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(15 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
            stringRequest.setShouldCache(false);
            AppController.getmInstance().addToRequestQueue(stringRequest);
        }

    }

    public synchronized void getCheckAdviceQueNumber(final Context context) {

        final String mURL = UtillClasses.BaseUrl + "check_advices_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        setlastcheck();
                        Log.e(TAG, "check advice number response is " + response);


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {

                                if (jsonObject.getInt("data") > 0) {
                                    //get advice list
                                    Log.e(TAG, "data is more then expected is :" + jsonObject.getInt("data"));
                                    showNotification(context, String.format(context.getResources().getString(R.string.you_have_new_advice), jsonObject.getString("data")), context.getResources().getString(R.string.tenderlive_notification));
                                    getAdviceList(context);
                                } else {


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error advice new count " + error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice count header is " + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();

                //String lastcheck = sessionManager.getAdviceLastCheck();
                String lastcheck = sessionManager.getAdviceLastCheckGMT();
                //String lastcheck = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                // String lastcheck ="2017-01-01 03:19:14";
                Log.e(TAG, "last check for the advice is " + lastcheck);
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public synchronized void getAdviceList(final Context context) {
        final String mURL = UtillClasses.BaseUrl + "get_advices_list";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("statusCode") == 200) {
                        //setlastcheck();
                        ArrayList<ModelAdviceList> modelAdviceListArrayList = new ArrayList<>();

                        sessionManager.setAdviceFirstTime(false);
                        if (!jsonObject.isNull("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            int size = jsonArray.length();
                            Log.e(TAG, "size of advice number is :" + size);
                            ArrayList<ModelAdviceList> modelAdviceListArrayList1 = new ArrayList<>();
                            modelAdviceListArrayList1.clear();
                            for (int i = 0; i < size; i++) {
                                JSONObject jsondata = jsonArray.getJSONObject(i);
                                ModelAdviceList modelAdviceList = new ModelAdviceList();
                                modelAdviceList.setTypeAdvice(jsondata.optString("typeAdvice"));
                                //  Log.e(TAG,"type of advice recived is "+jsondata.optString("typeAdvice"));
                                modelAdviceList.setIdAdvice(jsondata.optString("idAdvice"));
                                modelAdviceList.setIdTheme(jsondata.optInt("idTheme"));
                                modelAdviceList.setNameTheme(jsondata.optString("nameTheme"));
                                modelAdviceList.setCountryFlagAdvice(jsondata.optString("countryFlagAdvice"));
                                modelAdviceList.setSourceAdvice(jsondata.optString("sourceAdvice"));
                                modelAdviceList.setTitleAdvice(jsondata.optString("titleAdvice"));
                                modelAdviceList.setDocumentAdvice(jsondata.optString("documentAdvice"));
                                modelAdviceList.setUpdatedAdvice(jsondata.optString("updatedAdvice"));
                                modelAdviceList.setPublicatedAdvice(jsondata.optString("publicatedAdvice"));
                                modelAdviceList.setHaveRateAdvice(jsondata.optInt("haveRateAdvice"));
                                modelAdviceList.setStatusAdvice(jsondata.optString("statusAdvice"));
                                modelAdviceList.setCountryAdvice(jsondata.optString("countryAdvice"));
                                //add model in the arraylist or update the list

                                // modelAdviceListList.add(modelAdviceList);
                                if (modelAdviceList.getTypeAdvice().equals("user")) {
                                    Log.e(TAG, "user data respnce is " + jsondata.toString());
                                    // modelAdviceList.setStatusAdvice("2");
                                    // dataBaseHalper.InsertAdvicdListUser(modelAdviceList);
                                    modelAdviceListArrayList.add(modelAdviceList);
                                    //showNotification(context,modelAdviceList.getTitleAdvice(),context.getResources().getString(R.string.new_quest_arrive));
                                }


                                modelAdviceListArrayList1.add(modelAdviceList);
                                // setDatafromDatabase("user",-1);
                                if (i + 1 == size) {
                                    dataBaseHalper.InsertAdvicdList(modelAdviceListArrayList1);
                                    EventBus.getDefault().post(modelAdviceListArrayList);
                                    Log.e(TAG, "post advice list from advice list ");
                                }
                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " volley responce error  reason is " + error.getMessage());
                //getAdviceList();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice list header data" + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();


                // String lastcheck = sessionManager.getAdviceLastCheck();
                String lastcheck = sessionManager.getAdviceLastCheckGMT();
                //String lastcheck ="2017-01-01 03:19:14";
                Log.e(TAG, "advice list parameter data " + lastcheck);
                if (sessionManager.getAdviceFirstTime()) { // remove due to check and remove on destroy condition
                    //  mapFooder.put(UtillClasses.lastCheck, "0000-00-00 00:00:00");
                } else if (!dataBaseHalper.getLastAdviceAssignedDate().equals("0000-00-00 00:00:00")) {
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());
                }
                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void setlastcheck() {
        String simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

        SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

        String GMtDate = simpleDateFormatGMT.format(new Date());
        sessionManager.setAdviceLastCheck(simpleDateFormat, GMtDate);


    }

    public void showNotification(Context mContext, String message, String title) {


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Channel", "tender", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }

        }


        Intent intent = new Intent(mContext, Splash.class);
        intent.putExtra(GlobalDataAcess.Notification_type, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.app_icon);

        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext, "Channel")
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle())
                        .setAutoCancel(true)
                        .setContentText(message)
                        .setSmallIcon(R.drawable.status_bar_icon)
                        .setLargeIcon(icon)
                        .setContentIntent(pendingIntent);

        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if (sessionManager.GetNotiSound()) {
            mBuilder.setSound(alarmsound);
        }
        int m = (int) ((new Date().getTime()));
        Log.e(TAG, "Notification from id advice answer thorugh last time check ");
        if (sessionManager.GetNotiEnableTimeInterval()) {
            Log.e(TAG, "notification enable ");
            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, mBuilder.build());
        } else {

            Log.e(TAG, "notification disable ");
            SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");

            String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
            String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();

            String current_session = time_format.format(new Date());

                                    /*Date startTime = time_format.parse("23:00");
                                    Date stopTime =  time_format.parse("07:00");*/

            //   Date startTime = time_format.parse(start_time_sesssion);
            //  Date stopTime =  time_format.parse(stop_time_sesssion);


            Date time = new Date();

            try {
                if (GlobalDataAcess.isTimeBetweenTwoTime(start_time_sesssion + ":00", stop_time_sesssion + ":00", current_session)) {
                    System.out.println("alarm enable");
                    Log.e(TAG, "alarm is enable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                } else {
                    Log.e(TAG, "alarm is disable with " + start_time_sesssion + ":00" + stop_time_sesssion + ":00" + current_session);
                    // int m =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                    NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(0, mBuilder.build());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

}
