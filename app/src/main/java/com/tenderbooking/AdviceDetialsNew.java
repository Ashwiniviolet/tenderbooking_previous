package com.tenderbooking;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.HelperClasses.CenteredToolbar;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.HelperClasses.ZoomLinearLayout;
import com.tenderbooking.MainTab.Fragment_advice;
import com.tenderbooking.Model.ModelAdviceDetials;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by laxmikant bolya on 05-04-2017.
 */

public class AdviceDetialsNew extends AppCompatActivity {


    TextView date;
    public static final String TAG = AdviceDetialsNew.class.getSimpleName();
    public static AlertDialog mAlertDialog;
    public final int rating_type_Yes = 1;
    public final int rating_type_no = 2;
    public final int rating_type_no_with_msg = 3;
    SessionManager sessionManager;
    ModelAdviceDetials modelAdviceDetials;
    Map<String, String> deviceinfoMap;
    DataBaseHalper dataBaseHalper;
    //ProgressDialog mProgressDialog;
    LinearLayout progress_ll;
    File file;
    LinearLayout review_layout;
    AlertDialog.Builder builder;
    String typeAdvice, idAdvice, documentName, mytitle;
    private CenteredToolbar mToolbar;
    private TextView advice_detail_quesAdv, advice_detail_textAdv, advice_detail_DocText, advice_answer_date, advice_question, advice_question_date;
    private Button advice_detail_helpyesBtn, advice_detail_helpNoBtn;
    private int haveRating, position, adviceStatus;
    private ZoomLinearLayout zoomLinearLayout;
    //Firebase
    private FirebaseAnalytics firebaseAnalytics;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(AdviceDetialsNew.this);
        //today
        invalidateOptionsMenu();
        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        String lang = sessionManager.getAppLangName();
        UtillClasses.langChange(lang, this);
        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small_Noactionbar);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium_Noactionbar);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large_Noactionbar);
                break;
        }

        setContentView(R.layout.advice_details_newlay);
        Initialize();


        //Firebase ANalytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setUserId(sessionManager.GetUserEmail());
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setSessionTimeoutDuration(20000);
        firebaseAnalytics.setCurrentScreen(this, "Advice details", "Advice Details");


        progress_ll = (LinearLayout) findViewById(R.id.progress_ll);
//        mProgressDialog = new ProgressDialog(AdviceDetialsNew.this);
//
//        mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
//        mProgressDialog.setCancelable(false);
        //sessionManager = new SessionManager(AdviceDetialsNew.this);
        deviceinfoMap = sessionManager.GetDeviceInfo();
        modelAdviceDetials = new ModelAdviceDetials();
        dataBaseHalper = new DataBaseHalper(AdviceDetialsNew.this);

        String title_String = sessionManager.GetAdviceToolbarTitle();

        // mToolbar.setTitle(title_String);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title_String);
        mToolbar.setTitleTextColor(ContextCompat.getColor(AdviceDetialsNew.this, R.color.white));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        typeAdvice = intent.getStringExtra(GlobalDataAcess.Ask_advice_typeAdvice);
        idAdvice = intent.getStringExtra(GlobalDataAcess.Ask_advice_idAdvice);
        documentName = intent.getStringExtra(GlobalDataAcess.Ask_advice_documentName);
        mytitle = intent.getStringExtra(GlobalDataAcess.Ask_advice_title);
        haveRating = intent.getIntExtra(GlobalDataAcess.Ask_advice_haveRated, 0);
        position = intent.getIntExtra(GlobalDataAcess.Ask_advice_position, 0);
        adviceStatus = Integer.parseInt(intent.getStringExtra(GlobalDataAcess.Ask_advice_status));
        // final int DocRating  = intent.getIntExtra(GlobalDataAcess.Ask_advice_haveRated,0);
        //final int DocRating  = dataBaseHalper.GetAdvRating(idAdvice);
        // int ratingtype  = dataBaseHalper.GetAdvRating(idAdvice);


        if (documentName != null && documentName.trim().length() > 0) {
            advice_detail_DocText.setText(documentName);
            advice_detail_DocText.setVisibility(View.VISIBLE);
        } else {
            advice_detail_DocText.setVisibility(View.INVISIBLE);
        }


        /*if (typeAdvice.equals("user") && DocRating ==0){
            review_layout.setVisibility(View.VISIBLE);
        }else{
            review_layout.setVisibility(View.GONE);
        }*/


        advice_detail_helpyesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mProgressDialog.show();
                progress_ll.setVisibility(View.VISIBLE);
                SendRating(typeAdvice, idAdvice, "1", "", rating_type_Yes);

            }
        });


        advice_detail_helpNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingMessaDialog();
            }
        });


        if (idAdvice != null)
            if (dataBaseHalper.checkidAdviceAval(idAdvice, typeAdvice)) {
                modelAdviceDetials = dataBaseHalper.getAdviceDetails(idAdvice, typeAdvice);
                advice_detail_quesAdv.setText(modelAdviceDetials.getQuestionAdvice() + "");

                //advice title
                if (modelAdviceDetials.getQuestionAdvice().toString().trim().equals("")) {
                    advice_detail_quesAdv.setText(mytitle + "");
                    //Log.e(TAG,"value of title is  when question is not come from the server  "+mytitle);
                } else {
                    advice_detail_quesAdv.setText(modelAdviceDetials.getQuestionAdvice() + "");
                    //Log.e(TAG,"value of title is  when question is come from the server  "+mytitle);
                }

                if (modelAdviceDetials.getTypeAdvice().equals("user")) {

                    advice_question.setVisibility(View.VISIBLE);
                    advice_question_date.setVisibility(View.VISIBLE);
                    advice_answer_date.setText(GlobalDataAcess.ConvertedDate(modelAdviceDetials.getUpdateAdvice()));
                    advice_question_date.setText(GlobalDataAcess.ConvertedDate(modelAdviceDetials.getPublicatedAdvice()));
                    advice_question.setText(modelAdviceDetials.getTitleAdvice());

                    if (modelAdviceDetials.getTextAdvice().length() != 0)
                        advice_answer_date.setVisibility(View.VISIBLE);
                } else {
                    advice_answer_date.setVisibility(View.GONE);
                    advice_question.setVisibility(View.GONE);
                    advice_question_date.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    advice_detail_textAdv.setText(Html.fromHtml(modelAdviceDetials.getTextAdvice(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    advice_detail_textAdv.setText(Html.fromHtml(modelAdviceDetials.getTextAdvice()));
                }

                //show the raing dialog
                Log.e(TAG, "rating from local database " + modelAdviceDetials.getStatusAdvice() + " text advice is " + modelAdviceDetials.getTextAdvice());
                if (modelAdviceDetials.getTextAdvice().length() > 0) {
                    getShowRatingDialog(Integer.parseInt(modelAdviceDetials.getStatusAdvice()), modelAdviceDetials.getTextAdvice());
                    Log.e(TAG, "answare is not blank ");

                } else {
                    Log.e(TAG, "answare is blank ");

                    if (GlobalDataAcess.hasConnection(AdviceDetialsNew.this)) {
                        getAdviceDetails(typeAdvice, idAdvice);
                    } else {
                        GlobalDataAcess.apiDialog(AdviceDetialsNew.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                    }
                }
                //Firebase
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                bundle.putString(GlobalDataAcess.Class_name, "Advice Details");
                bundle.putString(GlobalDataAcess.Tender_id, modelAdviceDetials.getIdTender());
                bundle.putString(GlobalDataAcess.Advice_id, modelAdviceDetials.getIdAdvice());
                bundle.putString(GlobalDataAcess.Advice_title, modelAdviceDetials.getTitleAdvice());
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                //  advice_detail_textAdv.setText(modelAdviceDetials.getTextAdvice());

            } else {
                if (GlobalDataAcess.hasConnection(AdviceDetialsNew.this)) {
                    getAdviceDetails(typeAdvice, idAdvice);
                } else {
                    GlobalDataAcess.apiDialog(AdviceDetialsNew.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                }
            }

        advice_detail_DocText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (documentName != null) {

//                    mProgressDialog.show();
                    progress_ll.setVisibility(View.VISIBLE);

                    String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                    final String filename = documentName;
                    file = new File(path, filename);

                    if (file.exists()) {

                        // Toast.makeText(AdviceDetialsNew.this, "File is already exist", Toast.LENGTH_SHORT).show();

//                        mProgressDialog.hide();
                        progress_ll.setVisibility(View.VISIBLE);
                        Snackbar snackbar = Snackbar
                                .make(advice_detail_DocText, getResources().getString(R.string.file_download_sucess), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.open), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        try {
                                            GlobalDataAcess.openFile(AdviceDetialsNew.this, file);
                                        } catch (IOException e) {
                                            Toast.makeText(AdviceDetialsNew.this, getResources().getString(R.string.no_handler_), Toast.LENGTH_LONG).show();

                                            e.printStackTrace();
                                        }
                                        //
                                        /*Intent intent = new Intent(Intent.ACTION_VIEW);
                                        Uri uri = Uri.parse( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                                        intent.setDataAndType(uri, "pdf/docx/doc");
                                        startActivity(Intent.createChooser(intent, "Open File"));*/

                                        /*MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                        Intent intent =new Intent(Intent.ACTION_VIEW);
                                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(filename)).substring(1);
                                        //intent.setDataAndType(Uri.fromFile(file),mimeType);
                                        intent.setType("file*//*");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        Intent intentChooser = Intent.createChooser(intent,"Open file");
                                        try {
                                            startActivity(intentChooser);
                                        } catch (Exception e) {
                                            Toast.makeText(AdviceDetialsNew.this, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                                            e.printStackTrace();*/

                                        //}
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();

                    } else {

                        if (sessionManager.GetIsOnlyWifi()) {
                            if (GlobalDataAcess.IsWifiEnable(AdviceDetialsNew.this)) {

                                if (GlobalDataAcess.hasConnection(AdviceDetialsNew.this)) {
                                    DownloadDocumentFile(typeAdvice, idAdvice, documentName);
                                } else {
                                    GlobalDataAcess.apiDialog(AdviceDetialsNew.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));

                                }

                            } else {
                                Toast.makeText(AdviceDetialsNew.this, getResources().getString(R.string.please_conn_wifi), Toast.LENGTH_SHORT).show();

                            }
                        } else {


                            if (GlobalDataAcess.hasConnection(AdviceDetialsNew.this)) {
                                DownloadDocumentFile(typeAdvice, idAdvice, documentName);
                            } else {
                                GlobalDataAcess.apiDialog(AdviceDetialsNew.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));

                            }

                        }
                    }

                    //  DownloadDocumentFile(typeAdvice,idAdvice,documentName);
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (sessionManager.getUserStatucCode() == 1 || sessionManager.getUserStatucCode() == 5 || sessionManager.getUserStatucCode() == 6) {
            GetCheckAdviceQuestionNumber();

        } else {


            if (advice_detail_textAdv != null)
                Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

//        MenuInflater inflater = getMenuInflater();
//
//        inflater.inflate(R.menu.home, menu);

        //today
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem action_search = menu.findItem(R.id.action_search);
        action_search.setVisible(false);


        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void Initialize() {
        date = (TextView) findViewById(R.id.date);
        mToolbar = (CenteredToolbar) findViewById(R.id.advice_detail_toolbar);
        advice_detail_quesAdv = (TextView) findViewById(R.id.advice_detail_quesAdv);
        advice_detail_DocText = (TextView) findViewById(R.id.advice_detail_DocText);
        advice_detail_textAdv = (TextView) findViewById(R.id.advice_detail_textAdv);
        advice_question_date = (TextView) findViewById(R.id.advice_question_date);
        advice_question = (TextView) findViewById(R.id.advice_question);
        advice_answer_date = (TextView) findViewById(R.id.advice_answer_date);
        advice_detail_helpyesBtn = (Button) findViewById(R.id.advice_detail_helpyesBtn);
        advice_detail_helpNoBtn = (Button) findViewById(R.id.advice_detail_helpNoBtn);
        review_layout = (LinearLayout) findViewById(R.id.review_layout);


        zoomLinearLayout = (ZoomLinearLayout) findViewById(R.id.zoom_layout);
        zoomLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomLinearLayout.init(AdviceDetialsNew.this);
                return false;
            }
        });
    }


    public void GetCheckAdviceQuestionNumber() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "check_advice_question_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {

                                    GlobalDataAcess globalDataAcess = new GlobalDataAcess();
                                    globalDataAcess.getAskQuesDialog(AdviceDetialsNew.this, "0", "0", 0);

                                } else {
                                    String message = jsonObject.optString("message");
                                    if (advice_detail_textAdv != null) {
                                        Snackbar.make(advice_detail_quesAdv, message, Snackbar.LENGTH_LONG).show();
                                    }
                                }
                            } else {

                                if (advice_detail_textAdv != null) {
                                    Snackbar.make(advice_detail_quesAdv, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(advice_detail_quesAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {
                    //
                } else if (error instanceof ServerError) {
                    //
                    Snackbar snackbar = Snackbar
                            .make(advice_detail_quesAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {
                    //
                    Snackbar snackbar = Snackbar
                            .make(advice_detail_quesAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });
// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {
                    //
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public void getAdviceDetails(final String typeAdvices, final String idAdvices) {


//        mProgressDialog.show();
        progress_ll.setVisibility(View.VISIBLE);
        final String mURL = UtillClasses.BaseUrl + "get_advice_details";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {


//                                    mProgressDialog.hide();
                                    progress_ll.setVisibility(View.GONE);
                                    JSONObject jsondata = jsonObject.getJSONObject("data");

                                    Log.e(TAG, "advice data is " + jsondata.toString());
                                    modelAdviceDetials.setIdAdvice(jsondata.optString("idAdvice"));
                                    modelAdviceDetials.setIdTender(jsondata.optString("idTender"));
                                    modelAdviceDetials.setTitleAdvice(jsondata.optString("titleAdvice"));
                                    modelAdviceDetials.setQuestionAdvice(jsondata.optString("questionAdvice"));
                                    modelAdviceDetials.setTextAdvice(jsondata.optString("textAdvice"));
                                    modelAdviceDetials.setStatusAdvice(jsondata.optString("statusAdvice"));
                                    modelAdviceDetials.setUpdateAdvice(jsondata.optString("updatedAdvice"));
                                    modelAdviceDetials.setPublicatedAdvice(jsondata.optString("publicatedAdvice"));
                                    modelAdviceDetials.setTypeAdvice(typeAdvices);
                                    date.setText(GlobalDataAcess.ConvertedDate(jsondata.optString("publicatedAdvice")));
                                    dataBaseHalper.InsertAdviceDetails(modelAdviceDetials);
                                    dataBaseHalper.updateStatusAdvice(modelAdviceDetials.getIdAdvice(), modelAdviceDetials.getTypeAdvice(), modelAdviceDetials.getStatusAdvice());

                                    if (jsondata.optString("typeAdvice").equals("clipping")) {
                                        date.setVisibility(View.VISIBLE);
                                    } else {
                                        date.setVisibility(View.GONE);
                                    }


                                    if (jsondata.optInt("statusAdvice") != adviceStatus) {
                                        Fragment_advice.modelAdviceListList.get(position).setStatusAdvice(jsondata.optString("statusAdvice"));
                                        Log.e(TAG, "advice status update of advice list");
                                    }


                                    if (modelAdviceDetials.getTypeAdvice().equals("user")) {

                                        advice_question.setVisibility(View.VISIBLE);
                                        advice_question_date.setVisibility(View.VISIBLE);
                                        advice_answer_date.setText(GlobalDataAcess.ConvertedDate(modelAdviceDetials.getUpdateAdvice()));
                                        advice_question_date.setText(GlobalDataAcess.ConvertedDate(modelAdviceDetials.getPublicatedAdvice()));
                                        advice_question.setText(modelAdviceDetials.getTitleAdvice());

                                        if (modelAdviceDetials.getTextAdvice().length() != 0)
                                            advice_answer_date.setVisibility(View.VISIBLE);
                                    } else {
                                        advice_answer_date.setVisibility(View.GONE);
                                        advice_question.setVisibility(View.GONE);
                                        advice_question_date.setVisibility(View.GONE);
                                    }

                                    //advice title
                                    if (modelAdviceDetials.getQuestionAdvice().toString().trim().equals("")) {
                                        advice_detail_quesAdv.setText(mytitle + "");
                                        Log.e(TAG, "value of title is  when question is not come from the server  " + mytitle);
                                    } else {
                                        advice_detail_quesAdv.setText(modelAdviceDetials.getQuestionAdvice() + "");
                                        Log.e(TAG, "value of title is  when question is come from the server  " + mytitle);
                                    }
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        advice_detail_textAdv.setText(Html.fromHtml(modelAdviceDetials.getTextAdvice(), Html.FROM_HTML_MODE_COMPACT));
                                    } else {
                                        advice_detail_textAdv.setText(Html.fromHtml(modelAdviceDetials.getTextAdvice()));
                                    }

                                    //show review dialog
                                    Log.e(TAG, "rating from is server status is " + jsondata.optInt("statusAdvice") + " text advice is " + jsondata.optInt("textAdvice"));
                                    //if(! modelAdviceDetials.getTextAdvice().equals(""))
                                    if (modelAdviceDetials.getTextAdvice().length() > 0) {
                                        getShowRatingDialog(jsondata.optInt("statusAdvice"), jsondata.optString("textAdvice"));
                                        Log.e(TAG, "answare is not blank ");

                                    } else {

                                        Log.e(TAG, "answare is blank ");

                                    }

                                    //Firebase
                                    Bundle bundle = new Bundle();
                                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                                    bundle.putString(GlobalDataAcess.Class_name, "Advice Details");
                                    bundle.putString(GlobalDataAcess.Tender_id, jsondata.optString("idTender"));
                                    bundle.putString(GlobalDataAcess.Advice_id, jsondata.optString("idAdvice"));
                                    bundle.putString(GlobalDataAcess.Advice_title, jsondata.optString("titleAdvice"));
                                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                                    //  advice_detail_textAdv.setText(modelAdviceDetials.getTextAdvice());
                                }
                            } else if (jsonObject.getInt("statusCode") == 401) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AdviceDetialsNew.this, R.style.MyDialogTheme);
                                builder.setTitle(getResources().getString(R.string.warning));

                                builder.setMessage(getResources().getString(R.string.criden_open));
                                builder.setPositiveButton(getResources().getString(R.string.login), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(AdviceDetialsNew.this, Login.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        //sessionManager.GetLogout(); // this removed becouse if user login from another device not need to delete all keys.
                                        sessionManager.clearUsingKey();
                                        startActivity(intent);

                                    }
                                });
                                builder.setCancelable(false);
                                builder.show();
                            } else {
//                                mProgressDialog.hide();
                                progress_ll.setVisibility(View.GONE);
                                if (advice_detail_textAdv != null) {
                                    Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                            bundle.putString(GlobalDataAcess.Class_name, "Advice Details");
                            //  bundle.putString(GlobalDataAcess.Tender_id,jsondata.optString("idTender"));
                            bundle.putString(GlobalDataAcess.Advice_id, idAdvices);
                            bundle.putString(GlobalDataAcess.Advice_detail_error, "true");
                            // bundle.putString(GlobalDataAcess.Advice_title,jsondata.optString("titleAdvice"));
                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
//                            mProgressDialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getAdviceDetails(typeAdvices, idAdvices);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<String, String>();
                mapFooder.put(UtillClasses.IdAdvice, idAdvices);
                mapFooder.put(UtillClasses.TypeAdvice, typeAdvices);
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));

        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public void DownloadDocumentFile(final String typeAdvices, final String idAdvices, final String DocName) {
        final String mURL = UtillClasses.BaseUrl + "get_advice_document";

//        mProgressDialog.show();
        progress_ll.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e(TAG, "downloaded file data " + response);


                         /*   String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();


                            File file = new File(path, "document.pdf");

                            if(file.exists()){
                                Toast.makeText(getActivity(), "File is already downloaded", Toast.LENGTH_SHORT).show();
                            }
                            else{*/

                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(response.getBytes());
                            fos.flush();
                            fos.close();

                            //Snackbar.make(advice_detail_textAdv, "File download successfully please check in download folder", BaseTransientBottomBar.LENGTH_LONG).show();
//                            mProgressDialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(advice_detail_DocText, getResources().getString(R.string.file_download_sucess), Snackbar.LENGTH_LONG)
                                    .setAction(getResources().getString(R.string.open), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //
                                           /* Intent intent = new Intent(Intent.ACTION_VIEW);
                                            Uri uri = Uri.parse( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                                            intent.setDataAndType(uri, "pdf/docx/doc");

                                            startActivity(Intent.createChooser(intent, "Open File"));*/
                                            /*MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                            Intent intent =new Intent(Intent.ACTION_VIEW);
                                            String mimeType = myMime.getMimeTypeFromExtension(fileExt(DocName)).substring(1);
                                            intent.setDataAndType(Uri.fromFile(file),mimeType);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                            try {
                                                startActivity(intent);
                                            } catch (Exception e) {
                                                Toast.makeText(AdviceDetialsNew.this, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            }*/

                                            try {
                                                GlobalDataAcess.openFile(AdviceDetialsNew.this, file);
                                            } catch (IOException e) {
                                                Toast.makeText(AdviceDetialsNew.this, getResources().getString(R.string.no_handler_), Toast.LENGTH_LONG).show();

                                                e.printStackTrace();
                                            }
                                        }
                                    });

// Changing message text color
                            snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                            View sbView = snackbar.getView();
                            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();

                        } catch (IOException e) {
                            e.printStackTrace();
//                            mProgressDialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            if (advice_detail_textAdv != null) {
                                Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DownloadDocumentFile(typeAdvices, idAdvices, DocName);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<String, String>();
                mapFooder.put(UtillClasses.IdAdvice, idAdvices);
                mapFooder.put(UtillClasses.TypeAdvice, typeAdvices);
                mapFooder.put("json", "false");
                Log.e(TAG, "body perameter is " + mapFooder.toString());
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));

        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public void SendRating(final String typeAdvices, final String idAdvices, final String statusRating, final String messageRating, final int rating_type) {


        String mURL = UtillClasses.BaseUrl + "send_advice_rating";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL, new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
//                                    mProgressDialog.hide();
                                    progress_ll.setVisibility(View.GONE);
                                    if (advice_detail_textAdv != null) {
                                        Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.Success_send_rating), Snackbar.LENGTH_SHORT).show();
                                    }
                                    review_layout.setVisibility(View.GONE);
                                    dataBaseHalper.InsAdvRatingSent(idAdvices, typeAdvices);
                                    //change the advice model have rating data
                                    Fragment_advice.modelAdviceListList.get(position).setHaveRateAdvice(1);
                                    switch (rating_type) {
                                        case rating_type_Yes:
                                            break;
                                        case rating_type_no:
                                            mAlertDialog.hide();
                                            break;
                                        case rating_type_no_with_msg:
                                            mAlertDialog.hide();
                                            break;
                                    }
                                    // mAlertDialog.hide();
                                } else
//                                    mProgressDialog.hide();
                                    progress_ll.setVisibility(View.GONE);
                            } else {
//                                mProgressDialog.hide();
                                progress_ll.setVisibility(View.GONE);
                                if (advice_detail_textAdv != null) {
                                    Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//mProgressDialog.hide();
                progress_ll.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NetworkError) {

                }
                if (advice_detail_textAdv != null) {
                    Snackbar.make(advice_detail_textAdv, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<String, String>();
                mapFooder.put(UtillClasses.IdAdvice, idAdvices);
                mapFooder.put(UtillClasses.TypeAdvice, typeAdvices);
                mapFooder.put(UtillClasses.statusRating, statusRating);
                mapFooder.put(UtillClasses.messageRating, messageRating);

                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void showRatingMessaDialog() {

        builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.send_rating, null, false);

        Button checheDia_canBtn = (Button) view.findViewById(R.id.sendRating_canBtn);
        Button checheDia_okBtn = (Button) view.findViewById(R.id.sendRating_okBtn);
        final EditText sendRat_editext = (EditText) view.findViewById(R.id.send_rating_edittext);
        // sendRat_editext.setFocusableInTouchMode(true);
        sendRat_editext.setCursorVisible(true);

        mAlertDialog.setView(view);


        checheDia_canBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.cancel();
//                mProgressDialog.show();
                progress_ll.setVisibility(View.GONE);
                GlobalDataAcess.hideSoftKeyboard(AdviceDetialsNew.this);
                mAlertDialog.dismiss();

               /* if (typeAdvice != null && idAdvice != null) {
                    SendRating(typeAdvice, idAdvice, "0", "", rating_type_no);
                }*/

            }
        });

        checheDia_okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GlobalDataAcess.hideSoftKeyboard(AdviceDetialsNew.this);
                if (sendRat_editext.getText().toString().trim().length() == 0) {
                    sendRat_editext.setError(getResources().getString(R.string.please_write_message));
                    sendRat_editext.requestFocus();
                } else {
//                    mProgressDialog.show();
                    progress_ll.setVisibility(View.GONE);
                    GlobalDataAcess.hideSoftKeyboard(AdviceDetialsNew.this);
                    String message = sendRat_editext.getText().toString().trim();
                    SendRating(typeAdvice, idAdvice, "0", message, rating_type_no_with_msg);
                }
            }
        });
        mAlertDialog.show();
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    private void getShowRatingDialog(int DocRating, String text_reply) {
        // final int DocRating  = dataBaseHalper.GetAdvRating(idAdvice);

        /*if (typeAdvice.equals("user") && DocRating ==0){
            review_layout.setVisibility(View.VISIBLE);
        }else{
            review_layout.setVisibility(View.GONE);
        }*/
        Log.e(TAG, "doc rating is " + DocRating);
        // if (typeAdvice.equals("user") && ( (DocRating&4) ==4) ){
        if (typeAdvice.equals("user") && (text_reply.length() > 0) && haveRating == 0) {
            review_layout.setVisibility(View.VISIBLE);
        } else {
            review_layout.setVisibility(View.GONE);
        }

    }


}
