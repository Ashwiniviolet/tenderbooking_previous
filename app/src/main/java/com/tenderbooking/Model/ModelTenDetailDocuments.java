package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 11-03-2017.
 */

public class ModelTenDetailDocuments {
    String idDocuments, langDocuments, nameDocuments, nameEllipsize, flagDocument;

    public String getIdDocuments() {

        return idDocuments;
    }

    public void setIdDocuments(String idDocuments) {
        this.idDocuments = idDocuments;
    }

    public String getLangDocuments() {
        return langDocuments;
    }

    public void setLangDocuments(String langDocuments) {
        this.langDocuments = langDocuments;
    }

    public String getNameDocuments() {
        return nameDocuments;
    }

    public void setNameDocuments(String nameDocuments) {
        this.nameDocuments = nameDocuments;
    }

    public String getNameEllipsize() {
        return nameEllipsize;
    }

    public void setNameEllipsize(String nameEllipsize) {
        this.nameEllipsize = nameEllipsize;
    }

    public String getFlagDocument() {
        return flagDocument;
    }

    public void setFlagDocument(String flagDocument) {
        this.flagDocument = flagDocument;
    }
}
