package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class ModelAdviceDetials {
    String idAdvice;
    String idTender;
    String titleAdvice;
    String questionAdvice;
    String textAdvice;
    String statusAdvice;
    String typeAdvice;
    String updateAdvice, publicatedAdvice;

    public String getIdAdvice() {
        return idAdvice;
    }

    public void setIdAdvice(String idAdvice) {
        this.idAdvice = idAdvice;
    }

    public String getIdTender() {
        return idTender;
    }

    public void setIdTender(String idTender) {
        this.idTender = idTender;
    }

    public String getTitleAdvice() {
        return titleAdvice;
    }

    public void setTitleAdvice(String titleAdvice) {
        this.titleAdvice = titleAdvice;
    }

    public String getQuestionAdvice() {
        return questionAdvice;
    }

    public void setQuestionAdvice(String questionAdvice) {
        this.questionAdvice = questionAdvice;
    }

    public String getTextAdvice() {
        return textAdvice;
    }

    public void setTextAdvice(String textAdvice) {
        this.textAdvice = textAdvice;
    }

    public String getStatusAdvice() {
        return statusAdvice;
    }

    public void setStatusAdvice(String statusAdvice) {
        this.statusAdvice = statusAdvice;
    }

    public String getTypeAdvice() {
        return typeAdvice;
    }

    public void setTypeAdvice(String typeAdvice) {
        this.typeAdvice = typeAdvice;
    }

    public String getUpdateAdvice() {
        return updateAdvice;
    }

    public void setUpdateAdvice(String updateAdvice) {
        this.updateAdvice = updateAdvice;
    }

    public String getPublicatedAdvice() {
        return publicatedAdvice;
    }

    public void setPublicatedAdvice(String publicatedAdvice) {
        this.publicatedAdvice = publicatedAdvice;
    }
}
