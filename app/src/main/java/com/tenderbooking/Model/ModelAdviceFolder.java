package com.tenderbooking.Model;

import java.util.List;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class ModelAdviceFolder {
    List<ModelAdviceFolTheme> themeArray;
    String typeAdvice, idCategory, nameCategory;

    public String getTypeAdvice() {
        return typeAdvice;
    }

    public void setTypeAdvice(String typeAdvice) {
        this.typeAdvice = typeAdvice;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public List<ModelAdviceFolTheme> getThemeArray() {
        return themeArray;
    }

    public void setThemeArray(List<ModelAdviceFolTheme> themeArray) {
        this.themeArray = themeArray;
    }

}
