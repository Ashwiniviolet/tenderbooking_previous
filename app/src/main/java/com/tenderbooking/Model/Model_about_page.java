package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 29-03-2017.
 */

public class Model_about_page {

    String titlepage, textpage;

    public String getTitlepage() {
        return titlepage;
    }

    public void setTitlepage(String titlepage) {
        this.titlepage = titlepage;
    }

    public String getTextpage() {
        return textpage;
    }

    public void setTextpage(String textpage) {
        this.textpage = textpage;
    }
}
