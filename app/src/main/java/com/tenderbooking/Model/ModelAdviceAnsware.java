package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 07-04-2017.
 */

public class ModelAdviceAnsware {
    String idQuestion;
    String titleQuestion;
    String detialsAnsware;

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getTitleQuestion() {
        return titleQuestion;
    }

    public void setTitleQuestion(String titleQuestion) {
        this.titleQuestion = titleQuestion;
    }

    public String getDetialsAnsware() {
        return detialsAnsware;
    }

    public void setDetialsAnsware(String detialsAnsware) {
        this.detialsAnsware = detialsAnsware;
    }
}
