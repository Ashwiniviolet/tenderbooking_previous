package com.tenderbooking.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by laxmikant bolya on 31-03-2017.
 */

public class ModelTenderResRetrofit {
    @SerializedName("statusCode")
    int statusCode;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    List<ModelTenderListRetrofit> modelTenderListRetrofitList;

}
