package com.tenderbooking.Model;

/**
 * Created by Abhishek jain on 08-03-2017.
 */

public class ModelTenDetalDetails {
    String languageTender;
    String titleTender;
    String subtitleTender;
    String descriptionTender;
    String flagTender;
    String languageTenderName;

    public String getLanguageTender() {
        return languageTender;
    }

    public void setLanguageTender(String languageTender) {
        this.languageTender = languageTender;
    }

    public String getLanguageTenderName() {
        return languageTenderName;
    }

    public void setLanguageTenderName(String languageTenderName) {
        this.languageTenderName = languageTenderName;
    }

    public String getTitleTender() {
        return titleTender;
    }

    public void setTitleTender(String titleTender) {
        this.titleTender = titleTender;
    }

    public String getSubtitleTender() {
        return subtitleTender;
    }

    public void setSubtitleTender(String subtitleTender) {
        this.subtitleTender = subtitleTender;
    }

    public String getDescriptionTender() {
        return descriptionTender;
    }

    public void setDescriptionTender(String descriptionTender) {
        this.descriptionTender = descriptionTender;
    }

    public String getFlagTender() {
        return flagTender;
    }

    public void setFlagTender(String flagTender) {
        this.flagTender = flagTender;
    }
}
