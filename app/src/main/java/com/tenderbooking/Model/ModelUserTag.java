package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 16-03-2017.
 */

public class ModelUserTag {
    String tagIndex, tagText, colorId, colorCSS, colorName;
    boolean istagset;

    public String getTagIndex() {
        return tagIndex;
    }

    public void setTagIndex(String tagIndex) {
        this.tagIndex = tagIndex;
    }

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorCSS() {
        return colorCSS;
    }

    public void setColorCSS(String colorCSS) {
        this.colorCSS = colorCSS;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public boolean istagset() {
        return istagset;
    }

    public void setIstagset(boolean istagset) {
        this.istagset = istagset;
    }
}
