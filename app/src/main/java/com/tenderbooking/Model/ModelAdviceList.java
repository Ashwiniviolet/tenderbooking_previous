package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class ModelAdviceList {
    int haveRateAdvice, idTheme;
    String countryFlagAdvice, documentAdvice, idAdvice, nameTheme, publicatedAdvice, sourceAdvice,
            statusAdvice, titleAdvice, typeAdvice, updatedAdvice;
    String countryAdvice;

    public int getHaveRateAdvice() {
        return haveRateAdvice;
    }

    public void setHaveRateAdvice(int haveRateAdvice) {
        this.haveRateAdvice = haveRateAdvice;
    }

    public int getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    public String getCountryFlagAdvice() {
        return countryFlagAdvice;
    }

    public void setCountryFlagAdvice(String countryFlagAdvice) {
        this.countryFlagAdvice = countryFlagAdvice;
    }

    public String getDocumentAdvice() {
        return documentAdvice;
    }

    public void setDocumentAdvice(String documentAdvice) {
        this.documentAdvice = documentAdvice;
    }

    public String getIdAdvice() {
        return idAdvice;
    }

    public void setIdAdvice(String idAdvice) {
        this.idAdvice = idAdvice;
    }

    public String getNameTheme() {
        return nameTheme;
    }

    public void setNameTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }

    public String getPublicatedAdvice() {
        return publicatedAdvice;
    }

    public void setPublicatedAdvice(String publicatedAdvice) {
        this.publicatedAdvice = publicatedAdvice;
    }

    public String getSourceAdvice() {
        return sourceAdvice;
    }

    public void setSourceAdvice(String sourceAdvice) {
        this.sourceAdvice = sourceAdvice;
    }

    public String getStatusAdvice() {
        return statusAdvice;
    }

    public void setStatusAdvice(String statusAdvice) {
        this.statusAdvice = statusAdvice;
    }

    public String getTitleAdvice() {
        return titleAdvice;
    }

    public void setTitleAdvice(String titleAdvice) {
        this.titleAdvice = titleAdvice;
    }

    public String getTypeAdvice() {
        return typeAdvice;
    }

    public void setTypeAdvice(String typeAdvice) {
        this.typeAdvice = typeAdvice;
    }

    public String getUpdatedAdvice() {
        return updatedAdvice;
    }

    public void setUpdatedAdvice(String updatedAdvice) {
        this.updatedAdvice = updatedAdvice;
    }

    public String getCountryAdvice() {
        return countryAdvice;
    }

    public void setCountryAdvice(String countryAdvice) {
        this.countryAdvice = countryAdvice;
    }
}
