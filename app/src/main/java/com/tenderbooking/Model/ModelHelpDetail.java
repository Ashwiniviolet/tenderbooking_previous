package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 25-03-2017.
 */

public class ModelHelpDetail {
    int idAnsware;
    String titleAnsware, textAnsware;

    public int getIdAnsware() {
        return idAnsware;
    }

    public void setIdAnsware(int idAnsware) {
        this.idAnsware = idAnsware;
    }

    public String getTitleAnsware() {
        return titleAnsware;
    }

    public void setTitleAnsware(String titleAnsware) {
        this.titleAnsware = titleAnsware;
    }

    public String getTextAnsware() {
        return textAnsware;
    }

    public void setTextAnsware(String textAnsware) {
        this.textAnsware = textAnsware;
    }
}
