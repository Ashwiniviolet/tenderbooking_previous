package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 30-03-2017.
 */

public class ModelTenderListIcons {

    int number, resourcepath;
    String colourcode;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getResourcepath() {
        return resourcepath;
    }

    public void setResourcepath(int resourcepath) {
        this.resourcepath = resourcepath;
    }

    public String getColourcode() {
        return colourcode;
    }

    public void setColourcode(String colourcode) {
        this.colourcode = colourcode;
    }

}
