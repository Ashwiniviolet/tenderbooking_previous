package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 21-04-2017.
 */

public class ModelTag {
    String tagName;
    String tagColId;
    String tagid;
    boolean istenderset;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagid() {
        return tagid;
    }

    public void setTagid(String tagid) {
        this.tagid = tagid;
    }

    public String getTagColId() {
        return tagColId;
    }

    public void setTagColId(String tagColId) {
        this.tagColId = tagColId;
    }

    public boolean istenderset() {
        return istenderset;
    }

    public void setIstenderset(boolean istenderset) {
        this.istenderset = istenderset;
    }
}
