package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 18-04-2017.
 */

public class ModelTenderDetailsDatabase {

    public ModelTenderDetails modelTenderDetails;
    public boolean value;

    public ModelTenderDetails getModelTenderDetails() {
        return modelTenderDetails;
    }

    public void setModelTenderDetails(ModelTenderDetails modelTenderDetails) {
        this.modelTenderDetails = modelTenderDetails;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
