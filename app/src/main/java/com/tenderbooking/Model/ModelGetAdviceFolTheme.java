package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 20-03-2017.
 */

public class ModelGetAdviceFolTheme {
    int idTheme;
    String nameTheme;

    public int getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    public String getNameTheme() {
        return nameTheme;
    }

    public void setNameTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }
}
