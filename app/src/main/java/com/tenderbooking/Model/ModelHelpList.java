package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 23-03-2017.
 */

public class ModelHelpList {
    int idhelp, idTopic;
    String nameTopic, questionHelp, updateHelp;

    public int getIdhelp() {
        return idhelp;
    }

    public void setIdhelp(int idhelp) {
        this.idhelp = idhelp;
    }

    public int getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(int idTopic) {
        this.idTopic = idTopic;
    }

    public String getNameTopic() {
        return nameTopic;
    }

    public void setNameTopic(String nameTopic) {
        this.nameTopic = nameTopic;
    }

    public String getQuestionHelp() {
        return questionHelp;
    }

    public void setQuestionHelp(String questionHelp) {
        this.questionHelp = questionHelp;
    }

    public String getUpdateHelp() {
        return updateHelp;
    }

    public void setUpdateHelp(String updateHelp) {
        this.updateHelp = updateHelp;
    }
}
