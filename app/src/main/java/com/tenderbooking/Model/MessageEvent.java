package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 17-05-2017.
 */

public class MessageEvent {
    private final String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
