package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 04-04-2017.
 */

public class ModelAdviceFolTheme {
    String idTheme, nameTheme;

    public String getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(String idTheme) {
        this.idTheme = idTheme;
    }

    public String getNameTheme() {
        return nameTheme;
    }

    public void setNameTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }
}
