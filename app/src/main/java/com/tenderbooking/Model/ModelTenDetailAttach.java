package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 11-03-2017.
 */

public class ModelTenDetailAttach {
    String idAttachment, languageAttachment, nameAttach, nameElipsize, flagAttachment;
    String docType;

    public String getIdAttachment() {
        return idAttachment;
    }

    public void setIdAttachment(String idAttachment) {
        this.idAttachment = idAttachment;
    }

    public String getLanguageAttachment() {
        return languageAttachment;
    }

    public void setLanguageAttachment(String languageAttachment) {
        this.languageAttachment = languageAttachment;
    }

    public String getNameAttach() {
        return nameAttach;
    }

    public void setNameAttach(String nameAttach) {
        this.nameAttach = nameAttach;
    }

    public String getNameElipsize() {
        return nameElipsize;
    }

    public void setNameElipsize(String nameElipsize) {
        this.nameElipsize = nameElipsize;
    }

    public String getFlagAttachment() {
        return flagAttachment;
    }

    public void setFlagAttachment(String flagAttachment) {
        this.flagAttachment = flagAttachment;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
