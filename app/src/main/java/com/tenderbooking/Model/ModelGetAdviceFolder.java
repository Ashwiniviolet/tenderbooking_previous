package com.tenderbooking.Model;

import java.util.ArrayList;

/**
 * Created by laxmikant bolya on 20-03-2017.
 */

public class ModelGetAdviceFolder {


    String idCategory, nameCategory, typeAdvice;
    ArrayList<ModelGetAdviceFolTheme> themeArrayList;

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getTypeAdvice() {
        return typeAdvice;
    }

    public void setTypeAdvice(String typeAdvice) {
        this.typeAdvice = typeAdvice;
    }

    public ArrayList<ModelGetAdviceFolTheme> getThemeArrayList() {
        return themeArrayList;
    }

    public void setThemeArrayList(ArrayList<ModelGetAdviceFolTheme> themeArrayList) {
        this.themeArrayList = themeArrayList;
    }
}
