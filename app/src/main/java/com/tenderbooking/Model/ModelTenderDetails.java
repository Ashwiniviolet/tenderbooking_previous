package com.tenderbooking.Model;

import java.util.ArrayList;

/**
 * Created by Abhishek jain on 08-03-2017.
 */

public class ModelTenderDetails {

    String nameProcedure;
    String typeTender, sourcetender, titleTender, assignedTender,
            optionsTender, statusTender, mainLanguage, nameBuyer, cityBuyer,
            documentationCurrency, amountCurrency, dateStart, dateStop,
            dateDocumentation, colorName, nameContract, idDocument,
            nameDocument, nameAwardCriteria, countryFlagTender, categoriesTender,
            urlTender, idNUTS;
    String tagTender;
    String idBuyer;
    String idTender;
    String idContract;
    String idProcedure;
    String idAwardCriteria;
    String cityTender;
    String countryTender;
    double documentationPrice, amountTender;
    String city_name, country_name;
    ArrayList<ModelTenDetalDetails> detailsArrayList;
    ArrayList<ModelTenDetailAttach> attachmentArraylist;
    ArrayList<ModelTenDetailDocuments> documentsArrayList;
    ArrayList<ModelTenDetailLinks> linksArrayList;

    public String getTypeTender() {
        return typeTender;
    }

    public void setTypeTender(String typeTender) {
        this.typeTender = typeTender;
    }

    public String getSourcetender() {
        return sourcetender;
    }

    public void setSourcetender(String sourcetender) {
        this.sourcetender = sourcetender;
    }

    public String getTitleTender() {
        return titleTender;
    }

    public void setTitleTender(String titleTender) {
        this.titleTender = titleTender;
    }

    public String getAssignedTender() {
        return assignedTender;
    }

    public void setAssignedTender(String assignedTender) {
        this.assignedTender = assignedTender;
    }

    public String getOptionsTender() {
        return optionsTender;
    }

    public void setOptionsTender(String optionsTender) {
        this.optionsTender = optionsTender;
    }

    public String getStatusTender() {
        return statusTender;
    }

    public void setStatusTender(String statusTender) {
        this.statusTender = statusTender;
    }

    public String getMainLanguage() {
        return mainLanguage;
    }

    public void setMainLanguage(String mainLanguage) {
        this.mainLanguage = mainLanguage;
    }

    public String getNameBuyer() {
        return nameBuyer;
    }

    public void setNameBuyer(String nameBuyer) {
        this.nameBuyer = nameBuyer;
    }

    public String getCityBuyer() {
        return cityBuyer;
    }

    public void setCityBuyer(String cityBuyer) {
        this.cityBuyer = cityBuyer;
    }

    public String getDocumentationCurrency() {
        return documentationCurrency;
    }

    public void setDocumentationCurrency(String documentationCurrency) {
        this.documentationCurrency = documentationCurrency;
    }

    public String getAmountCurrency() {
        return amountCurrency;
    }

    public void setAmountCurrency(String amountCurrency) {
        this.amountCurrency = amountCurrency;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateStop() {
        return dateStop;
    }

    public void setDateStop(String dateStop) {
        this.dateStop = dateStop;
    }

    public String getDateDocumentation() {
        return dateDocumentation;
    }

    public void setDateDocumentation(String dateDocumentation) {
        this.dateDocumentation = dateDocumentation;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getNameContract() {
        return nameContract;
    }

    public void setNameContract(String nameContract) {
        this.nameContract = nameContract;
    }

    public String getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(String idDocument) {
        this.idDocument = idDocument;
    }

    public String getNameDocument() {
        return nameDocument;
    }

    public void setNameDocument(String nameDocument) {
        this.nameDocument = nameDocument;
    }

    public String getNameAwardCriteria() {
        return nameAwardCriteria;
    }

    public void setNameAwardCriteria(String nameAwardCriteria) {
        this.nameAwardCriteria = nameAwardCriteria;
    }

    public String getCountryFlagTender() {
        return countryFlagTender;
    }

    public void setCountryFlagTender(String countryFlagTender) {
        this.countryFlagTender = countryFlagTender;
    }

    public String getCategoriesTender() {
        return categoriesTender;
    }

    public void setCategoriesTender(String categoriesTender) {
        this.categoriesTender = categoriesTender;
    }

    public String getUrlTender() {
        return urlTender;
    }

    public void setUrlTender(String urlTender) {
        this.urlTender = urlTender;
    }

    public String getIdNUTS() {
        return idNUTS;
    }

    public void setIdNUTS(String idNUTS) {
        this.idNUTS = idNUTS;
    }

    public double getDocumentationPrice() {
        return documentationPrice;
    }

    public void setDocumentationPrice(double documentationPrice) {
        this.documentationPrice = documentationPrice;
    }

    public double getAmountTender() {
        return amountTender;
    }

    public void setAmountTender(double amountTender) {
        this.amountTender = amountTender;
    }

    public String getTagTender() {
        return tagTender;
    }

    public void setTagTender(String tagTender) {
        this.tagTender = tagTender;
    }

    public String getIdBuyer() {
        return idBuyer;
    }

    public void setIdBuyer(String idBuyer) {
        this.idBuyer = idBuyer;
    }

    public String getIdTender() {
        return idTender;
    }

    public void setIdTender(String idTender) {
        this.idTender = idTender;
    }

    public String getIdContract() {
        return idContract;
    }

    public void setIdContract(String idContract) {
        this.idContract = idContract;
    }

    public String getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(String idProcedure) {
        this.idProcedure = idProcedure;
    }

    public String getIdAwardCriteria() {
        return idAwardCriteria;
    }

    public void setIdAwardCriteria(String idAwardCriteria) {
        this.idAwardCriteria = idAwardCriteria;
    }

    public String getCityTender() {
        return cityTender;
    }

    public void setCityTender(String cityTender) {
        this.cityTender = cityTender;
    }

    public String getCountryTender() {
        return countryTender;
    }

    public void setCountryTender(String countryTender) {
        this.countryTender = countryTender;
    }

    public String getNameProcedure() {
        return nameProcedure;
    }

    public void setNameProcedure(String nameProcedure) {
        this.nameProcedure = nameProcedure;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public ArrayList<ModelTenDetalDetails> getDetailsArrayList() {
        return detailsArrayList;
    }

    public void setDetailsArrayList(ArrayList<ModelTenDetalDetails> detailsArrayList) {
        this.detailsArrayList = detailsArrayList;
    }

    public ArrayList<ModelTenDetailAttach> getAttachmentArraylist() {
        return attachmentArraylist;
    }

    public void setAttachmentArraylist(ArrayList<ModelTenDetailAttach> attachmentArraylist) {
        this.attachmentArraylist = attachmentArraylist;
    }

    public ArrayList<ModelTenDetailDocuments> getDocumentsArrayList() {
        return documentsArrayList;
    }

    public void setDocumentsArrayList(ArrayList<ModelTenDetailDocuments> documentsArrayList) {
        this.documentsArrayList = documentsArrayList;
    }

    public ArrayList<ModelTenDetailLinks> getLinksArrayList() {
        return linksArrayList;
    }

    public void setLinksArrayList(ArrayList<ModelTenDetailLinks> linksArrayList) {
        this.linksArrayList = linksArrayList;
    }
}