package com.tenderbooking.Model;

/**
 * Created by Abhishek jain on 01-03-2017.
 */

public class ModelCheckUser {
    String apiKey, email, password, mobModel, mobPlatform, uuid, mobPlatVer, mobDevicName, appVer;
    String mobile;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobModel() {
        return mobModel;
    }

    public void setMobModel(String mobModel) {
        this.mobModel = mobModel;
    }

    public String getMobPlatform() {
        return mobPlatform;
    }

    public void setMobPlatform(String mobPlatform) {
        this.mobPlatform = mobPlatform;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMobPlatVer() {
        return mobPlatVer;
    }

    public void setMobPlatVer(String mobPlatVer) {
        this.mobPlatVer = mobPlatVer;
    }

    public String getMobDevicName() {
        return mobDevicName;
    }

    public void setMobDevicName(String mobDevicName) {
        this.mobDevicName = mobDevicName;
    }

    public String getAppVer() {
        return appVer;
    }

    public void setAppVer(String appVer) {
        this.appVer = appVer;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
