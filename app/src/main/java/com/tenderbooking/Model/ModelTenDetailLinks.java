package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 11-03-2017.
 */

public class ModelTenDetailLinks {
    String idTenderLinks, typeTenderLink, titleTenderLink, dateStartLink, dateStopLink;

    public String getIdTenderLinks() {
        return idTenderLinks;
    }

    public void setIdTenderLinks(String idTenderLinks) {
        this.idTenderLinks = idTenderLinks;
    }

    public String getTypeTenderLink() {
        return typeTenderLink;
    }

    public void setTypeTenderLink(String typeTenderLink) {
        this.typeTenderLink = typeTenderLink;
    }

    public String getTitleTenderLink() {
        return titleTenderLink;
    }

    public void setTitleTenderLink(String titleTenderLink) {
        this.titleTenderLink = titleTenderLink;
    }

    public String getDateStartLink() {
        return dateStartLink;
    }

    public void setDateStartLink(String dateStartLink) {
        this.dateStartLink = dateStartLink;
    }

    public String getDateStopLink() {
        return dateStopLink;
    }

    public void setDateStopLink(String dateStopLink) {
        this.dateStopLink = dateStopLink;
    }
}
