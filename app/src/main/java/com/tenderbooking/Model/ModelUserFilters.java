package com.tenderbooking.Model;

/**
 * Created by laxmikant bolya on 16-03-2017.
 */

public class ModelUserFilters {
    String filterType, filterId, filterName, filterAll;

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterAll() {
        return filterAll;
    }

    public void setFilterAll(String filterAll) {
        this.filterAll = filterAll;
    }
}
