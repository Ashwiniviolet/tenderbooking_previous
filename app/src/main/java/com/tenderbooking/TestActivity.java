package com.tenderbooking;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tenderbooking.Adapter.TestExpandableAdapter;

public class TestActivity extends AppCompatActivity {

    public RecyclerView rv_test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        rv_test = (RecyclerView) findViewById(R.id.rv_test);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            rv_test.setHasFixedSize(true);
            rv_test.setLayoutManager(new LinearLayoutManager(this));
            rv_test.setAdapter(new TestExpandableAdapter(this));
        }


    }
}
