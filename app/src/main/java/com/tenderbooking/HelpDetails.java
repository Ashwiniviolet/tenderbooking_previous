package com.tenderbooking;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.Adapter.ExpandableDetailAdapter;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Model.ModelHelpDetail;
import com.tenderbooking.Volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HelpDetails extends AppCompatActivity {

    static int y = 12;
    public static final String TAG = HelpDetails.class.getSimpleName();
    public String helpIdString = "";
    SessionManager sessionManager;
    String helpTitle = "";
    String help_aval_database;
    DataBaseHalper dataBaseHalper;
    private int helpId;
    private ExpandableDetailAdapter adapter;
    private WebView webView;
    private ArrayList<ModelHelpDetail> modelHelpDetailArrayList;

    private TextView help_det_main_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(HelpDetails.this);


        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        String lang = sessionManager.getAppLangName();
        UtillClasses.langChange(lang, this);
        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large);
                break;
        }
        setContentView(R.layout.activity_help_details);

        // mRecyclerview = (RecyclerView) findViewById(R.id.help_details_recyclerView);
        help_det_main_title = (TextView) findViewById(R.id.help_det_main_title);
        webView = (WebView) findViewById(R.id.help_det_custlay_text);
        modelHelpDetailArrayList = new ArrayList<>();

        dataBaseHalper = new DataBaseHalper(HelpDetails.this);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(HelpDetails.this);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                if (y == 12) {
                    handler.proceed();
                } else {
                    dialog.show();
                }

            }
        });

        //adapter = new ExpandableDetailAdapter(HelpDetails.this, modelHelpDetailArrayList);
        // mRecyclerview.setHasFixedSize(true);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //ab.setTitle("Help Details");
        ab.setTitle(getResources().getString(R.string.help_detials));

        // sessionManager = new SessionManager(HelpDetails.this);

        Intent intent = getIntent();

       /* helpId = intent.getIntExtra(GlobalDataAcess.HelpDetail,0);
        helpTitle = intent.getStringExtra(GlobalDataAcess.HelpTitle);*/
        helpIdString = intent.getStringExtra(GlobalDataAcess.Datatbase_help_id);
        helpTitle = intent.getStringExtra(GlobalDataAcess.Database_help_Title);
        help_aval_database = intent.getStringExtra(GlobalDataAcess.Database_helpid_aval);


        // help_det_main_title.setText(getResources().getString(R.string.category)+helpTitle+"");

        // mRecyclerview.setLayoutManager(new LinearLayoutManager(HelpDetails.this));
        //mRecyclerview.setAdapter(adapter);

        //GetHelpDetail(helpIdString);


        if (!(help_aval_database.equals("-1"))) {
            modelHelpDetailArrayList.addAll(dataBaseHalper.getHelpDetail(helpIdString));
            help_det_main_title.setText(modelHelpDetailArrayList.get(0).getTitleAnsware());
            webView.loadData(modelHelpDetailArrayList.get(0).getTextAnsware(), "text/html", null);

            //adapter.notifyDataSetChanged();
        } else {
            GetHelpDetail(helpIdString);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == android.R.id.home) {
            this.finish();
        }
        return true;
    }

    public void GetHelpDetail(final String helpId) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "get_help_details";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " user tag " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int sizeArray = jsonArray.length();

                                    for (int i = 0; i < sizeArray; i++) {
                                        JSONObject jsonData = jsonArray.getJSONObject(i);

                                        ModelHelpDetail modelHelpDetail = new ModelHelpDetail();
                                        modelHelpDetail.setIdAnsware(jsonData.optInt("idAnswer"));
                                        modelHelpDetail.setTitleAnsware(jsonData.optString("titleAnswer"));
                                        modelHelpDetail.setTextAnsware(jsonData.optString("textAnswer"));
                                        modelHelpDetailArrayList.add(modelHelpDetail);
                                        webView.loadData(jsonData.optString("textAnswer"), "text/html", null);
                                        if (i == 0)
                                            help_det_main_title.setText(jsonData.optString("titleAnswer"));
                                        // adapter.notifyDataSetChanged();
                                    }

                                    dataBaseHalper.InsertHelpDetail(modelHelpDetailArrayList, helpId);
                                } else {
                                    //  progressDialog.hide();
                                    if (webView != null)
                                        Snackbar.make(webView, getResources().getString(R.string.not_new_list_avali), Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                // progressDialog.hide();
                                String messages = jsonObject.optString("message");
                                if (webView != null)
                                    Snackbar.make(webView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.hide();
                if (webView != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(webView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetHelpDetail(helpId);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(webView, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetHelpDetail(helpId);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                //mapFooder.put(UtillClasses.lastCheck,sessionManager.getLastCheck());
                mapFooder.put(UtillClasses.IdHelp, helpId + "");
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }
}
