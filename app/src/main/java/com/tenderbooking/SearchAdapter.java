package com.tenderbooking;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.Model.ModelSearchTenderList;

import java.util.ArrayList;

/**
 * Created by Violet on 11/28/2017.
 */

class SearchAdapter extends BaseAdapter {
    static boolean close_linear;
    private static LayoutInflater inflater = null;
    SessionManager sessionManager;
    ArrayList<ModelSearchTenderList> arrayList;
    Context context;

    public SearchAdapter(SearchResult searchResult, ArrayList<ModelSearchTenderList> arrayList) {


        this.arrayList = arrayList;
        this.context = searchResult;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        sessionManager = new SessionManager(context);


    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.search_layout, null);
        holder.cityBuyer = (TextView) rowView.findViewById(R.id.search_city_buyer);
        holder.date = (TextView) rowView.findViewById(R.id.search_date);
        holder.nameBuyer = (TextView) rowView.findViewById(R.id.search_NameBuyer);
        holder.titleTender = (TextView) rowView.findViewById(R.id.search_titleTender);
        holder.countryFlag = (ImageView) rowView.findViewById(R.id.search_country_flag);

        holder.searchAttach = (ImageView) rowView.findViewById(R.id.search_attach);
        holder.searchDoc = (ImageView) rowView.findViewById(R.id.search_doc);
        holder.searchFlag = (ImageView) rowView.findViewById(R.id.search_flag);

        Picasso.with(context).load("https://www.tenderilive.com/images/icons/flags/"
                + arrayList.get(position).getCountryFlagTender() + "32.png").error(R.drawable.app_icon).error(R.drawable.app_icon).into(((holder.countryFlag)));


        String s;

        s = String.valueOf(arrayList.get(position).getDateStart().charAt(8));
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(9));
        s = s + ".";
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(5));
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(6));
        s = s + ".";
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(0));
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(1));
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(2));
        s = s + String.valueOf(arrayList.get(position).getDateStart().charAt(3));


        holder.date.setText(s);
        holder.cityBuyer.setText(arrayList.get(position).getCityBuyer());
        holder.nameBuyer.setText(arrayList.get(position).getNameBuyer());
        holder.titleTender.setText(arrayList.get(position).getTitleTender());


        if ((Long.parseLong(arrayList.get(position).getStatusTender()) & 8192) == 8192) {

            holder.searchFlag.setVisibility(View.VISIBLE);
        } else {
            holder.searchFlag.setVisibility(View.GONE);
        }


        if ((Long.parseLong(arrayList.get(position).getOptionsTender()) & 256) == 256) {

            holder.searchAttach.setVisibility(View.VISIBLE);

        }


        if ((Long.parseLong(arrayList.get(position).getOptionsTender()) & 512) == 512) {

            holder.searchDoc.setVisibility(View.VISIBLE);

        }

        if ((Long.parseLong(arrayList.get(position).getOptionsTender()) & 256) == 256 && (Long.parseLong(arrayList.get(position).getOptionsTender()) & 512) == 512) {


            holder.searchAttach.setVisibility(View.VISIBLE);
            holder.searchDoc.setVisibility(View.VISIBLE);

        }


        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                close_linear = true;
                sessionManager.SetTenderIdDetails(arrayList.get(position).getIdTender());
                context.startActivity(new Intent(context, TenderDetails.class));


                // Toast.makeText(context, "You Clicked "+ arrayList.get(position).getIdTender(), Toast.LENGTH_SHORT).show();
            }
        });
        return rowView;


    }

    public class Holder {

        TextView nameBuyer, cityBuyer, date, titleTender;
        ImageView countryFlag, searchAttach, searchDoc, searchFlag;


    }
}
