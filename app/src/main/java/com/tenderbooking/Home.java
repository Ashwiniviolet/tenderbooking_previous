package com.tenderbooking;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.Adapter.ExpListAdviceAdpter;
import com.tenderbooking.Adapter.TestExpandableAdapter;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.RatingDialog;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Interface.DrawerItemAdviceClick;
import com.tenderbooking.MainTab.ExpanAdviDrawData;
import com.tenderbooking.MainTab.ExpandableListAdapter;
import com.tenderbooking.MainTab.ExpandableListDataSource;
import com.tenderbooking.MainTab.FragmentMore;
import com.tenderbooking.MainTab.Fragment_advice;
import com.tenderbooking.MainTab.Fragment_help;
import com.tenderbooking.MainTab.NewExpandableListDataSource;
import com.tenderbooking.Model.ModelGetAdviceFolTheme;
import com.tenderbooking.Model.ModelGetAdviceFolder;
import com.tenderbooking.Model.ModelUserFilters;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.Volley.AppController;
import com.tenderbooking.event_bus.HomeCallEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static Toolbar toolbar;
    public static TabLayout tabLayout;
    public static ExpandableListAdapter adapter;
    public static ArrayList<ModelUserTag> modelUserTagArrayList;
    public static ArrayList<ModelUserFilters> modelUserFiltersArrayList;
    public static String TAG = Home.class.getSimpleName();
    public static DrawerLayout drawer;
    public static ArrayList<ModelGetAdviceFolder> modelGetAdviceFolderArrayList;
    public static Handler handler;

    public static int position_menu_clicked = -1;
    public static Button buttonAskAdvisoty;
    public static TextView txtAskAdvisoty;
    public static RecyclerView mRecyclerview;
    public RecyclerView rv_test;
    public ExpandableListView mExpandableLIstview;
    public ExpandableListView mExpandableLIstview_advice;
    public boolean isAdviceActive = false;

    ExpListAdviceAdpter adviceAdpter;
    RadioButton radioEnglish, radioSerbian, radioBosnian;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    Button langConfirm;
    RadioGroup radioGroupAppLang;
    int checkedID;

    RatingDialog ratingDialog;

    ProgressDialog mProgressDialog;
    //firebase console
    private FirebaseAnalytics firebaseAnalytics;
    private ViewPager viewPager;
    private ActionBarDrawerToggle mDrawerToggle;
    // private int pager_position = 0;
    //private LinkedHashMap<String, List<String>> mExpandableListData;
    private LinkedHashMap<String, LinkedHashMap<String, List<String>>> mExpandableListDataNew;
    private LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> mExpandableListDataHashMapNew;
    private List<String> mExpandableListTitle;
    private SessionManager sessionManager;
    private int lastExpandadPosition = -1;
    private int lastExpandadPosition_advice = -1;
    //advice NaviData
    private LinkedHashMap<String, List<String>> mExpandableListAdvData;
    private List<String> mExpandableListAdvTitle;
    private LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> advicehashmapMain;
    private String adviceTitleName = "";
    private LinearLayout progress_ll;
    private boolean isAdviceNotification = false;
    private String Key_Title = "key_title";


    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(Home.this);

        // notificaiton handle by laxmikant @28-Aug-2017
        Intent intent = getIntent();
        if (intent.hasExtra(GlobalDataAcess.Notification_type)) ;
        {
            isAdviceNotification = intent.getBooleanExtra(GlobalDataAcess.Notification_type, false);
        }
        // notification handle
//rating
        ratingDialog = new RatingDialog(this);
        adviceTitleName = getResources().getString(R.string.myquestion);
        sessionManager = new SessionManager(Home.this);

        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        String lang = sessionManager.getAppLangName();
        UtillClasses.langChange(lang, Home.this);

        int size = sessionManager.GetSizeTheme();
        Log.e(TAG, "selected theme value is " + size);


        advicehashmapMain = new LinkedHashMap<>();
        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small_Noactionbar);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium_Noactionbar);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large_Noactionbar);
                break;
        }

        setContentView(R.layout.activity_home);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progress_ll = (LinearLayout) findViewById(R.id.progress_ll);
        rv_test = (RecyclerView) findViewById(R.id.rv_test);
        rv_test.setHasFixedSize(true);
        rv_test.setLayoutManager(new LinearLayoutManager(this));
        rv_test.setAdapter(new TestExpandableAdapter(this));

        setSupportActionBar(toolbar);

        ActionBar firstTimeSetup = getSupportActionBar();

        String s = GlobalDataAcess.titleString.trim().isEmpty() ? getResources().getString(R.string.active) : GlobalDataAcess.titleString;
        firstTimeSetup.setTitle(s);


        //firebase analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "Home_Screen");

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setMinimumSessionDuration(20000);
        firebaseAnalytics.setSessionTimeoutDuration(500);

        firebaseAnalytics.setUserId(sessionManager.GetUserEmail());
        firebaseAnalytics.setUserProperty("HOME_SCREEN", "home_screen data");


//        mProgressDialog = new ProgressDialog(Home.this);
//        //mProgressDialog.setMessage("Please wait");
//        mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
//        mProgressDialog.setCancelable(false);
        //mProgressDialog.setProgressStyle(ProgressDialog.S);
        modelUserTagArrayList = new ArrayList<>();
        modelUserFiltersArrayList = new ArrayList<>();
        modelGetAdviceFolderArrayList = new ArrayList<>();

        //sessionManager = new SessionManager(this);
        mExpandableLIstview = (ExpandableListView) findViewById(R.id.expan_listview);
        mExpandableLIstview_advice = (ExpandableListView) findViewById(R.id.expan_listview_adv_two);
        // mExpandableLIstview.setSelectedGroup(0);

        //get tag,cat from the webservices

        if (GlobalDataAcess.hasConnection(Home.this)) {
            GetUserTags();
            getAdviceFolder();
        } else {
            GlobalDataAcess.apiDialog(Home.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));

        }

        //getAdviceFolder();
        /*mExpandableListData = ExpandableListDataSource.getData(this);
        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());



        //set the data from the list
        adapter = new ExpandableListAdapter(Home.this,mExpandableListTitle,mExpandableListData);
        mExpandableLIstview.setAdapter(adapter);
        mExpandableLIstview.setDividerHeight(0);
*/

        Button btn = (Button) findViewById(R.id.seeDetails);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, TenderDetails.class));
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
        fab.hide();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // navigationView.setNavigationItemSelectedListener(this);
        viewPager = (ViewPager) findViewById(R.id.tabViewpager);
        setupViewPager(viewPager);

        viewPager.setOffscreenPageLimit(4);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.white), ContextCompat.getColor(this, R.color.white));

        // tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawerToggle.syncState();

        ActionBar ab = getSupportActionBar();
        ab.setElevation(0);
        drawer.addDrawerListener(mDrawerToggle);

        if (GlobalDataAcess.titleString.toString().trim().length() == 0) {
            GlobalDataAcess.titleString = getResources().getString(R.string.active);
        }

        //GlobalDataAcess.titleString = getResources().getString(R.string.active);
        Log.e(TAG, "view pager position " + viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() == 3) {
            GlobalDataAcess.titleString = getResources().getString(R.string.setting);

        }
        //closed for check
        //toolbar.setTitle(GlobalDataAcess.titleString);

        Log.e(TAG, "home ONcreate methos value of title is " + GlobalDataAcess.titleString);


        if (isAdviceNotification) { // change notification for the advice section
            TabLayout.Tab tab = tabLayout.getTabAt(1);
            tab.select();

            // this do due to when click on notification it change drawer to new state
            mExpandableLIstview.setVisibility(View.GONE);
            mExpandableLIstview_advice.setVisibility(View.VISIBLE);

            //laxmikant for testing navitiaon bar changing
            if (mExpandableListDataNew != null && mExpandableListTitle != null) {
                isAdviceActive = true;
                GlobalDataAcess.pager_position = 1;
                mExpandableListAdvData = ExpanAdviDrawData.getData(Home.this);
                mExpandableListAdvTitle = new ArrayList(mExpandableListAdvData.keySet());
                //adapter = new ExpandableListAdapter(Home.this,mExpandableListAdvTitle,mExpandableListAdvData);
                adviceAdpter = new ExpListAdviceAdpter(Home.this, mExpandableListAdvTitle, mExpandableListAdvData, advicehashmapMain);

                // mExpandableLIstview.setAdapter(adviceAdpter);
                mExpandableLIstview_advice.setAdapter(adviceAdpter);

                //toolbar.setTitle(adviceTitleName);
                toolbar.setTitle(sessionManager.GetAdviceToolbarTitle());
                sessionManager.SetAdviceToolbarTitle(adviceTitleName);
            }
//            tabLayout.setScrollPosition(1,0.0f,true); // test for checking notification
        } else {
            mExpandableListDataHashMapNew = NewExpandableListDataSource.GetTenderListData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);

            TabLayout.Tab tab = tabLayout.getTabAt(0);
            tab.select();

            // this do due to when click on notification it change drawer to new state
            mExpandableLIstview.setVisibility(View.VISIBLE);
            mExpandableLIstview_advice.setVisibility(View.GONE);

            if (mExpandableListDataNew != null && mExpandableListTitle != null) {
                GlobalDataAcess.pager_position = 0;
                isAdviceActive = false;
                adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
                mExpandableLIstview.setAdapter(adapter);
                toolbar.setTitle(GlobalDataAcess.titleString);
            }
        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                //  Log.e(TAG,"viewpager positoin "+position +" mexpandable data list is null "+(mExpandableListData!= null) +" exptitle is null "+(mExpandableListTitle!=null));


                if ((mExpandableListDataNew == null) && (mExpandableListTitle == null) && (position == 3)) {
                    toolbar.setTitle(getResources().getString(R.string.setting));
                }

                mExpandableListDataHashMapNew = NewExpandableListDataSource.GetTenderListData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);


                if (mExpandableListDataNew != null && mExpandableListTitle != null)
                    if (position == 0) {
                        GlobalDataAcess.pager_position = 0;
                        isAdviceActive = false;
                        mExpandableLIstview.setVisibility(View.VISIBLE);
                        mExpandableLIstview_advice.setVisibility(View.GONE);
                        //adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
                        // mExpandableLIstview.setAdapter(adapter);
                        // mExpandableLIstview.expandGroup(0);
                   /* if (GlobalDataAcess.titleString.equals("")) {
                        toolbar.setTitle("Active");
                    }else
                    {
                        toolbar.setTitle(GlobalDataAcess.titleString);
                    }*/
                        toolbar.setTitle(GlobalDataAcess.titleString);
                    } else if (position == 1) {
                        isAdviceActive = true;
                        GlobalDataAcess.pager_position = 1;
                        mExpandableLIstview.setVisibility(View.GONE);
                        mExpandableLIstview_advice.setVisibility(View.VISIBLE);

//                        mExpandableListAdvData = ExpanAdviDrawData.getData(Home.this);
//                        mExpandableListAdvTitle = new ArrayList(mExpandableListAdvData.keySet());
//                        //adapter = new ExpandableListAdapter(Home.this,mExpandableListAdvTitle,mExpandableListAdvData);
//                        adviceAdpter = new ExpListAdviceAdpter(Home.this, mExpandableListAdvTitle, mExpandableListAdvData, advicehashmapMain);
//
////                        mExpandableLIstview.setAdapter(adviceAdpter);
//                        mExpandableLIstview_advice.setAdapter(adviceAdpter);
//                        toolbar.setTitle(adviceTitleName);
                        //sessionManager.SetAdviceToolbarTitle(adviceTitleName);

                        String adviceTitleName = "";
                        if (sessionManager.getAppLangChange() && GlobalDataAcess.group_select_advice_main != 0 && GlobalDataAcess.group_select_advice_main != 3 && GlobalDataAcess.group_select_advice != -1 && GlobalDataAcess.group_select_advice_main != -1 && GlobalDataAcess.group_select_advice_child != -1) {
                            sessionManager.setAppLangChange(false);

                            LinkedList<String> listMainTilte = new LinkedList(advicehashmapMain.keySet());
                            ArrayList<LinkedHashMap<String, List<String>>> hashmapSubCatArray = advicehashmapMain.get(listMainTilte.get(GlobalDataAcess.group_select_advice_main));

                            //sub menu
                            LinkedHashMap<String, List<String>> subMenu = hashmapSubCatArray.get(GlobalDataAcess.group_select_advice);
                            LinkedList<String> listSubTitle = new LinkedList(subMenu.keySet());

                            // child menu
                            adviceTitleName = subMenu.get(listSubTitle.get(0)).get(GlobalDataAcess.group_select_advice_child);
                            sessionManager.SetAdviceToolbarTitle(adviceTitleName);

                        }
                        toolbar.setTitle(sessionManager.GetAdviceToolbarTitle());


                    } else if (position == 2) {
                        isAdviceActive = false;
                        GlobalDataAcess.pager_position = 2;

                        mExpandableLIstview.setVisibility(View.VISIBLE);
                        mExpandableLIstview_advice.setVisibility(View.GONE);
                        // adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
                        // mExpandableLIstview.setAdapter(adapter);
                        //toolbar.setTitle("Help");
                        toolbar.setTitle(getResources().getString(R.string.help));
                        // mExpandableLIstview.expandGroup(0);
                    } else if (position == 3) {
                        GlobalDataAcess.pager_position = 3;
                        isAdviceActive = false;

                        mExpandableLIstview.setVisibility(View.VISIBLE);
                        mExpandableLIstview_advice.setVisibility(View.GONE);
                        // adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
                        // mExpandableLIstview.setAdapter(adapter);
                        //toolbar.setTitle("Setting");
                        toolbar.setTitle(getResources().getString(R.string.setting));
                        //  mExpandableLIstview.expandGroup(0);
                        Log.e(TAG, "viewpager positoin " + position + " value of the title is " + getResources().getString(R.string.setting));
                    }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mExpandableLIstview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                //// TODO: 16-10-2017 changge was done here
//                if (lastExpandadPosition != -1 && groupPosition != lastExpandadPosition) {
//                    mExpandableLIstview.collapseGroup(lastExpandadPosition);
//                }
//                lastExpandadPosition = groupPosition;
            }
        });

        mExpandableLIstview_advice.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                try {
                    if (lastExpandadPosition_advice != -1 && groupPosition != lastExpandadPosition_advice) {
                        mExpandableLIstview_advice.collapseGroup(lastExpandadPosition_advice);
                    }
                    lastExpandadPosition_advice = groupPosition;
                } catch (Exception e) {
                    Log.e("crash ", "crash at time of getting data ");
                    e.printStackTrace();
                }
            }
        });


        mExpandableLIstview_advice.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int i, long id) {
                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                //  Log.e(TAG,"explist click group : int "+i+" long value "+l);
//                if ((GlobalDataAcess.pager_position != 0) && !(isAdviceActive)) {
//                    viewPager.setCurrentItem(0, true);
//                }


                if (Fragment_advice.track == true) {


                    buttonAskAdvisoty = Fragment_advice.buttonAskAdvisoty;
                    txtAskAdvisoty = Fragment_advice.txtAskAdvisoty;
                    mRecyclerview = Fragment_advice.mRecyclerview;

                    if (i == 0) {

                        Fragment_advice.updateTest();

                    } else {

                        mRecyclerview.setVisibility(View.VISIBLE);
                        buttonAskAdvisoty.setVisibility(View.GONE);
                        txtAskAdvisoty.setVisibility(View.GONE);
                    }

                }


                if (isAdviceActive) {

                    if (i == 0) {
                        //  if (notificationManager.)
                        notificationManager.cancel(0); // cancel  advice notificaiton
                        adviceTitleName = getResources().getString(R.string.myquestion);
                        toolbar.setTitle(adviceTitleName);
                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
                        drawer.closeDrawer(Gravity.LEFT);
                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
                        adviceClick.GEtADviceMenuClick(1, 0);
                        GlobalDataAcess.group_select_advice = -1;
                        GlobalDataAcess.group_select_advice_child = -1;
                        // laxmikant toast
                        // Toast.makeText(Home.this, "click on my question", Toast.LENGTH_SHORT).show();

                        if (Fragment_advice.track == true) {


                            buttonAskAdvisoty = Fragment_advice.buttonAskAdvisoty;
                            txtAskAdvisoty = Fragment_advice.txtAskAdvisoty;
                            mRecyclerview = Fragment_advice.mRecyclerview;

                            if (i == 0) {

                                Fragment_advice.updateTest();

                            }
                        }


                    } else if (i == 1) {

                        adviceTitleName = getResources().getString(R.string.advices_replies);

                        // sessionManager.SetAdviceToolbarTitle(adviceTitleName);

                        //if(advicehashmapMain.get("Advisers Replies").size()== 0)
                        if (advicehashmapMain.get(getResources().getString(R.string.advices_replies)).size() == 0)
                            drawer.closeDrawer(Gravity.LEFT);
                        // toolbar.setTitle(getResources().getString(R.string.advices_replies));
                        //adviceClick.GEtADviceMenuClick(1,0);
                        //laxmikant toast
                        // Toast.makeText(Home.this, "click on Adviser_resly", Toast.LENGTH_SHORT).show();
                    } else if (i == 2) {

                        adviceTitleName = getResources().getString(R.string.regulation);
                        // sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.regulation));
                        //Log.e(TAG,"value of the  size of folder click is "+advicehashmapMain.keySet().toString());
                        // if(advicehashmapMain.get("Regulation").size()== 0)
                        if (advicehashmapMain.get(getResources().getString(R.string.regulation)).size() == 0)
                            drawer.closeDrawer(Gravity.LEFT);
                        // toolbar.setTitle(getResources().getString(R.string.regulation));
                        //laxmikant toast
                        // Toast.makeText(Home.this, "click on Law", Toast.LENGTH_SHORT).show();
                    } else if (i == 3) {
                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news));
                        adviceTitleName = getResources().getString(R.string.news);
                        toolbar.setTitle(adviceTitleName);
                        drawer.closeDrawer(Gravity.LEFT);
                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
                        adviceClick.GEtADviceMenuClick(4, 0);

                        GlobalDataAcess.group_select_advice = -1;
                        GlobalDataAcess.group_select_advice_child = -1;
                        //laxmikant toast
                        //  Toast.makeText(Home.this, "click on news", Toast.LENGTH_SHORT).show();
                    }
                    Log.e(TAG, "clicked in the advice menu");
                }
                return false;
            }
        });

        mExpandableLIstview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                //  Log.e(TAG,"explist click group : int "+i+" long value "+l);
                if ((GlobalDataAcess.pager_position != 0) && !(isAdviceActive)) {
                    viewPager.setCurrentItem(0, true);
                }

                if (isAdviceActive) {

                    if (i == 0) {
                        //  if (notificationManager.)
                        notificationManager.cancel(0); // cancel  advice notificaiton
                        adviceTitleName = getResources().getString(R.string.myquestion);
                        toolbar.setTitle(adviceTitleName);
                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
                        drawer.closeDrawer(Gravity.LEFT);
                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
                        adviceClick.GEtADviceMenuClick(1, 0);
                        //Toast.makeText(Home.this, "click on my question", Toast.LENGTH_SHORT).show();
                    } else if (i == 1) {

                        adviceTitleName = getResources().getString(R.string.advices_replies);
                        // sessionManager.SetAdviceToolbarTitle(adviceTitleName);

                        //if(advicehashmapMain.get("Advisers Replies").size()== 0)
                        if (advicehashmapMain.get(getResources().getString(R.string.advices_replies)).size() == 0)
                            drawer.closeDrawer(Gravity.LEFT);
                        // toolbar.setTitle(getResources().getString(R.string.advices_replies));
                        //adviceClick.GEtADviceMenuClick(1,0);
                        // Toast.makeText(Home.this, "click on Adviser_resly", Toast.LENGTH_SHORT).show();
                    } else if (i == 2) {

                        adviceTitleName = getResources().getString(R.string.regulation);
                        // sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.regulation));
                        //Log.e(TAG,"value of the  size of folder click is "+advicehashmapMain.keySet().toString());
                        // if(advicehashmapMain.get("Regulation").size()== 0)
                        if (advicehashmapMain.get(getResources().getString(R.string.regulation)).size() == 0)
                            drawer.closeDrawer(Gravity.LEFT);
                        // toolbar.setTitle(getResources().getString(R.string.regulation));
                        // Toast.makeText(Home.this, "click on Law", Toast.LENGTH_SHORT).show();
                    } else if (i == 3) {
                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news));
                        adviceTitleName = getResources().getString(R.string.news);
                        toolbar.setTitle(adviceTitleName);
                        drawer.closeDrawer(Gravity.LEFT);
                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
                        adviceClick.GEtADviceMenuClick(4, 0);

                        //  Toast.makeText(Home.this, "click on news", Toast.LENGTH_SHORT).show();
                    }
                    Log.e(TAG, "clicked in the advice menu");
                }
                return false;
            }
        });

//        mExpandableLIstview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
//
//                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//
//              //  Log.e(TAG,"explist click group : int "+i+" long value "+l);
//                if ( (GlobalDataAcess.pager_position!=0) && !(isAdviceActive)){
//                    viewPager.setCurrentItem(0,true);
//                }
//
//                if (isAdviceActive){
//
//                    if (i==0){
//                      //  if (notificationManager.)
//                        notificationManager.cancel(0); // cancel  advice notificaiton
//                        adviceTitleName = getResources().getString(R.string.myquestion);
//                        toolbar.setTitle(adviceTitleName);
//                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
//                        drawer.closeDrawer(Gravity.LEFT);
//                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
//                        adviceClick.GEtADviceMenuClick(1,0);
//                        //Toast.makeText(Home.this, "click on my question", Toast.LENGTH_SHORT).show();
//                    }else if (i ==1){
//
//                        adviceTitleName = getResources().getString(R.string.advices_replies);
//                        sessionManager.SetAdviceToolbarTitle(adviceTitleName);
//
//                        //if(advicehashmapMain.get("Advisers Replies").size()== 0)
//                        if(advicehashmapMain.get(getResources().getString(R.string.advices_replies)).size()== 0)
//                            drawer.closeDrawer(Gravity.LEFT);
//                       // toolbar.setTitle(getResources().getString(R.string.advices_replies));
//                        //adviceClick.GEtADviceMenuClick(1,0);
//                       // Toast.makeText(Home.this, "click on Adviser_resly", Toast.LENGTH_SHORT).show();
//                    }else if (i ==2){
//
//                        adviceTitleName  = getResources().getString(R.string.regulation);
//                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.regulation));
//                        //Log.e(TAG,"value of the  size of folder click is "+advicehashmapMain.keySet().toString());
//                       // if(advicehashmapMain.get("Regulation").size()== 0)
//                        if(advicehashmapMain.get(getResources().getString(R.string.regulation)).size()== 0)
//                            drawer.closeDrawer(Gravity.LEFT);
//                       // toolbar.setTitle(getResources().getString(R.string.regulation));
//                       // Toast.makeText(Home.this, "click on Law", Toast.LENGTH_SHORT).show();
//                    }else if(i ==3){
//                        sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news));
//                        adviceTitleName =getResources().getString(R.string.news);
//                        toolbar.setTitle(adviceTitleName);
//                        drawer.closeDrawer(Gravity.LEFT);
//                        DrawerItemAdviceClick adviceClick = new Fragment_advice();
//                        adviceClick.GEtADviceMenuClick(4,0);
//
//                      //  Toast.makeText(Home.this, "click on news", Toast.LENGTH_SHORT).show();
//                    }
//                    Log.e(TAG,"clicked in the advice menu");
//                }else if (i<=8) {
//                    notificationManager.cancel(1);// cancel tender notification
//                    if (tabLayout != null)
//                    Snackbar.make(tabLayout, mExpandableListTitle.get(i) +" "+ getResources().getString(R.string.clicked), Snackbar.LENGTH_SHORT).show();
//                    drawer.closeDrawer(Gravity.LEFT);
//                    DrawerItemClicked drawerItemClicked = new FragmentTender();
//                    drawerItemClicked.GetDrawerItemClicked(i,"Empty");
//                    toolbar.setTitle(""+mExpandableListTitle.get(i));
//                    GlobalDataAcess.titleString = mExpandableListTitle.get(i);
//
//                }
//                return false;
//            }
//        });

//       mExpandableLIstview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//           @Override
//           public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
//             //  Log.e(TAG,"explist click chield : int "+i+" child number  "+i1+" long value "+l);
//               // drawer.closeDrawer(Gravity.LEFT);
//                 List<String> filmGenres = Arrays.asList(getResources().getStringArray(R.array.drawer_types));
//               if ( (GlobalDataAcess.pager_position!=0) && !(isAdviceActive)){
//                   viewPager.setCurrentItem(0,true);
//               }
//
//               // Snackbar.make(tabLayout, mExpandableListData.get(i).get(i1) + " Clicked", Snackbar.LENGTH_SHORT).show();
//
//               if (isAdviceActive){
//
//                  // CustExpListView secondExpList = (CustExpListView)view;
//
//
//
//               }else {
//
//                   if (i == 9) {
//                       String tagvalue = modelUserTagArrayList.get(i1).getTagIndex();
//                       toolbar.setTitle(modelUserTagArrayList.get(i1).getTagText());
//                       GlobalDataAcess.titleString = modelUserTagArrayList.get(i1).getTagText();
//                       if (!tagvalue.equals(null)) {
//                           drawer.closeDrawer(Gravity.LEFT);
//                           DrawerItemClicked drawerItemClicked = new FragmentTender();
//                           drawerItemClicked.GetDrawerItemClicked(i, tagvalue);
//                       }
//
//                   } else if (i == 10) {// for categories
//                       String categoriesValue = mExpandableListData.get(mExpandableListTitle.get(i)).get(i1);
//                       toolbar.setTitle(categoriesValue);
//                       GlobalDataAcess.titleString = categoriesValue;
//                       drawer.closeDrawer(Gravity.LEFT);
//                       int filterSize = modelUserFiltersArrayList.size();
//                       String filterId = "notAvalible";
//                       for (int j = 0; j < filterSize; j++) {
//                           if (modelUserFiltersArrayList.get(j).getFilterName().equals(categoriesValue)) {
//
//                               filterId = modelUserFiltersArrayList.get(j).getFilterId();
//                               DrawerItemClicked drawerItemClicked = new FragmentTender();
//                               drawerItemClicked.GetDrawerItemClicked(i, filterId);
//                               return true;
//                           }
//                       }
//
//
//                 /* if (!filterId.equals("notAvalible")){
//                       drawer.closeDrawer(Gravity.LEFT);
//                       DrawerItemClicked drawerItemClicked = new FragmentTender();
//                       drawerItemClicked.GetDrawerItemClicked(i,filterId);
//                   }
//*/
//                   } else if (i == 11) {//   for countries
//                       String categoriesValue = mExpandableListData.get(mExpandableListTitle.get(i)).get(i1);
//                       toolbar.setTitle(categoriesValue);
//                       GlobalDataAcess.titleString = categoriesValue;
//                       int filterSize = modelUserFiltersArrayList.size();
//                       String filterId = "notAvalible";
//                       drawer.closeDrawer(Gravity.LEFT);
//                       for (int j = 0; j < filterSize; j++) {
//                           if (modelUserFiltersArrayList.get(j).getFilterName().equals(categoriesValue)) {
//
//                               filterId = modelUserFiltersArrayList.get(j).getFilterId();
//                               DrawerItemClicked drawerItemClicked = new FragmentTender();
//                               drawerItemClicked.GetDrawerItemClicked(i, filterId);
//                               return true;
//                           }
//                       }
//
//
//                 /* if (!filterId.equals("notAvalible")){
//                      drawer.closeDrawer(Gravity.LEFT);
//                      DrawerItemClicked drawerItemClicked = new FragmentTender();
//                      drawerItemClicked.GetDrawerItemClicked(i,filterId);
//                  }*/
//
//                   } else if (i == 12) {//  for cities
//                       String categoriesValue = mExpandableListData.get(mExpandableListTitle.get(i)).get(i1);
//                       toolbar.setTitle(categoriesValue);
//                       GlobalDataAcess.titleString = categoriesValue;
//                       int filterSize = modelUserFiltersArrayList.size();
//                       String filterId = "notAvalible";
//                       drawer.closeDrawer(Gravity.LEFT);
//                       for (int j = 0; j < filterSize; j++) {
//
//                           if (modelUserFiltersArrayList.get(j).getFilterName().equals(categoriesValue)) {
//                               filterId = modelUserFiltersArrayList.get(j).getFilterId();
//                               DrawerItemClicked drawerItemClicked = new FragmentTender();
//                               drawerItemClicked.GetDrawerItemClicked(i, filterId);
//                               return true;
//                           }
//                       }
//
//
//                 /* if (!filterId.equals("notAvalible")){
//                      drawer.closeDrawer(Gravity.LEFT);
//                      DrawerItemClicked drawerItemClicked = new FragmentTender();
//                      drawerItemClicked.GetDrawerItemClicked(i,filterId);
//                  }*/
//
//                   } else if (i == 13) {//  for buyers
//                       String categoriesValue = mExpandableListDataNew.get(mExpandableListTitle.get(i)).get(i1);
//                       GlobalDataAcess.titleString = categoriesValue;
//                       toolbar.setTitle(categoriesValue);
//                       int filterSize = modelUserFiltersArrayList.size();
//                       String filterId = "notAvalible";
//                       drawer.closeDrawer(Gravity.LEFT);
//                       for (int j = 0; j < filterSize; j++) {
//                           if (modelUserFiltersArrayList.get(j).getFilterName().equals(categoriesValue)) {
//
//                               filterId = modelUserFiltersArrayList.get(j).getFilterId();
//                               DrawerItemClicked drawerItemClicked = new FragmentTender();
//                               drawerItemClicked.GetDrawerItemClicked(i, filterId);
//                               return true;
//                           }
//                       }
//
//
//                 /* if (!filterId.equals("notAvalible")){
//                      drawer.closeDrawer(Gravity.LEFT);
//                      DrawerItemClicked drawerItemClicked = new FragmentTender();
//                      drawerItemClicked.GetDrawerItemClicked(i,filterId);
//                  }
//*/
//                   } else {
//                       if (tabLayout!= null)
//                       Snackbar.make(tabLayout,getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
//
//                   }
//
//               }
//
//               return true;
//           }
//       });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // super.handleMessage(msg);
                switch (msg.what) {
                    case 11:
                        if (viewPager != null) {
                            if ((GlobalDataAcess.pager_position != 0) && !(isAdviceActive)) {
                                viewPager.setCurrentItem(0, true);
                            } else {
                                Log.e("Test BY Papa", "papa yha the");
                            }
                        }
                        break;
                    case 12:

                        break;
                }
                if (adapter != null && mExpandableLIstview != null) {
                    //  adapter.notifyDataSetChanged();
//                    adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
//mExpandableLIstview.setAdapter(adapter);
//                    mExpandableLIstview.expandGroup(0);
                }
            }
        };


    }

    @Override
    protected void onStart() {
        super.onStart();
        ratingDialog.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        /*adapter.addFragment(new FragmentTender(), "Tenders");
        adapter.addFragment(new Fragment_advice(), "Advices");
        adapter.addFragment(new Fragment_help(), "Help");
        adapter.addFragment(new FragmentMore(), "...");*/

        adapter.addFragment(new FragmentTender(), getResources().getString(R.string.tenders));
        adapter.addFragment(new Fragment_advice(), getResources().getString(R.string.advices));
        adapter.addFragment(new Fragment_help(), getResources().getString(R.string.help));
        adapter.addFragment(new FragmentMore(), "...");


        viewPager.setAdapter(adapter);

        //  ActionBar ab = getSupportActionBar();
        // ab.setDisplayHomeAsUpEnabled(true);
        //ab.setTitle("Active");
        //ab.setTitle(getResources().getString(R.string.active));
    }

    @Override
    protected void onResume() {
        super.onResume();


        invalidateOptionsMenu();

        ratingDialog.showIfNeeded();


        Log.e(TAG, "home ON Resume methos value of title is " + GlobalDataAcess.titleString);
        if (isAdviceNotification) {
            if (mExpandableListAdvData != null && mExpandableListAdvTitle != null) {
                isAdviceActive = true;
                GlobalDataAcess.pager_position = 1;
                mExpandableListAdvData = ExpanAdviDrawData.getData(Home.this);
                mExpandableListAdvTitle = new ArrayList(mExpandableListAdvData.keySet());
                //adapter = new ExpandableListAdapter(Home.this,mExpandableListAdvTitle,mExpandableListAdvData);
                adviceAdpter = new ExpListAdviceAdpter(Home.this, mExpandableListTitle, mExpandableListAdvData, advicehashmapMain);

//                mExpandableLIstview.setAdapter(adviceAdpter);
                mExpandableLIstview_advice.setAdapter(adviceAdpter);
                //bug
                toolbar.setTitle(adviceTitleName);
                sessionManager.SetAdviceToolbarTitle(adviceTitleName);
            }
        }

//        try {
//            mExpandableLIstview.expandGroup(0);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (viewPager != null) {
                if (viewPager.getCurrentItem() != 0) {
                    TabLayout.Tab tab = tabLayout.getTabAt(0);
                    tab.select();
                } else {
                    // super.onBackPressed();
                    moveTaskToBack(true);
                    sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
                    GlobalDataAcess.titleString = getResources().getString(R.string.active);
                    GlobalDataAcess.group_selected = -1;
                    GlobalDataAcess.group_select_advice = -1;
                    GlobalDataAcess.group_select_advice_main = -1;
                    // do change for application advice menu
                    GlobalDataAcess.default_string = null;
                    GlobalDataAcess.default_int = -2;
                }
            } else {
                super.onBackPressed();
                sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion));
                GlobalDataAcess.group_selected = -1;
                GlobalDataAcess.group_select_advice = -1;
                GlobalDataAcess.group_select_advice_main = -1;
                // do change for application advice menu
                GlobalDataAcess.default_string = null;
                GlobalDataAcess.default_int = -2;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_askAdvisor) {

            if (GlobalDataAcess.hasConnection(Home.this)) {

                if (sessionManager.getUserStatucCode() == 1 || sessionManager.getUserStatucCode() == 5 || sessionManager.getUserStatucCode() == 6) {
                    GetCheckAdviceQuestionNumber();

                } else {


                    if (viewPager != null)
                        Snackbar.make(viewPager, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                }

            } else {
                GlobalDataAcess.apiDialog(Home.this, getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));

            }
        }

        if (id == R.id.action_search) {

            Intent intent = new Intent(this, SearchResult.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void GetUserTags() {

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "/get_user_tags";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //temp place data in tags
                        // response ="{\"data\":[{\"tagIndex\":1,\"tagText\":\"test yellow\",\"colorID\":1,\"colorCSS\":\"f7e298\",\"colorName\":\"yellow\"},{\"tagIndex\":2,\"tagText\":\"test red\",\"colorID\":3,\"colorCSS\":\"fba3a1\",\"colorName\":\"red\"},{\"tagIndex\":4,\"tagText\":\"test orange\",\"colorID\":1,\"colorCSS\":\"fba3a1\",\"colorName\":\"orange\"},{\"tagIndex\":8,\"tagText\":\"test blue\",\"colorID\":1,\"colorCSS\":\"f1afec\",\"colorName\":\"blue\"},{\"tagIndex\":16,\"tagText\":\"test yellow\",\"colorID\":1,\"colorCSS\":\"f7e298\",\"colorName\":\"yellow\"}],\"statusCode\":\"200\",\"message\":\"Ok\"}";
                        Log.e(TAG, "user tags responce is : " + response);

                      /*  try {
                            Logger.getInstance().createFileOnDevice(true, "user tags responce is : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);


                            if (jsonObject.getInt("statusCode") == 200) {
                                sessionManager.SetTempTag(response);

                                modelUserTagArrayList.clear();

                                if (!jsonObject.isNull("data")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int size = jsonArray.length();
                                    for (int i = 0; i < size; i++) {
                                        JSONObject jsonData = jsonArray.getJSONObject(i);
                                        ModelUserTag modelUserTag = new ModelUserTag();
                                        modelUserTag.setTagIndex(jsonData.optString("tagIndex"));
                                        modelUserTag.setTagText(jsonData.optString("tagText"));
                                        modelUserTag.setColorId(jsonData.optString("colorID"));
                                        modelUserTag.setColorCSS(jsonData.optString("colorCSS"));
                                        modelUserTag.setColorName(jsonData.optString("colorName"));
                                        modelUserTagArrayList.add(modelUserTag);
                                    }
                                    GetUserFilter();
                                }

                            } else if (jsonObject.getInt("statusCode") == 401) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(Home.this, R.style.MyDialogTheme);
                                builder.setTitle(getResources().getString(R.string.warning));

                                builder.setMessage(getResources().getString(R.string.criden_open));
                                builder.setPositiveButton(getResources().getString(R.string.login), new DialogInterface.OnClickListener() {


                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Home.this, Login.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        //sessionManager.GetLogout(); // this removed becouse if user login from another device not need to delete all keys.
                                        sessionManager.clearUsingKey();
                                        startActivity(intent);

                                    }
                                });
                                builder.setCancelable(false);
                                builder.show();

                            } else {
                            }
                            String message = jsonObject.optString("message");
                            if (tabLayout != null)
                                Snackbar.make(tabLayout, "" + message, Snackbar.LENGTH_LONG).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        // Log.e(TAG,"responce user tag : "+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " volley get user tag error :" + error.getMessage() + " Cause " + error.getCause());
                GetUserTags();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //   return super.getHeaders();
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                //mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void GetUserFilter() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "/get_user_filters";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e(TAG, "filters reponce is " + response);

               /*         try {
                            Logger.getInstance().createFileOnDevice(true, "user filters responce is : " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {

                                modelUserFiltersArrayList.clear();

                                if (!jsonObject.isNull("data")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int size = jsonArray.length();
                                    for (int i = 0; i < size; i++) {
                                        JSONObject jsonData = jsonArray.getJSONObject(i);
                                        ModelUserFilters modelUserFilters = new ModelUserFilters();
                                        modelUserFilters.setFilterType(jsonData.optString("filterType"));
                                        modelUserFilters.setFilterId(jsonData.optString("filterID"));
                                        modelUserFilters.setFilterName(jsonData.optString("filterName"));
                                        modelUserFilters.setFilterAll(jsonData.optString("filterAll"));
                                        // modelUserFilters.setColorName(jsonData.optString("colorName"));
                                        modelUserFiltersArrayList.add(modelUserFilters);
                                    }
                                    GetMakeList();
                                }

                            } else {
                                String message = jsonObject.optString("message");
                                if (tabLayout != null)
                                    Snackbar.make(tabLayout, "" + message, Snackbar.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Log.e(TAG, "responce user tag : " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " volley get user tag error :" + error.getMessage() + " Cause " + error.getCause());
                GetUserFilter();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //   return super.getHeaders();
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                Log.e(TAG, " filters language for api is: " + deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                //mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void GetMakeList() {

        //data form the slider menu


        //  mExpandableListData = ExpandableListDataSource.getData(Home.this,modelUserTagArrayList,modelUserFiltersArrayList);
//        mExpandableListData = NewExpandableListDataSource.getData(Home.this,modelUserTagArrayList,modelUserFiltersArrayList);
//        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
//        //set the data from the list
//        adapter = new ExpandableListAdapter(Home.this,mExpandableListTitle,mExpandableListData);
//        mExpandableLIstview.setAdapter(adapter);
//        mExpandableLIstview.setDividerHeight(0);

        mExpandableListDataNew = NewExpandableListDataSource.getData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);
        mExpandableListDataHashMapNew = NewExpandableListDataSource.GetTenderListData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);
        mExpandableListTitle = new ArrayList(mExpandableListDataNew.keySet());
        //set the data from the list
        adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
        mExpandableLIstview.setAdapter(adapter);
        mExpandableLIstview.setDividerHeight(0);
        try {
            mExpandableLIstview.expandGroup(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (GlobalDataAcess.group_selected != -1) {
            try {
                mExpandableLIstview.expandGroup(1);

            } catch (Exception e) {
                e.printStackTrace();
                mExpandableLIstview.collapseGroup(0);
                Toast.makeText(this, "expadable list view is having exception ", Toast.LENGTH_SHORT).show();
            }
        }

        if (GlobalDataAcess.group_select_advice_main != -1 && GlobalDataAcess.group_select_advice_main != 0 && GlobalDataAcess.group_select_advice_main != 3) {
            try {
                mExpandableLIstview_advice.expandGroup(GlobalDataAcess.group_select_advice_main);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    public void GetCheckAdviceQuestionNumber() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "check_advice_question_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   mProgressDialog.hide();
                        progress_ll.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
                                    // call send
                                    //startActivity(new Intent(Home.this, AskAdvisor.class));
//                                    Intent intent = new Intent(Home.this,AskAdvisor.class);
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_tendAval,0);
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_countryId,0);
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_tenderId,0);
//                                    startActivity(intent);
                                    GlobalDataAcess globalDataAccess = new GlobalDataAcess();
                                    globalDataAccess.getAskQuesDialog(Home.this, "0", "0", 0);

                                } else {
                                    String message = jsonObject.optString("message");
                                    if (viewPager != null)
                                        Snackbar.make(viewPager, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                if (viewPager != null)
                                    Snackbar.make(viewPager, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });
// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                //mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void getAdviceFolder() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "get_advice_folders";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e(TAG, "responce of the advice folder " + response);
                            if (jsonObject.getInt("statusCode") == 200) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                int sizeArray = jsonArray.length();
                                for (int i = 0; i < sizeArray; i++) {
                                    JSONObject jsondata = jsonArray.getJSONObject(i);
                                    ModelGetAdviceFolder modelGetAdviceFolder = new ModelGetAdviceFolder();

                                    modelGetAdviceFolder.setTypeAdvice(jsondata.optString("typeAdvice"));
                                    modelGetAdviceFolder.setIdCategory(jsondata.optString("idCategory"));
                                    modelGetAdviceFolder.setNameCategory(jsondata.optString("nameCategory"));

                                    JSONArray jsonTheme = jsondata.getJSONArray("themes");
                                    int sizeTheme = jsonTheme.length();

                                    ArrayList<ModelGetAdviceFolTheme> modelGetAdviceFolThemes = new ArrayList<>();
                                    for (int j = 0; j < sizeTheme; j++) {
                                        JSONObject jsonThemeData = jsonTheme.getJSONObject(j);
                                        ModelGetAdviceFolTheme modelGetAdviceFolTheme = new ModelGetAdviceFolTheme();
                                        modelGetAdviceFolTheme.setIdTheme(jsonThemeData.optInt("idTheme"));
                                        modelGetAdviceFolTheme.setNameTheme(jsonThemeData.optString("nameTheme"));
                                        //modelGetAdviceFolder.getThemeArrayList().add(modelGetAdviceFolTheme);
                                        modelGetAdviceFolThemes.add(modelGetAdviceFolTheme);
                                    }
                                    modelGetAdviceFolder.setThemeArrayList(modelGetAdviceFolThemes);
                                    modelGetAdviceFolderArrayList.add(modelGetAdviceFolder);

                                    if (i + 1 == sizeArray) {
                                        MakeAdviceMenuList(modelGetAdviceFolderArrayList);
                                    }

                                }
//                                MakeAdviceMenuList(modelGetAdviceFolderArrayList);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(tabLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getAdviceFolder();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(tabLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getAdviceFolder();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(tabLayout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getAdviceFolder();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                // mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void MakeAdviceMenuList(ArrayList<ModelGetAdviceFolder> modelGetAdviceFolderArrayList) {

        advicehashmapMain.clear();
        advicehashmapMain = ExpandableListDataSource.GetAdviceMenuData(modelGetAdviceFolderArrayList, Home.this);

        if (isAdviceNotification) {
            if (mExpandableListAdvData != null && mExpandableListAdvTitle != null) {
                isAdviceActive = true;
                GlobalDataAcess.pager_position = 1;
                mExpandableListAdvData = ExpanAdviDrawData.getData(Home.this);
                mExpandableListAdvTitle = new ArrayList(mExpandableListAdvData.keySet());
                //adapter = new ExpandableListAdapter(Home.this,mExpandableListAdvTitle,mExpandableListAdvData);
                adviceAdpter = new ExpListAdviceAdpter(Home.this, mExpandableListAdvTitle, mExpandableListAdvData, advicehashmapMain);

//                mExpandableLIstview.setAdapter(adviceAdpter);
                mExpandableLIstview_advice.setAdapter(adviceAdpter);
                // toolbar.setTitle(adviceTitleName);
                toolbar.setTitle(sessionManager.GetAdviceToolbarTitle());
                sessionManager.SetAdviceToolbarTitle(adviceTitleName);
            }
        }

        mExpandableListAdvData = ExpanAdviDrawData.getData(Home.this);
        mExpandableListAdvTitle = new ArrayList(mExpandableListAdvData.keySet());
        adviceAdpter = new ExpListAdviceAdpter(Home.this, mExpandableListAdvTitle, mExpandableListAdvData, advicehashmapMain);
        //mExpandableLIstview.setAdapter(adviceAdpter);
        mExpandableLIstview_advice.setAdapter(adviceAdpter);


    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(HomeCallEvent homeCallEvent) {
        HomeCallEvent homeCallEvent1 = EventBus.getDefault().removeStickyEvent(HomeCallEvent.class);

        if (adapter != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    mExpandableListDataNew = NewExpandableListDataSource.getData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);
                    mExpandableListDataHashMapNew = NewExpandableListDataSource.GetTenderListData(Home.this, modelUserTagArrayList, modelUserFiltersArrayList);
                    //  adapter.notifyDataSetChanged();

                    mExpandableLIstview = (ExpandableListView) findViewById(R.id.expan_listview);
                    adapter = null;
                    adapter = new ExpandableListAdapter(Home.this, mExpandableListTitle, mExpandableListDataNew, mExpandableListDataHashMapNew, modelUserTagArrayList, modelUserFiltersArrayList);
                    mExpandableLIstview.setAdapter(adapter);
                    mExpandableLIstview.invalidate();

                    if (GlobalDataAcess.group_selected != -1) {
                        mExpandableLIstview.expandGroup(1);
                    }

//                            //below change made by ashwini
                    mExpandableLIstview.expandGroup(0);
//                    if(listTwoisExpanded==true) {
//                        mExpandableLIstview.expandGroup(1);
//                    }


                    //}
                }
            });
        }
        if (homeCallEvent1 != null) {
            if (homeCallEvent.isStart()) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString(Key_Title, String.valueOf(getSupportActionBar().getTitle()));

        super.onSaveInstanceState(outState);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }


    }


    public void setAppLangDialog(int lang) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(Home.this).inflate(R.layout.set_language_layout, null, false);
        builder = new AlertDialog.Builder(Home.this);
        builder.setCancelable(false);
        builder.setView(view);
        alertDialog = builder.create();

        radioGroupAppLang = (RadioGroup) view.findViewById(R.id.radioGroupAppLang);
        radioEnglish = (RadioButton) view.findViewById(R.id.radioEnglish);
        radioSerbian = (RadioButton) view.findViewById(R.id.radioSerbian);
        radioBosnian = (RadioButton) view.findViewById(R.id.radioBosnian);
        langConfirm = (Button) view.findViewById(R.id.confirm);
        langConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                checkedID = radioGroupAppLang.getCheckedRadioButtonId();
                String lang = "";
                String[] tenderMenu = getResources().getStringArray(R.array.drawer_types);
                int counter_tender = 0;
                int counter_advice_menu = -1;
                for (String s : tenderMenu) {
                    if (GlobalDataAcess.titleString.equalsIgnoreCase(s)) {
                        break;
                    }
                    counter_tender++;
                }

                if (sessionManager.GetAdviceToolbarTitle().equalsIgnoreCase(getResources().getString(R.string.myquestion))) {
                    counter_advice_menu = 0;
                } else if (sessionManager.GetAdviceToolbarTitle().equalsIgnoreCase(getResources().getString(R.string.news))) {
                    counter_advice_menu = 1;

                }

                switch (checkedID) {

                    case R.id.radioEnglish:


                        lang = "en";

                        sessionManager.setUserLang("english");
                        sessionManager.setApplanguage(1, "en");

                        String[] menu_array = getResources().getStringArray(R.array.drawer_types_english);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array[counter_tender];
                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_english));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_english));
                        }

                        break;

                    case R.id.radioSerbian:
                        lang = "sr";

                        sessionManager.setUserLang("serbian");
                        sessionManager.setApplanguage(2, "sr");

                        String[] menu_array_serbian = getResources().getStringArray(R.array.drawer_types_serbian);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array_serbian[counter_tender];
                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));
                        }

                        break;

                    case R.id.radioBosnian:
                        lang = "bs";
                        sessionManager.setUserLang("bosnian");
                        sessionManager.setApplanguage(3, "bs");

                        String[] menu_array_bosnian = getResources().getStringArray(R.array.drawer_types_bosnian);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array_bosnian[counter_tender];

                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));
                        }

                        break;
                }
                Log.e(TAG, " value of the language is " + lang);
                Locale locale = new Locale(lang);
                locale.setDefault(locale);
                Configuration configuration = new Configuration();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {


                    Resources resources = getResources();
                    configuration.setLocale(locale);
                    Home.this.createConfigurationContext(configuration);

                } else {
                    configuration.locale = locale;
                    getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                }
                editor = preferences.edit();
                editor.putBoolean("isFirst", false);
                editor.commit();
                AppController.getmInstance().onConfigurationChanged(configuration);
                alertDialog.cancel();
                sessionManager.setAppLangChange(true);
                Home.this.recreate();
                alertDialog.dismiss();
            }
        });

        switch (lang) {

            case 1:
                radioEnglish.setChecked(true);
                radioSerbian.setText("Serbian");
                radioBosnian.setText("Bosnia and Herzegovina");
                break;
            case 2:
                radioSerbian.setText("Srbija");
                radioBosnian.setText("Bosna i Hercegovina");
                radioSerbian.setChecked(true);
                break;
            case 3:
                radioSerbian.setText("Srbija");
                radioBosnian.setText("Bosna i Hercegovina");
                radioBosnian.setChecked(true);
                break;

        }


        alertDialog.show();


    }


}
