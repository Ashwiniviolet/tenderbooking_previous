package com.tenderbooking.MainTab;

import android.content.Context;

import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Vikaash on 2/25/2017.
 */
public class ExpanAdviDrawData {

    public static LinkedHashMap<String, List<String>> expandListImage;

    public static int icon[] = {R.drawable.star_transparent, R.drawable.hollow_star, R.drawable.have_value, R.drawable.tender_with_flag, R.drawable.hot_tenders, R.drawable.procurement_plan, R.drawable.expired_tender, R.drawable.delete_tenders, R.drawable.user_defiled_filters, R.drawable.tender_with_flag, R.drawable.hot_tenders, R.drawable.procurement_plan, R.drawable.expired_tender, R.drawable.delete_tenders};

    public static ArrayList<String> iconarray = new ArrayList<>();


    public static LinkedHashMap<String, List<String>> getData(Context context) {
        LinkedHashMap<String, List<String>> expandableListData = new LinkedHashMap<>();
        expandListImage = new LinkedHashMap<>();
        List<String> filmGenres = Arrays.asList(context.getResources().getStringArray(R.array.advi_drawer));

        List<String> my_question = Arrays.asList(context.getResources().getStringArray(R.array.myquestion));
        List<String> advisor_reply = Arrays.asList(context.getResources().getStringArray(R.array.advison));
        List<String> leegal_regu = Arrays.asList(context.getResources().getStringArray(R.array.legal_reg));
        List<String> public_proc = Arrays.asList(context.getResources().getStringArray(R.array.public_proc));


        expandableListData.put(filmGenres.get(0), my_question);
        expandableListData.put(filmGenres.get(1), advisor_reply);
        expandableListData.put(filmGenres.get(2), leegal_regu);
        expandableListData.put(filmGenres.get(3), public_proc);


        return expandableListData;
    }
}
