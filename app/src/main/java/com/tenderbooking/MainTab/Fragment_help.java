package com.tenderbooking.MainTab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.Adapter.ExpandableListAdapterHelp;
import com.tenderbooking.HelpDetails;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Model.ModelHelpList;
import com.tenderbooking.R;
import com.tenderbooking.Volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vikaash on 2/21/2017.
 */
public class Fragment_help extends Fragment {

    public static String TAG = Fragment_help.class.getSimpleName();
    public static ArrayList<ModelHelpList> modelHelpListArrayList;
    public static ExpandableListAdapterHelp adapter;
    public static boolean languageChanged = false;
    public List<String> helpTitleList;
    public LinkedHashMap<String, List<String>> helpDetailList;
    public DataBaseHalper dataBaseHalper;
    int data = 0;
    LinearLayout linearLayout;
    ExpandableListView mExpandableListview;
    Context mContext;
    //database data
    List<String> helpDataTitleList;
    LinkedHashMap<String, List<String>> hashmapHelpTitle;
    private SessionManager sessionManager;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        linearLayout = (LinearLayout) view.findViewById(R.id.llhelp);
        mExpandableListview = (ExpandableListView) view.findViewById(R.id.expan_listview_help);

        dataBaseHalper = new DataBaseHalper(getActivity());

        mContext = getActivity();

        mExpandableListview.setDividerHeight(0);
        mExpandableListview.setGroupIndicator(null);
        modelHelpListArrayList = new ArrayList<>();


        sessionManager = new SessionManager(getActivity());


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HelpDetails.class));
            }
        });
        getHelpCheck();

        mExpandableListview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        mExpandableListview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                //String helpDetailName = helpDetailList.get(helpTitleList.get(groupPosition)).get(childPosition);
                String helpDetailName = hashmapHelpTitle.get(helpDataTitleList.get(groupPosition)).get(childPosition);

                /*int size = modelHelpListArrayList.size();
                int helpDetail= 0;

                for (int i = 0;i<size;i++){
                    if (modelHelpListArrayList.get(i).getQuestionHelp().equals(helpDetailName)){

                        helpDetail = modelHelpListArrayList.get(i).getIdhelp();
                        Intent intent = new Intent(mContext,HelpDetails.class);
                        intent.putExtra(GlobalDataAcess.HelpDetail,helpDetail);
                        intent.putExtra(GlobalDataAcess.HelpTitle,helpTitleList.get(groupPosition));
                        mContext.startActivity(intent);
                    }
                }*/

                // data using database

                HashMap<String, String> hashmapString = dataBaseHalper.getHelpId(helpDetailName);

                if (!hashmapString.get(GlobalDataAcess.Database_helpid_aval).equals("-1")) ;
                {

                    Intent intent = new Intent(mContext, HelpDetails.class);
                    intent.putExtra(GlobalDataAcess.Datatbase_help_id, hashmapString.get(GlobalDataAcess.Datatbase_help_id));
                    intent.putExtra(GlobalDataAcess.Database_help_Title, hashmapString.get(GlobalDataAcess.Database_help_Title));
                    intent.putExtra(GlobalDataAcess.Database_helpid_aval, hashmapString.get(GlobalDataAcess.Database_helpid_aval));
                    mContext.startActivity(intent);
                }


                return true;
            }
        });

        if (languageChanged) {

            dataBaseHalper.getDeleteHelpTable();

            languageChanged = false;

        }
        return view;
    }

    @Override
    public void onResume() {

        if (languageChanged) {

            dataBaseHalper.getDeleteHelpTable();

            languageChanged = false;

        }


        super.onResume();
        try {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getHelpCheck() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "check_help_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " check  help number " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
                                    getHelpList();
                                   /* String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                                    Log.e(TAG, " Header time and date "+date_time);
                                    sessionManager.setLastCheck(date_time);
                                    main_updateStatus.setText("Last Updated on "+date_time);*/

                                } else {
                                    makeList();
                                    //  progressDialog.hide();
                                    // Snackbar.make(linearLayout," Not new List Avalible ",Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                // progressDialog.hide();
                                makeList();
                                String messages = jsonObject.optString("message");
                                Log.e(TAG, "message from the helo check  " + messages);
                                // Snackbar.make(linearLayout,""+messages,Snackbar.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.hide();
                makeList();

                //Snackbar.make(linearLayout,"  "+error.getMessage(),Snackbar.LENGTH_SHORT).show();
                if (linearLayout != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getActivity().getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getHelpCheck();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getActivity().getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getHelpCheck();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                //                mapFooder.put(UtillClasses.lastCheck,sessionManager.getHelpLastCheck());
                if (dataBaseHalper == null)
                    dataBaseHalper = new DataBaseHalper(getActivity());
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastHelpAssignedDate());
                Log.e(TAG, "help check number " + sessionManager.getHelpLastCheck());
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void getHelpList() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "get_help_list";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " helo list " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {
                                    setlastcheck();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    int arraysize = jsonArray.length();

                                    for (int i = 0; i < arraysize; i++) {

                                        JSONObject jsondata = jsonArray.getJSONObject(i);
                                        ModelHelpList modelHelpList = new ModelHelpList();
                                        modelHelpList.setIdhelp(jsondata.optInt("idHelp"));
                                        modelHelpList.setIdTopic(jsondata.optInt("idTopic"));
                                        modelHelpList.setQuestionHelp(jsondata.optString("questionHelp"));
                                        modelHelpList.setUpdateHelp(jsondata.optString("updatedHelp"));
                                        modelHelpList.setNameTopic(jsondata.optString("nameTopic"));
                                        modelHelpListArrayList.add(modelHelpList);
                                        Log.e(TAG, " dta for the Help list is " + modelHelpList);

                                    }
                                    dataBaseHalper.InsertHelpTitle(modelHelpListArrayList);
                                    makeList();

                                   /* String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                                    Log.e(TAG, " Header time and date "+date_time);
                                    sessionManager.setLastCheck(date_time);
                                    main_updateStatus.setText("Last Updated on "+date_time);*/

                                } else {
                                    //  progressDialog.hide();
                                    if (linearLayout != null)
                                        Snackbar.make(linearLayout, " Not new List Avalible ", Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                // progressDialog.hide();
                                String messages = jsonObject.optString("message");
                                if (linearLayout != null)
                                    Snackbar.make(linearLayout, getActivity().getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.hide();
                //Snackbar.make(linearLayout,"  "+error.getMessage(),Snackbar.LENGTH_SHORT).show();
                if (linearLayout != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar.make(linearLayout, R.string.some_went_wrong, Snackbar.LENGTH_LONG)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getHelpCheck();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {
                        //
                    } else if (error instanceof ServerError) {
                        //
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getActivity().getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getHelpCheck();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {
                        //
                    } else if (error instanceof ParseError) {
                        //
                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");

                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

//                mapFooder.put(UtillClasses.lastCheck,sessionManager.getHelpLastCheck());
                if (dataBaseHalper == null)
                    dataBaseHalper = new DataBaseHalper(getActivity());
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastHelpAssignedDate());

                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/
                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }


    public void makeList() {

       /* helpDetailList = new LinkedHashMap<>();
        List<String> helpltenderlist = new ArrayList<>();
        List<String> helpadviserList = new ArrayList<>();

        int arraysize = modelHelpListArrayList.size();


        if (arraysize>0){
            for (int i = 0;i<arraysize;i++){
                switch (modelHelpListArrayList.get(i).getNameTopic()){

                    case GlobalDataAcess.Helptenderlist:
                        helpltenderlist.add(modelHelpListArrayList.get(i).getQuestionHelp());
                        break;

                    case GlobalDataAcess.Helpadviceslist:
                        helpadviserList.add(modelHelpListArrayList.get(i).getQuestionHelp());
                        break;
                }
            }

            helpDetailList.put(GlobalDataAcess.Helptenderlist,helpltenderlist);
            helpDetailList.put(GlobalDataAcess.Helpadviceslist,helpadviserList);

            helpTitleList = new ArrayList(helpDetailList.keySet());
*/
        //take data form data base
        hashmapHelpTitle = new LinkedHashMap<>();
        hashmapHelpTitle.putAll(dataBaseHalper.getHelpTitle());

        helpDataTitleList = new ArrayList<>();

        helpDataTitleList = new ArrayList<>(hashmapHelpTitle.keySet());

        // adapter = new ExpandableListAdapterHelp(mContext,helpTitleList,helpDetailList);
        adapter = new ExpandableListAdapterHelp(mContext, helpDataTitleList, hashmapHelpTitle, sessionManager.GetHelpSize());

        mExpandableListview.setAdapter(adapter);


        for (int i = 0; i < adapter.getGroupCount(); i++) {
            try {
                mExpandableListview.expandGroup(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void setlastcheck() {
        String simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        Log.e(TAG, " current simpledate and time is " + simpleDateFormat);

        sessionManager.setHelpLastCheck(simpleDateFormat);
        String converteddata = GlobalDataAcess.ConvertedDate(simpleDateFormat);
        Log.e(TAG, "covnerted date " + GlobalDataAcess.ConvertedDate(simpleDateFormat));
        // advice_lastupdateTxt.setText("Last Update on "+ GlobalDataAcess.ConvertedDate(simpleDateFormat));

    }
}