package com.tenderbooking.MainTab;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Abhishek jain on 25-02-2017.
 */

public class GlobalDataAccess {


    public static int GetColorresr(int id, Activity mActivity) {

        return ContextCompat.getColor(mActivity, id);
    }

    public static void hideSoftKeyboard(Activity mActivity) {

        if (mActivity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
