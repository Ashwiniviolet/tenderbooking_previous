package com.tenderbooking.MainTab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.AdviceDetialsNew;
import com.tenderbooking.Advices_detail;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.DrawerItemAdviceClick;
import com.tenderbooking.Interface.InterfaceAdviceChildClick;
import com.tenderbooking.Model.ModelAdviceAnsware;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.Model.ModelGetAdviceFolTheme;
import com.tenderbooking.Model.ModelGetAdviceFolder;
import com.tenderbooking.R;
import com.tenderbooking.RecycerItemClickListener;
import com.tenderbooking.Volley.AppController;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.tenderbooking.TenderDetails.viewPager;

/**
 * Created by Vikaash on 2/25/2017.
 */
public class Fragment_advice extends Fragment implements DrawerItemAdviceClick, InterfaceAdviceChildClick, View.OnClickListener {


    public static final String TAG = Fragment_advice.class.getSimpleName();
    public static int advice_scheduleType = 1;
    public static AdviceDetailAdapter adviceDetailAdapter;
    public static TextView advice_lastupdateTxt;
    public static SessionManager sessionManager;
    public static List<ModelAdviceList> modelAdviceListList;
    public static DataBaseHalper dataBaseHalper;
    public static String lastUpdateString;
    public static Button buttonAskAdvisoty;
    public static TextView txtAskAdvisoty;
    public static RecyclerView mRecyclerview;
    public static LayoutInflater layoutInflater;
    public static ViewGroup viewGroup;
    static public boolean track = false;
    public static ArrayList<ModelAdviceList> modelAdviceListArrayList;
    public Home home;
    public Context mContext;
    GlobalDataAcess globalDataAcess;
    TextView advice_btn;
    ArrayList<ModelGetAdviceFolder> modelGetAdviceFolderArrayList;
    Map<String, String> deviceinfoMap;
    LinearLayout linearLayout;
    private LinearLayout progress_ll;
    //Firebase ANalytics
    private FirebaseAnalytics firebaseAnalytics;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private ImageView scroll_up;

//    public  static String default_string= null;
//    public static int default_int = -2;

    public static void updateTest() {
        if (modelAdviceListList != null) {

            if (modelAdviceListList.size() <= 0) {
                mRecyclerview.setVisibility(View.GONE);
                txtAskAdvisoty.setVisibility(View.GONE);
                buttonAskAdvisoty.setVisibility(View.VISIBLE);

            } else {

                txtAskAdvisoty.setVisibility(View.GONE);
                buttonAskAdvisoty.setVisibility(View.GONE);
                mRecyclerview.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //   this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        //  getContext().getTheme().applyStyle(R.style.AChekppTheme, true);


        home = new Home();


        this.mContext = container.getContext();


        layoutInflater = inflater;

        viewGroup = container;


        View view = inflater.inflate(R.layout.advices_fragment, container, false);


        advice_btn = (TextView) view.findViewById(R.id.advice_btn);
        advice_lastupdateTxt = (TextView) view.findViewById(R.id.advice_lastupdateTxt);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.advice_swipe_lay);


        mRecyclerview = (RecyclerView) view.findViewById(R.id.advice_recyclerview);
        buttonAskAdvisoty = (Button) view.findViewById(R.id.buttonAskAdvisoty);
        txtAskAdvisoty = (TextView) view.findViewById(R.id.txtAskAdvisoty);


        scroll_up = (ImageView) view.findViewById(R.id.scroll_up);
        lastUpdateString = getActivity().getResources().getString(R.string.Last_updated_on);

        modelGetAdviceFolderArrayList = new ArrayList<>();
        sessionManager = new SessionManager(getActivity());
        dataBaseHalper = new DataBaseHalper(getActivity());
        modelAdviceListList = new ArrayList<>();


        buttonAskAdvisoty.setOnClickListener(this);
        deviceinfoMap = sessionManager.GetDeviceInfo();

        //firebase ANalytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        firebaseAnalytics.setCurrentScreen(getActivity(), "Advice Section", "Advice Section");
        firebaseAnalytics.setUserId(sessionManager.GetUserEmail());
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setSessionTimeoutDuration(20000);
        firebaseAnalytics.setMinimumSessionDuration(500);


        mRecyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.addItemDecoration(new RecyclerviewDevider(1));


        adviceDetailAdapter = new AdviceDetailAdapter(getActivity(), modelAdviceListList, sessionManager.GetAdviceSize());

        globalDataAcess = new GlobalDataAcess();

        mRecyclerview.setAdapter(adviceDetailAdapter);


        // Toast.makeText(getActivity(),Integer.toString(adviceDetailAdapter.getItemCount()),Toast.LENGTH_SHORT).show();


        //
        String start_time_sesssion = sessionManager.GetNotiEnableStartTime();
        String stop_time_sesssion = sessionManager.GetNotiEnableStopTime();

        Log.e(TAG, "start " + start_time_sesssion + " stop time " + stop_time_sesssion);
        //

        scroll_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Test", "listner 0");
                scrollToPosition(0);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //mRecyclerview.setClickable(false);
                getCheckAdviceQueNumber();


                swipeRefreshLayout.setRefreshing(true);
            }
        });

        mRecyclerview.addOnItemTouchListener(new RecycerItemClickListener(getActivity(), new RecycerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //Toast.makeText(getActivity(),Integer.toString(adviceDetailAdapter.getItemCount()),Toast.LENGTH_SHORT).show();


                Log.e(TAG, "stauts of user is " + sessionManager.getUserStatucCode());


                if ((!((Integer.parseInt(modelAdviceListList.get(position).getStatusAdvice().trim()) & 2) > 0)) && ((sessionManager.getUserStatucCode() == 2) || (sessionManager.getUserStatucCode() == 3) || (sessionManager.getUserStatucCode() == 4))) {
                    if (mRecyclerview != null)
                        Snackbar.make(mRecyclerview, getResources().getString(R.string.detail_for_subs), Snackbar.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getActivity(), AdviceDetialsNew.class);
                    //intent.putExtra();
                    intent.putExtra(GlobalDataAcess.Ask_advice_typeAdvice, modelAdviceListList.get(position).getTypeAdvice());
                    intent.putExtra(GlobalDataAcess.Ask_advice_idAdvice, modelAdviceListList.get(position).getIdAdvice());
                    intent.putExtra(GlobalDataAcess.Ask_advice_documentName, modelAdviceListList.get(position).getDocumentAdvice());
                    intent.putExtra(GlobalDataAcess.Ask_advice_title, modelAdviceListList.get(position).getTitleAdvice());
                    intent.putExtra(GlobalDataAcess.Ask_advice_haveRated, modelAdviceListList.get(position).getHaveRateAdvice());
                    intent.putExtra(GlobalDataAcess.Ask_advice_position, position);
                    intent.putExtra(GlobalDataAcess.Ask_advice_status, modelAdviceListList.get(position).getStatusAdvice());
                    Log.e(TAG, "title of the clicked advice  is " + modelAdviceListList.get(position).getTitleAdvice());
                    startActivity(intent);
                }


            }


            //}
        }));


        advice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), Advices_detail.class);
                startActivity(intent);
            }

        });

        if (GlobalDataAcess.hasConnection(getActivity())) {
            getCheckAdviceQueNumber();
            getAdviceFolder();
        } else {
            GlobalDataAcess.apiDialog(getActivity(), getActivity().getResources().getString(R.string.network_error), getActivity().getResources().getString(R.string.error_msg));
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
//        GlobalDataAcess.default_int = -2;
//        GlobalDataAcess.default_string = null;
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        GlobalDataAcess.default_int = -2;
//        GlobalDataAcess.default_string = null;
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {


            if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) {
                setDatafromDatabase("user", -1);
            } else {
                setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
            }
            Log.e(TAG, "onresume call");
            //start a shadular
            advice_scheduleType = 2;
            //  getTaskSchedular();
            // adviceDetailAdapter.notifyDataSetChanged();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCheckAdviceQueNumber() {

        final String mURL = UtillClasses.BaseUrl + "check_advices_number";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        setlastcheck();
                        Log.e(TAG, "check advice number response is " + response);
                        swipeRefreshLayout.setRefreshing(false);

                       /* try {
                            Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            track = true;
                            updateTest();
                            if (jsonObject.getInt("statusCode") == 200) {

                                if (jsonObject.getInt("data") > 0) {
                                    //get advice list
                                    Log.e(TAG, "data is more then expected is :" + jsonObject.getInt("data"));
                                    getAdviceList();

                                } else {
                                    if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) {
                                        setDatafromDatabase("user", -1);
                                    } else {

                                        setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) {
                                setDatafromDatabase("user", -1);
                            } else {
                                setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error advice new count " + error.getMessage());
                swipeRefreshLayout.setRefreshing(false);

               /* try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) { // Default data set in case of error getting
                    setDatafromDatabase("user", -1);
                } else {
                    setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice count header is " + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();

                //String lastcheck = sessionManager.getAdviceLastCheck();
                String lastcheck = sessionManager.getAdviceLastCheckGMT();
                //String lastcheck = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                // String lastcheck ="2017-01-01 03:19:14";
                Log.e(TAG, "last check for the advice is " + lastcheck);
                mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void getAdviceFolder() {

        final String mURL = UtillClasses.BaseUrl + "get_advice_folders";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {

                               /* try {
                                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                int sizeArray = jsonArray.length();
                                for (int i = 0; i < sizeArray; i++) {
                                    JSONObject jsondata = jsonArray.getJSONObject(i);
                                    ModelGetAdviceFolder modelGetAdviceFolder = new ModelGetAdviceFolder();

                                    modelGetAdviceFolder.setTypeAdvice(jsondata.optString("typeAdvice"));
                                    modelGetAdviceFolder.setIdCategory(jsondata.optString("idCategory"));
                                    modelGetAdviceFolder.setNameCategory(jsondata.optString("nameCategory"));

                                    JSONArray jsonTheme = jsondata.getJSONArray("themes");
                                    int sizeTheme = jsonTheme.length();

                                    ArrayList<ModelGetAdviceFolTheme> modelGetAdviceFolThemes = new ArrayList<>();
                                    for (int j = 0; j < sizeTheme; j++) {
                                        JSONObject jsonThemeData = jsonTheme.getJSONObject(j);
                                        ModelGetAdviceFolTheme modelGetAdviceFolTheme = new ModelGetAdviceFolTheme();
                                        modelGetAdviceFolTheme.setIdTheme(jsonThemeData.optInt("idTheme"));
                                        modelGetAdviceFolTheme.setNameTheme(jsonThemeData.optString("nameTheme"));
                                        //modelGetAdviceFolder.getThemeArrayList().add(modelGetAdviceFolTheme);
                                        modelGetAdviceFolThemes.add(modelGetAdviceFolTheme);
                                    }
                                    modelGetAdviceFolder.setThemeArrayList(modelGetAdviceFolThemes);
                                    modelGetAdviceFolderArrayList.add(modelGetAdviceFolder);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();

/*
                try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                if (mRecyclerview != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(mRecyclerview, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getCheckAdviceQueNumber();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerview, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getCheckAdviceQueNumber();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                        Snackbar snackbar = Snackbar
                                .make(mRecyclerview, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getCheckAdviceQueNumber();
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof ParseError) {

                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                // mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public void getAdviceList() {
        final String mURL = UtillClasses.BaseUrl + "get_advices_list";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("statusCode") == 200) {
                        //setlastcheck();

                       /* try {
                            Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/

                        sessionManager.setAdviceFirstTime(false);
                        if (!jsonObject.isNull("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            int size = jsonArray.length();

                            modelAdviceListArrayList = new ArrayList<>();
                            modelAdviceListArrayList.clear();
                            Log.e(TAG, "size of advice number is :" + size);
                            for (int i = 0; i < size; i++) {
                                JSONObject jsondata = jsonArray.getJSONObject(i);
                                ModelAdviceList modelAdviceList = new ModelAdviceList();
                                modelAdviceList.setTypeAdvice(jsondata.optString("typeAdvice"));
                                //  Log.e(TAG,"type of advice recived is "+jsondata.optString("typeAdvice"));
                                modelAdviceList.setIdAdvice(jsondata.optString("idAdvice"));
                                modelAdviceList.setIdTheme(jsondata.optInt("idTheme"));
                                modelAdviceList.setNameTheme(jsondata.optString("nameTheme"));
                                modelAdviceList.setCountryFlagAdvice(jsondata.optString("countryFlagAdvice"));
                                modelAdviceList.setSourceAdvice(jsondata.optString("sourceAdvice"));
                                modelAdviceList.setTitleAdvice(jsondata.optString("titleAdvice"));
                                modelAdviceList.setDocumentAdvice(jsondata.optString("documentAdvice"));
                                modelAdviceList.setUpdatedAdvice(jsondata.optString("updatedAdvice"));
                                modelAdviceList.setPublicatedAdvice(jsondata.optString("publicatedAdvice"));
                                modelAdviceList.setHaveRateAdvice(jsondata.optInt("haveRateAdvice"));
                                modelAdviceList.setStatusAdvice(jsondata.optString("statusAdvice"));
                                modelAdviceList.setCountryAdvice(jsondata.optString("countryAdvice"));
                                //add model in the arraylist or update the list

                                // modelAdviceListList.add(modelAdviceList);
                                // Log.e(TAG,"Insetion for the advice number is :"+i);

                                //  adviceDetailAdapter.notifyDataSetChanged();
                                // setDatafromDatabase("user",-1);

                                modelAdviceListArrayList.add(modelAdviceList);
                                if (i + 1 == size) {
                                    dataBaseHalper.InsertAdvicdList(modelAdviceListArrayList);
                                    if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) {
                                        setDatafromDatabase("user", -1);
                                    } else {
                                        setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                                    }
                                }
                            }


                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) { // Default conditon form the if parse error come
                        setDatafromDatabase("user", -1);
                    } else {
                        setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " volley responce error  reason is " + error.getMessage());
                //getAdviceList();

               /* try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) { // default error come after that time out handling
                    setDatafromDatabase("user", -1);
                } else {
                    setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e(TAG, "advice list header data" + mapheader.toString());
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> mapFooder = new HashMap<String, String>();


                // String lastcheck = sessionManager.getAdviceLastCheck();
                String lastcheck = sessionManager.getAdviceLastCheckGMT();
                //String lastcheck ="2017-01-01 03:19:14";
                Log.e(TAG, "advice list parameter data " + lastcheck);
                if (sessionManager.getAdviceFirstTime()) {
                    mapFooder.put(UtillClasses.lastCheck, "0000-00-00 00:00:00");
                } else
                    mapFooder.put(UtillClasses.lastCheck, dataBaseHalper.getLastAdviceAssignedDate());

                mapFooder.put(UtillClasses.ListOrder, UtillClasses.Order_aces);
                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void setlastcheck() {

        String simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

        SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

        String GMtDate = simpleDateFormatGMT.format(new Date());
        Log.e(TAG, " current simpledate and time is " + simpleDateFormat + " time in gmt is " + GMtDate);


        sessionManager.setAdviceLastCheck(simpleDateFormat, GMtDate);
        if (Fragment_advice.this.isVisible())
            advice_lastupdateTxt.setText(getResources().getString(R.string.Last_updated_on) + " " + new SimpleDateFormat("dd.MM.yyyy. HH:mm", Locale.getDefault()).format(Calendar.getInstance().getTime()));
        else
            Log.e(TAG, "Fragment is not visible ");
    }

    public void setDatafromDatabase(String type_advice, int idTheme) {
        //modelAdviceListList.addAll(dataBaseHalper.getAdviceListarray(type_advice));

        GlobalDataAcess.default_int = idTheme;
        GlobalDataAcess.default_string = type_advice;


//        int total_advice_data = dataBaseHalper.getAdviceListCount();
//        Log.e(TAG, "Total number of advices is stored in the list " + total_advice_data);
        modelAdviceListList.clear();
        adviceDetailAdapter.notifyDataSetChanged();
       /* modelAdviceListList.addAll(dataBaseHalper.getAdviceList(type_advice,idTheme));
        Log.e(TAG,"total number of advice that fetched is :"+modelAdviceListList.size());
        advice_lastupdateTxt.setText("Last Update on "+ GlobalDataAcess.ConvertedDate(sessionManager.getAdviceLastCheck())+" "+sessionManager.getAdviceLastCheck().substring(11,16));
        adviceDetailAdapter.notifyDataSetChanged();
*/
        if (dataBaseHalper.getAdviceList(type_advice, idTheme)) {
            if (type_advice.equals("user")) {
                //Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.main)

                if (Home.tabLayout.getSelectedTabPosition() == 1) {


//                   Home.toolbar.setTitle(getResources().getString(R.string.myquestion));

                    switch (sessionManager.getAppLanguage()) {
                        case 1:
                            Home.toolbar.setTitle("My Questions");
                            break;
                        case 2:
                            Home.toolbar.setTitle("Moja pitanja");
                            break;
                        case 3:
                            Home.toolbar.setTitle("Moja pitanja");
                            break;
                        default:
                            Home.toolbar.setTitle("Moja pitanja");
                            break;
                    }

                    // here you test your condition that will helpp you find no of data comde from database
                    //can you wirte that condition for me
                    // here
                    //ohk thanks just test it and told me but this will not work correctly
                    // let me explain you on ca

                }
            } else {


                /*mRecyclerview.setVisibility(View.VISIBLE);
                buttonAskAdvisoty.setVisibility(View.GONE);
                txtAskAdvisoty.setVisibility(View.GONE);
*/

                adviceDetailAdapter.notifyDataSetChanged();

            }
            Log.e(TAG, "total number  of advice that fetched is :" + modelAdviceListList.size() + " " + sessionManager.getAdviceLastCheckGMT());

            // advice_lastupdateTxt.setText(lastUpdateString + GlobalDataAcess.ConvertedDate(sessionManager.getAdviceLastCheck()) + " " + sessionManager.getAdviceLastCheck().substring(11, 16));
            // advice_lastupdateTxt.setText("Last Updated On :" + GlobalDataAcess.ConvertedDate(sessionManager.getAdviceLastCheck()) + " " + sessionManager.getAdviceLastCheck().substring(11, 16));
            try {


            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "crash notifydatasetChanged");
            }
        }

    }

    @Override
    public void GEtADviceMenuClick(int main, int sub) {

        if (main == 1) {
            setDatafromDatabase("user", -1);
        } else if (main == 2) {
            setDatafromDatabase("adviser", sub);
        } else if (main == 3) {
            setDatafromDatabase("law", sub);
        } else if (main == 4) {
            setDatafromDatabase("clipping", -1);
        }
        //laxmikant toast
        // Toast.makeText(getActivity(), "getAdviceMenu is called for main ="+main, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void GetAdviceChildName(String clickName) {
        int arraylist = Home.modelGetAdviceFolderArrayList.size();
        if (arraylist > 0)
            for (int i = 0; i < arraylist; i++) {
                int themesize = Home.modelGetAdviceFolderArrayList.get(i).getThemeArrayList().size();
                for (int j = 0; j < themesize; j++) {
                    if (Home.modelGetAdviceFolderArrayList.get(i).getThemeArrayList().get(j).getNameTheme().toString().equals(clickName)) {
                        int id_theme = Home.modelGetAdviceFolderArrayList.get(i).getThemeArrayList().get(j).getIdTheme();
                        Log.e(TAG, "find the id is :" + id_theme + " theme name is " + clickName);
                        setDatafromDatabase(Home.modelGetAdviceFolderArrayList.get(i).getTypeAdvice(), id_theme);
                    }
                }

            }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        advice_scheduleType = 2;
        Log.e(TAG, "fragmet on create called");
    }

    @Override
    public void onPause() {
        super.onPause();
        advice_scheduleType = 1;
        Log.e(TAG, "fragmet on Pause called");
        EventBus.getDefault().unregister(this);
    }

    public void getTaskSchedular() {
        final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1000);
        executor.schedule(new Runnable() {
            @Override
            public void run() {
                if (advice_scheduleType == 2) {
                    Log.e(TAG, "task schedular is called ");
                    getCheckAdviceQueNumber();
                } else {
                    Log.e(TAG, "task schedular is  not called ");
                }
            }
        }, 1, TimeUnit.MINUTES);
    }

    @Subscribe(sticky = false, threadMode = ThreadMode.MAIN)
    public void onEvent(Object o) {

        if (o instanceof ArrayList) {

            if (((ArrayList) o).get(0) instanceof ModelAdviceAnsware) {
                //  ModelAdviceAnsware modeladvice = EventBus.getDefault().getStickyEvent(ModelAdviceAnsware.class);
                ArrayList<ModelAdviceAnsware> modelAdviceAnswareArrayList = new ArrayList<>();
                modelAdviceAnswareArrayList.addAll(((ArrayList<ModelAdviceAnsware>) o));
                if (true) {
                    // EventBus.getDefault().removeStickyEvent(modeladvice);
                    if (modelAdviceListList != null && modelAdviceListList.size() > 0) {
                        for (ModelAdviceAnsware modelAdviceAnsware : modelAdviceAnswareArrayList) {
                            int array_size = modelAdviceListList.size();
                            for (int i = 0; i < array_size; i++) {
                                if (modelAdviceListList.get(i).getIdAdvice().equals(modelAdviceAnsware.getIdQuestion()) & modelAdviceListList.get(i).getTypeAdvice().equals("user")) {
                                    modelAdviceListList.get(i).setStatusAdvice("7");
                                    adviceDetailAdapter.notifyDataSetChanged();
                                    Log.e(TAG, "Event bus answer is update on for title " + modelAdviceAnsware.getTitleQuestion());

                                }
                            }
                        }
                    } else {
                        Log.e(TAG, "Event bus answer list is empty  is update on for title ");
                    }
                } else {
                    Log.e(TAG, "Event bus answer is empty  is update on for title ");
                }


            } else if (((ArrayList) o).get(0) instanceof ModelAdviceList) {
                //  ModelAdviceList modeladvice = EventBus.getDefault().getStickyEvent(ModelAdviceList.class);
//                ArrayList<ModelAdviceList> modelAdviceListArrayList = new ArrayList<>();
//                modelAdviceListArrayList.addAll(((ArrayList<ModelAdviceList>) o));
//                if (true) {
//                   if (modelAdviceListList != null & modelAdviceListList.size() > 0 & modelAdviceListArrayList.size()>0 & modelAdviceListList.get(0).getTypeAdvice().equals("user")) {
//                      // int array_size = modelAdviceListList.size();
//                       for (ModelAdviceList modelAdviceList : modelAdviceListArrayList) {
//                           boolean flag = true;
//                           for (int i = 0;i<modelAdviceListList.size();i++){
//                               if (modelAdviceListList.get(i).getIdAdvice().equals(modelAdviceList.getIdAdvice())){
//                                   flag = false;
//                                   break;
//                               }else{
//                                   flag = true;
//                               }
//                               if (i+1== modelAdviceListList.size() && flag){
//                                   addNewtendersfrom(modelAdviceList);
//                                   break;
//                               }
//                           }
//
//                        }
//
//
//                    }else{
//                        Log.e(TAG,"Event bus answer list is empty  is update on for title ");
//                    }
//
//                }else{
//                    Log.e(TAG,"Event bus  is empty  ");
//                }

//                new java.util.Timer().schedule(
//                        new java.util.TimerTask() {
//                            @Override
//                            public void run() {
//                                // your code here
//                                if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2){ // default error come after that time out handling
//                                    setDatafromDatabase("user", -1);
//                                }else{
//                                    setDatafromDatabase(GlobalDataAcess.default_string,GlobalDataAcess.default_int);
//                                }
//                            }
//                        },
//                        2000
//                );

                if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) { // default error come after that time out handling
                    setDatafromDatabase("user", -1);
                } else {
                    setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
                }

            } else {
                {
                    Log.e(TAG, "Event bus is another type");
                }
            }
        } else if (o instanceof Boolean) {
            if (GlobalDataAcess.default_string == null | GlobalDataAcess.default_int == -2) { // default error come after that time out handling
                setDatafromDatabase("user", -1);
            } else {
                setDatafromDatabase(GlobalDataAcess.default_string, GlobalDataAcess.default_int);
            }
        }

    }

    public void addNewtendersfrom(ModelAdviceList modelAdviceList) {
        modelAdviceListList.add(0, modelAdviceList);
        adviceDetailAdapter.notifyDataSetChanged();
    }

    private void scrollToPosition(int i) {

        mRecyclerview.scrollToPosition(i);
        if (i == 0) {
            linearLayoutManager.scrollToPositionWithOffset(0, 0);
            //mScrollView.fullScroll(View.FOCUS_UP);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAskAdvisoty:
                if (GlobalDataAcess.hasConnection(getActivity())) {

                    if (sessionManager.getUserStatucCode() == 1 || sessionManager.getUserStatucCode() == 5 || sessionManager.getUserStatucCode() == 6) {
                        GetCheckAdviceQuestionNumber();

                    } else {


                        if (getView() != null)
                            Snackbar.make(getView(), getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                    }
                } else {

                    GlobalDataAcess.apiDialog(getActivity(), getResources().getString(R.string.network_error), getResources().getString(R.string.error_msg));
                }
                break;
        }
    }

    public void GetCheckAdviceQuestionNumber() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "check_advice_question_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   mProgressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
                                    globalDataAcess.getAskQuesDialog(getActivity(), "0", "0", 0);


                                } else {
                                    String message = jsonObject.optString("message");
                                    if (getView() != null)
                                        Snackbar.make(getView(), getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                if (getView() != null)
                                    Snackbar.make(getView(), getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });
// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                //mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

}

