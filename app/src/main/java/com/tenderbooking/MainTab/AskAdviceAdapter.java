package com.tenderbooking.MainTab;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tenderbooking.R;

import java.util.ArrayList;


public class AskAdviceAdapter extends RecyclerView.Adapter<AskAdviceAdapter.MyViewHolder> {


    public static String TAG = AskAdviceAdapter.class.getSimpleName();
    Activity mActivity;
    ArrayList<String> arrayList = new ArrayList();
    int textsize;


    public AskAdviceAdapter(Activity mActivity, ArrayList arrayList, int textsize) {
        this.mActivity = mActivity;
        this.arrayList = arrayList;
        this.textsize = textsize;
    }

    @Override
    public AskAdviceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.advices_row, parent, false);
        return new AskAdviceAdapter.MyViewHolder(view);
        // return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        try {
            if (arrayList.size() > 0) {

                holder.advice_list_question.setText(arrayList.get(position));

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();


        }


    }


    @Override
    public int getItemCount() {
        return arrayList.size() > 0 ? arrayList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout adcive_rowLL;
        ImageView advice_list_astroimg, advice_list_flag, advice_list_tick_mark;
        TextView advice_list_question;

        public MyViewHolder(View itemView) {
            super(itemView);
            adcive_rowLL = (LinearLayout) itemView.findViewById(R.id.adcive_rowLL);
            advice_list_astroimg = (ImageView) itemView.findViewById(R.id.advice_list_astroimg);
            advice_list_flag = (ImageView) itemView.findViewById(R.id.advice_list_tick_mark);
            advice_list_tick_mark = (ImageView) itemView.findViewById(R.id.advice_list_tick_mark);
            advice_list_question = (TextView) itemView.findViewById(R.id.advice_list_question);
            advice_list_flag.setImageResource(R.drawable.app_icon);

            adcive_rowLL.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.adcive_rowLL) {


            }
        }
    }


}
