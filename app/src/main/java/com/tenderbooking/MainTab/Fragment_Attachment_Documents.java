package com.tenderbooking.MainTab;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tenderbooking.Adapter.TenderAttachmentAdapter;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.InputStreamVolleyRequest;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.HelperClasses.ZoomLinearLayout;
import com.tenderbooking.Model.ModelTenDetailAttach;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.R;
import com.tenderbooking.RecycerItemClickListener;
import com.tenderbooking.TenderDetails;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikaash on 2/18/2017.
 */
public class Fragment_Attachment_Documents extends Fragment implements View.OnClickListener {

    public static String TAG = Fragment_Attachment_Documents.class.getSimpleName();
    int data = 0;
    ArrayList<ModelTenDetailAttach> modelTenDetailAttachArrayList;
    TenderAttachmentAdapter adapter;
    SessionManager sessionManager;
    File file;
    LinearLayout progress_ll;
    TextView tenderDet_att_title, tender_det_tenderid, tender_det_publDate, tender_det_expiDate;
    String filename;
    ModelTenderDetails modelTenderDetails;
    //Firebase analytics
    private FirebaseAnalytics firebaseAnalytics;
    private RecyclerView mRecyclerview;
    private ZoomLinearLayout zoomLinearLayout;
    private View.OnClickListener onClick;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, co0ntainer, savedInstanceState);
        View view = inflater.inflate(R.layout.fragments_attachment_documents, container, false);
        mRecyclerview = (RecyclerView) view.findViewById(R.id.tender_detail_attAndDoc);
        tenderDet_att_title = (TextView) view.findViewById(R.id.tenderDet_att_title);
        tender_det_tenderid = (TextView) view.findViewById(R.id.tender_det_tenderid);
        tender_det_publDate = (TextView) view.findViewById(R.id.tender_det_publDate);
        tender_det_expiDate = (TextView) view.findViewById(R.id.tender_det_expiDate);

        zoomLinearLayout = (ZoomLinearLayout) view.findViewById(R.id.zoom_layout);
        zoomLinearLayout.setClickable(true);
        zoomLinearLayout.setFocusable(true);
        zoomLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomLinearLayout.init(getActivity());
                return false;
            }
        });

        progress_ll = (LinearLayout) view.findViewById(R.id.progress_ll);
        mRecyclerview.setHasFixedSize(true);

        sessionManager = new SessionManager(getActivity());

        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        modelTenDetailAttachArrayList = TenderDetails.modelTenderDetails.getAttachmentArraylist();
        adapter = new TenderAttachmentAdapter(getActivity(), TenderDetails.modelTenderDetails.getAttachmentArraylist(), this);

        // mRecyclerview.addItemDecoration(new RecyclerviewDevider(5));
        mRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerview.setAdapter(adapter);
        mRecyclerview.setLayoutFrozen(true);


        // mRecyclerview.addItemDecoration(new RecyclerviewDivider(getActivity(),R.drawable.divider));
        tenderDet_att_title.setText(TenderDetails.modelTenderDetails.getTitleTender());

        modelTenderDetails = TenderDetails.modelTenderDetails;

        tender_det_tenderid.setText(modelTenderDetails.getIdTender());


        String assigndatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getAssignedTender().split(" ");
            assigndatevalue = assigndate[0];
        } catch (Exception e) {
            assigndatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }
        // calculating expiry date
        String expDatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getDateStop().split(" ");
            expDatevalue = assigndate[0];
        } catch (Exception e) {
            expDatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }

        tender_det_publDate.setText(GlobalDataAcess.ConvertedDate(assigndatevalue));

        if (GlobalDataAcess.ConvertedDate(expDatevalue).equals("00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals("0000-00-00.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 0000-00-00.")) {
            tender_det_expiDate.setText("-");
        } else {
            tender_det_expiDate.setText(GlobalDataAcess.ConvertedDate(expDatevalue));
        }


        mRecyclerview.addOnItemTouchListener(new RecycerItemClickListener(getActivity(), new RecycerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

//                if (position != RecyclerView.NO_POSITION) {
//                    Map<String, String> mapDocDetail = new HashMap<String, String>();
//
//                    mapDocDetail.put(UtillClasses.idtender, TenderDetails.modelTenderDetails.getIdTender());
//                    mapDocDetail.put(UtillClasses.DocType, modelTenDetailAttachArrayList.get(position).getDocType());
//                    mapDocDetail.put(UtillClasses.DocId, modelTenDetailAttachArrayList.get(position).getIdAttachment());
//                    mapDocDetail.put(UtillClasses.DocName, modelTenDetailAttachArrayList.get(position).getNameAttach());
//                    // mapDocDetail.put(UtillClasses.DocType, modelTenDetailAttachArrayList.get(position).);
//
//                    //DemoFileDownload();
//
//                    String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
//                     filename = modelTenDetailAttachArrayList.get(position).getNameAttach();
//                    file = new File(path, filename);
//
//                    if (file.exists()) {
//
//                        try{
//                            Logger.getInstance().createFileOnDevice(true, "download file already exists : file name "+path);
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//
//                        //Toast.makeText(getActivity(), "File is already exist", Toast.LENGTH_SHORT).show();
//                        Snackbar snackbar = Snackbar
//                                .make(mRecyclerview, getResources().getString(R.string.file_download_sucess), Snackbar.LENGTH_LONG)
//                                .setAction(getResources().getString(R.string.open), new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        //
//                                        /*Intent intent = new Intent(Intent.ACTION_VIEW);
//                                        Uri uri = Uri.parse( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
//                                        intent.setDataAndType(uri, "pdf/docx/doc");
//                                        startActivity(Intent.createChooser(intent, "Open File"));*/
//
//
//                                       /* MimeTypeMap myMime = MimeTypeMap.getSingleton();
//                                        Intent intent =new Intent(Intent.ACTION_VIEW);
//                                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(filename)).substring(1);
//                                        intent.setDataAndType(Uri.fromFile(file),mimeType);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                                        try {
//                                            startActivity(intent);
//                                        } catch (Exception e) {
//                                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
//                                            e.printStackTrace();
//                                        }*/
//                                        try {
//                                            GlobalDataAcess.openFile(getActivity(),file);
//                                        } catch (IOException e) {
//                                            Toast.makeText(getActivity(), getResources().getString(R.string.no_handler_), Toast.LENGTH_LONG).show();
//
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                });
//
//// Changing message text color
//                        snackbar.setActionTextColor(Color.RED);
//
//// Changing action button text color
//                        View sbView = snackbar.getView();
//                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(Color.WHITE);
//                        snackbar.show();
//                    } else {
//
//                        if(sessionManager.GetIsOnlyWifi()){
//                            if (GlobalDataAcess.IsWifiEnable(getActivity())) {
//
//                                FIleDownload(mapDocDetail, modelTenDetailAttachArrayList.get(position).getNameAttach());
//                            } else {
//                                Toast.makeText(getActivity(), getResources().getString(R.string.please_conn_wifi), Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//                        else{
//                            FIleDownload(mapDocDetail, modelTenDetailAttachArrayList.get(position).getNameAttach());
//
//
//                        }
//
//
//
//
//                    }
//
//
//                }
            }
        }));

        tender_det_tenderid.setSelected(true);


        return view;
    }

    public void FIleDownload(final Map<String, String> mapDocDetial, final String fileName) {
//        final ProgressDialog dialog = new ProgressDialog(getActivity());
//        dialog.setMessage(getResources().getString(R.string.downloading));
//        dialog.setCancelable(false);
//        dialog.show();
        progress_ll.setVisibility(View.VISIBLE);

        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        String mURL = UtillClasses.BaseUrl + "get_tender_document";

        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.POST, mURL,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {

                        /*try {
                            Logger.getInstance().createFileOnDevice(true, " file is download successfully " + response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/

                        String fileResponse = new String(response);
                        // Log.e("pdfRes", fileResponse + "a");

//                        dialog.dismiss();
                        progress_ll.setVisibility(View.GONE);
                        try {
                         /*   String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();


                            File file = new File(path, "document.pdf");

                            if(file.exists()){
                                Toast.makeText(getActivity(), "File is already downloaded", Toast.LENGTH_SHORT).show();
                            }
                            else{*/

                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(response);
                            fos.flush();
                            fos.close();
                            // Snackbar.make(mRecyclerview, "File download successfully please check in download folder", BaseTransientBottomBar.LENGTH_LONG).show();

                            Snackbar snackbar = Snackbar
                                    .make(mRecyclerview, getResources().getString(R.string.file_download_sucess), Snackbar.LENGTH_LONG)
                                    .setAction(getResources().getString(R.string.open), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //
                                           /* Intent intent = new Intent(Intent.ACTION_VIEW);
                                            Uri uri = Uri.parse( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                                            intent.setDataAndType(uri, "pdf/docx/doc");

                                            startActivity(Intent.createChooser(intent, "Open File"));*/
                                           /* MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                            Intent intent =new Intent(Intent.ACTION_VIEW);
                                            String mimeType = myMime.getMimeTypeFromExtension(fileExt(filename)).substring(1);
                                            intent.setDataAndType(Uri.fromFile(file),mimeType);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                            try {
                                                startActivity(intent);
                                            } catch (Exception e) {
                                                Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            }*/
                                            try {
                                                GlobalDataAcess.openFile(getActivity(), file);
                                            } catch (IOException e) {
                                                Toast.makeText(getActivity(), getResources().getString(R.string.no_handler_), Toast.LENGTH_LONG).show();

                                                e.printStackTrace();
                                            }
                                        }
                                    });

// Changing message text color
                            snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                            View sbView = snackbar.getView();
                            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();

                        } catch (IOException e) {
//                            dialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            e.printStackTrace();

                           /* try {
                                Logger.getInstance().createFileOnDevice(true, " file is download successfully but getting error ");
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }*/


                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dialog.dismiss();
                progress_ll.setVisibility(View.GONE);
                //  Log.e(TAG,"getting error in downloading file and error is : "+error.getMessage()+" and reson is "+error.getCause());

                if (mapDocDetial.get(UtillClasses.DocType) == null) {
//                    mapDocDetial.put(UtillClasses.DocType,"document");
                    mapDocDetial.put(UtillClasses.DocType, "attachment");
                    FIleDownload(mapDocDetial, "");
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.some_went_wrong), Toast.LENGTH_SHORT).show();

                }


               /* try {
                    Logger.getInstance().createFileOnDevice(true, " file download error  cause" + error.getCause() + " message " + error.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/


            }
        }, null) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");


              /*  try {
                    Logger.getInstance().createFileOnDevice(true, " file is download header part  Api Key" + UtillClasses.ApiKeyValue
                            + " email : " + deviceinfoMap.get(SessionManager.EMAIL) + ", api_signature : " + deviceinfoMap.get(SessionManager.SIGNATURE)
                            + " uuid : " + deviceinfoMap.get(SessionManager.UUID) + ", model " + deviceinfoMap.get(SessionManager.MODEL) + ", platform : " + deviceinfoMap.get(SessionManager.PLATFORM));
                } catch (IOException e) {
                    e.printStackTrace();
                }*/


                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();
                mapFooder.put(UtillClasses.idtender, mapDocDetial.get(UtillClasses.idtender));
                mapFooder.put(UtillClasses.DocId, mapDocDetial.get(UtillClasses.DocId));
                mapFooder.put(UtillClasses.json, "false");
//                mapFooder.put(UtillClasses.DocType, mapDocDetial.get(UtillClasses.DocType) != null ? mapDocDetial.get(UtillClasses.DocType) : "attachment");
                mapFooder.put(UtillClasses.DocType, mapDocDetial.get(UtillClasses.DocType) != null ? mapDocDetial.get(UtillClasses.DocType) : "document");


                /*try {
                    Logger.getInstance().createFileOnDevice(true, " file is download body Perameter, idtender" + mapFooder.get(UtillClasses.idtender)
                            + " docId :" + mapFooder.get(UtillClasses.DocId) + ", json: " + mapFooder.get(UtillClasses.json) + ", doc type : " + mapFooder.get(UtillClasses.DocType));
                } catch (IOException e) {
                    e.printStackTrace();
                }*/


                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                bundle.putString(GlobalDataAcess.Class_name, "Download File");
                bundle.putString(GlobalDataAcess.Tender_id, mapDocDetial.get(UtillClasses.idtender));
                bundle.putString(GlobalDataAcess.Documents_id, mapDocDetial.get(UtillClasses.DocId));
                bundle.putString(GlobalDataAcess.Type_documents, mapDocDetial.get(UtillClasses.DocType));
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                return mapFooder;
            }
        };

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        request.setRetryPolicy(new DefaultRetryPolicy(20 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        mRequestQueue.add(request);

    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;

        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    @Override
    public void onClick(View v) {
        switch (1) {
            case 1:
                int position = (Integer) v.getTag();
                downloadFile(position);
                break;
        }
    }

    public void downloadFile(int position) {
        if (position != RecyclerView.NO_POSITION) {
            Map<String, String> mapDocDetail = new HashMap<String, String>();

            mapDocDetail.put(UtillClasses.idtender, TenderDetails.modelTenderDetails.getIdTender());
            mapDocDetail.put(UtillClasses.DocType, modelTenDetailAttachArrayList.get(position).getDocType());
            mapDocDetail.put(UtillClasses.DocId, modelTenDetailAttachArrayList.get(position).getIdAttachment());
            mapDocDetail.put(UtillClasses.DocName, modelTenDetailAttachArrayList.get(position).getNameAttach());
            // mapDocDetail.put(UtillClasses.DocType, modelTenDetailAttachArrayList.get(position).);

            //DemoFileDownload();

            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            filename = modelTenDetailAttachArrayList.get(position).getNameAttach();
            file = new File(path, filename);

            if (file.exists()) {

              /*  try {
                    Logger.getInstance().createFileOnDevice(true, "download file already exists : file name " + path);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                //Toast.makeText(getActivity(), "File is already exist", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(mRecyclerview, getResources().getString(R.string.file_download_sucess), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.open), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //
                                        /*Intent intent = new Intent(Intent.ACTION_VIEW);
                                        Uri uri = Uri.parse( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                                        intent.setDataAndType(uri, "pdf/docx/doc");
                                        startActivity(Intent.createChooser(intent, "Open File"));*/


                                       /* MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                        Intent intent =new Intent(Intent.ACTION_VIEW);
                                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(filename)).substring(1);
                                        intent.setDataAndType(Uri.fromFile(file),mimeType);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        try {
                                            startActivity(intent);
                                        } catch (Exception e) {
                                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }*/
                                try {
                                    GlobalDataAcess.openFile(getActivity(), file);
                                } catch (IOException e) {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.no_handler_), Toast.LENGTH_LONG).show();

                                    e.printStackTrace();
                                }
                            }
                        });

// Changing message text color
                snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            } else {

                if (sessionManager.GetIsOnlyWifi()) {
                    if (GlobalDataAcess.IsWifiEnable(getActivity())) {

                        FIleDownload(mapDocDetail, modelTenDetailAttachArrayList.get(position).getNameAttach());
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.please_conn_wifi), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    FIleDownload(mapDocDetail, modelTenDetailAttachArrayList.get(position).getNameAttach());


                }


            }


        }
    }
}