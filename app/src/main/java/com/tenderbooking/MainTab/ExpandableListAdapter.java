package com.tenderbooking.MainTab;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tenderbooking.Adapter.ExpaTenderSecLevelAdapter;
import com.tenderbooking.FragmentTender;
import com.tenderbooking.HelperClasses.CustExpListView;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.DrawerItemClicked;
import com.tenderbooking.Model.ModelUserFilters;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Abhishek jain on 18-02-2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> hashmapList;
    ArrayList<ModelUserTag> modelUserTagArrayList;
    ArrayList<ModelUserFilters> modelUserFilterArrayList;
    private Context mContext;
    private List<String> mExpandableListTitle;
    //private LinkedHashMap<String, List<String>> mExpandableListDetail;
    private LinkedHashMap<String, LinkedHashMap<String, List<String>>> mExpandableListDetail;
    private ArrayList<String> drawer_array;
    private LayoutInflater mLayoutInflater;
    private String TAG = ExpandableListAdapter.class.getSimpleName();

    //public ExpandableListAdapter(Context context, List<String> expandableListTitle, LinkedHashMap<String, List<String>> expandableListDetail) {
    public ExpandableListAdapter(Context context, List<String> expandableListTitle, LinkedHashMap<String, LinkedHashMap<String, List<String>>> expandableListDetail, LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> hashmapList, ArrayList<ModelUserTag> modelUserTagArrayList, ArrayList<ModelUserFilters> modelUserFilterArrayList) {
        mContext = context;
        mExpandableListTitle = expandableListTitle;
        mExpandableListDetail = expandableListDetail;
        this.hashmapList = hashmapList;
        this.modelUserTagArrayList = modelUserTagArrayList;
        this.modelUserFilterArrayList = modelUserFilterArrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return mExpandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size();
        return 1;
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mExpandableListTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).get(childPosition);
        return null;
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

//        GlobalDataAcess.group_select_advice_main = groupPosition; //comment due to this look like it belong to advice menu

        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.tender_menu_parent_layout, null);
        }


        LinearLayout linearDevider = (LinearLayout) convertView.findViewById(R.id.custlay_div);
        RelativeLayout custlay_rl = (RelativeLayout) convertView.findViewById(R.id.custlay_rl);

        if (groupPosition == 0) ;
        {
            linearDevider.setVisibility(View.VISIBLE);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.custlay_text);
        listTitleTextView.setTypeface(Typeface.DEFAULT_BOLD);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon_navidra);
        final ImageView custlay_openSubMen = (ImageView) convertView.findViewById(R.id.custlay_openSubMen);
        // icon.setImageDrawable(ExpandableListDataSource.expandListImage.get(groupPosition).get(0));

        custlay_openSubMen.setTag("1");
        custlay_rl.setTag("1");
        if (mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size() > 0) {
            custlay_openSubMen.setVisibility(View.VISIBLE);
        } else {
            custlay_openSubMen.setVisibility(View.GONE);
        }


        if (isExpanded) {
            custlay_openSubMen.setImageResource(R.drawable.folder_opened);
        } else {
            custlay_openSubMen.setImageResource(R.drawable.folder_closed);
        }
//        custlay_openSubMen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String type = ((String) v.getTag());
//                if (type.equals("1")){
//                    custlay_openSubMen.setImageResource(R.drawable.folder_opened);
//                    custlay_openSubMen.setTag("2");
//                }else if(type.equals("2") ){
//                    custlay_openSubMen.setImageResource(R.drawable.folder_closed);
//                    custlay_openSubMen.setTag("1");
//                }
//            }
//        });
//        custlay_rl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String type = ((String) v.getTag());
//                if (getChildrenCount(groupPosition)>0)
//                if (type.equals("1")){
//                    custlay_openSubMen.setImageResource(R.drawable.folder_opened);
//                    custlay_openSubMen.setTag("2");
//                }else if(type.equals("2") ){
//                    custlay_openSubMen.setImageResource(R.drawable.folder_closed);
//                    custlay_openSubMen.setTag("1");
//                }
//            }
//        });


        //custlay_openSubMen.setOnClickListener(this);
//        try {
//            icon.setVisibility(View.INVISIBLE);
//            Picasso.with(mContext).load(ExpandableListDataSource.icon[groupPosition]).into(icon);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition_head, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//        final String expandedListText = (String) getChild(groupPosition_head, childPosition);
//        if (convertView == null) {
//            convertView = mLayoutInflater.inflate(R.layout.explist_submenu, null);
//        }
//        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.exp_chil_custtext);
//
//
//        expandedListTextView.setText(expandedListText);
//        return convertView;

        CustExpListView secondExpList = new CustExpListView(mContext);
        secondExpList.setDividerHeight(0);
        ExpaTenderSecLevelAdapter adapter;
        secondExpList.setGroupIndicator(null);


        ArrayList<LinkedHashMap<String, List<String>>> hashmapSubCatArray = hashmapList.get(mExpandableListTitle.get(groupPosition_head));
        final LinkedList<String> childTitle = new LinkedList<>();
        final LinkedHashMap<String, List<String>> childHashMap = new LinkedHashMap<>();

        int subCatArray = hashmapSubCatArray.size();
        ArrayList<String> listvalue = new ArrayList<>();

        for (int k = 0; subCatArray > k; k++) {
            listvalue.clear();
            listvalue = new ArrayList(hashmapSubCatArray.get(k).keySet());
            childTitle.add(listvalue.get(0));
            //childTitle.add(hashmapSubCatArray.get(k).keySet());
            // childTitle.add(setdata);
            //childHashMap.put(childTitle.get(k),hashmapSubCatArray.get(k).get(childTitle.get(k)));
            // childHashMap.put(childTitle.get(k),hashmapSubCatArray.get(k).get(hashmapSubCatArray.get(k).keySet().toString()));
            childHashMap.putAll(hashmapSubCatArray.get(k));
        }

        adapter = new ExpaTenderSecLevelAdapter(mContext, childTitle, childHashMap, modelUserTagArrayList, modelUserFilterArrayList);
        secondExpList.setTag(convertView);
        secondExpList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (GlobalDataAcess.group_selected != -1 && groupPosition_head == 1) { //group_position_head is responsible for the myfilters data
            try {
                if (secondExpList.getCount() >= GlobalDataAcess.group_selected) {
                    secondExpList.expandGroup(GlobalDataAcess.group_selected, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }/*else if(groupPosition_head ==0  ){
            GlobalDataAcess.group_selected = -1;
        }*/

        secondExpList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (groupPosition_head == 1) { // group_position 1 is only for hte group of the filters
                    //GlobalDataAcess.group_selected = groupPosition;

                } else {
                    GlobalDataAcess.group_selected = -1;
                }

                //by ashwini for scrolling problem
                if (groupPosition_head == 0) {
                    //FragmentTender.currentPage=0;
                    //FragmentTender.setLastPage(false);
                }

                if (childTitle.size() > 5) { // taat belog to my tender file
                    if (Home.handler != null) {
                        Home.handler.sendEmptyMessage(11);
                    }
                    Home.drawer.closeDrawer(Gravity.LEFT);
                    DrawerItemClicked drawerItemClicked = new FragmentTender();
                    drawerItemClicked.GetDrawerItemClicked(groupPosition, "Empty");
                    Home.toolbar.setTitle("" + childTitle.get(groupPosition));
                    GlobalDataAcess.titleString = childTitle.get(groupPosition);
                } else { // belong to myFilter
                    // TODO: 26-09-2017 find child of that filter and then close drawer
                }
                String child_value = childTitle.get(groupPosition);
                Log.e(TAG, "selected child value is :" + child_value);

                return false;
            }

        });
        secondExpList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (Home.handler != null) {
                    Home.handler.sendEmptyMessage(11);
                }

                String child_value = childTitle.get(groupPosition);
                String child_child_value = childHashMap.get(childTitle.get(groupPosition)).get(childPosition);
                Log.e(TAG, "group name is :" + child_value + " and child value is :" + child_child_value);
                return false;
            }
        });
       /*if (groupPosition==2){

       }*/


        return secondExpList;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.custlay_openSubMen:
                //  Toast.makeText(mContext, "Click on list", Toast.LENGTH_SHORT).show();
        }

    }
}
