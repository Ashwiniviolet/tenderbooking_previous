package com.tenderbooking.MainTab;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.About;
import com.tenderbooking.FontSettings;
import com.tenderbooking.FragmentTender;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.DrawerItemClicked;
import com.tenderbooking.Login;
import com.tenderbooking.Model.Model_about_page;
import com.tenderbooking.NotificationSettings;
import com.tenderbooking.PrivacyPolicy;
import com.tenderbooking.R;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class FragmentMore extends Fragment {
    // SessionManager sessionManager;
    public static final String TAG = FragmentMore.class.getSimpleName();
    AlertDialog mAlertDialog;
    AlertDialog.Builder builder;
    TextView logout, selected_app_lang;
    ImageView share_image, setting_twitter, setting_facebook, setting_googlePlust, more_noti_iconImg;

    LinearLayout frag_more_about, sett_termcondi, fragmore_feedbackLL, more_rateApp, dndNotification, more_noti_sound, logout_LL, sett_app_langLL;
    String message = "Download app from  https://play.google.com/store/apps/details?id=com.tenderbooking";
    Switch more_wifi_enable_swich;

    //noti_update
    RadioButton radio_day_one, radio_day_two, radio_day_three, radio_day_four, radio_day_five, radio_day_six, radio_day_seven;
    RadioGroup noti_upd_RadioGrp;
    Button noti_day_canBtn, noti_day_updBtn;
    Switch noti_updSwitch;


    //font size
    AlertDialog mAlertDialogfont;
    AlertDialog.Builder builderfont;
    RadioGroup radioGroupTExt;
    RadioButton radioTiny, radiosmall, radioMedium, radioLarge, radioHuge;
    SessionManager sessionManager;
    Button textsize_cancelBtn;

    //app lang
    AlertDialog mAlertDialogAppLang;
    AlertDialog.Builder builderAppLang;
    RadioGroup radioGroupAppLang;
    RadioButton radioEnglish, radioSerbian, radioBosnian;
    Button applang_cancelBtn;
    private int updation_duration;
    private int updation_type;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        int fontsize = sessionManager.GetSizeTheme();
        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 120, 6);
        switch (fontsize) {
            case 1:
                getActivity().setTheme(R.style.AppTheme_Small_Noactionbar);
                break;
            case 2:
                getActivity().setTheme(R.style.AppTheme_medium_Noactionbar);
                break;
            case 3:
                getActivity().setTheme(R.style.AppTheme_Large_Noactionbar);
                break;
        }

        View view = inflater.inflate(R.layout.fragment_more_screen, container, false);

        frag_more_about = (LinearLayout) view.findViewById(R.id.frag_more_about);
        sett_app_langLL = (LinearLayout) view.findViewById(R.id.sett_app_langLL);
        sett_termcondi = (LinearLayout) view.findViewById(R.id.sett_termcondi);
        more_rateApp = (LinearLayout) view.findViewById(R.id.more_rateApp);
        more_rateApp = (LinearLayout) view.findViewById(R.id.more_rateApp);
        //share_image = (ImageView)view.findViewById(R.id.share_image);
        setting_twitter = (ImageView) view.findViewById(R.id.setting_twitter);
        setting_facebook = (ImageView) view.findViewById(R.id.setting_facebook);
        setting_googlePlust = (ImageView) view.findViewById(R.id.setting_googleplus);
        fragmore_feedbackLL = (LinearLayout) view.findViewById(R.id.fragmore_feedbackLL);
        more_wifi_enable_swich = (Switch) view.findViewById(R.id.more_wifi_enable_swich);
        more_noti_iconImg = (ImageView) view.findViewById(R.id.more_noti_iconImg);
        more_noti_sound = (LinearLayout) view.findViewById(R.id.more_noti_sound);
        logout_LL = (LinearLayout) view.findViewById(R.id.logout_LL);
        selected_app_lang = (TextView) view.findViewById(R.id.selected_app_lang);


        // GetAboutPage("tc");

        //setup wifi switch
        if (sessionManager.GetIsOnlyWifi()) {
            more_wifi_enable_swich.setChecked(true);
        } else {
            more_wifi_enable_swich.setChecked(false);
        }

        more_wifi_enable_swich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sessionManager.SetIsonlyWifi(true);
                } else {
                    sessionManager.SetIsonlyWifi(false);
                }
            }
        });


        //rate app
        more_rateApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.unable_find_market_app), Toast.LENGTH_LONG).show();
                }
            }
        });

        //opne app lang dialog

        sett_app_langLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager != null)
                    Log.e(TAG, "value of the app language is " + sessionManager.getAppLanguage() + " app language name is " + sessionManager.getAppLangName());
                setAppLangDialog(sessionManager.getAppLanguage());
            }
        });


        switch (sessionManager.getAppLanguage()) {
            case 1:

                selected_app_lang.setText("UK");
                break;
            case 2:
                selected_app_lang.setText("Srbija");
                break;
            case 3:
                selected_app_lang.setText("Bosna i Hercegovina");
                break;
        }

        //notification sound
        if (sessionManager.GetNotiSound()) {
            more_noti_iconImg.setImageResource(R.drawable.soundonicon);
        } else {
            more_noti_iconImg.setImageResource(R.drawable.soundofficon);
        }

        more_noti_sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.GetNotiSound()) {
                    more_noti_iconImg.setImageResource(R.drawable.soundofficon);
                    sessionManager.SetNotiSound(false);
                } else {
                    more_noti_iconImg.setImageResource(R.drawable.soundonicon);
                    sessionManager.SetNotiSound(true);
                }
            }
        });

        logout_LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // sessionManager.GetLogout();
                Log.e("Logging out : ", "Local Data About To Clear");
                sessionManager.clearUsingKey();
                //DataBaseHalper dataBaseHalper = new DataBaseHalper(getActivity());
                //dataBaseHalper.getDeleteAllTable();
                FragmentTender.filteredArraylist.clear();
                FragmentTender.adapter.notifyDataSetChanged();
                Fragment_help.modelHelpListArrayList.clear();
                Fragment_help.adapter.notifyDataSetChanged();
                Fragment_advice.modelAdviceListList.clear();
                Fragment_advice.adviceDetailAdapter.notifyDataSetChanged();
                Log.e("Logging out : ", "Local Data Cleared");


// ye wala aapne kiya tha .ha
                //ye konsa point ke liye hai?
                // logout ke baad me default language login screen pe serbian aaye uske liye hi shyad ye new walli list me ho gaya hai . aapne ye resolve kar diya hai.. ha ye kr diya maine
                //ok baki ke liye maine list me update kar diya hai maine aapko mail kar diya ahi . ok good night . gud nyt and bye bye
                sessionManager.setUserLang("serbian");
                sessionManager.setApplanguage(2, "sr");
                Locale locale = new Locale("sr");
                locale.setDefault(locale);
                Configuration configuration = new Configuration();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

//                config.setLocale(locale);
//                Context context = createConfigurationContext(config);
//                Resources resources = context.getResources();
                    Resources resources = getResources();
                    configuration.setLocale(locale);
                    getContext().createConfigurationContext(configuration);

                } else {
                    configuration.locale = locale;
                    getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                }

//                configuration.locale = locale; // hide and upper metod is used
//                //configuration.setLocale(locale);
//                getActivity().getBaseContext().getResources().updateConfiguration(configuration,
//                        getActivity().getBaseContext().getResources().getDisplayMetrics());


                AppController.getmInstance().onConfigurationChanged(configuration);
                sessionManager.setAppLangChange(true);


                DrawerItemClicked drawerItemClicked = new FragmentTender();
                drawerItemClicked.GetDrawerItemClicked(0, "Empty");
                GlobalDataAcess.titleString = getResources().getString(R.string.active);
                startActivity(new Intent(getActivity(), Login.class));
                Home.toolbar.setTitle("" + getResources().getString(R.string.active));
                getActivity().recreate();
                getActivity().finish();
            }
        });


        setting_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tweetIntent = new Intent(Intent.ACTION_SEND);
                tweetIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.download_app_from) + "https://play.google.com/store/apps/details?id=com.tenderbooking");
                tweetIntent.setType("text/plain");

                PackageManager packManager = getActivity().getPackageManager();
                List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolvedInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                        tweetIntent.setClassName(
                                resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(tweetIntent);
                } else {
                    Intent i = new Intent();
                    i.putExtra(Intent.EXTRA_TEXT, message);
                    i.setAction(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
                    startActivity(i);
                    Toast.makeText(getActivity(), getResources().getString(R.string.twiter_not_found), Toast.LENGTH_LONG).show();
                }
            }

            private String urlEncode(String s) {
                try {
                    return URLEncoder.encode(s, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Log.wtf(TAG, "UTF-8 should always be supported", e);
                    return "";
                }
            }
        });

        setting_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tweetIntent = new Intent(Intent.ACTION_SEND);
                tweetIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.download_app_from) + "https://play.google.com/store/apps/details?id=com.tenderbooking");
                tweetIntent.setType("text/plain");

                PackageManager packManager = getActivity().getPackageManager();
                List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolvedInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.Facebook.katana")) {
                        tweetIntent.setClassName(
                                resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(tweetIntent);
                } else {
                    if (logout != null)
                        Snackbar.make(logout, getResources().getString(R.string.app_not_avalible), Snackbar.LENGTH_SHORT).show();
                    /*Intent i = new Intent();
                    i.putExtra(Intent.EXTRA_TEXT, message);
                    i.setAction(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
                    startActivity(i);
                    Toast.makeText(getActivity(), "Twitter app isn't found", Toast.LENGTH_LONG).show();
             */
                }
            }

            private String urlEncode(String s) {
                try {
                    return URLEncoder.encode(s, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Log.wtf(TAG, "UTF-8 should always be supported", e);
                    return "";
                }
            }

        });


        setting_googlePlust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tweetIntent = new Intent(Intent.ACTION_SEND);
                tweetIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.download_app_from) + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName());
                tweetIntent.setType("text/plain");

                PackageManager packManager = getActivity().getPackageManager();
                List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolvedInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.google.android.apps.plus")) {
                        tweetIntent.setClassName(
                                resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(tweetIntent);
                } else {
           /* Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, message);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
            startActivity(i);*/
                    Toast.makeText(getActivity(), getResources().getString(R.string.goo_app_not_found), Toast.LENGTH_LONG).show();
                }
            }

            private String urlEncode(String s) {
                try {
                    return URLEncoder.encode(s, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Log.wtf(TAG, "UTF-8 should always be supported", e);
                    return "";
                }
            }

        });


        logout = (TextView) view.findViewById(R.id.logout);
        /*logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Login.class));
            }
        });*/



        /*TextView privacy=(TextView)view.findViewById(R.id.privacyPage);
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAboutPage("pp",1);

            }
        });*/

        LinearLayout updateLogin = (LinearLayout) view.findViewById(R.id.update_login);
        updateLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUpdateLoginDialog();
            }
        });

        LinearLayout clearCache = (LinearLayout) view.findViewById(R.id.clearCache);
        clearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showClearCacheDialog();
            }
        });


        LinearLayout cachingInterval = (LinearLayout) view.findViewById(R.id.cachingInterval);
        cachingInterval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showClearCacheIntervalDialog();
            }
        });


        LinearLayout tenderUpdatePeriod = (LinearLayout) view.findViewById(R.id.tenderUpdatePeriod);
        tenderUpdatePeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTenderNotificationPeriod();
            }
        });


        LinearLayout fontSettings = (LinearLayout) view.findViewById(R.id.fontSettings);
        fontSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // fontSettingDialog();
                showRadioButtonDialog(0, sessionManager.GetSizeTheme());
            }
        });

        dndNotification = (LinearLayout) view.findViewById(R.id.dndNotification);
        dndNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dndDialog();
            }
        });


        frag_more_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), About.class));
            }
        });

        sett_termcondi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetAboutPage("tc", 1);
            }
        });

        fragmore_feedbackLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // GlobalDataAcess.getDatabaseBackup();

                Log.i("Send email", "");
                String[] TO = {""};
                String[] CC = {""};

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:podrska@boljibiznis.com"));


                //emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{getResources().getString(R.string.email_address_feedback)});
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_subject));
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                try {

                    startActivity(emailIntent);
                    //startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_mail)));
                    Log.i(TAG, "finsih mailing");
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_client_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void dndDialog() {
        startActivity(new Intent(getActivity(), NotificationSettings.class));
    }

    public void fontSettingDialog() {
        startActivity(new Intent(getActivity(), FontSettings.class));
    }


    private void setAppLangDialog(int value) {
        builderAppLang = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        mAlertDialogAppLang = builderAppLang.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.app_lang_layout, null, false);
        radioGroupAppLang = (RadioGroup) view.findViewById(R.id.radioGroupAppLang);
        radioEnglish = (RadioButton) view.findViewById(R.id.radioEnglish);
        radioSerbian = (RadioButton) view.findViewById(R.id.radioSerbian);
        radioBosnian = (RadioButton) view.findViewById(R.id.radioBosnian);
        applang_cancelBtn = (Button) view.findViewById(R.id.applang_cancelBtn);

        switch (value) {

            case 1:
                radioEnglish.setChecked(true);
                radioSerbian.setText("Serbian");
                radioBosnian.setText("Bosnia and Herzegovina");
                Fragment_help.modelHelpListArrayList.clear();
                Fragment_help.adapter.notifyDataSetChanged();
                Fragment_help.languageChanged = true;
                break;
            case 2:
                radioSerbian.setText("Srbija");
                radioBosnian.setText("Bosna i Hercegovina");
                radioSerbian.setChecked(true);
                Fragment_help.modelHelpListArrayList.clear();
                Fragment_help.adapter.notifyDataSetChanged();
                Fragment_help.languageChanged = true;
                break;
            case 3:
                radioSerbian.setText("Srbija");
                radioBosnian.setText("Bosna i Hercegovina");
                radioBosnian.setChecked(true);
                Fragment_help.modelHelpListArrayList.clear();
                Fragment_help.adapter.notifyDataSetChanged();
                Fragment_help.languageChanged = true;
                break;

        }

        applang_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialogAppLang.cancel();
            }
        });

        radioGroupAppLang.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                String lang = "";

                String[] tenderMenu = getResources().getStringArray(R.array.drawer_types);
                //  String [] adviceMenu = getResources().getStringArray(R.array.advi_drawer);
                // for drawer we should check with only two status that will be best for
                // checking
                int counter_tender = 0;
                int counter_advice_menu = -1;

                for (String s : tenderMenu) {
                    if (GlobalDataAcess.titleString.equalsIgnoreCase(s)) {
                        break;
                    }
                    counter_tender++;
                }

                if (sessionManager.GetAdviceToolbarTitle().equalsIgnoreCase(getResources().getString(R.string.myquestion))) {
                    counter_advice_menu = 0;
                } else if (sessionManager.GetAdviceToolbarTitle().equalsIgnoreCase(getResources().getString(R.string.news))) {
                    counter_advice_menu = 1;

                }

                switch (checkedId) {

                    case R.id.radioEnglish:


                        lang = "en";

                        sessionManager.setUserLang("english");
                        sessionManager.setApplanguage(1, "en");

                        String[] menu_array = getResources().getStringArray(R.array.drawer_types_english);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array[counter_tender];

                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_english));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_english));
                        }

                        break;

                    case R.id.radioSerbian:
                        lang = "sr";

                        sessionManager.setUserLang("serbian");
                        sessionManager.setApplanguage(2, "sr");

                        String[] menu_array_serbian = getResources().getStringArray(R.array.drawer_types_serbian);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array_serbian[counter_tender];

                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));
                        }

                        break;

                    case R.id.radioBosnian:
                        lang = "bs";
                        sessionManager.setUserLang("bosnian");
                        sessionManager.setApplanguage(3, "bs");

                        String[] menu_array_bosnian = getResources().getStringArray(R.array.drawer_types_bosnian);
                        if (counter_tender <= 8)
                            GlobalDataAcess.titleString = menu_array_bosnian[counter_tender];

                        if (counter_advice_menu != -1 && counter_advice_menu == 0) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.myquestion_serbian));
                        } else if (counter_advice_menu != -1 && counter_advice_menu == 1) {
                            sessionManager.SetAdviceToolbarTitle(getResources().getString(R.string.news_serbian));
                        }

                        break;
                }
                Log.e(TAG, " value of the language is " + lang);
                Locale locale = new Locale(lang);
                locale.setDefault(locale);
                Configuration configuration = new Configuration();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

//                config.setLocale(locale);
//                Context context = createConfigurationContext(config);
//                Resources resources = context.getResources();
                    Resources resources = getResources();
                    configuration.setLocale(locale);
                    getContext().createConfigurationContext(configuration);

                } else {
                    configuration.locale = locale;
                    getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                }

//                configuration.locale = locale; // hide and upper metod is used
//                //configuration.setLocale(locale);
//                getActivity().getBaseContext().getResources().updateConfiguration(configuration,
//                        getActivity().getBaseContext().getResources().getDisplayMetrics());


                AppController.getmInstance().onConfigurationChanged(configuration);


                mAlertDialogAppLang.cancel();

                sessionManager.setAppLangChange(true);
                getActivity().recreate();

                /*String [] tender_menu_after_change = getResources().getStringArray(R.array.drawer_types);
                GlobalDataAcess.titleString = tender_menu_after_change[counter_tender];*/
            }
        });

        mAlertDialogAppLang.setView(view);
        mAlertDialogAppLang.show();
    }

    private void showRadioButtonDialog(final int position, final int size) {

        builderfont = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        mAlertDialogfont = builderfont.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.textsize_radio_button, null, false);
        radioGroupTExt = (RadioGroup) view.findViewById(R.id.radioGroupTExt);
        radioTiny = (RadioButton) view.findViewById(R.id.radioTiny);
        radiosmall = (RadioButton) view.findViewById(R.id.radiosmall);
        radioMedium = (RadioButton) view.findViewById(R.id.radioMedium);
        radioLarge = (RadioButton) view.findViewById(R.id.radioLarge);
        radioHuge = (RadioButton) view.findViewById(R.id.radioHuge);
        textsize_cancelBtn = (Button) view.findViewById(R.id.textsize_cancelBtn);


        switch (size) {

            case 1:
                radiosmall.setChecked(true);
                break;
            case 2:
                radioMedium.setChecked(true);
                break;
            case 3:
                radioLarge.setChecked(true);
                break;

        }


        textsize_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialogfont.dismiss();
            }
        });


        radioGroupTExt.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {


                switch (checkedId) {
                    case R.id.radiosmall:
                        sessionManager.SetSizeTheme(1);
                        getActivity().getApplication().setTheme(R.style.AppTheme_Small_Noactionbar);
                        getActivity().recreate();
                        break;

                    case R.id.radioMedium:
                        sessionManager.SetSizeTheme(2);
                        getActivity().getApplication().setTheme(R.style.AppTheme_medium_Noactionbar);
                        getActivity().recreate();
                        break;

                    case R.id.radioLarge:
                        sessionManager.SetSizeTheme(3);
                        getActivity().getApplication().setTheme(R.style.AppTheme_Large_Noactionbar);
                        getActivity().recreate();
                        break;

                }
                mAlertDialogfont.dismiss();



                /*Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mAlertDialog.dismiss();
                               adapter.notifyDataSetChanged();
                            }
                        },2500);
                    }
                };
                runnable.run();*/
            }
        });

        mAlertDialogfont.setView(view);


        mAlertDialogfont.show();
    }

    public void updateTenderNotificationPeriod() {
        builder = new AlertDialog.Builder(getActivity());
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.update_new_tenders_popup, null, false);
        radio_day_one = (RadioButton) view.findViewById(R.id.radio_day_one);
        radio_day_two = (RadioButton) view.findViewById(R.id.radio_day_two);
        radio_day_three = (RadioButton) view.findViewById(R.id.radio_day_three);
        radio_day_four = (RadioButton) view.findViewById(R.id.radio_day_four);
        radio_day_five = (RadioButton) view.findViewById(R.id.radio_day_five);
        radio_day_six = (RadioButton) view.findViewById(R.id.radio_day_six);
        radio_day_seven = (RadioButton) view.findViewById(R.id.radio_day_seven);
        noti_upd_RadioGrp = (RadioGroup) view.findViewById(R.id.noti_upd_RadioGrp);
        noti_day_canBtn = (Button) view.findViewById(R.id.noti_day_canBtn);
        noti_day_updBtn = (Button) view.findViewById(R.id.noti_day_updBtn);
        noti_updSwitch = (Switch) view.findViewById(R.id.noti_updSwitch);

       /* updation_duration = 1000* 60 * 15 ;
        updation_type = 1;*/

        updation_duration = sessionManager.GetNotiTimeInterval();
        updation_type = sessionManager.GetNotiTimeIntervalType();

        if (sessionManager.GetNotiEnable()) {
            noti_updSwitch.setChecked(true);
        } else {
            noti_updSwitch.setChecked(false);
        }

        switch (sessionManager.GetNotiTimeIntervalType()) {
            case 1:
                radio_day_one.setChecked(true);
                break;
            case 2:
                radio_day_two.setChecked(true);
                break;
            case 3:
                radio_day_three.setChecked(true);
                break;
            case 4:
                radio_day_four.setChecked(true);
                break;
            case 5:
                radio_day_five.setChecked(true);
                break;
            case 6:
                radio_day_six.setChecked(true);
                break;
            case 7:
                radio_day_seven.setChecked(true);
                break;
        }

        noti_updSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sessionManager.SetNotiEnable(true);
                } else {
                    sessionManager.SetNotiEnable(false);
                }
            }
        });


        noti_day_canBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.cancel();
            }
        });

        //1000*60*60*24*7;
        noti_upd_RadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radio_day_one:
                        updation_duration = 1000 * 60 * 15;
                        updation_type = 1;
//                        sessionManager.SetNotiTimeInte(1000*60*15,1);

                        break;

                    case R.id.radio_day_two:
                        updation_duration = 1000 * 60 * 30;
                        updation_type = 2;
//                        sessionManager.SetNotiTimeInte(1000*60*30,2);
                        break;

                    case R.id.radio_day_three:
                        updation_duration = 1000 * 60 * 60;
                        updation_type = 3;
//                        sessionManager.SetNotiTimeInte(1000*60*60,3);
                        break;

                    case R.id.radio_day_four:
                        updation_duration = 1000 * 60 * 60 * 2;
                        updation_type = 4;
//                        sessionManager.SetNotiTimeInte(1000*60*60*2,4);
                        break;

                    case R.id.radio_day_five:
                        updation_duration = 1000 * 60 * 60 * 6;
                        updation_type = 5;
//                        sessionManager.SetNotiTimeInte(1000*60*60*6,5);
                        break;

                    case R.id.radio_day_six:
                        updation_duration = 1000 * 60 * 60 * 12;
                        updation_type = 6;
//                        sessionManager.SetNotiTimeInte(1000*60*60*12,6);
                        break;
                    case R.id.radio_day_seven:
                        updation_duration = 1000 * 60 * 60 * 24;
                        updation_type = 7;
//                        sessionManager.SetNotiTimeInte(1000*60*60*24,7);
                }

            }
        });

        noti_day_updBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.SetNotiTimeInte(updation_duration, updation_type);
                sessionManager.SetIsNotiFirst(true);
                GlobalDataAcess.resetAlarm(getActivity(), updation_duration);
                mAlertDialog.hide();
            }
        });
        mAlertDialog.setView(view);


        mAlertDialog.show();
    }

    public void showClearCacheIntervalDialog() {
        RadioButton radio_day_one, radio_day_two, radio_day_three, radio_day_four, radio_day_five, radio_day_six;
        RadioGroup noti_upd_RadioGrp;
        Button noti_day_canBtn, noti_day_updBtn;
        builder = new AlertDialog.Builder(getActivity());
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.radiobtn_dialog_cache, null, false);
        radio_day_one = (RadioButton) view.findViewById(R.id.chache_radio_day_one);
        radio_day_two = (RadioButton) view.findViewById(R.id.chache_radio_day_two);
        radio_day_three = (RadioButton) view.findViewById(R.id.chache_radio_day_three);
        radio_day_four = (RadioButton) view.findViewById(R.id.chache_radio_day_four);
        radio_day_five = (RadioButton) view.findViewById(R.id.chache_radio_day_five);
        radio_day_six = (RadioButton) view.findViewById(R.id.chache_radio_day_six);
        noti_upd_RadioGrp = (RadioGroup) view.findViewById(R.id.chache_upd_RadioGrp);
        noti_day_canBtn = (Button) view.findViewById(R.id.chache_inter_canBtn);
        noti_day_updBtn = (Button) view.findViewById(R.id.chache_inter_UpdBtn);


        switch (sessionManager.getChecheIntervalType()) {
            case 1:
                radio_day_one.setChecked(true);
                break;
            case 2:
                radio_day_two.setChecked(true);
                break;
            case 3:
                radio_day_three.setChecked(true);
                break;
            case 4:
                radio_day_four.setChecked(true);
                break;
            case 5:
                radio_day_five.setChecked(true);
                break;
            case 6:
                radio_day_six.setChecked(true);
                break;
        }


        noti_day_canBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.cancel();
            }
        });

        //1000*60*60*24*7;
        noti_upd_RadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radio_day_one:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 7, 1);
                        break;

                    case R.id.radio_day_two:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 15, 2);
                        break;

                    case R.id.radio_day_three:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 30, 3);
                        break;

                    case R.id.radio_day_four:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 60, 4);
                        break;

                    case R.id.radio_day_five:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 90, 5);
                        break;

                    case R.id.radio_day_six:
                        sessionManager.SetchacheInterval(1000 * 60 * 60 * 24 * 120, 6);
                        break;
                }
            }
        });

        noti_day_updBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.hide();

            }
        });


        mAlertDialog.setView(view);

        mAlertDialog.show();
    }

    public void showUpdateLoginDialog() {
        builder = new AlertDialog.Builder(getActivity());
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.update_login_details_popup, null, false);
        mAlertDialog.setView(view);

        mAlertDialog.show();
    }

    public void showClearCacheDialog() {
        builder = new AlertDialog.Builder(getActivity());
        mAlertDialog = builder.create();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.clearcachedialog, null, false);

        Button checheDia_canBtn = (Button) view.findViewById(R.id.checheDia_canBtn);
        Button checheDia_okBtn = (Button) view.findViewById(R.id.checheDia_okBtn);
        mAlertDialog.setView(view);

        checheDia_canBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.cancel();
            }
        });
        checheDia_okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear chache
                DataBaseHalper dataBaseHalper = new DataBaseHalper(getActivity());
                dataBaseHalper.getDeleteAllTable();
                FragmentTender.filteredArraylist.clear();
                //FragmentTender.adapter.notifyDataSetChanged();
                Fragment_help.modelHelpListArrayList.clear();
                Fragment_help.adapter.notifyDataSetChanged();
                Fragment_advice.modelAdviceListList.clear();
                Fragment_advice.adviceDetailAdapter.notifyDataSetChanged();
                // getActivity().recreate();
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    public void GetAboutPage(final String typePage, final int type) {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();


        final String mURL = UtillClasses.BaseUrl + "get_about_page";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, " user tag " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") == 200) {
                                if (!jsonObject.isNull("data")) {
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    Model_about_page model_about_page = new Model_about_page();
                                    model_about_page.setTitlepage(jsonObjectData.optString("titlePage"));
                                    model_about_page.setTextpage(jsonObjectData.optString("textPage"));

                                    getAboutPageResult(model_about_page, type);

                                } else {
                                    //  progressDialog.hide();
                                    if (logout != null)
                                        Snackbar.make(logout, getResources().getString(R.string.not_new_list_avali), Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                // progressDialog.hide();
                                String messages = jsonObject.optString("message");
                                if (logout != null)
                                    Snackbar.make(logout, "" + messages, Snackbar.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // progressDialog.hide();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.hide();
                //Snackbar.make(logout,"  "+error.getMessage(),Snackbar.LENGTH_SHORT).show();
                if (logout != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        //Log.e(TAG ," Error Network timeout! Try again");
                        // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(logout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetAboutPage(typePage, type);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                        Snackbar snackbar = Snackbar
                                .make(logout, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GetAboutPage(typePage, type);
                                    }
                                });

// Changing message text color
                        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                //mapFooder.put(UtillClasses.lastCheck,sessionManager.getLastCheck());
                mapFooder.put(UtillClasses.Typepage, typePage + "");
                //sessionManager.setLastCheck("0000-00-00 00:00:00");
                /*String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());
                Log.e(TAG, " Header time and date "+date_time);
                sessionManager.setLastCheck(date_time);*/

                return mapFooder;
            }
        };
        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(200000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        stringRequest.setShouldCache(false);
        AppController.getmInstance().addToRequestQueue(stringRequest);

    }

    public void getAboutPageResult(Model_about_page model_about_page, int typeuser) {

        if (typeuser == 1) {
            Intent intent = new Intent(getActivity(), PrivacyPolicy.class);
            intent.putExtra(GlobalDataAcess.About_title, model_about_page.getTitlepage());
            intent.putExtra(GlobalDataAcess.About_text, model_about_page.getTextpage());
            intent.putExtra(GlobalDataAcess.About_usertype, typeuser);
            startActivity(intent);
        }

    }
}
