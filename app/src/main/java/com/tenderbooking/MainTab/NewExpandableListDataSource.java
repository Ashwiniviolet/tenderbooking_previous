package com.tenderbooking.MainTab;

import android.app.Activity;
import android.content.Context;

import com.tenderbooking.Model.ModelGetAdviceFolder;
import com.tenderbooking.Model.ModelUserFilters;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by namok on 24-09-2017.
 */

public class NewExpandableListDataSource {
    /**
     * Returns fake data of films
     *
     * @param context
     * @return
     */
    public static HashMap<String, List<String>> expandListImage;

    public static int icon[] = {R.drawable.active, R.drawable.secondary, R.drawable.havevalue, R.drawable.tender_with_flag, R.drawable.hot_tenders, R.drawable.procurement_plan, R.drawable.spam_tenders, R.drawable.expiredtenders, R.drawable.deleteicon, R.drawable.tag, R.drawable.categories, R.drawable.countries, R.drawable.cities, R.drawable.buyers};
    public static int iconAdvice[] = {R.drawable.my_questions, R.drawable.adviser, R.drawable.law, R.drawable.newsletter};


    public static ArrayList<String> iconarray = new ArrayList<>();


    // LinkedHashMap<String,LinkedHashMap<String, List<String>>> f;

    // public static LinkedHashMap<String, List<String>> getData(Context context, ArrayList<ModelUserTag> modelUserTagArrayList, ArrayList<ModelUserFilters> modelUserFiltersArrayList) {
    public static LinkedHashMap<String, LinkedHashMap<String, List<String>>> getData(Context context, ArrayList<ModelUserTag> modelUserTagArrayList, ArrayList<ModelUserFilters> modelUserFiltersArrayList) {
        LinkedHashMap<String, List<String>> expandableListData = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> expandableListData_myFilter = new LinkedHashMap<>();
        expandListImage = new HashMap<>();


        LinkedHashMap<String, LinkedHashMap<String, List<String>>> new_tender_menu = new LinkedHashMap<>();


        List<String> filmGenres = Arrays.asList(context.getResources().getStringArray(R.array.drawer_types));


        List<String> Active = Arrays.asList(context.getResources().getStringArray(R.array.active));
        List<String> Secondary = Arrays.asList(context.getResources().getStringArray(R.array.secondary));
        List<String> Have_Value = Arrays.asList(context.getResources().getStringArray(R.array.have_value));
        List<String> Tender_with_Flagtive = Arrays.asList(context.getResources().getStringArray(R.array.tender_with_flag));
        List<String> hot_tender = Arrays.asList(context.getResources().getStringArray(R.array.hot_tender));
        List<String> Procurement_plan = Arrays.asList(context.getResources().getStringArray(R.array.procurement_plant));
        List<String> spam = Arrays.asList(context.getResources().getStringArray(R.array.spam_tenders));
        List<String> expired_tenders = Arrays.asList(context.getResources().getStringArray(R.array.expired_tenders));
        List<String> deleted_tenders = Arrays.asList(context.getResources().getStringArray(R.array.deleted_tenders));


        List<String> categories = new ArrayList<>();
        List<String> countries = new ArrayList<>();
        List<String> cities = new ArrayList<>();
        List<String> buyers = new ArrayList<>();

        categories.clear();
        countries.clear();
        cities.clear();
        buyers.clear();

        List<String> tag = new ArrayList<>();

        for (int i = 0; i < modelUserTagArrayList.size(); i++) {
            tag.add(i, modelUserTagArrayList.get(i).getTagText());
        }
        //List<String> tag = Arrays.asList(context.getResources().getStringArray(R.array.tag));

        for (int j = 0; j < modelUserFiltersArrayList.size(); j++) {

            switch (modelUserFiltersArrayList.get(j).getFilterType()) {
                case "city":
                    cities.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "country":
                    countries.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "buyer":
                    buyers.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "category":
                    categories.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
            }
        }

        List<String> active_icon = Arrays.asList("R.drawable.star");
        List<String> secondary_icon = Arrays.asList("R.drawable.hollow_star");
        List<String> haveval_icon = Arrays.asList("R.drawable.have_value");
        List<String> tender_icon = Arrays.asList("R.drawable.tender_with_flag");
        List<String> hotten_icon = Arrays.asList("R.drawable.hot_tenders");
        List<String> proce_icon = Arrays.asList("R.drawable.procurement_plan");
        List<String> expten_icon = Arrays.asList("R.drawable.expired_tender");
        List<String> deleten_icon = Arrays.asList("R.drawable.expired_tender");
        List<String> user_icon = Arrays.asList("R.drawable.user_defiled_filters");


        expandableListData.put(filmGenres.get(0), Active);
        expandableListData.put(filmGenres.get(1), Secondary);
        expandableListData.put(filmGenres.get(2), Have_Value);
        expandableListData.put(filmGenres.get(3), Tender_with_Flagtive);
        expandableListData.put(filmGenres.get(4), hot_tender);
        expandableListData.put(filmGenres.get(5), Procurement_plan);
        expandableListData.put(filmGenres.get(6), spam);
        expandableListData.put(filmGenres.get(7), expired_tenders);
        expandableListData.put(filmGenres.get(8), deleted_tenders);
        expandableListData_myFilter.put(filmGenres.get(9), tag);
        expandableListData_myFilter.put(filmGenres.get(10), categories);
        expandableListData_myFilter.put(filmGenres.get(11), countries);
        expandableListData_myFilter.put(filmGenres.get(12), cities);
        expandableListData_myFilter.put(filmGenres.get(13), buyers);
       /* expandableListData.put(filmGenres.get(14), advanced_search);*/

        new_tender_menu.put(context.getString(R.string.my_tenders), expandableListData);
        new_tender_menu.put(context.getString(R.string.my_filters), expandableListData_myFilter);

        return new_tender_menu;
    }

    public static LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> GetTenderListData(Activity mActivity, ArrayList<ModelUserTag> modelUserTagArrayList, ArrayList<ModelUserFilters> modelUserFiltersArrayList) {

        LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> tenderListmainMaparraylist = new LinkedHashMap<>();


        ArrayList<LinkedHashMap<String, List<String>>> tenderListMyTender = new ArrayList<>();
        ArrayList<LinkedHashMap<String, List<String>>> tenderListMyFilter = new ArrayList<>();


        LinkedHashMap<String, List<String>> tenderListActive = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListSecondary = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListHaveValue = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListTenderWithFlag = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListHotTender = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListProcurement = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListSpamTenders = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListExpiredTender = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListDeletedTender = new LinkedHashMap<>();

        LinkedHashMap<String, List<String>> tenderListTag = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListCategories = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListCountries = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListCities = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> tenderListBuyers = new LinkedHashMap<>();

        LinkedHashMap<String, List<String>> hashmapTenderTag = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> hashmapTenderCategories = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> hashmapTenderCountries = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> hashmapTenderCities = new LinkedHashMap<>();
        LinkedHashMap<String, List<String>> hashmapTenderBuyer = new LinkedHashMap<>();

        List<String> tag = new ArrayList<>();

        for (int i = 0; i < modelUserTagArrayList.size(); i++) {
            tag.add(i, modelUserTagArrayList.get(i).getTagText());
        }
        hashmapTenderTag.put(mActivity.getString(R.string.tag), tag);
        // tenderListTag.add(hashmapTenderTag);

        List<String> categories = new ArrayList<>();
        List<String> countries = new ArrayList<>();
        List<String> cities = new ArrayList<>();
        List<String> buyers = new ArrayList<>();

        categories.clear();
        countries.clear();
        cities.clear();
        buyers.clear();

        for (int j = 0; j < modelUserFiltersArrayList.size(); j++) {

            switch (modelUserFiltersArrayList.get(j).getFilterType()) {
                case "city":
                    cities.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "country":
                    countries.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "buyer":
                    buyers.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
                case "category":
                    categories.add(modelUserFiltersArrayList.get(j).getFilterName());
                    break;
            }
        }

        hashmapTenderCities.put(mActivity.getString(R.string.cities), cities);
        hashmapTenderCountries.put(mActivity.getString(R.string.countries), countries);
        hashmapTenderBuyer.put(mActivity.getString(R.string.buyer_nav), buyers);
        hashmapTenderCategories.put(mActivity.getString(R.string.categories), categories);


//        //tender data
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.active),tenderListActive);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.secondary),tenderListSecondary);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.have_value),tenderListHaveValue);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.tender_with_flag),tenderListTenderWithFlag);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.hot_tender),tenderListHotTender);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.procurement_plant),tenderListProcurement);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.spam_tenders),tenderListSpamTenders);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.expired_tenders),tenderListExpiredTender);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.deleted_tenders),tenderListDeletedTender);
//        //filters and tag start
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.tag),tenderListTag);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.cities),tenderListCities);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.countries),tenderListCountries);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.buyer_nav),tenderListBuyers);
//        tenderListmainMaparraylist.put(mActivity.getString(R.string.categories),tenderListCategories);

        List<String> emptyList = new ArrayList<>();

        tenderListActive.put(mActivity.getString(R.string.active), emptyList);
        tenderListSecondary.put(mActivity.getString(R.string.secondary), emptyList);
        tenderListHaveValue.put(mActivity.getString(R.string.have_value), emptyList);
        tenderListTenderWithFlag.put(mActivity.getString(R.string.tender_with_flag), emptyList);
        tenderListHotTender.put(mActivity.getString(R.string.hot_tender), emptyList);
        tenderListProcurement.put(mActivity.getString(R.string.procurement_plant), emptyList);
        tenderListSpamTenders.put(mActivity.getString(R.string.spam_tenders), emptyList);
        tenderListExpiredTender.put(mActivity.getString(R.string.expired_tenders), emptyList);
        tenderListDeletedTender.put(mActivity.getString(R.string.deleted_tenders), emptyList);

        tenderListMyTender.add(tenderListActive);
        tenderListMyTender.add(tenderListSecondary);
        tenderListMyTender.add(tenderListHaveValue);
        tenderListMyTender.add(tenderListTenderWithFlag);
        tenderListMyTender.add(tenderListHotTender);
        tenderListMyTender.add(tenderListProcurement);
        tenderListMyTender.add(tenderListSpamTenders);
        tenderListMyTender.add(tenderListExpiredTender);
        tenderListMyTender.add(tenderListDeletedTender);

        tenderListMyFilter.add(hashmapTenderTag);
        tenderListMyFilter.add(hashmapTenderCategories);
        tenderListMyFilter.add(hashmapTenderCountries);
        tenderListMyFilter.add(hashmapTenderCities);
        tenderListMyFilter.add(hashmapTenderBuyer);

        tenderListmainMaparraylist.put(mActivity.getString(R.string.my_tenders), tenderListMyTender);
        tenderListmainMaparraylist.put(mActivity.getString(R.string.my_filters), tenderListMyFilter);

        return tenderListmainMaparraylist;
    }

    public static LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> GetAdviceMenuData(ArrayList<ModelGetAdviceFolder> modelGetAdviceFolderArrayList, Activity mActiviy) {

        LinkedHashMap<String, LinkedHashMap<String, List<String>>> advicefoldermainMap = new LinkedHashMap<>();
        LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> advicefoldermainMaparraylist = new LinkedHashMap<>();

        ArrayList<LinkedHashMap<String, List<String>>> advsubfolAdvisorlist = new ArrayList<>();
        ArrayList<LinkedHashMap<String, List<String>>> advsubfollawLawlist = new ArrayList<>();

        ArrayList<LinkedHashMap<String, List<String>>> advsubfolUserlist = new ArrayList<>();
        ArrayList<LinkedHashMap<String, List<String>>> advsubfolclipplist = new ArrayList<>();

        LinkedHashMap<String, List<String>> advicefolderSubMap = new LinkedHashMap<>();

       /* List<String> listdata = new ArrayList<>();
        listdata.add("hello ");
        listdata.add("hello 2");
        listdata.add("hello 3");

        advicefolderSubMap.put("data",listdata);

        advicefoldermainMap.put("data",advicefolderSubMap);
        List<String> arraykeyset = new ArrayList(advicefoldermainMap.keySet());
        advicefoldermainMap.get(arraykeyset).keySet();
        advicefolderSubMap.get(arraykeyset.get(0));*/

        //start work

        int size = modelGetAdviceFolderArrayList.size();

        for (int i = 0; i < size; i++) {

            switch (modelGetAdviceFolderArrayList.get(i).getTypeAdvice()) {
                case "adviser":

                    LinkedHashMap<String, List<String>> hashmapAdviceCat = new LinkedHashMap<>();
                    List<String> arraylist = new ArrayList<>();
                    int themeSize = modelGetAdviceFolderArrayList.get(i).getThemeArrayList().size();

                    for (int j = 0; j < themeSize; j++) {
                        arraylist.add(modelGetAdviceFolderArrayList.get(i).getThemeArrayList().get(j).getNameTheme());
                    }
                    hashmapAdviceCat.put(modelGetAdviceFolderArrayList.get(i).getNameCategory(), arraylist);
                    advsubfolAdvisorlist.add(hashmapAdviceCat);
                    break;

                case "law":
                    LinkedHashMap<String, List<String>> hashmapAdviceCatlaw = new LinkedHashMap<>();
                    List<String> arraylistlaw = new ArrayList<>();
                    int themesizelaw = modelGetAdviceFolderArrayList.get(i).getThemeArrayList().size();

                    for (int k = 0; k < themesizelaw; k++) {
                        arraylistlaw.add(modelGetAdviceFolderArrayList.get(i).getThemeArrayList().get(k).getNameTheme());
                    }
                    hashmapAdviceCatlaw.put(modelGetAdviceFolderArrayList.get(i).getNameCategory(), arraylistlaw);
                    advsubfollawLawlist.add(hashmapAdviceCatlaw);
                    break;
            }

        }

        //for the user
        LinkedHashMap<String, List<String>> hashmapAdviceCatUser = new LinkedHashMap<>();
        List<String> arraylistuser = new ArrayList<>();
        hashmapAdviceCatUser.put("", arraylistuser);
        //  advsubfolUserlist.add(hashmapAdviceCatUser);


        //for clipping
        LinkedHashMap<String, List<String>> hashmapAdviceCatClipping = new LinkedHashMap<>();
        List<String> arraylistClip = new ArrayList<>();
        hashmapAdviceCatClipping.put("", arraylistClip);
        // advsubfolclipplist.add(hashmapAdviceCatClipping);


        //uploading the data into the main hashmap

        advicefoldermainMaparraylist.put(mActiviy.getString(R.string.myquestion), advsubfolUserlist);
        advicefoldermainMaparraylist.put(mActiviy.getString(R.string.advices_replies), advsubfolAdvisorlist);
        advicefoldermainMaparraylist.put(mActiviy.getString(R.string.regulation), advsubfollawLawlist);
        advicefoldermainMaparraylist.put(mActiviy.getString(R.string.news), advsubfolclipplist);

        return advicefoldermainMaparraylist;
    }
}