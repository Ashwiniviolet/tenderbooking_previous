package com.tenderbooking.MainTab;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.ZoomLinearLayout;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.R;
import com.tenderbooking.TenderDetails;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Vikaash on 2/18/2017.
 */
public class Fragment_summary extends Fragment {


    TextView tender_det_titlemain, tender_det_tenderidlist, tender_det_tenderid, tender_det_publDate, tender_det_expiDate, tender_det_source,
            tender_det_buyer, tender_det_publcDateDet, tender_det_docuUntill, tender_det_expiDateList, tender_det_priDocu, tender_det_estiVal, tender_det_tendwithSmallVal, tender_det_origLang, tender_det_country, tender_det_place, tender_det_nutsCode, tender_det_categories, tender_det_type_of_cont, tender_det_procedure_type, tender_det_document_type, tender_det_assign_criteria, tender_det_additional_info;

    ModelTenderDetails modelTenderDetails;
    private ZoomLinearLayout zoomLinearLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        Initialize(view);
        modelTenderDetails = TenderDetails.modelTenderDetails;
        zoomLinearLayout = (ZoomLinearLayout) view.findViewById(R.id.zoom_layout);
        tender_det_tenderid.setText(modelTenderDetails.getIdTender());
        tender_det_tenderid.setSelected(true);
        tender_det_tenderidlist.setText(modelTenderDetails.getIdTender());

        String assigndatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getAssignedTender().split(" ");
            assigndatevalue = assigndate[0];
        } catch (Exception e) {
            assigndatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }
        // calculating expiry date
        String expDatevalue = "-";
        String expDatevalueTime = "";
        try {
            String[] assigndate = modelTenderDetails.getDateStop().split(" ");
            expDatevalue = assigndate[0];
            expDatevalueTime = assigndate[1].toString().substring(0, 5);
        } catch (Exception e) {
            expDatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }

        tender_det_publDate.setText(GlobalDataAcess.ConvertedDate(assigndatevalue));

        tender_det_expiDate.setText(GlobalDataAcess.ConvertedDate(expDatevalue));


        if (GlobalDataAcess.ConvertedDate(expDatevalue).equals("00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals("0000-00-00.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 0000-00-00.")) {
            tender_det_expiDate.setText("-");
        } else {
            tender_det_expiDate.setText(GlobalDataAcess.ConvertedDate(expDatevalue));
        }


        tender_det_titlemain.setText(modelTenderDetails.getTitleTender().length() == 0 ? "-" : modelTenderDetails.getTitleTender());

        tender_det_source.setText(modelTenderDetails.getSourcetender().length() == 0 ? "-" : modelTenderDetails.getSourcetender());
        tender_det_buyer.setText(modelTenderDetails.getNameBuyer().length() == 0 ? "-" : modelTenderDetails.getNameBuyer());

        if ((Long.parseLong(modelTenderDetails.getOptionsTender()) & 2048) > 0)
            tender_det_tendwithSmallVal.setText(getResources().getString(R.string.yes));
        else
            tender_det_tendwithSmallVal.setText(getResources().getString(R.string.no));

        tender_det_origLang.setText(modelTenderDetails.getMainLanguage().length() == 0 ? "-" : modelTenderDetails.getMainLanguage());
        // tender_det_country.setText(modelTenderDetails.getCountryTender()+" - "+modelTenderDetails.getCountryFlagTender());
        tender_det_country.setText(modelTenderDetails.getCountry_name().length() == 0 ? "-" : modelTenderDetails.getCountry_name());

        // tender_det_place.setText(modelTenderDetails.getCityTender());
        tender_det_place.setText(modelTenderDetails.getCity_name().length() == 0 ? "-" : modelTenderDetails.getCity_name());
        tender_det_nutsCode.setText(modelTenderDetails.getIdNUTS().length() == 0 ? "-" : modelTenderDetails.getIdNUTS());
        tender_det_categories.setText(modelTenderDetails.getCategoriesTender().length() == 0 ? "-" : modelTenderDetails.getCategoriesTender());

        tender_det_categories.setSelected(true);
        //check for date 0000-00-00
        if (modelTenderDetails.getDateDocumentation().toString().equals("0000-00-00")) {
            tender_det_docuUntill.setText("-");
        } else {
            tender_det_docuUntill.setText(modelTenderDetails.getDateDocumentation());
        }


        String startdatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getDateStart().split(" ");
            startdatevalue = assigndate[0];
        } catch (Exception e) {
            startdatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }
        // calculating expiry date


        tender_det_publcDateDet.setText(GlobalDataAcess.ConvertedDate(startdatevalue));

        String s = GlobalDataAcess.ConvertedDate(expDatevalue) + " " + expDatevalueTime;
        if (s.equals("00.00.0000. 00:00") || s.equals("0000-00-00.") || s.equals("0000-00-00. 00:00:00") || s.equals(" 00.00.0000. 00:00") || s.equals(" 0000-00-00.") || s.equals(" 0000-00-00. 00:00:00")) {
            tender_det_expiDateList.setText("-");
        } else {
            tender_det_expiDateList.setText(s);
        }


        //set coast

        if (modelTenderDetails.getDocumentationPrice() > 0.0) {

            tender_det_priDocu.setText(NumberFormat.getCurrencyInstance(Locale.GERMANY).format(modelTenderDetails.getDocumentationPrice()).substring(0, NumberFormat.getCurrencyInstance(Locale.GERMANY).format(modelTenderDetails.getDocumentationPrice()).length() - 2) + " " + modelTenderDetails.getDocumentationCurrency());
        } else {
            tender_det_priDocu.setText("-");
        }

        if (modelTenderDetails.getAmountTender() > 0.0) {

            tender_det_estiVal.setText(NumberFormat.getCurrencyInstance(Locale.GERMANY).format(modelTenderDetails.getAmountTender()).substring(0, NumberFormat.getCurrencyInstance(Locale.GERMANY).format(modelTenderDetails.getAmountTender()).length() - 2)
                    + " " + modelTenderDetails.getAmountCurrency());
        } else {
            tender_det_estiVal.setText("-");
        }


        if (modelTenderDetails.getNameAwardCriteria() != null & modelTenderDetails.getNameAwardCriteria().length() == 0) {
            tender_det_assign_criteria.setText("-");
        } else {
            tender_det_assign_criteria.setText(modelTenderDetails.getNameAwardCriteria());
        }

        if (modelTenderDetails.getNameContract() != null & modelTenderDetails.getNameContract().length() == 0) {
            tender_det_type_of_cont.setText("-");
        } else {
            tender_det_type_of_cont.setText(modelTenderDetails.getNameContract());
        }

        if (modelTenderDetails.getNameDocument() != null & modelTenderDetails.getNameDocument().length() == 0) {
            tender_det_document_type.setText("-");
        } else {
            tender_det_document_type.setText(modelTenderDetails.getNameDocument());
        }

        if (modelTenderDetails.getNameProcedure() != null & modelTenderDetails.getNameProcedure().length() == 0) {
            tender_det_procedure_type.setText("-");
        } else {
            tender_det_procedure_type.setText(modelTenderDetails.getNameProcedure());
        }

        if (modelTenderDetails.getUrlTender() != null & modelTenderDetails.getUrlTender().length() == 0) {
            tender_det_additional_info.setText("-");
        } else {
            tender_det_additional_info.setText(modelTenderDetails.getUrlTender());
        }


        tender_det_additional_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tender_det_additional_info.getText().toString()));
                    //  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse( "www.google.com"));
                    startActivity(browserIntent);

//                    Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
//                   // File file = new File(tender_det_additional_info.getText().toString());
//                    File file = new File("www.google.com");
//                    String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
//                    String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//                    myIntent.setDataAndType(Uri.fromFile(file),mimetype);
//                    startActivity(myIntent);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.some_went_wrong), Toast.LENGTH_SHORT).show();
                    //  Toast.makeText(this, "this", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });


        zoomLinearLayout.setClickable(true);
        zoomLinearLayout.setFocusable(true);
        zoomLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomLinearLayout.init(getActivity());
                return false;
            }
        });


        return view;
    }

    public void Initialize(View view) {
     /* TextView  tender_det_titlemain,tender_det_tenderid,tender_det_publDate,tender_det_expiDate,tender_det_summary,tender_det_source,
                tender_det_buyer,tender_det_publcDateDet,tender_det_docuUntill,tender_det_expiDateList,tender_det_estiVal,tender_det_tendwithSmallVal
                ,tender_det_origLang,tender_det_country,tender_det_place,tender_det_nutsCode,tender_det_categories ;
*/
        tender_det_titlemain = (TextView) view.findViewById(R.id.tender_det_titlemain);
        tender_det_tenderid = (TextView) view.findViewById(R.id.tender_det_tenderid);
        tender_det_publDate = (TextView) view.findViewById(R.id.tender_det_publDate);
        tender_det_expiDate = (TextView) view.findViewById(R.id.tender_det_expiDate);
        tender_det_tenderidlist = (TextView) view.findViewById(R.id.tender_det_tenderidlist);
        tender_det_tenderid = (TextView) view.findViewById(R.id.tender_det_tenderid);
        tender_det_tenderid = (TextView) view.findViewById(R.id.tender_det_tenderid);
        tender_det_source = (TextView) view.findViewById(R.id.tender_det_source);
        tender_det_buyer = (TextView) view.findViewById(R.id.tender_det_buyer);
        tender_det_publcDateDet = (TextView) view.findViewById(R.id.tender_det_publcDateDet);
        tender_det_docuUntill = (TextView) view.findViewById(R.id.tender_det_docuUntill);
        tender_det_expiDateList = (TextView) view.findViewById(R.id.tender_det_expiDateList);
        tender_det_estiVal = (TextView) view.findViewById(R.id.tender_det_estiVal);
        tender_det_priDocu = (TextView) view.findViewById(R.id.tender_det_priDocu);
        tender_det_tendwithSmallVal = (TextView) view.findViewById(R.id.tender_det_tendwithSmallVal);
        tender_det_origLang = (TextView) view.findViewById(R.id.tender_det_origLang);
        tender_det_country = (TextView) view.findViewById(R.id.tender_det_country);
        tender_det_place = (TextView) view.findViewById(R.id.tender_det_place);
        tender_det_nutsCode = (TextView) view.findViewById(R.id.tender_det_nutsCode);
        tender_det_categories = (TextView) view.findViewById(R.id.tender_det_categories);
        tender_det_type_of_cont = (TextView) view.findViewById(R.id.tender_det_type_of_cont);
        tender_det_procedure_type = (TextView) view.findViewById(R.id.tender_det_procedure_type);
        tender_det_document_type = (TextView) view.findViewById(R.id.tender_det_document_type);
        tender_det_assign_criteria = (TextView) view.findViewById(R.id.tender_det_assign_criteria);
        tender_det_additional_info = (TextView) view.findViewById(R.id.tender_det_additional_info);

    }


}