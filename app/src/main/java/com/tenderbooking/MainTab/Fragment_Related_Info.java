package com.tenderbooking.MainTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tenderbooking.Adapter.TenderLInkAdapter;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.ZoomLinearLayout;
import com.tenderbooking.Model.ModelTenDetailLinks;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.R;
import com.tenderbooking.RecycerItemClickListener;
import com.tenderbooking.TenderDetails;

import java.util.ArrayList;

/**
 * Created by Vikaash on 2/18/2017.
 */
public class Fragment_Related_Info extends Fragment implements View.OnClickListener {

    ArrayList<ModelTenDetailLinks> modelTenDetailLinksArrayList;
    //    PinchRecyclerView mRecyclerview;
    RecyclerView mRecyclerview;
    TextView idtender, titleTender, tender_det_tenderid, tender_det_publDate, tender_det_expiDate;
    TenderLInkAdapter adapter;
    ModelTenderDetails modelTenderDetails;
    SessionManager sessionManager;
    // for list in ll
    TextView tend_link_custlay_title, tend_link_custlay_typeofDoc, tend_link_custlay_pubDate, tend_link_custlay_closDate;
    LinearLayout tend_related_ll;
    private View.OnClickListener onClick;
    private LinearLayout ten_related_list;
    private ZoomLinearLayout zoomLinearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_related_information, container, false);


//        mRecyclerview = (PinchRecyclerView) view.findViewById(R.id.rel_info_recyclerview) ;
        mRecyclerview = (RecyclerView) view.findViewById(R.id.rel_info_recyclerview);
        idtender = (TextView) view.findViewById(R.id.tend_link_idtender);
        titleTender = (TextView) view.findViewById(R.id.tend_link_idtender);

        tender_det_tenderid = (TextView) view.findViewById(R.id.tender_det_tenderid);
        tender_det_publDate = (TextView) view.findViewById(R.id.tender_det_publDate);
        tender_det_expiDate = (TextView) view.findViewById(R.id.tender_det_expiDate);
        ten_related_list = (LinearLayout) view.findViewById(R.id.ten_related_list);
        sessionManager = new SessionManager(getActivity());
        idtender.setText(TenderDetails.modelTenderDetails.getIdTender());
        titleTender.setText(TenderDetails.modelTenderDetails.getTitleTender());

        modelTenDetailLinksArrayList = new ArrayList<>();
        modelTenDetailLinksArrayList = TenderDetails.modelTenderDetails.getLinksArrayList();

        zoomLinearLayout = (ZoomLinearLayout) view.findViewById(R.id.zoom_layout);

        zoomLinearLayout.setClickable(true);
        zoomLinearLayout.setFocusable(true);

        zoomLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomLinearLayout.init(getActivity());
                return false;
            }
        });




        adapter = new TenderLInkAdapter(getActivity(), modelTenDetailLinksArrayList, this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.setHasFixedSize(true);


        mRecyclerview.setAdapter(adapter);
        mRecyclerview.setLayoutFrozen(true);

        mRecyclerview.addOnItemTouchListener(new RecycerItemClickListener(getActivity(), new RecycerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//
            }
        }));

        //
        modelTenderDetails = TenderDetails.modelTenderDetails;

        tender_det_tenderid.setText(modelTenderDetails.getIdTender());


        String assigndatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getAssignedTender().split(" ");
            assigndatevalue = assigndate[0];
        } catch (Exception e) {
            assigndatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }


        // calculating expiry date
        String expDatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getDateStop().split(" ");
            expDatevalue = assigndate[0];
        } catch (Exception e) {
            expDatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }

        tender_det_publDate.setText(GlobalDataAcess.ConvertedDate(assigndatevalue));


        if (GlobalDataAcess.ConvertedDate(expDatevalue).equals("00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals("0000-00-00.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 0000-00-00.")) {
            tender_det_expiDate.setText("-");
        } else {
            tender_det_expiDate.setText(GlobalDataAcess.ConvertedDate(expDatevalue));
        }





        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tend_related_ll:
                int position = (Integer) v.getTag();
                String Tender_id = modelTenDetailLinksArrayList.get(position).getIdTenderLinks();
                sessionManager.SetTenderIdDetails(Tender_id);
                Intent intent = new Intent(getActivity(), TenderDetails.class);
                startActivity(intent);
                break;


        }
    }
}