package com.tenderbooking.MainTab;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Abhishek jain on 08-10-2016.
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTR = new int[]{android.R.attr.listDivider};

    private Drawable mDivider;

    //default divider will be used

    public DividerItemDecoration(Context mContext) {
        final TypedArray styledAttributes = mContext.obtainStyledAttributes(ATTR);
        mDivider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    //custom divider will be used
    public DividerItemDecoration(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        //super.onDraw(c, parent, state);

        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }

    }
}
