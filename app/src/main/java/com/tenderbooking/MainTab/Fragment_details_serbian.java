package com.tenderbooking.MainTab;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.ZoomLinearLayout;
import com.tenderbooking.Model.ModelTenDetalDetails;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.R;
import com.tenderbooking.TenderDetails;

/**
 * Created by Vikaash on 2/18/2017.
 */
public class Fragment_details_serbian extends Fragment {

    ModelTenderDetails modelTenderDetails;
    boolean isDetialsAval = false;
    int detailIndex = 0;
    TextView tender_det_sub_htmlText, tender_det_sub_subTitle, tender_det_sub_expDete, tender_det_sub_pubDate, tender_det_sub_tenderId, tender_det_sub_title;
    private ZoomLinearLayout zoomLinearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_details_serbian, container, false);
        Initialize(view);
        modelTenderDetails = TenderDetails.modelTenderDetails;


        String assigndatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getAssignedTender().split(" ");
            assigndatevalue = assigndate[0];
        } catch (Exception e) {
            assigndatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }
        // calculating expiry date
        String expDatevalue = "-";
        try {
            String[] assigndate = modelTenderDetails.getDateStop().split(" ");
            expDatevalue = assigndate[0];
        } catch (Exception e) {
            expDatevalue = modelTenderDetails.getAssignedTender();
            e.printStackTrace();
        }

        tender_det_sub_pubDate.setText(GlobalDataAcess.ConvertedDate(assigndatevalue));
        if (GlobalDataAcess.ConvertedDate(expDatevalue).equals("00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals("0000-00-00.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 00.00.0000.")
                || GlobalDataAcess.ConvertedDate(expDatevalue).equals(" 0000-00-00.")) {
            tender_det_sub_expDete.setText("-");
        } else {
            tender_det_sub_expDete.setText(GlobalDataAcess.ConvertedDate(expDatevalue));
        }

        //set title and subtitle in corrosponding format

        ModelTenDetalDetails modelTenDetalDetails = new ModelTenDetalDetails();

        /*for (int i = 0;i<modelTenderDetails.getDetailsArrayList().size();i++){
            if (modelTenderDetails.getDetailsArrayList().get(i).getLanguageTender().equals("rs_RS")){
                modelTenDetalDetails = modelTenderDetails.getDetailsArrayList().get(i);
            }
           // i++;
        }
        modelTenDetalDetails.setSubtitleTender("");
        modelTenDetalDetails.setTitleTender("");
        modelTenDetalDetails.setLanguageTender("");
        modelTenDetalDetails.setFlagTender("");
        modelTenDetalDetails.setDescriptionTender("");

       *//* for (int i = 0;i<modelTenderDetails.getDetailsArrayList().size();i++){
            if (modelTenderDetails.getDetailsArrayList().get(i).getLanguageTender().equals("en_EN")){
                // modelTenDetalDetails = modelTenderDetails.getDetailsArrayList().get(i);
                isDetialsAval = true;
                detailIndex = i;
            }

            i++;
        }*//*

        if (isDetialsAval){
            modelTenDetalDetails = modelTenderDetails.getDetailsArrayList().get(detailIndex);
        }else{
            modelTenDetalDetails = modelTenderDetails.getDetailsArrayList().get(0);
        }*/
        //temp change
        modelTenDetalDetails = modelTenderDetails.getDetailsArrayList().get(1);
//temp changes


        tender_det_sub_subTitle.setText(modelTenDetalDetails.getSubtitleTender());
        tender_det_sub_title.setText(modelTenDetalDetails.getTitleTender());
        tender_det_sub_tenderId.setText(modelTenderDetails.getIdTender());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tender_det_sub_htmlText.setText(Html.fromHtml(modelTenDetalDetails.getDescriptionTender(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tender_det_sub_htmlText.setText(Html.fromHtml(modelTenDetalDetails.getDescriptionTender()));
        }


        return view;
    }

    public void Initialize(View view) {
        tender_det_sub_htmlText = (TextView) view.findViewById(R.id.tender_det_sub_htmlText);
        tender_det_sub_subTitle = (TextView) view.findViewById(R.id.tender_det_sub_subTitle);
        tender_det_sub_expDete = (TextView) view.findViewById(R.id.tender_det_sub_expDete);
        tender_det_sub_pubDate = (TextView) view.findViewById(R.id.tender_det_sub_pubDate);
        tender_det_sub_tenderId = (TextView) view.findViewById(R.id.tender_det_sub_tenderId);
        tender_det_sub_title = (TextView) view.findViewById(R.id.tender_det_sub_title);
        // = (TextView)view.findViewById(R.id. );
        tender_det_sub_tenderId.setSelected(true);

        zoomLinearLayout = (ZoomLinearLayout) view.findViewById(R.id.zoom_layout);
        zoomLinearLayout.setClickable(true);
        zoomLinearLayout.setFocusable(true);
        zoomLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomLinearLayout.init(getActivity());
                return false;
            }
        });
    }

}
