package com.tenderbooking.MainTab;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.R;

import java.util.List;


public class AdviceDetailAdapter extends RecyclerView.Adapter<AdviceDetailAdapter.MyViewHolder> {

    public static String TAG = AdviceDetailAdapter.class.getSimpleName();
    Activity mActivity;
    List<ModelAdviceList> modelAdviceListArrayList;
    int textsize;

    public AdviceDetailAdapter(Activity mActivity, List<ModelAdviceList> modelAdviceListArrayList, int textsize) {
        this.mActivity = mActivity;
        this.modelAdviceListArrayList = modelAdviceListArrayList;
        this.textsize = textsize;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.advices_row, parent, false);
        return new AdviceDetailAdapter.MyViewHolder(view);
        // return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//00100 for serbia 00200 for Montenegro , 00300 for  Bosnia and Herzegovina  , 00400 for Croatia
        //laxmikant toast
        // Toast.makeText(mActivity, "advice menu onbind for positon "+position, Toast.LENGTH_SHORT).show();
        // Log.e("laxmikant toast","advice menu onbind for positon "+position );
        try {
            if (modelAdviceListArrayList.size() > 0) {
                // holder.advice_list_question.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
                holder.advice_list_question.setText(modelAdviceListArrayList.get(position).getTitleAdvice());

                // Log.e(TAG,"name of advice is "+modelAdviceListArrayList.get(position).getTitleAdvice()+" status of hte advice is :"+modelAdviceListArrayList.get(position).getStatusAdvice());
                // if (modelAdviceListArrayList.get(position).getStatusAdvice().equals("2")|| modelAdviceListArrayList.get(position).getStatusAdvice().toString().equals("3")){


                if (modelAdviceListArrayList.get(position).getStatusAdvice() != null)

                    //if (( (Integer.parseInt(modelAdviceListArrayList.get(position).getStatusAdvice().trim()) & 2) >0) && (!(modelAdviceListArrayList.get(position).getTypeAdvice().equals("user")))  ){

                    if (modelAdviceListArrayList.get(position).getTypeAdvice().equals("user")) {

                        if (((Integer.parseInt(modelAdviceListArrayList.get(position).getStatusAdvice().trim()) & 4) > 0)/* && (!(modelAdviceListArrayList.get(position).getTypeAdvice().equals("user")))*/) {

                            holder.advice_list_tick_mark.setVisibility(View.VISIBLE);
                            holder.advice_list_astroimg.setVisibility(View.INVISIBLE);
                        } else {
                            holder.advice_list_astroimg.setVisibility(View.INVISIBLE);
                            holder.advice_list_tick_mark.setVisibility(View.INVISIBLE);
                        }

                    } else {
                        if (((Integer.parseInt(modelAdviceListArrayList.get(position).getStatusAdvice().trim()) & 2) > 0)/* && (!(modelAdviceListArrayList.get(position).getTypeAdvice().equals("user")))*/) {

                            holder.advice_list_tick_mark.setVisibility(View.INVISIBLE);
                            holder.advice_list_astroimg.setVisibility(View.VISIBLE);
                        } else {
                            holder.advice_list_tick_mark.setVisibility(View.INVISIBLE);
                            holder.advice_list_astroimg.setVisibility(View.INVISIBLE);
                        }

                    }

                //  Log.e(TAG,"value of the status advice is :"+modelAdviceListArrayList.get(position).getStatusAdvice());

                // holder.advice_list_astroimg.setVisibility(View.VISIBLE);
                /*switch (modelAdviceListArrayList.get(position).getCountryAdvice()){
                    case "00100":
                        holder.advice_list_flag.setImageResource(R.drawable.serbia_flag);
                        break;
                    case "00200":
                        holder.advice_list_flag.setImageResource(R.drawable.montenegro);
                        break;
                    case "00300":
                        holder.advice_list_flag.setImageResource(R.drawable.bosnia_herzegovina);
                        break;
                    case "00400":
                        holder.advice_list_flag.setImageResource(R.drawable.croatia);
                        break;
                }*/

                try {
                    Picasso.with(mActivity).load("https://www.tenderilive.com/images/icons/flags/" + modelAdviceListArrayList.get(position).getCountryFlagAdvice() + "32.png").error(R.drawable.app_icon).error(R.drawable.app_icon).into(holder.advice_list_flag);
                } catch (Exception e) {
                    Picasso.with(mActivity).load(R.drawable.app_icon).into(holder.advice_list_flag);
                    e.printStackTrace();

                }


            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            //laxmikant toast
            //  Toast.makeText(mActivity, "advice menu onbind crash  for positon "+position, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public int getItemCount() {
        return modelAdviceListArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout adcive_rowLL;
        ImageView advice_list_astroimg, advice_list_flag, advice_list_tick_mark;
        TextView advice_list_question;

        public MyViewHolder(View itemView) {
            super(itemView);
            adcive_rowLL = (LinearLayout) itemView.findViewById(R.id.adcive_rowLL);
            advice_list_astroimg = (ImageView) itemView.findViewById(R.id.advice_list_astroimg);
            advice_list_flag = (ImageView) itemView.findViewById(R.id.advice_list_flag);
            advice_list_tick_mark = (ImageView) itemView.findViewById(R.id.advice_list_tick_mark);
            advice_list_question = (TextView) itemView.findViewById(R.id.advice_list_question);

            adcive_rowLL.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.adcive_rowLL) {
               /* Intent intent = new Intent(mActivity, Advices_detail.class);
                int position = getAdapterPosition();
                intent.putExtra(GlobalDataAcess.Ask_advice_typeAdvice,modelAdviceListArrayList.get(position).getTypeAdvice());
                intent.putExtra(GlobalDataAcess.Ask_advice_idAdvice,modelAdviceListArrayList.get(position).getIdAdvice());
                mActivity.startActivity(intent);*/
            }
        }
    }
}
