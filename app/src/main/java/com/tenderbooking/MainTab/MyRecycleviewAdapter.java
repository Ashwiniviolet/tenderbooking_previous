package com.tenderbooking.MainTab;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.squareup.picasso.Picasso;
import com.tenderbooking.Adapter.GridViewAdapter;
import com.tenderbooking.Adapter.TenderListSetTagAdapter;
import com.tenderbooking.FragmentTender;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.InterfaceTendListClick;
import com.tenderbooking.Model.ModelTenderList;
import com.tenderbooking.Model.ModelTenderListIcons;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;
import com.tenderbooking.RecycerItemClickListener;
import com.tenderbooking.TenderDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.tenderbooking.Home.handler;

public class MyRecycleviewAdapter extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public static final String TAG = MyRecycleviewAdapter.class.getSimpleName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    //for tag alertDialog
    public RecyclerView mRecyclerviewTag;
    public AlertDialog mAlertDialog;
    public AlertDialog.Builder build;
    public TenderListSetTagAdapter adapterTag;
    public ArrayList<ModelUserTag> modelTagArrayList;
    public Button setTagCanBtn, setTagUpdBtn;
    public View view_alert;
    public SessionManager sessionManager;
    ArrayList<ModelTenderList> modelTenderListArrayList;
    ArrayList<ModelUserTag> modelUserTagArrayList;
    FlexboxLayoutManager flexboxLayoutManager;
    List<Integer> iconArray;
    List<ModelTenderListIcons> modelTenderListIconsList;
    int textsize;
    DataBaseHalper dataBaseHalper;
    int dpAsPixel;
    SwipeItemRecyclerMangerImpl mItemManager;
    private Activity mActivity;
    private boolean isLoadingAdded = false;

    public MyRecycleviewAdapter(Activity mActivity, ArrayList<ModelTenderList> modelTenderListArrayList, ArrayList<ModelUserTag> modelUserTagArrayList, FlexboxLayoutManager mFlaxlayoutmanager, int textsize) {
        this.mActivity = mActivity;
        this.modelTenderListArrayList = modelTenderListArrayList;
        this.modelUserTagArrayList = modelUserTagArrayList;
        this.flexboxLayoutManager = mFlaxlayoutmanager;
        this.textsize = textsize;
        dataBaseHalper = new DataBaseHalper(mActivity);
        sessionManager = new SessionManager(mActivity);
        float scale = mActivity.getResources().getDisplayMetrics().density;
        dpAsPixel = (int) (10 * scale + 0.5f);
        mItemManager = new SwipeItemRecyclerMangerImpl(this);

    }

    public MyRecycleviewAdapter(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(mActivity);

        switch (viewType) {
            case ITEM:

                viewHolder = new MyRecycleviewAdapter.MyViewholder(inflater.inflate(R.layout.recycleview_custlay, parent, false));
                break;
            case LOADING:
                viewHolder = new MyRecycleviewAdapter.LoadingVH(inflater.inflate(R.layout.loadder_cust_layout, parent, false));
                break;
        }


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
       /* if (position==9){
            holder.recy_custlaymain.setVisibility(View.GONE);
            holder.recy_fooder.setVisibility(View.VISIBLE);
        }else{
            holder.recy_custlaymain.setVisibility(View.VISIBLE);
            holder.recy_fooder.setVisibility(View.GONE);
        }*/
        switch (getItemViewType(position)) {
            case ITEM:


                if (modelTenderListArrayList.size() > 0) {
                    ((MyViewholder) holder).tender_list_icons.removeAllViews();
                    modelTenderListIconsList = new ArrayList<>();
                    iconArray = new ArrayList<>();
                    iconArray.clear();

                    ((MyViewholder) holder).tend_custlay_countnam.setText(modelTenderListArrayList.get(position).getCityBuyer());
                    ((MyViewholder) holder).tend_custlay_purName.setText(modelTenderListArrayList.get(position).getNameBuyer());
                    ((MyViewholder) holder).tend_custlay_tenderfor.setText(modelTenderListArrayList.get(position).getTitleTender());
                    //((MyViewholder)holder).tend_custlay_tenderfor.setText(modelTenderListArrayList.get(position).getNameContract());

          /* ((MyViewholder)holder).tend_custlay_countnam.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
           ((MyViewholder)holder).tend_custlay_purName.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
           ((MyViewholder)holder).tend_custlay_tenderfor.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
           ((MyViewholder)holder).tend_custlay_date.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
*/


                    ((MyViewholder) holder).set_tag.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                    ((MyViewholder) holder).set_flag.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                    ((MyViewholder) holder).set_unread.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                    ((MyViewholder) holder).set_delete.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                    String[] date = modelTenderListArrayList.get(position).getAssignedTender().split(" ");
                    // ((MyViewholder)holder).tend_custlay_date.setText(modelTenderListArrayList.get(position).getAssignedTender());

                    ((MyViewholder) holder).tend_custlay_date.setText(GlobalDataAcess.ConvertedDate(date[0]));

                    if (modelTenderListArrayList.get(position).isReaded()) {
                        ((MyViewholder) holder).tend_custlay_purName.setTypeface(null, Typeface.NORMAL);
                        ((MyViewholder) holder).tend_custlay_tenderfor.setTypeface(null, Typeface.NORMAL);
                        ((MyViewholder) holder).tend_custlay_countnam.setTypeface(null, Typeface.NORMAL);

                    } else {
                        ((MyViewholder) holder).tend_custlay_purName.setTypeface(null, Typeface.BOLD);
                        ((MyViewholder) holder).tend_custlay_tenderfor.setTypeface(null, Typeface.BOLD);
                        ((MyViewholder) holder).tend_custlay_countnam.setTypeface(null, Typeface.BOLD);
                    }

                    try {
                        Picasso.with(mActivity).load("https://www.tenderilive.com/images/icons/flags/" + modelTenderListArrayList.get(position).getCountryFlagTender() + "32.png").error(R.drawable.app_icon).error(R.drawable.app_icon).into(((MyViewholder) holder).tend_custlay_flagimg);
                    } catch (Exception e) {
                        Picasso.with(mActivity).load(R.drawable.app_icon).into(((MyViewholder) holder).tend_custlay_flagimg);
                        e.printStackTrace();
                    }


                    //
                    //if (modelTenderListArrayList.get(position).getStatusTender().equals("8192"))
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getStatusTender()) & Long.parseLong("8192")) > 0))) {
                        //red flag show
                        iconArray.add(R.drawable.tender_flag);
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_flag);
                        modelTenderListIconsList.add(modelTenderListIcons);

                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(R.drawable.tender_flag);
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                        ((MyViewholder) holder).set_flag.setText(mActivity.getResources().getString(R.string.remove_flag));
                    } else {
                        ((MyViewholder) holder).set_flag.setText(mActivity.getResources().getString(R.string.set_flag));
                    }

                    // old concept

           /*//for the tag tender
           String tagTender = modelTenderListArrayList.get(position).getTagTender();
           String tagusername = "";

           //this is not specified if the multiple tag is avalible
           String[] data ;

           data = tagTender.split(",");
           Log.e(TAG,"tender list tag values : "+tagTender.toString());

           if (data.length>1){
               ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
               modelTenderListIcons.setNumber(0);
               //modelTenderListIcons.setColourcode(modelUserTagArrayList.get(i).getColorCSS());
               modelTenderListIcons.setResourcepath(R.drawable.doubletaggedicon);
               modelTenderListIconsList.add(modelTenderListIcons);
               //set icons
               ImageView   mImageview = new ImageView(mActivity);
               mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
               mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
               mImageview.setBackgroundResource(R.drawable.doubletaggedicon);
               ((MyViewholder)holder).tender_list_icons.addView(mImageview);

               LinearLayout linearLayout = new LinearLayout(mActivity);
               linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20,20));
               ((MyViewholder)holder).tender_list_icons.addView(linearLayout);
           }
           else {
               for (int i = 0; i < modelUserTagArrayList.size(); i++) {
                   if (modelUserTagArrayList.get(i).getTagIndex().equals(tagTender)) {
                       //set the css color of hte tag to the data in image in tint not
                       ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                       modelTenderListIcons.setNumber(0);

                       switch(modelUserTagArrayList.get(i).getColorCSS()){

                           case "f7e298":
                               modelTenderListIcons.setResourcepath(R.drawable.yellowtaggedicon);
                               break;
                           case "fdc396":
                               modelTenderListIcons.setResourcepath(R.drawable.orangetaggedicon);
                               break;
                           case "fba3a1":
                               modelTenderListIcons.setResourcepath(R.drawable.redtaggedicon);
                               break;
                           case "f1afec":
                               modelTenderListIcons.setResourcepath(R.drawable.pinktaggedicon);
                               break;
                           case "a492df":
                               modelTenderListIcons.setResourcepath(R.drawable.purpletaggedicon);
                               break;
                           case "a3c088":
                               modelTenderListIcons.setResourcepath(R.drawable.greentaggedicon);
                               break;
                           case "9ed7ff":
                               modelTenderListIcons.setResourcepath(R.drawable.bluetaggedicon);
                               break;
                           case "000000":
                               modelTenderListIcons.setResourcepath(R.drawable.blacktaggedicon);
                               break;
                       }


                       //modelTenderListIcons.setResourcepath(R.drawable.tag_blue);
                       modelTenderListIconsList.add(modelTenderListIcons);
                       //set icons
                       ImageView   mImageview = new ImageView(mActivity);
                       mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                       mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                       mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                       ((MyViewholder)holder).tender_list_icons.addView(mImageview);

                       LinearLayout linearLayout = new LinearLayout(mActivity);
                       linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20,20));
                       ((MyViewholder)holder).tender_list_icons.addView(linearLayout);
                   }
               }

           }
           */
                    //tag old concept

                    //tag new concept
                    String tagTender = modelTenderListArrayList.get(position).getTagTender();
                    String tagusername = "";

                    int tagCount = 0;
                    String colorValue = "";

                    for (int i = 0; i < Home.modelUserTagArrayList.size(); i++) {
                        if ((Long.parseLong(tagTender) & Long.parseLong(Home.modelUserTagArrayList.get(i).getTagIndex())) > 0) {
                            colorValue = Home.modelUserTagArrayList.get(i).getColorCSS();
                            tagCount++;
                        }
                    }

                    if (tagCount > 1) {
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        //modelTenderListIcons.setColourcode(modelUserTagArrayList.get(i).getColorCSS());
                        modelTenderListIcons.setResourcepath(R.drawable.tag_multiple);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(R.drawable.tag_multiple);
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                    } else {
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        switch (colorValue) {
                            case "f7e298":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_yellow);
                                break;
                            case "fdc396":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_orange);
                                break;
                            case "fba3a1":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_red);
                                break;
                            case "f1afec":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_pink);
                                break;
                            case "a492df":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_purple);
                                break;
                            case "a3c088":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_green);
                                break;
                            case "9ed7ff":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_blue);
                                break;
                            case "000000":
                                modelTenderListIcons.setResourcepath(R.drawable.tag_black);
                                break;
                        }


                        //modelTenderListIcons.setResourcepath(R.drawable.tag_blue);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());

                        if (!TextUtils.isEmpty(colorValue)) { //if color value is not null then add
                            ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                            LinearLayout linearLayout = new LinearLayout(mActivity);
                            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                            ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                        }
                    }

                    //this is not specified if the multiple tag is avalible
           /*String[] data ;

           data = tagTender.split(",");
           Log.e(TAG,"tender list tag values : "+tagTender.toString());

           if (data.length>1){
               ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
               modelTenderListIcons.setNumber(0);
               //modelTenderListIcons.setColourcode(modelUserTagArrayList.get(i).getColorCSS());
               modelTenderListIcons.setResourcepath(R.drawable.doubletaggedicon);
               modelTenderListIconsList.add(modelTenderListIcons);
               //set icons
               ImageView   mImageview = new ImageView(mActivity);
               mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
               mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
               mImageview.setBackgroundResource(R.drawable.doubletaggedicon);
               ((MyViewholder)holder).tender_list_icons.addView(mImageview);

               LinearLayout linearLayout = new LinearLayout(mActivity);
               linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20,20));
               ((MyViewholder)holder).tender_list_icons.addView(linearLayout);
           }
           else {
               for (int i = 0; i < modelUserTagArrayList.size(); i++) {
                   if (modelUserTagArrayList.get(i).getTagIndex().equals(tagTender)) {
                       //set the css color of hte tag to the data in image in tint not
                       ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                       modelTenderListIcons.setNumber(0);

                       switch(modelUserTagArrayList.get(i).getColorCSS()){

                           case "f7e298":
                               modelTenderListIcons.setResourcepath(R.drawable.yellowtaggedicon);
                               break;
                           case "fdc396":
                               modelTenderListIcons.setResourcepath(R.drawable.orangetaggedicon);
                               break;
                           case "fba3a1":
                               modelTenderListIcons.setResourcepath(R.drawable.redtaggedicon);
                               break;
                           case "f1afec":
                               modelTenderListIcons.setResourcepath(R.drawable.pinktaggedicon);
                               break;
                           case "a492df":
                               modelTenderListIcons.setResourcepath(R.drawable.purpletaggedicon);
                               break;
                           case "a3c088":
                               modelTenderListIcons.setResourcepath(R.drawable.greentaggedicon);
                               break;
                           case "9ed7ff":
                               modelTenderListIcons.setResourcepath(R.drawable.bluetaggedicon);
                               break;
                           case "000000":
                               modelTenderListIcons.setResourcepath(R.drawable.blacktaggedicon);
                               break;
                       }


                       //modelTenderListIcons.setResourcepath(R.drawable.tag_blue);
                       modelTenderListIconsList.add(modelTenderListIcons);
                       //set icons
                       ImageView   mImageview = new ImageView(mActivity);
                       mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                       mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                       mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                       ((MyViewholder)holder).tender_list_icons.addView(mImageview);

                       LinearLayout linearLayout = new LinearLayout(mActivity);
                       linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20,20));
                       ((MyViewholder)holder).tender_list_icons.addView(linearLayout);
                   }
               }

           }*/

                    //tag new Concept
                    //fire icon
                    // if (modelTenderListArrayList.get(position).getOptionsTender().equals("65536")) {
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getOptionsTender()) & Long.parseLong("65536")) > 0))) {


                        iconArray.add(R.drawable.tender_hot);

                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_hot);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                    }

                    //for paperClip
                    // if (modelTenderListArrayList.get(position).getOptionsTender().equals("256")) {
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getOptionsTender()) & Long.parseLong("256")) > 0))) {
                        iconArray.add(R.drawable.tender_attchments);
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_attchments);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                    }

                    //for green document depends on procurment plan
                    if (modelTenderListArrayList.get(position).getIdDocument().equals("A")) {
                        iconArray.add(R.drawable.tender_procurenent_plan);
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_procurenent_plan);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                    }


                    //red documetns
                    //if (modelTenderListArrayList.get(position).getOptionsTender().equals("4")) {
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getOptionsTender()) & Long.parseLong("512")) > 0))) {
                        // iconArray.add();
                        iconArray.add(R.drawable.tender_documents);
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_documents);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);

                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);
                    }

                    //! red sign
                    // if (modelTenderListArrayList.get(position).getStatusTender().equals("4096") && modelTenderListArrayList.get(position).isiconShow() ) {
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getStatusTender()) & Long.parseLong("4096")) > 0)) && modelTenderListArrayList.get(position).isiconShow()) {
                        //if (1==1 && modelTenderListArrayList.get(position).isiconShow() ) {
                        iconArray.add(R.drawable.tender_important);
                        ModelTenderListIcons modelTenderListIcons = new ModelTenderListIcons();
                        modelTenderListIcons.setNumber(0);
                        modelTenderListIcons.setColourcode("0");
                        modelTenderListIcons.setResourcepath(R.drawable.tender_important);
                        modelTenderListIconsList.add(modelTenderListIcons);
                        //set icons
                        ImageView mImageview = new ImageView(mActivity);
                        mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                        mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
                        //mImageview.setImageBitmap(null);

                        mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
                        ((MyViewholder) holder).tender_list_icons.addView(mImageview);
                        LinearLayout linearLayout = new LinearLayout(mActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                        ((MyViewholder) holder).tender_list_icons.addView(linearLayout);

                    }


                    //set delete of undelete according to statusTender
                    // if (modelTenderListArrayList.get(position).getStatusTender().equals("2147483648") ){
                    //  Log.e(TAG,"value of the status tender is "+modelTenderListArrayList.get(position).getStatusTender());
                    // Log.e(TAG,"value of the option tender is "+modelTenderListArrayList.get(position).getOptionsTender());
                    if ((((Long.parseLong(modelTenderListArrayList.get(position).getStatusTender()) & Long.parseLong("2147483648")) == 2147483648l))) {
                        ((MyViewholder) holder).set_delete.setText(mActivity.getResources().getString(R.string.undelete));
                    } else {
                        ((MyViewholder) holder).set_delete.setText(mActivity.getResources().getString(R.string.delete));
                    }
           /*// temp data created
           int i = 0;
           while(i< 5) {
               ModelTenderListIcons modelTenderListIcons = new MfodelTenderListIcons();
               modelTenderListIcons.setNumber(1);
               modelTenderListIcons.setColourcode("6367fa");
               if (i%2==1) {
                   modelTenderListIcons.setResourcepath(R.drawable.status_document_icon);
               }else{RecyclerSwipeAdapter
                   modelTenderListIcons.setResourcepath(R.drawable.red_flag);
               }
               modelTenderListIconsList.add(modelTenderListIcons);
               i++;

               //set icons
               ImageView   mImageview = new ImageView(mActivity);

              mImageview.setLayoutParams(new LinearLayout.LayoutParams(45, 45));
               mImageview.setPadding(100, 100, 100, 100);

               mImageview.setBackgroundResource(modelTenderListIcons.getResourcepath());
               ((MyViewholder)holder).tender_list_icons.addView(mImageview);

               LinearLayout linearLayout = new LinearLayout(mActivity);
               linearLayout.setLayoutParams(new LinearLayout.LayoutParams(20,20));
               ((MyViewholder)holder).tender_list_icons.addView(linearLayout);

           }
           ModelTenderListIcons modelTenderListIcons1 = new ModelTenderListIcons();
           modelTenderListIcons1.setNumber(1);
           modelTenderListIcons1.setColourcode("343453");
           modelTenderListIcons1.setResourcepath(R.drawable.fire_icon);

           modelTenderListIconsList.add(modelTenderListIcons1);

           //set icons
           ImageView   mImageview = new ImageView(mActivity);
           mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
           mImageview.setPadding(dpAsPixel, dpAsPixel, dpAsPixel, dpAsPixel);
           mImageview.setBackgroundResource(modelTenderListIcons1.getResourcepath());
           ((MyViewholder)holder).tender_list_icons.addView(mImageview);
*/
                    //Log.e(TAG,"number of icon is "+ modelTenderListIconsList.size()+ " at position :"+ position);
                    GridViewAdapter adapter = new GridViewAdapter(mActivity, modelTenderListIconsList);
                    ((MyViewholder) holder).gridView.setAdapter(adapter);

                    mItemManager.bindView(((MyViewholder) holder).itemView, position);

                }
                break;
            case LOADING:
                ((LoadingVH) holder).mProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                // do nothing
                break;
        }


    }

    @Override
    public int getItemCount() {
        return modelTenderListArrayList == null ? 0 : modelTenderListArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == modelTenderListArrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swaplay1;
    }

    public void showTagAlert(final int adapterPosition) {

        if (Home.modelUserTagArrayList.size() > 0) {
            build = new AlertDialog.Builder(mActivity);
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view_alert = layoutInflater.inflate(R.layout.tenderlist_tag_recylay, null, false);
            //show tag dilaog
            final ArrayList<String> first_list = new ArrayList();
            final ArrayList<String> final_list = new ArrayList();

            mAlertDialog = build.create();

            modelTagArrayList = new ArrayList<>();
            modelTagArrayList.clear();
            //modelTagArrayList = modelUserTagArrayList;
            modelTagArrayList.addAll(Home.modelUserTagArrayList);

            int sizearraylist = modelTagArrayList.size();
            for (int b = 0; sizearraylist > b; b++) {
                modelTagArrayList.get(b).setIstagset(false);
            }

            final String tagTender = modelTenderListArrayList.get(adapterPosition).getTagTender();

            //new concept 18-08-2017
            for (int i = 0; i < modelTagArrayList.size(); i++) {
                if ((Long.parseLong(tagTender) & Long.parseLong(modelTagArrayList.get(i).getTagIndex())) > 0) {
                    modelTagArrayList.get(i).setIstagset(true);
                    first_list.add(modelTagArrayList.get(i).getTagIndex());
                }
            }
            //new concept 18-08-2017

            //old concept 18-05-2017
    /*//this is not specified if the multiple tag is avalible
    String[] data;

    data = tagTender.split(",");

    // set check user tags
    if (data.length > 0)
        for (String data_tag : data) {
            for (int i = 0; i < modelTagArrayList.size(); i++) {
                if (modelTagArrayList.get(i).getTagIndex().equals(data_tag)) {
                    modelTagArrayList.get(i).setIstagset(true);
                }
            }
        }
    */
            //old  concept 18-08-2017




   /* for(int i = 0;i<5 ;i++){
        ModelTag modelTag = new ModelTag();
        modelTag.setTagName("tag "+i);
        modelTagArrayList.add(modelTag);
    }*/
                /* LayoutInflater layoutInflater= (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.tenderlist_tag_recylay,null,false);*/

            mRecyclerviewTag = (RecyclerView) view_alert.findViewById(R.id.tender_list_tag_recy);
            setTagCanBtn = (Button) view_alert.findViewById(R.id.tender_list_tag_canBtn);
            setTagUpdBtn = (Button) view_alert.findViewById(R.id.tender_list_tag_updBtn);

            mRecyclerviewTag.setHasFixedSize(true);
            //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
            GridLayoutManager linearLayoutManager = new GridLayoutManager(mActivity, 1);
            linearLayoutManager.setAutoMeasureEnabled(true);
            mRecyclerviewTag.setLayoutManager(linearLayoutManager);

            adapterTag = new TenderListSetTagAdapter(mActivity, modelTagArrayList);

            mRecyclerviewTag.setAdapter(adapterTag);

            //handling recyclerivew clicking
            mRecyclerviewTag.addOnItemTouchListener(new RecycerItemClickListener(mActivity, new RecycerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position) {
                    final CheckBox checkBox = (CheckBox) view.findViewById(R.id.tender_list_tag_radiobtn);

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                Log.e(TAG, "radion button is checked at  position " + position);
                                modelTagArrayList.get(position).setIstagset(true);
                                checkBox.setChecked(true);
                            } else {
                                // Log.e(TAG,"radion button is checked at  position "+position);
                                Log.e(TAG, "radion button is not checked at  position " + position);
                                modelTagArrayList.get(position).setIstagset(false);
                                checkBox.setChecked(false);
                            }
                        }
                    });


                }
            }));


            setTagCanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyDataSetChanged();
                    mAlertDialog.dismiss();
                    first_list.clear();
                    final_list.clear();

                }
            });

            setTagUpdBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long tagStringdata = 0;
                    boolean first = true;
                    for (int i = 0; i < modelTagArrayList.size(); i++) {
                        if (modelTagArrayList.get(i).istagset()) {
                    /*if (first) {
                        tagStringdata = modelTagArrayList.get(i).getTagIndex();
                        first = false;
                    } else {
                        tagStringdata = tagStringdata + "," + modelTagArrayList.get(i).getTagIndex();
                    }*/
                            final_list.add(modelTagArrayList.get(i).getTagIndex());
                            //new concept 18-05-2017
                            tagStringdata = tagStringdata + Long.parseLong(modelTagArrayList.get(i).getTagIndex());
                        }
                    }

                    //set the data to database
                    if (dataBaseHalper.UpdateTagList(tagStringdata + "", modelTenderListArrayList.get(adapterPosition).getIdTender())) {
                        mAlertDialog.dismiss();
                        modelTenderListArrayList.get(adapterPosition).setTagTender(tagStringdata + "");

                        // newly added tag
                        ArrayList<String> newlyAdded = new ArrayList<String>(final_list);
                        newlyAdded.removeAll(first_list); // it gives newly added data
//
//                        first_list.removeAll(newlyAdded); // this will give all removed tag list
//                        ArrayList<String> removedTags = new ArrayList<String>();
                        first_list.removeAll(intersect(first_list, final_list));


                        //List<String> result_this = final_list.stream().filter(elem -> ! first_list.contains(elem)).collect(Collectors.<String>toList());

                        for (String newAdded : newlyAdded) {
                            String tender_action = modelTenderListArrayList.get(adapterPosition).getIdTender() + "|" + newAdded;
                            GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_tag_set, tender_action);
                        }

                        for (String removed_tag : first_list) {
                            String tender_action = modelTenderListArrayList.get(adapterPosition).getIdTender() + "|" + removed_tag;
                            GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_tag_unset, tender_action);
                        }

                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                }
            });


            mAlertDialog.setView(view_alert);

            mAlertDialog.show();

        } else {
            if (build == null)
                build = new AlertDialog.Builder(mActivity, R.style.MyDialogTheme);
            build.setTitle("");
            notifyDataSetChanged();
            build.setMessage(mActivity.getResources().getString(R.string.not_tag_found));
            build.setCancelable(true);
        /*build.setPositiveButton(DialogInterface.BUTTON_NEUTRAL, mActivity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/
            build.setPositiveButton(mActivity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            build.show();
        }
    }

    <T> Collection<T> union(Collection<T> coll1, Collection<T> coll2) {
        Set<T> union = new HashSet<>(coll1);
        union.addAll(new HashSet<>(coll2));
        return union;
    }

    <T> Collection<T> intersect(Collection<T> coll1, Collection<T> coll2) {
        Set<T> intersection = new HashSet<>(coll1);
        intersection.retainAll(new HashSet<>(coll2));
        return intersection;
    }

    <T> Collection<T> nonOverLap(Collection<T> coll1, Collection<T> coll2) {
        Collection<T> result = union(coll1, coll2);
        result.removeAll(intersect(coll1, coll2));
        return result;
    }

    // helper methods paggination
    public void add(ModelTenderList modelTenderList) {
        modelTenderListArrayList.add(modelTenderList);
        notifyItemInserted(modelTenderListArrayList.size() - 1);
    }

    public void remove(ModelTenderList modelTenderList) {
        int position = modelTenderListArrayList.indexOf(modelTenderList);

        if (position > -1) {
            modelTenderListArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            //  remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ModelTenderList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        if (modelTenderListArrayList.size() > 0) {
            int position = modelTenderListArrayList.size() - 1;
            ModelTenderList result = getItem(position);

            if (result != null) {
                modelTenderListArrayList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public ModelTenderList getItem(int position) {
        return modelTenderListArrayList.get(position);
    }

    private void deleteTender(int adapterPosition) {


        // long tender_delete_val = ((Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getOptionsTender() ) | Long.parseLong("2147483648")));
        long tenderId = Long.parseLong(modelTenderListArrayList.get(adapterPosition).getStatusTender());
        long tender_delete_val = (tenderId | Long.parseLong("2147483648"));
        Log.e(TAG, "Tender was not deleted and delete value is " + tender_delete_val);
        if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(adapterPosition).getIdTender(), tender_delete_val + "")) {
            GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_delete, modelTenderListArrayList.get(adapterPosition).getIdTender());
            GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_read, modelTenderListArrayList.get(adapterPosition).getIdTender());
            // dataBaseHalper.SetReadedStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), 1);
            modelTenderListArrayList.remove(adapterPosition);
            //modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("2147483648");

            notifyDataSetChanged();
            //Toast.makeText(mActivity, "Tender  Deleted", Toast.LENGTH_SHORT).show();
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.tender_deleted), Toast.LENGTH_SHORT).show();

        } else {
            notifyDataSetChanged();
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.tender_not_deleted), Toast.LENGTH_SHORT).show();
        }


    }

    public class LoadingVH extends RecyclerView.ViewHolder {
        ProgressBar mProgressBar;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.myProgressbar);

        }
    }

    public class MyViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        SwipeLayout sample1;
        LinearLayout recy_custlaymain, recy_fooder, tender_list_icons;

        TextView tend_custlay_countnam, tend_custlay_purName, tend_custlay_tenderfor, tend_custlay_date, set_flag, set_tag, set_unread, set_delete;
        ImageView tend_custlay_flagimg;
        GridView gridView;
        int a = modelTenderListArrayList.size();
        int b = a;

        public MyViewholder(final View itemView) {
            super(itemView);

            final InterfaceTendListClick interfaceTendListClick = new FragmentTender();
            final int position = getAdapterPosition();


            tend_custlay_flagimg = (ImageView) itemView.findViewById(R.id.tend_custlay_flagimg);
            tend_custlay_countnam = (TextView) itemView.findViewById(R.id.tend_custlay_countnam);
            tend_custlay_purName = (TextView) itemView.findViewById(R.id.tend_custlay_purName);
            tend_custlay_tenderfor = (TextView) itemView.findViewById(R.id.tend_custlay_tenderfor);
            tend_custlay_date = (TextView) itemView.findViewById(R.id.tend_custlay_date);
            gridView = (GridView) itemView.findViewById(R.id.tenderList_gridView);
            tender_list_icons = (LinearLayout) itemView.findViewById(R.id.tender_list_icons);
            tender_list_icons.setOrientation(LinearLayout.HORIZONTAL);
            /* =(TextView)itemView.findViewById(R.id. );
            =(TextView)itemView.findViewById(R.id. );
            =(TextView)itemView.findViewById(R.id. );
            */
            recy_custlaymain = (LinearLayout) itemView.findViewById(R.id.recy_custlaymain);
            recy_fooder = (LinearLayout) itemView.findViewById(R.id.recy_fooder);

            sample1 = (SwipeLayout) itemView.findViewById(R.id.swaplay1);
            sample1.setShowMode(SwipeLayout.ShowMode.LayDown);
            sample1.addDrag(SwipeLayout.DragEdge.Right, sample1.findViewWithTag("bottom2"));

            set_flag = (TextView) sample1.findViewById(R.id.ten_custlay_setflag_text);
            set_tag = (TextView) sample1.findViewById(R.id.ten_custlay_setTag_text);
            set_unread = (TextView) sample1.findViewById(R.id.ten_custlay_setunread_text);
            set_delete = (TextView) sample1.findViewById(R.id.ten_custlay_delet_text);


            //1 = set flag, 2 = set tag , 3 =set as unread ,4 = deletee

            //f7e298 = , fdc396 = , fba3a1 = , f1afec = , a492df = , a3c088 = , 9ed7ff = , 000000 =

           /* sample1.getCurrentBottomView().findViewById(R.id.swaplay_flagLL).setOnClickListener(this);
            sample1.getCurrentBottomView().findViewById(R.id.swaplay_TagLL).setOnClickListener(this);
            sample1.getCurrentBottomView().findViewById(R.id.swaplay_DeleteLL).setOnClickListener(this);
            sample1.getCurrentBottomView().findViewById(R.id.swaplay_UnreadLL).setOnClickListener(this);

            sample1.getSurfaceView().setOnClickListener(this);*/

            /*sample1.findViewById(R.id.ten_custlay_setflag).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(mActivity, "star clicked", Toast.LENGTH_SHORT).show();
                   // interfaceTendListClick.TenderlistClickHandle(1,getAdapterPosition());
                    if (!modelTenderListArrayList.get(getAdapterPosition()).getStatusTender().equals("8192"))
                    {
                        if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(),"8192")) {
                            modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("8192");
                            notifyDataSetChanged();
                        }
                    }else{
                        if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(),"4294967295-8192")) {
                            modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("4294967295-8192");
                            notifyDataSetChanged();
                        }
                    }
                }
            });*/

            // TODO: 11/24/2017 action perform for clicking
            sample1.findViewById(R.id.recy_custlaymain).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e(TAG, "hello main layout clicked and size of arraylist is :" + modelTenderListArrayList.size() + " adapter clicked position " + getAdapterPosition());
                    TenderDetails.pinPoint = getAdapterPosition();
                    TenderDetails.isRemoved = false;
                    TenderDetails.activityDetails = mActivity;
                    //test
                    dataBaseHalper.SetReadedStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), 1, Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()));
                    sessionManager.SetTenderIdDetails(modelTenderListArrayList.get(getAdapterPosition()).getIdTender());
                    modelTenderListArrayList.get(getAdapterPosition()).setReaded(true);
                    modelTenderListArrayList.get(getAdapterPosition()).setIsiconShow(false);
                    GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_read, modelTenderListArrayList.get(getAdapterPosition()).getIdTender()); // send action tender read
                    notifyDataSetChanged();
                    //tenderId = filteredArraylist.get(position).getIdTender();
                    doTheAutoRefresh();
                    TenderDetails.modelTenderListArrayList = modelTenderListArrayList;

                    mActivity.startActivity(new Intent(mActivity, TenderDetails.class));

                }
            });


            // TODO: 11/24/2017 this action perform the flag set and unset
            sample1.findViewById(R.id.swaplay_flagLL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(mActivity, "star clicked", Toast.LENGTH_SHORT).show();
                    // interfaceTendListClick.TenderlistClickHandle(1,getAdapterPosition());
                    // if (!modelTenderListArrayList.get(getAdapterPosition()).getStatusTender().equals("8192"))

//                    sample1.close(true);

                    sample1.close();


                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {// TODO: 11/24/2017 for tag set actions
                            if ((Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()) & 8192) != 8192) {
                                long flag_status_value = Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()) | 8192;
                                if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), flag_status_value + "")) {
                                    modelTenderListArrayList.get(getAdapterPosition()).setStatusTender(flag_status_value + "");
                                    GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_flag_set, modelTenderListArrayList.get(getAdapterPosition()).getIdTender());
                                    notifyDataSetChanged();
                                }
                            } else { // TODO: 11/24/2017 for tag un-set actions
                                long unset_flat_val = 4294967295l - 8192l;
                                long flag_deSelect_value = Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()) & unset_flat_val;
                                if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), flag_deSelect_value + "")) {
                                    modelTenderListArrayList.get(getAdapterPosition()).setStatusTender(flag_deSelect_value + "");
                                    GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_flag_unset, modelTenderListArrayList.get(getAdapterPosition()).getIdTender());
                                    notifyDataSetChanged();
                                }


                            }
                        }
                    }, 500);


                }
            });

            /*sample1.findViewById(R.id.ten_custlay_setTag).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(mActivity, "Trash clicked", Toast.LENGTH_SHORT).show();
                    //interfaceTendListClick.TenderlistClickHandle(2,getAdapterPosition());
                    showTagAlert(v);

                }
            });*/
            // TODO: 11/24/2017 this will set / un-set tag . this open a dialog and all options are shown there
            sample1.findViewById(R.id.swaplay_TagLL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(mActivity, "Trash clicked", Toast.LENGTH_SHORT).show();
                    //interfaceTendListClick.TenderlistClickHandle(2,getAdapterPosition());
                    showTagAlert(getAdapterPosition());
//                    sample1.close(true);
                    sample1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sample1.close();
                        }
                    }, 100);

                }
            });
            // TODO: 11/24/2017 this unread is not required there as per mentioned in list only three are matcing from them and ask aboutback functionility . ok i am done now please do like that and i will be avalible on call if you got any problem .bye  thanks bye
            sample1.findViewById(R.id.swaplay_UnreadLL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(mActivity, "magnifire clicked", Toast.LENGTH_SHORT).show();
                    //  interfaceTendListClick.TenderlistClickHandle(3,getAdapterPosition());
//                    sample1.close(true);
                    sample1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sample1.close();
                        }
                    }, 100);

                    if (dataBaseHalper.SetReadedStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), 0, Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()))) {
                        modelTenderListArrayList.get(getAdapterPosition()).setReaded(false);

                        GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_Unread, modelTenderListArrayList.get(getAdapterPosition()).getIdTender());
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            /* sample1.findViewById(R.id.ten_custlay_setunread).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 //Toast.makeText(mActivity, "magnifire clicked", Toast.LENGTH_SHORT).show();
                  //  interfaceTendListClick.TenderlistClickHandle(3,getAdapterPosition());
                    if (dataBaseHalper.SetReadedStatus(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(),0)){
                        modelTenderListArrayList.get(getAdapterPosition()).setReaded(false);
                        notifyDataSetChanged();
                    }else{
                        Toast.makeText(mActivity, "Try Again", Toast.LENGTH_SHORT).show();
                    }

                }
            });*/

            /*sample1.findViewById(R.id.ten_custlay_delet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Toast.makeText(mActivity, "search clicked", Toast.LENGTH_SHORT).show();
                    if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(getAdapterPosition()).getIdTender())) {
                        modelTenderListArrayList.remove(getAdapterPosition());
                        notifyDataSetChanged();
                        Toast.makeText(mActivity,"Tender  Deleted", Toast.LENGTH_SHORT).show();

                    }else{
                        notifyDataSetChanged();
                        Toast.makeText(mActivity,"Tender not Deleted", Toast.LENGTH_SHORT).show();
                    }
                    //interfaceTendListClick.TenderlistClickHandle(4,getAdapterPosition());


                }
            });*/


            sample1.findViewById(R.id.swaplay_DeleteLL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.e(TAG,"tender list  delete is con");
                    //  When delete tender use: optionTender | 2147483648
                    //  When restore tender use: optionTender & 2147483647
                    //  Toast.makeText(mActivity, "search clicked", Toast.LENGTH_SHORT).show();
                    // if ( !modelTenderListArrayList.get(getAdapterPosition()).getStatusTender().equals("2147483648")) {

                    sample1.close(true);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (((Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender()) & Long.parseLong("2147483648")) != Long.parseLong("2147483648"))) {

                                AlertDialog.Builder builder;
                                builder = new AlertDialog.Builder(mActivity);
                                mAlertDialog = builder.create();
                                LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = inflater.inflate(R.layout.confirm_delete, null, false);

                                Button checheDia_canBtn = (Button) view.findViewById(R.id.checheDia_canBtn);
                                Button checheDia_okBtn = (Button) view.findViewById(R.id.checheDia_okBtn);

                                mAlertDialog.setView(view);

                                checheDia_canBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mAlertDialog.dismiss();
                                    }
                                });

                                checheDia_okBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        deleteTender(getAdapterPosition());
                                        mAlertDialog.dismiss();
                                    }
                                });

                                mAlertDialog.show();


                            } else {
                                //  long tender_restore_val = ((Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getOptionsTender() ) & Long.parseLong("2147483647")));
                                long tenderId = Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender());
                                long tender_restore_val = (tenderId & Long.parseLong("2147483647"));
                                Log.e(TAG, "Tender was deleted and restore value is " + tender_restore_val);
                                if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), "" + tender_restore_val)) {
                                    GlobalDataAcess.SendSyncAction(mActivity, sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_undelete, modelTenderListArrayList.get(getAdapterPosition()).getIdTender());
                                    modelTenderListArrayList.remove(getAdapterPosition());
                                    notifyDataSetChanged();


                                    //modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("2147483648");
                                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.tender_restored), Toast.LENGTH_SHORT).show();

                                } else {
                                    notifyDataSetChanged();
                                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.tender_not_restored), Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    }, 500);
                    /*  if ( !modelTenderListArrayList.get(getAdapterPosition()).getStatusTender().equals("2147483648")) {
                        Log.e(TAG,"Tender was not deleted");

                        if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), "2147483648")) {
                            modelTenderListArrayList.remove(getAdapterPosition());
                            //modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("2147483648");
                            notifyDataSetChanged();
                            Toast.makeText(mActivity, "Tender  Deleted", Toast.LENGTH_SHORT).show();

                        } else {
                            notifyDataSetChanged();
                            Toast.makeText(mActivity, "Tender not Deleted", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Log.e(TAG,"Tender was deleted");
                        if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), "2147483647")) {
                            modelTenderListArrayList.remove(getAdapterPosition());
                            //modelTenderListArrayList.get(getAdapterPosition()).setStatusTender("2147483648");
                            notifyDataSetChanged();
                            Toast.makeText(mActivity, "Tender  Restored", Toast.LENGTH_SHORT).show();

                        } else {
                            notifyDataSetChanged();
                            Toast.makeText(mActivity, "Tender not Restored", Toast.LENGTH_SHORT).show();
                        }
                    }*/
                    //interfaceTendListClick.TenderlistClickHandle(4,getAdapterPosition());


                }
            });

            sample1.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {
                    //  sample1.close(true);
                    //  notifyDataSetChanged();
                }

                @Override
                public void onOpen(SwipeLayout layout) {
                    Log.e(TAG, "start open");

                    mItemManager.closeAllExcept(layout);

                }

                @Override
                public void onStartClose(SwipeLayout layout) {
                    Log.e(TAG, "start closeing");

                }

                @Override
                public void onClose(SwipeLayout layout) {
                    Log.e(TAG, "closeing");
                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                    Log.e(TAG, "start update");
                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                    Log.e(TAG, "start handRelase ");
                }


            });

            sample1.getSurfaceView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // sample1.close(true);
                    // sample1.close();
                    //   mActivity.startActivity(new Intent(mActivity,TenderDetails.class));
                    //  Toast.makeText(mActivity, "Surface clicked", Toast.LENGTH_SHORT).show();
                    //interfaceTendListClick.TenderlistClickHandle(1,position);

                }
            });
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.swaplay_flagLL:
                    //  Toast.makeText(mActivity, " Flag clicked ", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.swaplay_TagLL:
                    //  Toast.makeText(mActivity, " Tag clicked ", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.swaplay_UnreadLL:
                    //Toast.makeText(mActivity, " unread clicked ", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.swaplay_DeleteLL:
                    //  Toast.makeText(mActivity, " Deleted clicked ", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.swaplay1:
                    //  Toast.makeText(mActivity, " swap surface clicked ", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        private void doTheAutoRefresh() {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {


                    if (TenderDetails.isDeleted == true) {

                        modelTenderListArrayList.remove(TenderDetails.pinPoint);
                        TenderDetails.isDeleted = false;
                        int a = modelTenderListArrayList.size();

                        int b = a;
                    }
                    notifyDataSetChanged();
                    doTheAutoRefresh();
                }
            }, 5000);
        }


    }


}
