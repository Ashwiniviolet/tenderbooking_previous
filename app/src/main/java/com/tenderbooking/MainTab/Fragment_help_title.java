package com.tenderbooking.MainTab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tenderbooking.R;

/**
 * Created by Vikaash on 2/21/2017.
 */
public class Fragment_help_title extends Fragment {

    TextView text1, text2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_help, container, false);

        text1 = (TextView) view.findViewById(R.id.help_text1);
        text2 = (TextView) view.findViewById(R.id.help_text2);

        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Fragment_help_detail();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_help_frame, fragment).addToBackStack(null).commit();
            }
        });

        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Fragment_help_detail();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_help_frame, fragment).addToBackStack(null).commit();
            }
        });
        return view;
    }
}
