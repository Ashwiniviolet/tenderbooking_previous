package com.tenderbooking;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tenderbooking.Adapter.TenderListSetTagAdapter;
import com.tenderbooking.HelperClasses.DataBaseHalper;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Interface.InteTenDetNotifier;
import com.tenderbooking.MainTab.Fragment_Attachment_Documents;
import com.tenderbooking.MainTab.Fragment_Related_Info;
import com.tenderbooking.MainTab.Fragment_details_serbian;
import com.tenderbooking.MainTab.Fragment_summary;
import com.tenderbooking.MainTab.GlobalDataAccess;
import com.tenderbooking.Model.ModelTenDetailAttach;
import com.tenderbooking.Model.ModelTenDetailLinks;
import com.tenderbooking.Model.ModelTenDetalDetails;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.Model.ModelTenderList;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.Services.TenderDetailsPackage.FragmentDetailsEnglish;
import com.tenderbooking.Services.TenderDetailsPackage.FragmentDetialsGerman;
import com.tenderbooking.Volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TenderDetails extends AppCompatActivity implements InteTenDetNotifier, View.OnClickListener {


    public static final String TAG = TenderDetails.class.getSimpleName();
    public static ViewPager viewPager;
    public static ModelTenderDetails modelTenderDetails;
    public static ProgressDialog progressDialog;
    public static ArrayList<ModelTenderList> modelTenderListArrayList;
    public static int pinPoint;
    public static boolean isDeleted = false;
    public static boolean isRemoved = false;
    public static Activity activityDetails;
    public Toolbar mToolbar;
    public RecyclerView mRecyclerviewTag;
    public AlertDialog mAlertDialog;
    public View view_alert;
    public AlertDialog.Builder build;
    public ArrayList<ModelUserTag> modelTagArrayList;
    public TenderListSetTagAdapter adapterTag;
    public Button setTagCanBtn, setTagUpdBtn;
    AlertDialog.Builder builder;
    SessionManager sessionManager;
    DataBaseHalper dataBaseHalper;
    ImageButton flag, tag, delete, back;
    //bitmap
    Bitmap bitmap_thee;
    //Firebase analytics
    private FirebaseAnalytics firebaseAnalytics;
    // private Toolbar toolbar;
    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.flag
    };
    private LinearLayout progress_ll;
    private LinearLayout linearLayout;

    private int a;

    // TODO: 11/24/2017 initialize all  actions and perform task
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        sessionManager = new SessionManager(TenderDetails.this);

        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        //today
        invalidateOptionsMenu();


        String lang = sessionManager.getAppLangName();
        UtillClasses.langChange(lang, TenderDetails.this);

        int size = sessionManager.GetSizeTheme();

        switch (size) {
            case 1:
                setTheme(R.style.AppTheme_Small_Noactionbar);
                break;
            case 2:
                setTheme(R.style.AppTheme_medium_Noactionbar);
                break;
            case 3:
                setTheme(R.style.AppTheme_Large_Noactionbar);
                break;
        }

        setContentView(R.layout.activity_tender_details);
        progress_ll = (LinearLayout) findViewById(R.id.progress_ll);
        linearLayout = (LinearLayout) findViewById(R.id.close_linear);
        mToolbar = (Toolbar) findViewById(R.id.tender_detials_toolbar);


        if (SearchAdapter.close_linear == true) {
            linearLayout.setVisibility(View.GONE);
            SearchAdapter.close_linear = false;
        } else {
            linearLayout.setVisibility(View.VISIBLE);
        }//till here by ashwini


        if (GlobalDataAcess.titleString.equals("")) {
            mToolbar.setTitle(getResources().getString(R.string.active));
        } else {
            mToolbar.setTitle(GlobalDataAcess.titleString);
        }

        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(ContextCompat.getColor(TenderDetails.this, R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        dataBaseHalper = new DataBaseHalper(TenderDetails.this);
        sessionManager = new SessionManager(TenderDetails.this);


        //firebase Analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        String myTender_id = FragmentTender.tenderId;
        if (myTender_id == null) {
            myTender_id = "Not Avalible";
        }

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
        bundle.putString(GlobalDataAcess.Class_name, "Tender Detials");
        bundle.putString(GlobalDataAcess.Tender_id, myTender_id);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


        modelTenderDetails = new ModelTenderDetails();
        // getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.tabViewpager);
        //setupViewPager(viewPager);

        viewPager.setOffscreenPageLimit(6);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.white), ContextCompat.getColor(this, R.color.white));

        //setup progress Dialog
//        progressDialog = new ProgressDialog(TenderDetails.this);
//        progressDialog.setMessage(getResources().getString(R.string.loading));
//
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        progress_ll.setVisibility(View.VISIBLE);
       /* tabLayout.getTabAt(1).setIcon(tabIcons[0]);
        tabLayout.getTabAt(2).setIcon(tabIcons[0]);
        tabLayout.getTabAt(3).setIcon(tabIcons[0]);
*/

        if (dataBaseHalper.GetTenDetilAval(sessionManager.GetTenderIdDetails())) {
            // if (false){
            //   Log.e(TAG,"tender detials from database ");

          /* GetDatabaseData getDatabaseData = new GetDatabaseData();
           getDatabaseData.execute();*/

            //modelTenderDetails = dataBaseHalper.GetTenderDetials(FragmentTender.tenderId);

          /* final Runnable runnableList = new Runnable() {
               @Override
               public void run() {
                   Log.e(TAG,"List featching start");
                   modelTenderDetails = dataBaseHalper.GetTenderDetials(FragmentTender.tenderId);
               }
           };

           final Runnable runnable = new Runnable() {
               @Override
               public void run() {

                   Log.e(TAG,"tab creation start");
                   progressDialog.hide();
                   setupViewPager(viewPager);
                   CreateTABIcon();
                  *//* new Handler().postDelayed(new Runnable() {
                       @Override
                       public void run() {
                           Log.e(TAG,"my thread is started");
                           progressDialog.hide();
                           setupViewPager(viewPager);
                           CreateTABIcon();
                           

                       }
                   },4000);*//*
               }
           };
           
           Thread mythreadList = new Thread(runnableList);
           Thread mythread = new Thread(runnable);

           mythreadList.start();

           try {
               mythreadList.join();
           } catch (InterruptedException e) {
               Log.e(TAG,"error to start list thread ");
               e.printStackTrace();
           }
           mythread.start();
          */

            //runnable.run();
           



          /* progressDialog.hide();
           setupViewPager(viewPager);
           CreateTABIcon();*/

          /* if(dataBaseHalper.GetTenderDetials(FragmentTender.tenderId)){
               Log.e(TAG,"my thread is started");
               progressDialog.hide();
               setupViewPager(viewPager);
               CreateTABIcon();
           }*/

            String tender_id = sessionManager.GetTenderIdDetails();
            if (tender_id.equals(""))
                Toast.makeText(TenderDetails.this, getResources().getString(R.string.tender_id_empty), Toast.LENGTH_SHORT).show();

            if ((dataBaseHalper.GetTenderDetialsMainTemp(tender_id)) && (dataBaseHalper.GetTederDetailsTemp(tender_id))
                    && (dataBaseHalper.GetTenderAttachTemp(tender_id)) && (dataBaseHalper.GetTenderLinksTemp(tender_id))) {
                //   Log.e(TAG,"my thread is started");
//               progressDialog.hide();
                progress_ll.setVisibility(View.GONE);
                setupViewPager(viewPager);
                CreateTABIcon();
            } else {
//               progressDialog.hide();
                progress_ll.setVisibility(View.GONE);
                Log.e(TAG, "Failed to get data from the database");
            }


        } else {
            Log.e(TAG, "tender details from the server");
            GetData();
        }


        flag = (ImageButton) findViewById(R.id.bottom_flag);
        tag = (ImageButton) findViewById(R.id.bottom_tag);
        delete = (ImageButton) findViewById(R.id.bottom_delete);
        back = (ImageButton) findViewById(R.id.bottom_back);


        tag.setOnClickListener(this);
        flag.setOnClickListener(this);
        delete.setOnClickListener(this);
        back.setOnClickListener(this);


        // CreateTABIcon();

    }

    //creating tab data from database


    public void CreateTABIcon() {

        modelTenderDetails.getDetailsArrayList().size();
        int tablayout_count = 0;

        TextView tabzero = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
        tabzero.setText(getResources().getString(R.string.summary));
        tabzero.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        tabzero.setPadding(0, 20, 0, 0);
        tabzero.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
        // tabzero.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu_camera,0,0,0);
        tabLayout.getTabAt(tablayout_count++).setCustomView(tabzero);

        if (modelTenderDetails.getDetailsArrayList().size() > 0) {
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
            tabOne.setText(modelTenderDetails.getDetailsArrayList().get(0).getLanguageTenderName());
            tabOne.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            tabOne.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
            //tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.uk_flag_16,0,0,0);
            /*try {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(GlobalDataAcess.drawableFromUrl("http://www.tenderilive.com/images/icons/flags/"+modelTenderDetails.getDetailsArrayList().get(0).getFlagTender()+"32.png",TenderDetails.this),null,null,null);
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            try {
                final ImageView imageview_three = new ImageView(this);
                Picasso.with(this).load("https://www.tenderilive.com/images/icons/flags/" + modelTenderDetails.getDetailsArrayList().get(0).getFlagTender() + "32.png").error(R.drawable.app_icon).into(imageview_three, new Callback() {
                    @Override
                    public void onSuccess() {
                        // bitmap_thee = ((BitmapDrawable)imageview_three.getDrawable()).getBitmap();
                    }

                    @Override
                    public void onError() {
                        // bitmap_thee = ((BitmapDrawable)imageview_three.getDrawable()).getBitmap();
                    }
                });
                // tabThree.setCompoundDrawablesWithIntrinsicBounds(GlobalDataAcess.drawableFromUrl_three("http://www.tenderilive.com/images/icons/flags/"+modelTenderDetails.getDetailsArrayList().get(2).getFlagTender()+"32.png",TenderDetails.this),null,null,null);
                tabOne.setCompoundDrawablesWithIntrinsicBounds(imageview_three.getDrawable(), null, null, null);
            } catch (Exception e) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_icon, 0, 0, 0);
                e.printStackTrace();

            }


            tabOne.setPadding(0, 20, 0, 0);
            tabOne.setCompoundDrawablePadding(15);

            tabLayout.getTabAt(tablayout_count++).setCustomView(tabOne);
        }
        if (modelTenderDetails.getDetailsArrayList().size() > 1) {
            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
            tabTwo.setText(modelTenderDetails.getDetailsArrayList().get(1).getLanguageTenderName());
            tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            tabTwo.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
            //tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.uk_flag_16,0,0,0);
          /*  try {
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(GlobalDataAcess.drawableFromUrl_two("http://www.tenderilive.com/images/icons/flags/"+modelTenderDetails.getDetailsArrayList().get(1).getFlagTender()+"32.png",TenderDetails.this),null,null,null);
            } catch (IOException e) {
                e.printStackTrace();
            }
*/
            try {
                final ImageView imageview_three = new ImageView(this);
                Picasso.with(this).load("https://www.tenderilive.com/images/icons/flags/" + modelTenderDetails.getDetailsArrayList().get(1).getFlagTender() + "32.png").error(R.drawable.app_icon).into(imageview_three, new Callback() {
                    @Override
                    public void onSuccess() {
                        // bitmap_thee = ((BitmapDrawable)imageview_three.getDrawable()).getBitmap();
                    }

                    @Override
                    public void onError() {
                        // bitmap_thee = ((BitmapDrawable)imageview_three.getDrawable()).getBitmap();
                    }
                });
                // tabThree.setCompoundDrawablesWithIntrinsicBounds(GlobalDataAcess.drawableFromUrl_three("http://www.tenderilive.com/images/icons/flags/"+modelTenderDetails.getDetailsArrayList().get(2).getFlagTender()+"32.png",TenderDetails.this),null,null,null);
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(imageview_three.getDrawable(), null, null, null);
            } catch (Exception e) {
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_icon, 0, 0, 0);
                e.printStackTrace();

            }

            tabTwo.setPadding(0, 20, 0, 0);
            tabTwo.setCompoundDrawablePadding(15);

            tabLayout.getTabAt(tablayout_count++).setCustomView(tabTwo);
        }
        if (modelTenderDetails.getDetailsArrayList().size() > 2) {
            TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
            tabThree.setText(modelTenderDetails.getDetailsArrayList().get(2).getLanguageTenderName());
            tabThree.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            tabThree.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
            //tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.uk_flag_16,0,0,0);

            try {
                final ImageView imageview_three = new ImageView(this);
                Picasso.with(this).load("https://www.tenderilive.com/images/icons/flags/" + modelTenderDetails.getDetailsArrayList().get(2).getFlagTender() + "32.png").error(R.drawable.app_icon).into(imageview_three, new Callback() {
                    @Override
                    public void onSuccess() {
                        bitmap_thee = ((BitmapDrawable) imageview_three.getDrawable()).getBitmap();
                    }

                    @Override
                    public void onError() {
                        bitmap_thee = ((BitmapDrawable) imageview_three.getDrawable()).getBitmap();
                    }
                });
                // tabThree.setCompoundDrawablesWithIntrinsicBounds(GlobalDataAcess.drawableFromUrl_three("http://www.tenderilive.com/images/icons/flags/"+modelTenderDetails.getDetailsArrayList().get(2).getFlagTender()+"32.png",TenderDetails.this),null,null,null);
                tabThree.setCompoundDrawablesWithIntrinsicBounds(imageview_three.getDrawable(), null, null, null);
            } catch (Exception e) {
                tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_icon, 0, 0, 0);
                e.printStackTrace();

            }
            tabThree.setPadding(0, 20, 0, 0);
            tabThree.setCompoundDrawablePadding(15);

            tabLayout.getTabAt(tablayout_count++).setCustomView(tabThree);
        }
        /*TextView tabOne = (TextView)LayoutInflater.from(this).inflate(R.layout.customtab_lay,null);
        tabOne.setText("English");
        tabOne.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
        tabOne.setTextColor(GlobalDataAccess.GetColorresr(R.color.white,this));
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.uk_flag_16,0,0,0);

        tabOne.setPadding(0,20,0,0);
        tabOne.setCompoundDrawablePadding(15);

        tabLayout.getTabAt(1).setCustomView(tabOne);


        TextView tabTwo = (TextView)LayoutInflater.from(this).inflate(R.layout.customtab_lay,null);
        tabTwo.setText("Serbian");
        tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
        tabTwo.setTextColor(GlobalDataAccess.GetColorresr(R.color.white,this));
        tabTwo.setPadding(0,20,0,0);
        tabTwo.setCompoundDrawablePadding(15);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.serbia_16,0,0,0);
        tabLayout.getTabAt(2).setCustomView(tabTwo);

        TextView tabThree = (TextView)LayoutInflater.from(this).inflate(R.layout.customtab_lay,null);
        tabThree.setText("German");
        tabThree.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
        tabThree.setPadding(0,20,0,0);
        tabThree.setCompoundDrawablePadding(15);
        tabThree.setTextColor(GlobalDataAccess.GetColorresr(R.color.white,this));

        tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.german_16,0,0,0);
        tabLayout.getTabAt(3).setCustomView(tabThree);*/

        if (modelTenderDetails.getAttachmentArraylist() != null)
            if (modelTenderDetails.getAttachmentArraylist().size() > 0) {
                TextView tabfour = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
                tabfour.setText(getResources().getString(R.string.Attach_and_document));
                tabfour.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                tabfour.setPadding(0, 20, 0, 0);
                // tabfour.setCompoundDrawablePadding(15);
                tabfour.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
                // tabzero.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu_camera,0,0,0);
                tabLayout.getTabAt(tablayout_count++).setCustomView(tabfour);
            }

        if (modelTenderDetails.getLinksArrayList() != null)
            if (modelTenderDetails.getLinksArrayList().size() > 0) {
                TextView tabfive = (TextView) LayoutInflater.from(this).inflate(R.layout.customtab_lay, null);
                tabfive.setText(getResources().getString(R.string.related_information));
                tabfive.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                tabfive.setPadding(0, 20, 0, 0);
                //tabfive.setCompoundDrawablePadding(15);

                tabfive.setTextColor(GlobalDataAccess.GetColorresr(R.color.white, this));
                // tabzero.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu_camera,0,0,0);
                tabLayout.getTabAt(tablayout_count++).setCustomView(tabfive);
            }


    }

    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_summary(), getResources().getString(R.string.summary));
        if (modelTenderDetails.getDetailsArrayList().size() > 0)
            adapter.addFragment(new FragmentDetailsEnglish(), "");

        if (modelTenderDetails.getDetailsArrayList().size() > 1)
            adapter.addFragment(new Fragment_details_serbian(), "");

        if (modelTenderDetails.getDetailsArrayList().size() > 2)
            adapter.addFragment(new FragmentDetialsGerman(), "");

        if (modelTenderDetails.getAttachmentArraylist() != null)
            if (modelTenderDetails.getAttachmentArraylist().size() > 0)
                adapter.addFragment(new Fragment_Attachment_Documents(), getResources().getString(R.string.Attach_and_document));

        if (modelTenderDetails.getLinksArrayList() != null)
            if (modelTenderDetails.getLinksArrayList().size() > 0)
                adapter.addFragment(new Fragment_Related_Info(), getResources().getString(R.string.related_information));

        // adapter.addFragment(new Fragment_Related_Info(), "Related information");

        viewPager.setAdapter(adapter);


    }

    @Override
    public void DataFatchNotifier() {
       /* progressDialog.hide();
        setupViewPager(viewPager);
        CreateTABIcon();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //today
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem action_search = menu.findItem(R.id.action_search);
        action_search.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {


            this.finish();

            return true;

        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_askAdvisor) {
            // first of all check the avaluble advice questions firstly
            Log.e(TAG, "value of hte get CountryTender is :" + modelTenderDetails.toString());
            String country_id = modelTenderDetails.getCountryTender().toString().substring(0, 3);

            if (country_id.equals("001") || country_id.equals("003")) {
               /* Intent intent = new Intent(this, AskAdvisor.class);
                intent.putExtra(GlobalDataAcess.Ask_advice_tendAval, 1);
                intent.putExtra(GlobalDataAcess.Ask_advice_tenderId, modelTenderDetails.getIdTender());
                intent.putExtra(GlobalDataAcess.Ask_advice_countryId, modelTenderDetails.getCountryTender());
                startActivity(intent);*/
//               progressDialog.show();
                progress_ll.setVisibility(View.VISIBLE);
                GetCheckAdviceQuestionNumber();

            } else {
                Toast.makeText(this, getResources().getString(R.string.not_allow_for_count), Toast.LENGTH_SHORT).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public void GetData() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();

        //final String mURL = UtillClasses.BaseUrl+"/get_tenders_list";
        final String mURL = "https://www.tenderilive.com/xmlrpcs/mobile/get_tender_details";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e(TAG ," tender list responce "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            // Log.e(TAG,"detials come from tender details "+jsonObject.toString());
                            if (jsonObject.getInt("statusCode") == 200) {
                                // Log.e(TAG,"responce of tender details is "+response);
                                //JSONArray jsonArray = jsonObject.getJSONArray("data");
                                JSONObject jsondataObject = jsonObject.getJSONObject("data");
                                //int size = jsonArray.length();


                                JSONObject jsondata = jsondataObject.getJSONObject("summary");
                                // Log.e(TAG,"tendr details summery table "+jsondata.toString());
                                //ModelTenderList modelTenderDetails = new ModelTenderList();
                                modelTenderDetails.setAmountCurrency(jsondata.optString("amountCurrency"));
                                modelTenderDetails.setAmountTender(jsondata.optDouble("amountTender"));
                                //Log.e(TAG,"amount of the tender is :"+jsondata.optDouble("amountTender"));
                                modelTenderDetails.setAssignedTender(jsondata.optString("assignedTender"));
                                modelTenderDetails.setCategoriesTender(jsondata.optString("categoriesTender"));
                                modelTenderDetails.setCityBuyer(jsondata.optString("cityBuyer"));
                                // modelTenderDetails.setCityTender(jsondata.optString("cityTender"));
                                modelTenderDetails.setCityTender(jsondata.optString("cityBuyerID"));
                                modelTenderDetails.setCountryFlagTender(jsondata.optString("countryFlagTender"));
                                //modelTenderDetails.setCountryTender(jsondata.optString("countryTender"));
                                modelTenderDetails.setCountryTender(jsondata.optString("countryTender"));
                                //  Log.e(TAG,"value of country trnder from server and value is "+jsondata.optString("countryTender"));
                                modelTenderDetails.setDateDocumentation(jsondata.optString("dateDocumentation"));
                                modelTenderDetails.setDateStart(jsondata.optString("dateStart"));
                                modelTenderDetails.setDateStop(jsondata.optString("dateStop"));
                                modelTenderDetails.setDocumentationCurrency(jsondata.optString("documentationCurrency"));
                                modelTenderDetails.setDocumentationPrice(jsondata.optDouble("documentationPrice"));
                                modelTenderDetails.setIdAwardCriteria(jsondata.optString("idAwardCriteria"));
                                modelTenderDetails.setIdBuyer(jsondata.optString("idBuyer"));
                                modelTenderDetails.setIdContract(jsondata.optString("idContract"));
                                modelTenderDetails.setIdDocument(jsondata.optString("idDocument"));
                                modelTenderDetails.setIdNUTS(jsondata.optString("idNUTS"));
                                modelTenderDetails.setIdProcedure(jsondata.optString("idProcedure"));
                                modelTenderDetails.setIdTender(jsondata.optString("idTender"));
                                // modelTenderDetails.setMainLanguage(jsondata.optString("mainLanguage"));
                                modelTenderDetails.setMainLanguage(jsondata.optString("mainLanguageName"));
                                modelTenderDetails.setNameAwardCriteria(jsondata.optString("nameAwardCriteria"));
                                modelTenderDetails.setNameBuyer(jsondata.optString("nameBuyer"));
                                modelTenderDetails.setNameContract(jsondata.optString("nameContract"));
                                modelTenderDetails.setNameDocument(jsondata.optString("nameDocument"));
                                modelTenderDetails.setNameProcedure(jsondata.optString("nameProcedure"));
                                modelTenderDetails.setOptionsTender(jsondata.optString("optionsTender"));
                                modelTenderDetails.setSourcetender(jsondata.optString("sourceTender"));
                                modelTenderDetails.setStatusTender(jsondata.optString("statusTender"));
                                modelTenderDetails.setTagTender(jsondata.optString("tagTender"));
                                modelTenderDetails.setTitleTender(jsondata.optString("titleTender"));
                                modelTenderDetails.setTypeTender(jsondata.optString("typeTender"));
                                modelTenderDetails.setUrlTender(jsondata.optString("urlTender"));
                                modelTenderDetails.setCountry_name(jsondata.optString("countryTenderName"));
                                // Log.e(TAG,"tender detail  country tender name is :"+jsondata.optString("countryTenderName"));
                                modelTenderDetails.setCity_name(jsondata.optString("cityBuyer"));

                                //Featching data for Detils section

                                if (!jsondataObject.isNull("details")) {
                                    ArrayList<ModelTenDetalDetails> detailsArrayList = new ArrayList<>();

                                    JSONArray jsonArrayDetial = jsondataObject.getJSONArray("details");
                                    int size = jsonArrayDetial.length();


                                    for (int i = 0; i < size; i++) {
                                        ModelTenDetalDetails modelDetails = new ModelTenDetalDetails();
                                        JSONObject jsonDetils = jsonArrayDetial.getJSONObject(i);

                                        modelDetails.setTitleTender(jsonDetils.optString("titleTender"));
                                        modelDetails.setDescriptionTender(jsonDetils.optString("descriptionTender"));
                                        modelDetails.setFlagTender(jsonDetils.optString("flagTender"));
                                        modelDetails.setLanguageTender(jsonDetils.optString("languageTender"));
                                        modelDetails.setSubtitleTender(jsonDetils.optString("subtitleTender"));
                                        modelDetails.setLanguageTenderName(jsonDetils.optString("languageTenderName"));
                                        // Log.e(TAG,"tender details value  for details is  "+jsonDetils.toString());
                                        detailsArrayList.add(modelDetails);
                                        if (i == size - 1) {
                                            //        Log.e(TAG," tender details details parse done");
                                        }
                                    }
                                    modelTenderDetails.setDetailsArrayList(detailsArrayList);
                                    // dataBaseHalper.InsertTenderDetailsDetisls(detailsArrayList,FragmentTender.tenderId);
                                    dataBaseHalper.InsertTenderDetailsDetisls(detailsArrayList, sessionManager.GetTenderIdDetails());
                                }

                                //Featching the data for Attachment
                                ArrayList<ModelTenDetailAttach> attachArrayList = new ArrayList<>();

                                attachArrayList.clear();
                                if (!jsondataObject.isNull("attachments")) {

                                    JSONArray jsonArrayDetial = jsondataObject.getJSONArray("attachments");
                                    int size = jsonArrayDetial.length();
                                    for (int i = 0; i < size; i++) {
                                        ModelTenDetailAttach modelDetails = new ModelTenDetailAttach();
                                        JSONObject jsonDetils = jsonArrayDetial.getJSONObject(i);
                                        modelDetails.setDocType("attachment");
//                                        modelDetails.setDocType("1");
                                        modelDetails.setIdAttachment(jsonDetils.optString("idAttachment"));
                                        modelDetails.setLanguageAttachment(jsonDetils.optString("languageAttachment"));
                                        modelDetails.setNameAttach(jsonDetils.optString("nameAttachment"));
                                        modelDetails.setNameElipsize(jsonDetils.optString("nameEllipsize"));
                                        modelDetails.setFlagAttachment(jsonDetils.optString("flagAttachment"));

                                        attachArrayList.add(modelDetails);
                                    }

                                    /*if (!jsondataObject.isNull("documents")){
                                        //ArrayList<ModelTenDetailDocuments> detailsArrayList = new ArrayList<>();

                                        JSONArray jsonArrayDetialDoc = jsondataObject.getJSONArray("documents");
                                        int sizedoc= jsonArrayDetial.length();

                                        for (int i= 0;i<sizedoc;i++) {
                                            //ModelTenDetailDocuments modelDetails = new ModelTenDetailDocuments();
                                            ModelTenDetailAttach modelDetails = new ModelTenDetailAttach();

                                            JSONObject jsonDetils = jsonArrayDetialDoc.getJSONObject(i);
                                            modelDetails.setIdAttachment(jsonDetils.optString("idDocument"));
                                            modelDetails.setLanguageAttachment(jsonDetils.optString("languageDocument"));
                                            modelDetails.setNameAttach(jsonDetils.optString("nameDocument"));
                                            modelDetails.setNameElipsize(jsonDetils.optString("nameEllipsize"));
                                            modelDetails.setFlagAttachment(jsonDetils.optString("flagDocument"));
                                            modelDetails.setDocType("document");
                                            attachArrayList.add(modelDetails);
                                        }
                                        //modelTenderDetails.setDocumentsArrayList(detailsArrayList);
                                    }*/


                                }
                                if (!jsondataObject.isNull("documents")) {
                                    //ArrayList<ModelTenDetailDocuments> detailsArrayList = new ArrayList<>();

                                    JSONArray jsonArrayDetialDoc = jsondataObject.getJSONArray("documents");
                                    int sizedoc = jsonArrayDetialDoc.length();

                                    for (int i = 0; i < sizedoc; i++) {
                                        //ModelTenDetailDocuments modelDetails = new ModelTenDetailDocuments();
                                        ModelTenDetailAttach modelDetails = new ModelTenDetailAttach();

                                        JSONObject jsonDetils = jsonArrayDetialDoc.getJSONObject(i);
                                        modelDetails.setDocType("document");
//                                        modelDetails.setDocType("2");
                                        modelDetails.setIdAttachment(jsonDetils.optString("idDocument"));
                                        modelDetails.setLanguageAttachment(jsonDetils.optString("languageDocument"));
                                        modelDetails.setNameAttach(jsonDetils.optString("nameDocument"));
                                        modelDetails.setNameElipsize(jsonDetils.optString("nameEllipsize"));
                                        modelDetails.setFlagAttachment(jsonDetils.optString("flagDocument"));
                                        attachArrayList.add(modelDetails);
                                    }
                                    //modelTenderDetails.setDocumentsArrayList(detailsArrayList);
                                }
                                modelTenderDetails.setAttachmentArraylist(attachArrayList);
                                //dataBaseHalper.InsertTenderDetailsAttachments(attachArrayList,FragmentTender.tenderId);
                                dataBaseHalper.InsertTenderDetailsAttachments(attachArrayList, sessionManager.GetTenderIdDetails());
                               /* //Featching the data  for documents
                                if (!jsondataObject.isNull("documents")){
                                    ArrayList<ModelTenDetailDocuments> detailsArrayList = new ArrayList<>();

                                    JSONArray jsonArrayDetial = jsondataObject.getJSONArray("documents");
                                    int size= jsonArrayDetial.length();

                                    for (int i= 0;i<size;i++) {
                                        ModelTenDetailDocuments modelDetails = new ModelTenDetailDocuments();
                                        JSONObject jsonDetils = jsonArrayDetial.getJSONObject(i);
                                        modelDetails.setIdDocuments(jsonDetils.optString("idDocument"));
                                        modelDetails.setLangDocuments(jsonDetils.optString("languageDocument"));
                                        modelDetails.setNameDocuments(jsonDetils.optString("nameDocument"));
                                        modelDetails.setNameEllipsize(jsonDetils.optString("nameEllipsize"));
                                        modelDetails.setFlagDocument(jsonDetils.optString("flagDocument"));
                                        detailsArrayList.add(modelDetails);
                                    }
                                    modelTenderDetails.setDocumentsArrayList(detailsArrayList);
                                }*/

                                //Featching the data  for Link
                                if (!jsondataObject.isNull("links")) {
                                    ArrayList<ModelTenDetailLinks> detailsArrayList = new ArrayList<>();

                                    JSONArray jsonArrayDetial = jsondataObject.getJSONArray("links");
                                    int size = jsonArrayDetial.length();

                                    for (int i = 0; i < size; i++) {
                                        ModelTenDetailLinks modelDetails = new ModelTenDetailLinks();
                                        JSONObject jsonDetils = jsonArrayDetial.getJSONObject(i);
                                        //  Log.e(TAG,"tender details link recived data is :"+jsonDetils.toString());
                                        modelDetails.setIdTenderLinks(jsonDetils.optString("idTenderLink"));
                                        modelDetails.setTypeTenderLink(jsonDetils.optString("typeTenderLink"));
                                        modelDetails.setTitleTenderLink(jsonDetils.optString("titleTenderLink"));
                                        modelDetails.setDateStartLink(jsonDetils.optString("dateStartLinkFmt"));
                                        // modelDetails.setDateStartLink(jsonDetils.optString("dateStartLink"));
                                        // modelDetails.setDateStopLink(jsonDetils.optString("dateStopLink"));
                                        modelDetails.setDateStopLink(jsonDetils.optString("dateStopLinkFmt"));

                                        // Log.e(TAG,"tender link start date :"+jsonDetils.optString("dateStartLinkFmt")+" or stop date is :"+jsonDetils.optString("dateStopLinkFmt"));
                                        detailsArrayList.add(modelDetails);
                                    }
                                    modelTenderDetails.setLinksArrayList(detailsArrayList);
                                    // dataBaseHalper.InsertTenderDetailsLinks(detailsArrayList,FragmentTender.tenderId);
                                    dataBaseHalper.InsertTenderDetailsLinks(detailsArrayList, sessionManager.GetTenderIdDetails());
                                }
                                dataBaseHalper.InsertTenDetailsMain(modelTenderDetails);
                                Log.e(TAG, "prog end at get data from server");
//                                progressDialog.hide();
                                progress_ll.setVisibility(View.GONE);
                                setupViewPager(viewPager);
                                CreateTABIcon();

                            } else {
                                if (viewPager != null)
                                    Snackbar.make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_SHORT).show();
                                progress_ll.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "detials come from tender details " + response);
                            Log.e(TAG, "error on getting the parse exception " + e.getCause());
                            if (viewPager != null)
                                Snackbar.make(viewPager, getResources().getString(R.string.error_in_tender), Snackbar.LENGTH_LONG).show();

                            //Firebase
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sessionManager.GetUserEmail());
                            bundle.putString(GlobalDataAcess.Class_name, "Tender Detials");
                            bundle.putString(GlobalDataAcess.Tender_id, sessionManager.GetTenderIdDetails());
                            bundle.putString(GlobalDataAcess.Tender_download_status, "Fail having parsing error");

                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                            firebaseAnalytics.setUserProperty("Tender_detial", "Error in dwonloding tender details");


//                            progressDialog.hide();
                            progress_ll.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                //Log.e(TAG,"detials come from tender details "+response);)

//                progressDialog.hide();
                progress_ll.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetData();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetData();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetData();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();


                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

         /*  @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }*/

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // return super.getParams();
                Map<String, String> mapFooder = new HashMap<>();
                //mapFooder.put(UtillClasses.idtender,FragmentTender.tenderId);
                mapFooder.put(UtillClasses.idtender, sessionManager.GetTenderIdDetails());
                mapFooder.put(UtillClasses.summary, "1");

                return mapFooder;
            }


        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    //check for hte advice question number is remaining
    public void GetCheckAdviceQuestionNumber() {
        final Map<String, String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "check_advice_question_number";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        progressDialog.hide();
                        progress_ll.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {

//                                    Intent intent = new Intent(TenderDetails.this, AskAdvisor.class);
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_tendAval, 1);
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_tenderId, modelTenderDetails.getIdTender());
//                                    intent.putExtra(GlobalDataAcess.Ask_advice_countryId, modelTenderDetails.getCountryTender());
//                                    startActivity(intent);
                                    GlobalDataAcess globalDataAcess = new GlobalDataAcess();
                                    globalDataAcess.getAskQuesDialog(TenderDetails.this, modelTenderDetails.getIdTender(), modelTenderDetails.getCountryTender(), 1);

                                } else {
                                    String message = jsonObject.optString("message");
                                    if (viewPager != null)
                                        Snackbar.make(viewPager, message, Snackbar.LENGTH_LONG).show();
                                }
                            } else {

                                if (viewPager != null)
                                    Snackbar.make(viewPager, getResources().getString(R.string.no_credit), Snackbar.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });

// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });
// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof NetworkError) {

                    Snackbar snackbar = Snackbar
                            .make(viewPager, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GetCheckAdviceQuestionNumber();
                                }
                            });
// Changing message text color
                    snackbar.setActionTextColor(Color.RED);

// Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                mapheader.put(UtillClasses.Content_Type, UtillClasses.multipart);
                //mapheader.put("Content-Type","application/x-www-form-urlencoded");
                return mapheader;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.bottom_flag:


                if ((Long.parseLong(modelTenderListArrayList.get(pinPoint).getStatusTender()) & 8192) != 8192) {
                    long flag_status_value = Long.parseLong(modelTenderListArrayList.get(pinPoint).getStatusTender()) | 8192;
                    if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(pinPoint).getIdTender(), flag_status_value + "")) {
                        modelTenderListArrayList.get(pinPoint).setStatusTender(flag_status_value + "");
                        GlobalDataAcess.SendSyncAction(getApplicationContext(), sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_flag_set, modelTenderListArrayList.get(pinPoint).getIdTender());
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.flag_select), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    long unset_flat_val = 4294967295l - 8192l;
                    long flag_deSelect_value = Long.parseLong(modelTenderListArrayList.get(pinPoint).getStatusTender()) & unset_flat_val;
                    if (dataBaseHalper.SetFlagStatus(modelTenderListArrayList.get(pinPoint).getIdTender(), flag_deSelect_value + "")) {
                        modelTenderListArrayList.get(pinPoint).setStatusTender(flag_deSelect_value + "");
                        GlobalDataAcess.SendSyncAction(getApplicationContext(), sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_flag_unset, modelTenderListArrayList.get(pinPoint).getIdTender());
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.flag_unselect), Toast.LENGTH_SHORT).show();
                    }


                }


                break;

            case R.id.bottom_tag:


                showTagAlert(pinPoint);

                break;

            case R.id.bottom_delete:

                builder = new AlertDialog.Builder(TenderDetails.this);
                mAlertDialog = builder.create();
                LayoutInflater inflater = (LayoutInflater) TenderDetails.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.confirm_delete, null, false);

                Button checheDia_canBtn = (Button) view.findViewById(R.id.checheDia_canBtn);
                Button checheDia_okBtn = (Button) view.findViewById(R.id.checheDia_okBtn);

                mAlertDialog.setView(view);

                checheDia_canBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                    }
                });

                checheDia_okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteTender();
                        mAlertDialog.dismiss();
                    }
                });

                mAlertDialog.show();


                break;
            case R.id.bottom_back:

                finish();

                break;
        }

    }

    private void deleteTender() {
        if (!isRemoved) {


            if (((Long.parseLong(modelTenderListArrayList.get(pinPoint).getStatusTender()) & Long.parseLong("2147483648")) != Long.parseLong("2147483648"))) {
                long tenderId = Long.parseLong(modelTenderListArrayList.get(pinPoint).getStatusTender());
                long tender_delete_val = (tenderId | Long.parseLong("2147483648"));
                Log.e(TAG, "Tender was deleted and delete value is " + tender_delete_val);
                if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(pinPoint).getIdTender(), tender_delete_val + "")) {
                    GlobalDataAcess.SendSyncAction(getApplicationContext(), sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_delete, modelTenderListArrayList.get(pinPoint).getIdTender());
                    Toast.makeText(this, this.getResources().getString(R.string.tender_deleted), Toast.LENGTH_SHORT).show();
                    isDeleted = true;
                    isRemoved = true;
                }
            } else {

                Toast.makeText(this, getResources().getString(R.string.already_deleted), Toast.LENGTH_SHORT).show();


                //client just asked for delete not for restore
                        /*
                        long tenderId = Long.parseLong(modelTenderListArrayList.get(getAdapterPosition()).getStatusTender());
                        long tender_restore_val = ( tenderId & Long.parseLong("2147483647"));
                        Log.e(TAG, "Tender was deleted and restore value is " + tender_restore_val);
                        if (dataBaseHalper.DeleteTenderData(modelTenderListArrayList.get(getAdapterPosition()).getIdTender(), "" + tender_restore_val)) {
                            GlobalDataAcess.SendSyncAction(this,sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_undelete, modelTenderListArrayList.get(getAdapterPosition()).getIdTender() );
                            modelTenderListArrayList.remove(getAdapterPosition());
                            Toast.makeText(this, this.getResources().getString(R.string.tender_restored), Toast.LENGTH_SHORT).show();

                        }*/
            }


        } else {

            Toast.makeText(this, getResources().getString(R.string.already_deleted), Toast.LENGTH_SHORT).show();
        }


    }

    public void showTagAlert(final int adapterPosition) {

        if (Home.modelUserTagArrayList.size() > 0) {
            build = new AlertDialog.Builder(this);
            LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view_alert = layoutInflater.inflate(R.layout.tenderlist_tag_recylay, null, false);
            //show tag dilaog
            final ArrayList<String> first_list = new ArrayList();
            final ArrayList<String> final_list = new ArrayList();

            mAlertDialog = build.create();

            modelTagArrayList = new ArrayList<>();
            modelTagArrayList.clear();
            modelTagArrayList.addAll(Home.modelUserTagArrayList);

            int sizearraylist = modelTagArrayList.size();
            for (int b = 0; sizearraylist > b; b++) {
                modelTagArrayList.get(b).setIstagset(false);
            }

            final String tagTender = modelTenderListArrayList.get(adapterPosition).getTagTender();

            //new concept 18-08-2017
            for (int i = 0; i < modelTagArrayList.size(); i++) {
                if ((Long.parseLong(tagTender) & Long.parseLong(modelTagArrayList.get(i).getTagIndex())) > 0) {
                    modelTagArrayList.get(i).setIstagset(true);
                    first_list.add(modelTagArrayList.get(i).getTagIndex());
                }
            }

            mRecyclerviewTag = (RecyclerView) view_alert.findViewById(R.id.tender_list_tag_recy);
            setTagCanBtn = (Button) view_alert.findViewById(R.id.tender_list_tag_canBtn);
            setTagUpdBtn = (Button) view_alert.findViewById(R.id.tender_list_tag_updBtn);

            mRecyclerviewTag.setHasFixedSize(true);
            //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
            GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 1);
            linearLayoutManager.setAutoMeasureEnabled(true);
            mRecyclerviewTag.setLayoutManager(linearLayoutManager);

            adapterTag = new TenderListSetTagAdapter(this, modelTagArrayList);

            mRecyclerviewTag.setAdapter(adapterTag);

            //handling recyclerivew clicking
            mRecyclerviewTag.addOnItemTouchListener(new RecycerItemClickListener(this, new RecycerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position) {
                    final CheckBox checkBox = (CheckBox) view.findViewById(R.id.tender_list_tag_radiobtn);

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                Log.e(TAG, "radion button is checked at  position " + position);
                                modelTagArrayList.get(position).setIstagset(true);
                                checkBox.setChecked(true);
                            } else {
                                // Log.e(TAG,"radion button is checked at  position "+position);
                                Log.e(TAG, "radion button is not checked at  position " + position);
                                modelTagArrayList.get(position).setIstagset(false);
                                checkBox.setChecked(false);
                            }
                        }
                    });


                }
            }));


            setTagCanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAlertDialog.dismiss();
                    first_list.clear();
                    final_list.clear();

                }
            });

            setTagUpdBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long tagStringdata = 0;
                    boolean first = true;
                    for (int i = 0; i < modelTagArrayList.size(); i++) {
                        if (modelTagArrayList.get(i).istagset()) {

                            final_list.add(modelTagArrayList.get(i).getTagIndex());
                            //new concept 18-05-2017
                            tagStringdata = tagStringdata + Long.parseLong(modelTagArrayList.get(i).getTagIndex());
                        }
                    }

                    //set the data to database
                    if (dataBaseHalper.UpdateTagList(tagStringdata + "", modelTenderListArrayList.get(adapterPosition).getIdTender())) {
                        mAlertDialog.dismiss();
                        modelTenderListArrayList.get(adapterPosition).setTagTender(tagStringdata + "");

                        // newly added tag
                        ArrayList<String> newlyAdded = new ArrayList<String>(final_list);
                        newlyAdded.removeAll(first_list); // it gives newly added data
//
//                        first_list.removeAll(newlyAdded); // this will give all removed tag list
//                        ArrayList<String> removedTags = new ArrayList<String>();
                        first_list.removeAll(intersect(first_list, final_list));


                        //List<String> result_this = final_list.stream().filter(elem -> ! first_list.contains(elem)).collect(Collectors.<String>toList());

                        for (String newAdded : newlyAdded) {
                            String tender_action = modelTenderListArrayList.get(adapterPosition).getIdTender() + "|" + newAdded;
                            GlobalDataAcess.SendSyncAction(getApplicationContext(), sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_tag_set, tender_action);
                        }

                        for (String removed_tag : first_list) {
                            String tender_action = modelTenderListArrayList.get(adapterPosition).getIdTender() + "|" + removed_tag;
                            GlobalDataAcess.SendSyncAction(getApplicationContext(), sessionManager.GetDeviceInfo(), UtillClasses.Action_tender_tag_unset, tender_action);
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }


                    Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.tag_info_update), Toast.LENGTH_SHORT).show();

                }
            });


            mAlertDialog.setView(view_alert);

            mAlertDialog.show();

        } else {
            if (build == null)
                build = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            build.setTitle("");

            build.setMessage(this.getResources().getString(R.string.not_tag_found));
            build.setCancelable(true);
        /*build.setPositiveButton(DialogInterface.BUTTON_NEUTRAL, mActivity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/
            build.setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            build.show();
        }
    }

    <T> Collection<T> union(Collection<T> coll1, Collection<T> coll2) {
        Set<T> union = new HashSet<>(coll1);
        union.addAll(new HashSet<>(coll2));
        return union;
    }

    <T> Collection<T> intersect(Collection<T> coll1, Collection<T> coll2) {
        Set<T> intersection = new HashSet<>(coll1);
        intersection.retainAll(new HashSet<>(coll2));
        return intersection;
    }

    <T> Collection<T> nonOverLap(Collection<T> coll1, Collection<T> coll2) {
        Collection<T> result = union(coll1, coll2);
        result.removeAll(intersect(coll1, coll2));
        return result;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }


    }

    private class GetDatabaseData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // modelTenderDetails = dataBaseHalper.GetTenderDetials(FragmentTender.tenderId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progressDialog.hide();
            progress_ll.setVisibility(View.GONE);
            setupViewPager(viewPager);
            CreateTABIcon();

        }
    }


}

