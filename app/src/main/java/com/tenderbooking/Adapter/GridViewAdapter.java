package com.tenderbooking.Adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.tenderbooking.Model.ModelTenderListIcons;

import java.util.List;

/**
 * Created by laxmikant bolya on 30-03-2017.
 */

public class GridViewAdapter extends BaseAdapter {

    Activity mActivity;
    List<ModelTenderListIcons> arraylist;

    public GridViewAdapter(Activity mActivity, List<ModelTenderListIcons> imageArray) {
        this.mActivity = mActivity;
        this.arraylist = imageArray;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView mImageview;
        if (arraylist.size() > 0) {

            if (convertView == null) {
                mImageview = new ImageView(mActivity);
                mImageview.setLayoutParams(new GridView.LayoutParams(45, 45));
                mImageview.setPadding(0, 0, 0, 0);
            } else {
                mImageview = (ImageView) convertView;
            }

            mImageview.setBackgroundResource(arraylist.get(position).getResourcepath());

            if (arraylist.get(position).getNumber() == 1) {
                //mImageview.setColorFilter(Color.parseColor("#5354e1"));
                //   mImageview.setColorFilter(Color.parseColor("#"+arraylist.get(position).getColourcode()));

            }
            mImageview.setId(position);

            return mImageview;
        } else {
            mImageview = (ImageView) convertView;
        }
        return mImageview;
    }
}
