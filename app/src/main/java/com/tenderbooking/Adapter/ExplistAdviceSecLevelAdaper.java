package com.tenderbooking.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.InterfaceAdviceChildClick;
import com.tenderbooking.MainTab.ExpandableListDataSource;
import com.tenderbooking.MainTab.Fragment_advice;
import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by laxmikant bolya on 20-04-2017.
 */

public class ExplistAdviceSecLevelAdaper extends BaseExpandableListAdapter implements View.OnClickListener {

    private Context mContext;
    private List<String> mExpandableListTitle;
    private LinkedHashMap<String, List<String>> mExpandableListDetail;
    private ArrayList<String> drawer_array;
    private LayoutInflater mLayoutInflater;
    private SessionManager sessionManager;

    public ExplistAdviceSecLevelAdaper(Context context, List<String> expandableListTitle,
                                       LinkedHashMap<String, List<String>> expandableListDetail) {
        mContext = context;
        mExpandableListTitle = expandableListTitle;
        mExpandableListDetail = expandableListDetail;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManager = new SessionManager(context);

    }

    @Override
    public int getGroupCount() {
        return mExpandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mExpandableListTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).get(childPosition);

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.custlay_adv_subcat, null);
        }
        LinearLayout linearDevider = (LinearLayout) convertView.findViewById(R.id.custlay_div);


        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.custlay_text);
        //finding size

        ImageView icon = (ImageView) convertView.findViewById(R.id.icon_navidra);
        ImageView custlay_openSubMen = (ImageView) convertView.findViewById(R.id.custlay_openSubMen);
        icon.setVisibility(View.GONE);
        // icon.setImageDrawable(ExpandableListDataSource.expandListImage.get(groupPosition).get(0));

        if (mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size() > 0) {
            custlay_openSubMen.setVisibility(View.GONE);
        } else {
            custlay_openSubMen.setVisibility(View.GONE);
        }

        //custlay_openSubMen.setOnClickListener(this);
        try {
            Picasso.with(mContext).load(ExpandableListDataSource.icon[groupPosition]).into(icon);
        } catch (Exception e) {
            e.printStackTrace();
        }
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        int a = 0;
        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.custlay_adv_child, null);
        }

        convertView.setTag(childPosition);
        //   convertView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        //  parent.setBackgroundColor(ContextCompat.getColor(mContext,R.color.blue));
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.exp_chil_custtext);
        // expandedListTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Home.toolbar.setTitle(sessionManager.GetAdviceToolbarTitle());
                Home.drawer.closeDrawer(Gravity.LEFT);
                InterfaceAdviceChildClick interfaceAdviceChildClick = new Fragment_advice();
                interfaceAdviceChildClick.GetAdviceChildName(expandedListText);

                String selected_menu = expandedListText.length() > 18 ? expandedListText.substring(0, Math.min(expandedListText.length(), 15)) : expandedListText;


                if (expandedListText.length() <= 18) {

                    sessionManager.SetAdviceToolbarTitle(expandedListText);
                    Home.toolbar.setTitle(expandedListText);
                } else {

                    sessionManager.SetAdviceToolbarTitle(selected_menu + "...");
                    Home.toolbar.setTitle(selected_menu + "...");
                }

                if (v.getTag() != null) {
                    GlobalDataAcess.group_select_advice_child = (Integer) v.getTag();
                }

            }
        });


        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.exp_chil_custtext:
                //  Toast.makeText(mContext, "click on ", Toast.LENGTH_SHORT).show();
                break;

            //  Toast.makeText(mContext, "Click on list", Toast.LENGTH_SHORT).show();
        }

    }


}