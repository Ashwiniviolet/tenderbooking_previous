package com.tenderbooking.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.Model.ModelTenDetailAttach;
import com.tenderbooking.R;

import java.util.ArrayList;

/**
 * Created by laxmikant bolya on 17-03-2017.
 */

public class TenderAttachmentAdapter extends RecyclerView.Adapter<TenderAttachmentAdapter.MYViewHolder> {


    ArrayList<ModelTenDetailAttach> modelTenDetailAttachArrayList;
    private Activity mActivity;
    private View.OnClickListener onClick;

    public TenderAttachmentAdapter(Activity mActivity, ArrayList<ModelTenDetailAttach> modelTenDetailAttachArrayList, View.OnClickListener onClick) {
        this.mActivity = mActivity;
        this.modelTenDetailAttachArrayList = modelTenDetailAttachArrayList;
        this.onClick = onClick;
    }


    @Override
    public MYViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.fragment_attachment_custlay, parent, false);


        return new TenderAttachmentAdapter.MYViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MYViewHolder holder, int position) {
        if (modelTenDetailAttachArrayList.size() > 0) {
            holder.fileName.setText(modelTenDetailAttachArrayList.get(position).getNameAttach());

            try {
                Picasso.with(mActivity).load("https://www.tenderilive.com/images/icons/flags/" + modelTenDetailAttachArrayList.get(position).getFlagAttachment() + "32.png").error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon).into(holder.imageview);
            } catch (Exception e) {
                Picasso.with(mActivity).load(R.drawable.app_icon).into(holder.imageview);
                e.printStackTrace();
            }
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(onClick);

        }
    }

    @Override
    public int getItemCount() {
        if (modelTenDetailAttachArrayList != null) {
            return modelTenDetailAttachArrayList.size();
        } else
            return 0;
    }

    public class MYViewHolder extends RecyclerView.ViewHolder {

        ImageView imageview;
        TextView fileName;
        LinearLayout tend_det_ll;

        public MYViewHolder(View itemView) {
            super(itemView);
            imageview = (ImageView) itemView.findViewById(R.id.tend_det_attCustlay_flagImg);
            fileName = (TextView) itemView.findViewById(R.id.tend_det_attCustlay_fileName);
            tend_det_ll = (LinearLayout) itemView.findViewById(R.id.tend_det_ll);


        }
    }
}
