package com.tenderbooking.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;

import java.util.ArrayList;

/**
 * Created by laxmikant bolya on 21-04-2017.
 */

public class TenderListSetTagAdapter extends RecyclerView.Adapter<TenderListSetTagAdapter.MyViewHolder> {

    public Activity mActivity;
    public ArrayList<ModelUserTag> modelTagArrayList;

    public TenderListSetTagAdapter(Activity mActivity, ArrayList<ModelUserTag> modelTagArrayList) {

        this.mActivity = mActivity;
        this.modelTagArrayList = modelTagArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.tender_list_add_tag, null, false);
        return new TenderListSetTagAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (modelTagArrayList.size() > 0) {
            holder.myradiobtn.setText(modelTagArrayList.get(position).getTagText());
            holder.myradiobtn.setChecked(modelTagArrayList.get(position).istagset());
            // holder.tender_list_tag_colorimg.setBackgroundColor("0x"+modelTagArrayList.get(position).getColorCSS());
            holder.tender_list_tag_colorimg.setBackgroundColor(Color.parseColor("#" + modelTagArrayList.get(position).getColorCSS()));
           /* holder.myradiobtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            modelTagArrayList.get(position).setIstagset(true);
                        }else{
                            modelTagArrayList.get(position).setIstagset(false);
                        }
                     }
            });*/
        }
    }

    @Override
    public int getItemCount() {
        return modelTagArrayList == null ? 0 : modelTagArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //RadioButton myradiobtn;
        CheckBox myradiobtn;
        ImageView tender_list_tag_colorimg;

        public MyViewHolder(View itemView) {
            super(itemView);
            myradiobtn = (CheckBox) itemView.findViewById(R.id.tender_list_tag_radiobtn);
            tender_list_tag_colorimg = (ImageView) itemView.findViewById(R.id.tender_list_tag_colorimg);
        }
    }
}
