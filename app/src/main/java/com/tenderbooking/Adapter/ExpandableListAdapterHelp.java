package com.tenderbooking.Adapter;

/**
 * Created by laxmikant bolya on 25-03-2017.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListAdapterHelp extends BaseExpandableListAdapter implements View.OnClickListener {

    int textsize;
    private Context mContext;
    private List<String> mExpandableListTitle;
    private LinkedHashMap<String, List<String>> mExpandableListDetail;
    private ArrayList<String> drawer_array;
    private LayoutInflater mLayoutInflater;

    public ExpandableListAdapterHelp(Context context, List<String> expandableListTitle,
                                     LinkedHashMap<String, List<String>> expandableListDetail, int textsize) {
        mContext = context;
        mExpandableListTitle = expandableListTitle;
        mExpandableListDetail = expandableListDetail;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.textsize = textsize;
    }

    @Override
    public int getGroupCount() {
        return mExpandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mExpandableListTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).get(childPosition);

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.helptitle_custlay, null);
        }


        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.help_titcustlay_title);


        listTitleTextView.setTypeface(null, Typeface.BOLD);
        //  listTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
        listTitleTextView.setText(listTitle + " (" + getChildrenCount(groupPosition) + ")");

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.help_detail_cutslay, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.help_titcustlay_Detail);


        expandedListTextView.setText(expandedListText);
        //expandedListTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textsize);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.custlay_openSubMen:
                Toast.makeText(mContext, "Click on list", Toast.LENGTH_SHORT).show();
        }

    }
}
