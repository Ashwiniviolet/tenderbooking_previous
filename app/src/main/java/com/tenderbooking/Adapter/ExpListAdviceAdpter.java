package com.tenderbooking.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.HelperClasses.CustExpListView;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.MainTab.ExpandableListDataSource;
import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by laxmikant bolya on 20-04-2017.
 */

public class ExpListAdviceAdpter extends BaseExpandableListAdapter implements View.OnClickListener {

    public static String TAG = ExpListAdviceAdpter.class.getSimpleName();
    LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> mainHashmap;
    private Context mContext;
    private List<String> mExpandableListTitle;
    private LinkedHashMap<String, List<String>> mExpandableListDetail;
    private ArrayList<String> drawer_array;
    private LayoutInflater mLayoutInflater;
    private LinkedList<String> listMainTilte;

    public ExpListAdviceAdpter(Context context, List<String> expandableListTitle,
                               LinkedHashMap<String, List<String>> expandableListDetail, LinkedHashMap<String, ArrayList<LinkedHashMap<String, List<String>>>> mainHashmap) {
        mContext = context;
        mExpandableListTitle = expandableListTitle;
        mExpandableListDetail = expandableListDetail;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mainHashmap = mainHashmap;
        this.listMainTilte = new LinkedList(mainHashmap.keySet());
    }

    @Override
    public int getGroupCount() {
        return listMainTilte.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return mainHashmap.get(listMainTilte.get(groupPosition)).size();
        return 1;
    }

/*
    @Override
    public int getChildrenCount(int groupPosition) {
       // return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size();

    }
*/

    @Override
    public Object getGroup(int groupPosition) {
        //return mExpandableListTitle.get(groupPosition);
        return listMainTilte.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

   /* @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).get(childPosition);

    }*/

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // GlobalDataAcess.group_select_advice_main = groupPosition;


        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.custlay, null);
        }
        LinearLayout linearDevider = (LinearLayout) convertView.findViewById(R.id.custlay_div);

        if (groupPosition == 8) ;
        {
            linearDevider.setVisibility(View.VISIBLE);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.custlay_text);

        ImageView icon = (ImageView) convertView.findViewById(R.id.icon_navidra);
        ImageView custlay_openSubMen = (ImageView) convertView.findViewById(R.id.custlay_openSubMen);
        // icon.setImageDrawable(ExpandableListDataSource.expandListImage.get(groupPosition).get(0));

        if (mainHashmap.get(listMainTilte.get(groupPosition)).size() > 0) {
            custlay_openSubMen.setVisibility(View.VISIBLE);
        } else {
            custlay_openSubMen.setVisibility(View.GONE);
        }

        custlay_openSubMen.setOnClickListener(this);
        try {
            Picasso.with(mContext).load(ExpandableListDataSource.iconAdvice[groupPosition]).into(icon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isExpanded) {
            custlay_openSubMen.setImageResource(R.drawable.folder_opened);
        } else {
            custlay_openSubMen.setImageResource(R.drawable.folder_closed);
        }

        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition_main, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //final String expandedListText = (String) getChild(groupPosition_main, childPosition);
       /* if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.explist_submenu, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.exp_chil_custtext);


        expandedListTextView.setText(expandedListText);
        return convertView;*/

        CustExpListView secondExpList = new CustExpListView(mContext);
        ExplistAdviceSecLevelAdaper adapter;
        secondExpList.setGroupIndicator(null);

        //List<String> expandableListTitle ;
        LinkedHashMap<String, List<String>> expandableListDetail;

        ArrayList<LinkedHashMap<String, List<String>>> hashmapSubCatArray = mainHashmap.get(listMainTilte.get(groupPosition_main));
        final LinkedList<String> childTitle = new LinkedList<>();
        final LinkedHashMap<String, List<String>> childHashMap = new LinkedHashMap<>();

        int subCatArray = hashmapSubCatArray.size();
        ArrayList<String> listvalue = new ArrayList<>();

        for (int k = 0; subCatArray > k; k++) {
            listvalue.clear();
            listvalue = new ArrayList(hashmapSubCatArray.get(k).keySet());
            childTitle.add(listvalue.get(0));
            //childTitle.add(hashmapSubCatArray.get(k).keySet());
            // childTitle.add(setdata);
            //childHashMap.put(childTitle.get(k),hashmapSubCatArray.get(k).get(childTitle.get(k)));
            // childHashMap.put(childTitle.get(k),hashmapSubCatArray.get(k).get(hashmapSubCatArray.get(k).keySet().toString()));
            childHashMap.putAll(hashmapSubCatArray.get(k));
        }

        adapter = new ExplistAdviceSecLevelAdaper(mContext, childTitle, childHashMap);
        secondExpList.setTag(convertView);
        secondExpList.setAdapter(adapter);

        if (GlobalDataAcess.group_select_advice != -1 && groupPosition_main == GlobalDataAcess.group_select_advice_main) {
            try {
                secondExpList.expandGroup(GlobalDataAcess.group_select_advice, true);
            } catch (Exception e) {
                e.printStackTrace();
                //  Toast.makeText(mContext, "crash is happing", Toast.LENGTH_SHORT).show();
            }

        }

        secondExpList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                GlobalDataAcess.group_select_advice = groupPosition;
                GlobalDataAcess.group_select_advice_main = groupPosition_main;
                String child_value = childTitle.get(groupPosition);
                Log.e(TAG, "selected child value is :" + child_value);

                return false;
            }
        });
        secondExpList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String child_value = childTitle.get(groupPosition);
                String child_child_value = childHashMap.get(childTitle.get(groupPosition)).get(childPosition);
                Log.e(TAG, "group name is :" + child_value + " and child value is :" + child_child_value);
                return false;
            }
        });
       /*if (groupPosition==2){

       }*/


        return secondExpList;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.custlay_openSubMen:
                //  Toast.makeText(mContext, "Click on list", Toast.LENGTH_SHORT).show();
        }

    }
}
