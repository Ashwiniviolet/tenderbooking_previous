package com.tenderbooking.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tenderbooking.Model.ModelTenDetailLinks;
import com.tenderbooking.R;

import java.util.ArrayList;

/**
 * Created by laxmikant bolya on 24-03-2017.
 */

public class TenderLInkAdapter extends RecyclerView.Adapter<TenderLInkAdapter.MYViewholder> {

    Activity mActivity;
    ArrayList<ModelTenDetailLinks> modelTenDetailLinksArrayList;
    private View.OnClickListener onClick;

    public TenderLInkAdapter(Activity mActivity, ArrayList<ModelTenDetailLinks> modelTenDetailLinksArrayList, View.OnClickListener onClick) {
        this.modelTenDetailLinksArrayList = modelTenDetailLinksArrayList;
        this.mActivity = mActivity;
        this.onClick = onClick;
    }


    @Override
    public MYViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.related_info_custlay, parent, false);
        return new TenderLInkAdapter.MYViewholder(view);
    }

    @Override

    public void onBindViewHolder(MYViewholder holder, int position) {
        if (modelTenDetailLinksArrayList != null)
            if (modelTenDetailLinksArrayList.size() > 0) {

                String s = modelTenDetailLinksArrayList.get(position).getTitleTenderLink();
                if (s.contains("&quot;")) {
                    s = s.replace("&quot;", "\"");
                }
                holder.tend_link_custlay_title.setText(s);

                if (modelTenDetailLinksArrayList.get(position).getTypeTenderLink().equals("1")) {
                    holder.tend_link_custlay_typeofDoc.setText(mActivity.getResources().getString(R.string.tender));
                } else if (modelTenDetailLinksArrayList.get(position).getTypeTenderLink().equals("2")) {
                    holder.tend_link_custlay_typeofDoc.setText(mActivity.getResources().getString(R.string.tender_winner));
                } else if (modelTenDetailLinksArrayList.get(position).getTypeTenderLink().equals("8")) {
                    holder.tend_link_custlay_typeofDoc.setText(mActivity.getResources().getString(R.string.public_procurement_plan));
                } else {
                    holder.tend_link_custlay_typeofDoc.setText("");
                }
                //  holder.tend_link_custlay_typeofDoc.setText(modelTenDetailLinksArrayList.get(position).getTypeTenderLink());

                if (modelTenDetailLinksArrayList.get(position).getDateStopLink().equals("0000.00.00")) {
                    holder.tend_link_custlay_closDate.setText("-");
                } else
                    holder.tend_link_custlay_closDate.setText(modelTenDetailLinksArrayList.get(position).getDateStopLink());

                if (modelTenDetailLinksArrayList.get(position).getDateStartLink().equals("0000.00.00")) {
                    holder.tend_link_custlay_pubDate.setText("-");
                } else {
                    holder.tend_link_custlay_pubDate.setText((modelTenDetailLinksArrayList.get(position).getDateStartLink()));
                }

                holder.tend_related_ll.setTag(position);
                holder.tend_related_ll.setOnClickListener(onClick);

            }
    }

    @Override
    public int getItemCount() {
        if (modelTenDetailLinksArrayList != null) {
            return modelTenDetailLinksArrayList.size();
        } else {
            return 0;
        }
    }

    public class MYViewholder extends RecyclerView.ViewHolder {

        TextView tend_link_custlay_title, tend_link_custlay_typeofDoc, tend_link_custlay_pubDate, tend_link_custlay_closDate;
        LinearLayout tend_related_ll;

        public MYViewholder(View itemView) {
            super(itemView);
            tend_link_custlay_title = (TextView) itemView.findViewById(R.id.tend_link_custlay_title);
            tend_link_custlay_typeofDoc = (TextView) itemView.findViewById(R.id.tend_link_custlay_typeofDoc);
            tend_link_custlay_pubDate = (TextView) itemView.findViewById(R.id.tend_link_custlay_pubDate);
            tend_link_custlay_closDate = (TextView) itemView.findViewById(R.id.tend_link_custlay_closDate);
            tend_related_ll = (LinearLayout) itemView.findViewById(R.id.tend_related_ll);

        }
    }
}
