package com.tenderbooking.Adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tenderbooking.FragmentTender;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.Home;
import com.tenderbooking.Interface.DrawerItemClicked;
import com.tenderbooking.MainTab.ExpandableListDataSource;
import com.tenderbooking.Model.ModelUserFilters;
import com.tenderbooking.Model.ModelUserTag;
import com.tenderbooking.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by namok on 25-09-2017.
 */

public class ExpaTenderSecLevelAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    ArrayList<ModelUserTag> modelUserTagArrayList;
    ArrayList<ModelUserFilters> modelUserFilterArrayList;
    private Context mContext;
    private List<String> mExpandableListTitle;
    private LinkedHashMap<String, List<String>> mExpandableListDetail;
    private ArrayList<String> drawer_array;
    private LayoutInflater mLayoutInflater;
    private SessionManager sessionManager;

    public ExpaTenderSecLevelAdapter(Context context, List<String> expandableListTitle,
                                     LinkedHashMap<String, List<String>> expandableListDetail, ArrayList<ModelUserTag> modelUserTagArrayList, ArrayList<ModelUserFilters> modelUserFilterArrayList) {
        mContext = context;
        mExpandableListTitle = expandableListTitle;
        mExpandableListDetail = expandableListDetail;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManager = new SessionManager(context);
        this.modelUserTagArrayList = modelUserTagArrayList;
        this.modelUserFilterArrayList = modelUserFilterArrayList;

    }

    @Override
    public int getGroupCount() {
        return mExpandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mExpandableListTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).get(childPosition);

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.tender_exp_parent_layout, null);
        }
        LinearLayout linearDevider = (LinearLayout) convertView.findViewById(R.id.custlay_div);
//// TODO: 28-10-2017  check that langage change effect open setting tab
       /* if (Home.handler != null) {
            Home.handler.sendEmptyMessage(11);
        }*/

        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.custlay_text);
        //finding size

        ImageView icon = (ImageView) convertView.findViewById(R.id.icon_navidra);
        ImageView custlay_openSubMen = (ImageView) convertView.findViewById(R.id.custlay_openSubMen);

        //icon.setVisibility(View.GONE);
        // icon.setImageDrawable(ExpandableListDataSource.expandListImage.get(groupPosition).get(0));

        if (mExpandableListDetail.get(mExpandableListTitle.get(groupPosition)).size() > 0) {
            custlay_openSubMen.setVisibility(View.GONE);
        } else {
            custlay_openSubMen.setVisibility(View.GONE);
        }

        //custlay_openSubMen.setOnClickListener(this);
        try {
            Picasso.with(mContext).load(mExpandableListTitle.size() > 5 ? ExpandableListDataSource.icon[groupPosition] : ExpandableListDataSource.icon_filter[groupPosition]).into(icon);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        int a = 0;
        return convertView;

    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.custlay_adv_child, null);
        }
        //   convertView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        //  parent.setBackgroundColor(ContextCompat.getColor(mContext,R.color.blue));
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.exp_chil_custtext);
        // expandedListTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Home.handler != null) {
                    Home.handler.sendEmptyMessage(11);
                }
                GlobalDataAcess.group_selected = groupPosition;
                GlobalDataAcess.group_selected_child = childPosition;
                if (groupPosition == 0) { // tag
                    String tagvalue = modelUserTagArrayList.get(childPosition).getTagIndex();
                    Home.toolbar.setTitle(modelUserTagArrayList.get(childPosition).getTagText());
                    GlobalDataAcess.titleString = modelUserTagArrayList.get(childPosition).getTagText();
                    if (!tagvalue.equals(null)) {
                        Home.drawer.closeDrawer(Gravity.LEFT);
                        DrawerItemClicked drawerItemClicked = new FragmentTender();
                        drawerItemClicked.GetDrawerItemClicked(10 + groupPosition, tagvalue);
                    }
                } else if (groupPosition == 1) { // Category
                    // String categoriesValue = mExpandableListData.get(mExpandableListTitle.get(i)).get(childPosition);
                    Home.toolbar.setTitle(expandedListText);
                    GlobalDataAcess.titleString = expandedListText;
                    Home.drawer.closeDrawer(Gravity.LEFT);
                    int filterSize = modelUserFilterArrayList.size();
                    String filterId = "notAvalible";
                    for (int j = 0; j < filterSize; j++) {
                        if (modelUserFilterArrayList.get(j).getFilterName().equals(expandedListText)) {

                            filterId = modelUserFilterArrayList.get(j).getFilterId();
                            DrawerItemClicked drawerItemClicked = new FragmentTender();
                            drawerItemClicked.GetDrawerItemClicked(10 + groupPosition, filterId);

                        }
                    }
                } else if (groupPosition == 2) { // Contries
                    Home.toolbar.setTitle(expandedListText);
                    GlobalDataAcess.titleString = expandedListText;
                    Home.drawer.closeDrawer(Gravity.LEFT);
                    int filterSize = modelUserFilterArrayList.size();
                    String filterId = "notAvalible";
                    for (int j = 0; j < filterSize; j++) {
                        if (modelUserFilterArrayList.get(j).getFilterName().equals(expandedListText)) {

                            filterId = modelUserFilterArrayList.get(j).getFilterId();
                            DrawerItemClicked drawerItemClicked = new FragmentTender();
                            drawerItemClicked.GetDrawerItemClicked(10 + groupPosition, filterId);

                        }
                    }
                } else if (groupPosition == 3) { // Cities
                    Home.toolbar.setTitle(expandedListText);
                    GlobalDataAcess.titleString = expandedListText;
                    Home.drawer.closeDrawer(Gravity.LEFT);
                    int filterSize = modelUserFilterArrayList.size();
                    String filterId = "notAvalible";
                    for (int j = 0; j < filterSize; j++) {
                        if (modelUserFilterArrayList.get(j).getFilterName().equals(expandedListText)) {

                            filterId = modelUserFilterArrayList.get(j).getFilterId();
                            DrawerItemClicked drawerItemClicked = new FragmentTender();
                            drawerItemClicked.GetDrawerItemClicked(10 + groupPosition, filterId);

                        }
                    }
                } else if (groupPosition == 4) { // for buyer
                    Home.toolbar.setTitle(expandedListText);
                    GlobalDataAcess.titleString = expandedListText;
                    Home.drawer.closeDrawer(Gravity.LEFT);
                    int filterSize = modelUserFilterArrayList.size();
                    String filterId = "notAvalible";
                    for (int j = 0; j < filterSize; j++) {
                        if (modelUserFilterArrayList.get(j).getFilterName().equals(expandedListText)) {

                            filterId = modelUserFilterArrayList.get(j).getFilterId();
                            DrawerItemClicked drawerItemClicked = new FragmentTender();
                            drawerItemClicked.GetDrawerItemClicked(10 + groupPosition, filterId);

                        }
                    }
                }


//                Home.toolbar.setTitle(sessionManager.GetAdviceToolbarTitle());
//                Home.drawer.closeDrawer(Gravity.LEFT);
//                InterfaceAdviceChildClick interfaceAdviceChildClick = new Fragment_advice();
//                interfaceAdviceChildClick.GetAdviceChildName(expandedListText);
//                Toast.makeText(mContext, "click on " + expandedListText, Toast.LENGTH_SHORT).show();
            }
        });


        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.exp_chil_custtext:
                //  Toast.makeText(mContext, "click on ", Toast.LENGTH_SHORT).show();
                break;

            //  Toast.makeText(mContext, "Click on list", Toast.LENGTH_SHORT).show();
        }

    }


}