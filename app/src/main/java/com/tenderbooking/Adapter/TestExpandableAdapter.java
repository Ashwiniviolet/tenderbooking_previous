package com.tenderbooking.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tenderbooking.R;
import com.vkondrav.swiftadapter.SwiftAdapter;

/**
 * Created by namok on 13-11-2017.
 */

public class TestExpandableAdapter extends SwiftAdapter<TestExpandableAdapter.MyViewHolder> {

    Activity mActivity;

    public TestExpandableAdapter(Activity mActivity) {
        this.mActivity = mActivity;
    }

    // level 0
    @Override
    public int getNumberOfLvl0Items() {
        return 2;
    }

    @Override
    public MyViewHolder onCreateLvl0ItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.custlay_adv_child, null, false);
        return new TestExpandableAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindLvl0Item(MyViewHolder holder, ItemIndex index) {
        // holder.exp_chil_custtext.setText("level 1");
    }


    //level 1
    @Override
    public int getNumberOfLvl1Sections() {
        return 2;
    }

    @Override
    public MyViewHolder onCreateLvl1ItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.custlay_adv_child, null, false);
        return new TestExpandableAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindLvl1Item(MyViewHolder holder, ItemIndex index) {
        //holder.exp_chil_custtext.setText("level 2");
    }

    //level 2
    @Override
    public int getNumberOfLvl2ItemsForSection(int lvl1Section, int lvl2Section) {
        return 3;
    }

    @Override
    public MyViewHolder onCreateLvl2ItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.custlay_adv_child, null, false);
        return new TestExpandableAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindLvl2Item(MyViewHolder holder, ItemIndex index) {
        // holder.exp_chil_custtext.setText("level 2");
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView exp_chil_custtext;

        public MyViewHolder(View itemView) {
            super(itemView);
            exp_chil_custtext = (TextView) itemView.findViewById(R.id.exp_chil_custtext);
        }
    }
}
