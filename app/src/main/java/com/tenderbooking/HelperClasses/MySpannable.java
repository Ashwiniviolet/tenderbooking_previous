package com.tenderbooking.HelperClasses;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by laxmikant bolya on 25-03-2017.
 */

public class MySpannable extends ClickableSpan {

    private boolean isUnderLine = true;

    public MySpannable(boolean isUnderLine) {
        this.isUnderLine = isUnderLine;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        //super.updateDrawState(ds);
        ds.setUnderlineText(isUnderLine);
    }

    @Override
    public void onClick(View widget) {

    }
}
