package com.tenderbooking.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.tenderbooking.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * Created by Abhishek jain on 01-03-2017.
 */

public class UtillClasses {

    public static final String ApiKey = "x-api-key";
    public static final String ApiKeyValue = "a7d335c1cb24c90eb578e8eff51948eb";
    public static final String x_api_ver = "x-api-ver";
    public static final String x_api_ver_Value = "1.0";
    public static final String x_email = "x-email";
    public static final String x_password = "x-password";
    public static final String x_mobile_no = "x-mobile-number";
    public static final String x_uuid = "x-uuid";
    public static final String x_model = "x-model";
    public static final String x_platform = "x-platform";
    public static final String x_platform_ver = "x-platform-ver";
    public static final String x_device_name = "x-device-name";
    public static final String x_app_ver = "x-app-ver";
    public static final String x_api_signature = "x-api-signature";
    public static final String x_user_language = "x-user-language";
    public static final String Content_Type = "Content-Type";
    public static final String multipart = "multipart/form-data";
    public static final String ListOrder = "listOrder";
    public static final String Order_aces = "ASC";
    public static final String Order_desc = "DESC";
    //device info
    public static final String device_model = "device_model";
    public static final String device_platmVer = "device_PlatVer";
    public static final String device_devName = "device_Name";
    //Download tender Documents
    public static final String lastCheck = "lastCheck";
    public static final String idtender = "idTender";
    public static final String summary = "summary";
    public static final String json = "json";
    public static final String DocId = "docID";
    public static final String DocType = "docType";
    public static final String DocName = "docName";
    //helpDetails
    public static final String IdHelp = "idHelp";
    //getAboutPage
    public static final String Typepage = "typePage";
    //send Advice Question
    public static final String TypeAdvice = "typeAdvice";
    public static final String CountryAdvice = "countryAdvice";
    public static final String TitleQuestion = "titleQuestion";
    public static final String DetailsQuestion = "detailsQuestion";
    public static final String statusRating = "statusRating";
    public static final String messageRating = "messageRating";
    public static final String AlarmType = "alarm_type";
    //demo_account
    public static final String Demo_Country = "country";
    public static final String Demo_firstname = "firstName";
    public static final String Demo_lastName = "lastName";
    public static final String Demo_gender = "gender";
    public static final String Demo_email = "email";
    public static final String Demo_mobile = "mobile";
    public static final String Demo_language = "demoLanguage";
    //getadvice Details
    public static final String IdAdvice = "idAdvice";
    public static final String IdList = "idList";
    public static final String Action_tender_read = "ten_read";
    public static final String Action_tender_Unread = "ten_unread";
    public static final String Action_tender_delete = "ten_delete";
    public static final String Action_tender_undelete = "ten_undelete";
    public static final String Action_tender_flag_set = "ten_flag_set";
    public static final String Action_tender_flag_unset = "ten_flag_unset";
    public static final String Action_tender_tag_set = "ten_tag_set";
    public static final String Action_tender_tag_unset = "ten_tag_unset";
    public static final String Action_tender_spam_set = "ten_spam_set";
    public static final String Action_tender_spam_unset = "ten_spam_unset";
    public static final String Action_tag_add = "tag_add";
    public static final String Action_tag_update = "tag_upd";
    public static final String Action_tag_del = "tag_del";
    public static final String Action_filter_add = "filter_add";
    public static final String Action_filter_update = "filter_upd";
    public static final String Action_filter_del = "filter_del";
    public static final String Action_ActionType = "actionType";
    public static final String Action_ActionDetails = "actionDetails";
    public static final String Keywords = "keywords";
    public static String BaseUrl = "https://www.tenderilive.com/xmlrpcs/mobile/";

    /* public static boolean isConnectingToInternet(Context context) {
         ConnectivityManager connectivity = (ConnectivityManager) context
                 .getSystemService(Context.CONNECTIVITY_SERVICE);
         if (connectivity != null) {
             NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null)
                 for (int i = 0; i < info.length; i++) {
                     if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                         return true;
                     }
                 }
         }else
         return false;
     }*/
    public static Boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static AlertDialog.Builder GetInternetAlertDialog(Activity mActivity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
        alert.setCancelable(false);
        alert.setTitle(mActivity.getResources().getString(R.string.noInternetCon));
        alert.setMessage(mActivity.getResources().getString(R.string.conntoInternet));
        /*alert.setPositiveButton(mActivity.getResources().getString(R.string.conntoInt), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setNegativeButton(mActivity.getResources().getString(R.string.goOffline), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });*/
        return alert;
    }

    public static void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass = Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
    }

    public static void langChange(String lang, Context mContext) {
        Configuration newConfig = mContext.getResources().getConfiguration();

        if (!"".equals(lang) && !newConfig.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);

            newConfig.locale = locale;
            mContext.getResources().updateConfiguration(newConfig, mContext.getResources().getDisplayMetrics());

        }
    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            e.printStackTrace();
        }
        int data = 0;
        return false;
    }

}
