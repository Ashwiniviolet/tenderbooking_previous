package com.tenderbooking.HelperClasses;

import android.os.Build;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Abhishek jain on 01-03-2017.
 */

public class DeviceName {
    public static Map<String, String> getDeviceName() {

        Map<String, String> deviceInfoMap = new HashMap<String, String>();


        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String modelDat = Build.DEVICE;
        String modelData1 = Build.BOARD;
        String modelData2 = Build.ID;
        String modelData3 = Build.PRODUCT;

        String myversion = Build.VERSION.RELEASE;


       /* String[] deviceDetail = model.split("-");

        try {
            deviceInfoMap.put(UtillClasses.device_devName,deviceDetail[0]);
            deviceInfoMap.put(UtillClasses.device_platmVer,deviceDetail[1]);
            deviceInfoMap.put(UtillClasses.device_model,modelData3);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        deviceInfoMap.put(UtillClasses.device_devName, model);
        deviceInfoMap.put(UtillClasses.device_platmVer, myversion);
        deviceInfoMap.put(UtillClasses.device_model, modelData3);

        return deviceInfoMap;
        //return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

}
