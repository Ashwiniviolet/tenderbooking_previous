package com.tenderbooking.HelperClasses;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tenderbooking.R;

import java.util.Date;

public class RatingDialog {


    private static final String PREFS_NAME = "erd_rating";
    private static final String KEY_WAS_RATED = "KEY_WAS_RATED";
    private static final String KEY_NEVER_REMINDER = "KEY_NEVER_REMINDER";
    private static final String KEY_FIRST_HIT_DATE = "KEY_FIRST_HIT_DATE";
    private static final String KEY_LAUNCH_TIMES = "KEY_LAUNCH_TIMES";
    private final Context mContext;
    private final SharedPreferences mPreferences;
    AlertDialog dialog;
    private RatingDialog.ConditionTrigger mCondition;
    private Dialog mDialog;

    public RatingDialog(Context context) {
        mContext = context;
        mPreferences = context.getSharedPreferences(PREFS_NAME, 0);
    }

    public void onStart() {
        if (didRate() || didNeverReminder()) return;

        int launchTimes = mPreferences.getInt(KEY_LAUNCH_TIMES, 0);
        long firstDate = mPreferences.getLong(KEY_FIRST_HIT_DATE, -1L);

        if (firstDate == -1L) {
            registerDate();
        }

        registerHitCount(++launchTimes);
    }

    public void showAnyway() {
        tryShow(mContext);
    }

    public void showIfNeeded() {
        if (mCondition != null) {
            if (mCondition.shouldShow())
                tryShow(mContext);
        } else {
            if (shouldShow())
                tryShow(mContext);
        }
    }

    public void neverReminder() {
        mPreferences.edit().putBoolean(KEY_NEVER_REMINDER, true).apply();
    }

    public void rateNow() {
        String appPackage = mContext.getPackageName();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackage));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
        mPreferences.edit().putBoolean(KEY_WAS_RATED, true).apply();
    }

    public void remindMeLater() {
        registerHitCount(0);
        registerDate();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public boolean didRate() {
        return mPreferences.getBoolean(KEY_WAS_RATED, false);
    }

    public boolean didNeverReminder() {
        return mPreferences.getBoolean(KEY_NEVER_REMINDER, false);
    }

    public void setConditionTrigger(RatingDialog.ConditionTrigger condition) {
        mCondition = condition;
    }

    public void setCancelable(boolean cancelable) {
        mDialog.setCancelable(cancelable);
    }

    private void tryShow(Context context) {
        if (isShowing())
            return;

        try {
            mDialog = null;
            mDialog = createDialog(context);
            mDialog.show();
        } catch (Exception e) {
            //It prevents many Android exceptions
            //when user interactions conflicts with UI thread or Activity expired window token
            //BadTokenException, IllegalStateException ...
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    private boolean shouldShow() {
        if (mPreferences.getBoolean(KEY_NEVER_REMINDER, false))
            return false;
        if (mPreferences.getBoolean(KEY_WAS_RATED, false))
            return false;

        int launchTimes = mPreferences.getInt(KEY_LAUNCH_TIMES, 0);
        long firstDate = mPreferences.getLong(KEY_FIRST_HIT_DATE, 0L);
        long today = new Date().getTime();
        int maxLaunchTimes = mContext.getResources().getInteger(R.integer.erd_launch_times);
        int maxDaysAfter = mContext.getResources().getInteger(R.integer.erd_max_days_after);

        return daysBetween(firstDate, today) > maxDaysAfter || launchTimes > maxLaunchTimes;
    }

    private void registerHitCount(int hitCount) {
        mPreferences
                .edit()
                .putInt(KEY_LAUNCH_TIMES, Math.min(hitCount, Integer.MAX_VALUE))
                .apply();
    }

    private void registerDate() {
        Date today = new Date();
        mPreferences
                .edit()
                .putLong(KEY_FIRST_HIT_DATE, today.getTime())
                .apply();
    }

    private long daysBetween(long firstDate, long lastDate) {
        return (lastDate - firstDate) / (1000 * 60 * 60 * 24);
    }

    private android.app.Dialog createDialog(Context context) {


        TextView rateNow, remindMeLater, never;

        @SuppressLint("InflateParams")
        View view1 = LayoutInflater.from(context).inflate(R.layout.rating_reminder, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view1);
        rateNow = (TextView) view1.findViewById(R.id.rate_now);
        remindMeLater = (TextView) view1.findViewById(R.id.remind_me_later);
        never = (TextView) view1.findViewById(R.id.never);

        rateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateNow();
                dialog.dismiss();

            }
        });

        remindMeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remindMeLater();
                dialog.dismiss();
            }
        });


        never.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                neverReminder();
                dialog.dismiss();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                remindMeLater();
            }
        });
        dialog = builder.create();

        return dialog;

    }

    public interface ConditionTrigger {
        boolean shouldShow();
    }


}
