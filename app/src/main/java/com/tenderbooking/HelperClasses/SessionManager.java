package com.tenderbooking.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.tenderbooking.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Abhishek jain on 04-03-2017.
 */


public class SessionManager {

    public static final String TAG = SessionManager.class.getSimpleName();
    public static final String TenderId_Details = "tender_id_Details";
    public static final String PREF_NAME = "session_info";
    public static final String PREF_NAME_DevicInfo = "session_info";
    public static final String SIGNATURE = "signature";
    public static final String STATUSUSER = "status_user";
    public static final String IsFirsttime = "isfirsttime";
    public static final String EMAIL = "email";
    public static final String UUID = "uuid";
    public static final String MODEL = "device_model";
    public static final String PLATFORM = "platform";
    public static final String PLATFORM_VER = "platform_ver";
    public static final String DeviceNmae = "devicename";
    public static final String App_Ver = "app_version";
    public static final String User_lang = "user_language";
    public static final String LAST_CHECK = "lastCheck";
    public static final String Not_Avalible = "not_avalible";
    public static final String Last_check_service = "last_check_service";
    public static final String Tender_noti_time = "tender_noti_time";
    public static final String Last_check_advice = "last_check_advices";
    public static final String Last_check_Advice_answare = "last_check_advice_ans";
    public static final String LastEmailLogin = "lastEmailLogin"; // for check user is new or old at tiem of login
    public static final String helpLastCheck = "helplastcheck";
    public static final String adviceLastCheck = "advicelastcheck";
    public static final String adviceLastCheckGMT = "advicelastcheck";
    public static final String TenderFirstTime = "tender_first_time";
    public static final String AdviceFirstTime = "advice_first_time";
    public static final String TagTemp = "temp_tag";
    //temp Stroe list of Tenders
    public static final String Tenderlist = "tenderlist";
    public static final String IsHavingList = "not_avalible";
    //textsize
    public static final String TextSizeTender = "tenderListSize";
    public static final String TextSizeAdvice = "adviceListSize";
    public static final String TextSizeHelp = "helpListSize";
    public static final String TextSizeContent = "contentListSize";
    public static final String TextSizeTenderName = "tenderListSizeName";
    public static final String TextSizeAdviceName = "adviceListSizeName";
    public static final String TextSizeHelpName = "helpListSizeName";
    public static final String TextSizeContentName = "contentListSizeName";
    public static final String IsOnlyWifi = "isOnlyWifi";
    public static final String IsNOtiSounEnable = "isNotiSoundEnable";
    //notificaion Setting
    public static final String IsAlwaysNoti = "isAlwaysNOti";
    public static final String StartTime = "doNotDisStartTime";
    public static final String StopTime = "doNotDisStopTime";
    public static final String StartTime24 = "doNotDisStartTime24";
    public static final String StopTime24 = "doNotDisStopTime24";
    //notification enable
    public static final String IsNotiFirst = "noti_firsttime";
    public static final String IsNotiEnable = "IsNotiEnable";
    public static final String NotiTimeInterval = "notiTimeInterval";
    public static final String NotiTimeIntervalType = "notiTimeIntervalType";
    public static final String NotiTenderCount = "notiTenderCount";
    //Checha time interval =
    public static final String IsChecheintEnab = "isChacheEnable";
    public static final String ChecheInterval = "chacheClerarInterval";
    public static final String ChecheIntervalType = "chacheClerarIntervalType";
    //advie detials toolbar title
    public static final String Advie_ToolTitle = "advice_tooltile";
    //aplication language
    public static final String Application_Language = "application_language";
    public static final String Application_Language_name = "application_lang_name";
    public static final String Application_Language_change = "application_lang_change";
    // seleted query
    public static final String SelectedQwery = "selected_qwery";
    SharedPreferences preferences;
    SharedPreferences preferencesDeviceInfo;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editorDevicInfo;
    Context mContext;
    int PRIVATE_MODE = 0;

    public SessionManager(Context mContext) {
        this.mContext = mContext;
        preferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        preferencesDeviceInfo = mContext.getSharedPreferences(PREF_NAME_DevicInfo, PRIVATE_MODE);
        editorDevicInfo = preferencesDeviceInfo.edit();
        editor = preferences.edit();
    }

    public void GetLogout() {
        editor.clear();
        editor.commit();
    }

    public void clearUsingKey() {

        editor.remove(SIGNATURE);
        editor.remove(STATUSUSER);
        editor.remove(IsFirsttime);
        editor.remove(EMAIL);
        editor.commit();
    }

    public void SetSessionInfo(String signature, int statusUser, String email) {
        editor.putString(SIGNATURE, signature);
        editor.putInt(STATUSUSER, statusUser);
        editor.putBoolean(IsFirsttime, true);
        editor.putString(EMAIL, email);
        editor.putString(LastEmailLogin, email);
        editor.commit();
    }

    public String GetUserLastLogedEmail() {
        return preferences.getString(LastEmailLogin, "Not Avalible");
    }


    public String GetUserEmail() {
        return preferences.getString(EMAIL, "Not Avalible");
    }

    public void SetSignature(String signature) {
        editor.putString(SIGNATURE, signature);
        editor.commit();
    }

    ;

    public String getLastCheck() {
        return preferences.getString(LAST_CHECK, "0000-00-00 00:00:00");
    }

    public void setLastCheck(String lastCheck) {
        editor.putString(LAST_CHECK, lastCheck);
        editor.commit();
    }

    public String getSignature() {
        return preferences.getString(SIGNATURE, "Data NOt Avalible");
    }

    public int getUserStatucCode() {
        return preferences.getInt(STATUSUSER, 2);
    }

    public boolean IsUserLogin() {
        return preferences.getBoolean(IsFirsttime, false);
    }


    public void SetDeviceINfo(Map<String, String> mapDeviceINfo) {
        editorDevicInfo.putString(UUID, mapDeviceINfo.get(UUID));
        editorDevicInfo.putString(MODEL, mapDeviceINfo.get(MODEL));
        editorDevicInfo.putString(PLATFORM, mapDeviceINfo.get(PLATFORM));
        editorDevicInfo.putString(PLATFORM_VER, mapDeviceINfo.get(PLATFORM_VER));
        editorDevicInfo.putString(DeviceNmae, mapDeviceINfo.get(DeviceNmae));
        editorDevicInfo.putString(App_Ver, mapDeviceINfo.get(App_Ver));
        editorDevicInfo.putString(User_lang, mapDeviceINfo.get(User_lang));

        editorDevicInfo.commit();
    }

    public void setUserLang(String userLangData) {
        editorDevicInfo.putString(User_lang, userLangData);
        editorDevicInfo.commit();

    }

    public Map<String, String> GetDeviceInfo() {
        Map<String, String> deviceInfo = new HashMap<>();
        deviceInfo.put(UUID, preferencesDeviceInfo.getString(UUID, Not_Avalible));
        deviceInfo.put(MODEL, preferencesDeviceInfo.getString(MODEL, Not_Avalible));
        deviceInfo.put(PLATFORM, preferencesDeviceInfo.getString(PLATFORM, Not_Avalible));
        deviceInfo.put(PLATFORM_VER, preferencesDeviceInfo.getString(PLATFORM_VER, Not_Avalible));
        deviceInfo.put(DeviceNmae, preferencesDeviceInfo.getString(DeviceNmae, Not_Avalible));
        deviceInfo.put(App_Ver, preferencesDeviceInfo.getString(App_Ver, Not_Avalible));
        deviceInfo.put(User_lang, preferencesDeviceInfo.getString(User_lang, Not_Avalible));
        deviceInfo.put(EMAIL, preferences.getString(EMAIL, Not_Avalible));
        deviceInfo.put(SIGNATURE, preferences.getString(SIGNATURE, Not_Avalible));
        deviceInfo.put(User_lang, preferencesDeviceInfo.getString(User_lang, Not_Avalible));
        // deviceInfo.put(PLATFORM_VER,preferencesDeviceInfo.getString(PLATFORM_VER,Not_Avalible));
        return deviceInfo;
    }

    //Adding temp list of tenders
    public void SetTenderList(String responce) {
        editor.putString(Tenderlist, responce);
        editor.putBoolean(IsHavingList, true);
        editor.commit();
    }

    public String GetTenderList() {
        return preferences.getString(Tenderlist, "{statusCode:400}");
    }

    public boolean GetIsListAval() {
        return preferences.getBoolean(IsHavingList, false);
    }

    public void setAdviceLastCheck(String adviceString, String gmtTime) {
        editor.putString(adviceLastCheck, adviceString);
        editor.putString(adviceLastCheckGMT, gmtTime);
        editor.commit();
    }

    public String getHelpLastCheck() {
        return preferences.getString(helpLastCheck, "0000-00-00 00:00:00");
    }

    public void setHelpLastCheck(String helpString) {
        editor.putString(helpLastCheck, helpString);
        editor.commit();
    }

    public String getAdviceLastCheck() {
        return preferences.getString(adviceLastCheck, "0000-00-00 00:00:00");
    }

    public String getAdviceLastCheckGMT() {
        return preferences.getString(adviceLastCheckGMT, "0000-00-00 00:00:00");
    }

    public boolean getTenderFirstTime() {
        return preferences.getBoolean(TenderFirstTime, true);
    }

    public void setTenderFirstTime(boolean value) {
        editor.putBoolean(TenderFirstTime, value);
        editor.commit();
    }

    public boolean getAdviceFirstTime() {
        return preferences.getBoolean(AdviceFirstTime, true);
    }

    public void setAdviceFirstTime(boolean value) {
        editor.putBoolean(AdviceFirstTime, value);
        editor.commit();
    }

    //temp save tag in savepreferance

    public void SetTempTag(String tagString) {
        editor.putString(SessionManager.TagTemp, tagString);
        editor.commit();
    }

    public String getTempTag() {
        return preferences.getString(SessionManager.TagTemp, Not_Avalible);
    }

    public void setLastCheckService(String checkTime) {
        editor.putString(Last_check_service, checkTime);
        editor.commit();
    }

    public String GetLastCheckServie() {
        SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

        String GMtDate = simpleDateFormatGMT.format(new Date());

        // return preferences.getString(SessionManager.Last_check_service, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date())+"");
        Log.e(TAG, "last time for tender check gmt is " + GMtDate + " and local time is" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date()));
        // return preferences.getString(SessionManager.Last_check_service,dateFormatGmt.format(new java.util.Date())+"");
        return preferences.getString(SessionManager.Last_check_service, GMtDate + "");

    }

    public int getTenderNotiTime() {
        return preferences.getInt(SessionManager.Tender_noti_time, 15 * 60 * 1000);
    }

    public void setTenderNotiTime(int value) {
        editor.putInt(SessionManager.Tender_noti_time, value);
        editor.commit();
    }

    //text size
    public void SetTenderSize(int size, String value) {
        editor.putInt(TextSizeTender, size);
        editor.putString(TextSizeTenderName, value);
        editor.commit();
    }

    public int GetTenderSize() {
        return preferences.getInt(TextSizeTender, 12);
    }

    public void SetSizeTheme(int position) {
        editor.putInt(TextSizeTender, position);
        editor.commit();
    }

    public int GetSizeTheme() {
        return preferences.getInt(TextSizeTender, 1);
    }

    public String GetTenderSizeString() {
        return preferences.getString(TextSizeTenderName, "Default");
    }

    public void SetAdviceSize(int size, String value) {
        editor.putInt(TextSizeAdvice, size);
        editor.putString(TextSizeAdviceName, value);
        editor.commit();
    }

    public int GetAdviceSize() {
        return preferences.getInt(TextSizeAdvice, 12);
    }

    public String GetAdviceSizeString() {
        return preferences.getString(TextSizeAdviceName, "Default");
    }

    public void SetHelpSize(int size, String value) {
        editor.putInt(TextSizeHelp, size);
        editor.putString(TextSizeHelpName, value);
        editor.commit();
    }

    public int GetHelpSize() {
        return preferences.getInt(TextSizeHelp, 12);
    }

    public String GetHelpSizeString() {
        return preferences.getString(TextSizeHelpName, "Default");
    }

    public void SetContentSize(int size, String value) {
        editor.putInt(TextSizeContent, size);
        editor.putString(TextSizeContentName, value);
        editor.commit();
    }

    public int GetContentSize() {
        return preferences.getInt(TextSizeContent, 12);

    }

    public String GetContentSizeString() {
        return preferences.getString(TextSizeContentName, "Default");
    }

    public void SetLastCheckAdvAns(String dateTime) {
        editor.putString(Last_check_Advice_answare, dateTime);
        editor.commit();
    }

    public String GetlastCheckAdvAns() {
        return preferences.getString(Last_check_Advice_answare, "0000-00-00 00:00:00");
    }

    public void SetIsonlyWifi(boolean enable) {
        editor.putBoolean(IsOnlyWifi, enable);
        editor.commit();
    }

    public boolean GetIsOnlyWifi() {
        return preferences.getBoolean(IsOnlyWifi, false);
    }

    public void SetNotiSound(boolean notisound) {
        editor.putBoolean(IsNOtiSounEnable, notisound);
        editor.commit();
    }

    public boolean GetNotiSound() {
        return preferences.getBoolean(IsNOtiSounEnable, true);
    }

    //notification enable

    public void SetNotiEnableTimeInterval(boolean notiStatus) {
        editor.putBoolean(IsAlwaysNoti, notiStatus);
        editor.commit();
    }

    public boolean GetNotiEnableTimeInterval() {
        return preferences.getBoolean(IsAlwaysNoti, true);
    }

    /*public void SetNotiEnableStartTime(String startTime,String starttimeString){
        editor.putString(StartTime,startTime);
        editor.putString(StartTime24,starttimeString);
        editor.commit();
    }*/

    public void SetNotiEnableStartTime(String starttimeString) {
        //editor.putString(StartTime,startTime);
        editor.putString(StartTime24, starttimeString);
        editor.commit();
    }

    public String GetNotiEnableStartTime() {
        return preferences.getString(StartTime24, "23:00");
    }

    public void SetNotiEnableStopTime(String stopTime) {
        editor.putString(StopTime24, stopTime);
        editor.commit();
    }

    public String GetNotiEnableStopTime() {
        return preferences.getString(StopTime24, "06:00");
    }
   /* public String GetNotiEnableStartTime(){
        return  preferences.getString(StartTime,"11:00 pm");
    }

    public String GetNotiEnableStartTime24(){
        return  preferences.getString(StartTime24,"23:00");
    }
    public void SetNotiEnableStopTime(String stoptime,String starttimeString){
        editor.putString(StopTime,stoptime);
        editor.putString(StopTime24,starttimeString);
        editor.commit();
    }

    public String GetNotiEnableStopTime(){
        return preferences.getString(StopTime,"06:00 am");
    }
    public String GetNotiEnableStopTime24(){
        return preferences.getString(StopTime24,"06:00");
    }

    */

    public void SetNotiEnable(boolean value) {
        editor.putBoolean(IsNotiEnable, value);
        editor.commit();
    }

    public boolean GetNotiEnable() {
        return preferences.getBoolean(IsNotiEnable, true);

    }

    public void SetIsNotiFirst(boolean value) {
        editor.putBoolean(IsNotiFirst, value);
        editor.commit();
    }

    public boolean getIsNotiFirst() {
        return preferences.getBoolean(IsNotiFirst, true);
    }

    public void SetNotiTimeInte(int timeinterval, int type) {
        editor.putInt(NotiTimeInterval, timeinterval);
        editor.putInt(NotiTimeIntervalType, type);
        editor.commit();
    }

    public int GetNotiTimeInterval() {
        return preferences.getInt(NotiTimeInterval, 900000);

    }

    public int GetNotiTimeIntervalType() {
        return preferences.getInt(NotiTimeIntervalType, 1);
    }

    public void SetNotiTenderCount(int tender_count) {
        editor.putInt(NotiTenderCount, tender_count);
        editor.commit();
    }

    public int GetNotiTenderCount() {
        return preferences.getInt(NotiTenderCount, 0);
    }

    public void SetchacheInterval(int time, int type) {
        editor.putInt(SessionManager.ChecheInterval, time);
        editor.putInt(ChecheIntervalType, type);
        editor.putBoolean(IsChecheintEnab, true);
        editor.commit();
    }

    public boolean GetChacheInter() {
        return preferences.getBoolean(IsChecheintEnab, false);
    }

    public int getChacheInterval() {
        return preferences.getInt(ChecheInterval, 1000 * 60 * 60 * 24 * 7);
    }

    public int getChecheIntervalType() {
        return preferences.getInt(ChecheIntervalType, 1);
    }

    public void SetTenderIdDetails(String tender_id) {
        editor.putString(SessionManager.TenderId_Details, tender_id);
        editor.commit();
    }

    public String GetTenderIdDetails() {
        return preferences.getString(SessionManager.TenderId_Details, "");
    }

    public void SetAdviceToolbarTitle(String titlename) {
        editor.putString(Advie_ToolTitle, titlename);
        editor.commit();
    }

    public String GetAdviceToolbarTitle() {
        return preferences.getString(Advie_ToolTitle, mContext.getResources().getString(R.string.myquestion));
    }

    //set application language
    public void setApplanguage(int appLang, String langName) {
        editorDevicInfo.putInt(Application_Language, appLang);
        editorDevicInfo.putString(Application_Language_name, langName);
        editorDevicInfo.commit();
    }

    public int getAppLanguage() {
        return preferencesDeviceInfo.getInt(Application_Language, 2);
    }

    public String getAppLangName() {
        //  return preferencesDeviceInfo.getString(Application_Language_name,"en");
        return preferencesDeviceInfo.getString(Application_Language_name, "sr");
    }

    public String getSelectedQwery() {
        return preferences.getString(SelectedQwery, "");
    }

    public void setSelectedQwery(String qwery) {
        editor.putString(SelectedQwery, qwery);
        editor.commit();
    }

    public boolean getAppLangChange() {
        return preferences.getBoolean(Application_Language_change, false);
    }

    public void setAppLangChange(boolean status) {
        editor.putBoolean(Application_Language_change, status);
        editor.commit();
    }

    public boolean getIsAppKill() {
        return preferences.getBoolean(GlobalDataAcess.IsAppRemoved, false);
    }

    public void setIsAppKill(boolean check) {
        editor.putBoolean(GlobalDataAcess.IsAppRemoved, check);
        editor.commit();
    }
}