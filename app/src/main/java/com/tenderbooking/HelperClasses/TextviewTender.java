package com.tenderbooking.HelperClasses;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by laxmikant bolya on 05-04-2017.
 */

public class TextviewTender extends android.support.v7.widget.AppCompatTextView {

    public TextviewTender(Context context) {
        super(context);
    }

    public TextviewTender(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TextviewTender(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
