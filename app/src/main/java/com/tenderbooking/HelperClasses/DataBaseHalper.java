package com.tenderbooking.HelperClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.tenderbooking.FragmentTender;
import com.tenderbooking.Interface.InterPagTenderlist;
import com.tenderbooking.MainTab.Fragment_advice;
import com.tenderbooking.Model.ModelAdviceAnsware;
import com.tenderbooking.Model.ModelAdviceDetials;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.Model.ModelHelpDetail;
import com.tenderbooking.Model.ModelHelpList;
import com.tenderbooking.Model.ModelTenDetailAttach;
import com.tenderbooking.Model.ModelTenDetailLinks;
import com.tenderbooking.Model.ModelTenDetalDetails;
import com.tenderbooking.Model.ModelTenderDetails;
import com.tenderbooking.Model.ModelTenderList;
import com.tenderbooking.TenderDetails;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by laxmikant bolya on 16-03-2017.
 */

public class DataBaseHalper extends SQLiteOpenHelper {

    public static final String TAG = DataBaseHalper.class.getSimpleName();

    public static final String DATABASE_NAME = "tender_list_database";

    //name of tables
    public static final String TABLE_NAME = "tenderList_table";
    public static final String TABLE_NAME_HELPTITLE = "help_title_table";
    public static final String TABLE_NAME_HELPDETAIL = "help_detail_table";
    public static final String TABLE_NAME_TENDER_DETAIL_MAIN = "tandel_detail_main";
    public static final String TABLE_NAME_TENDER_DETAIL__DETAILS = "tender_detial_details";
    public static final String TABLE_NAME_TENDER_DETAIL_ATT_DOC = "tendr_detail_att_doc";
    public static final String TABLE_NAME_TENDER_DETAIL_LINKS = "tendr_detail_links";
    public static final String TABLE_NAME_ADVICE_LIST = "advice_list";
    public static final String TABLE_NAME_ADVICE_DETIALS = "advice_details";
    public static final String TABLE_NAME_ADVICE_QUESANSWARE = "advice_qusAnsware";
    public static final String TABLE_NAME_TAG = "tag_table";
    public static final String TABLE_NAME_FILTER = "filter_table";


    public static final int DATABASE_VERSION = 2;

    public static final String KEY_DateTime = "current_datetime";
    public static final String KEY_NameProcedure = "name_procedure";
    public static final String KEY_TypeTender = "type_tender";
    public static final String KEY_SourceTender = "source_tender";
    public static final String KEY_TitleTender = "title_tender";
    public static final String KEY_AssignedTender = "assigned_tender";
    public static final String KEY_OptionsTender = "options_tender";
    public static final String KEY_StatusTender = "status_tender";
    public static final String KEY_MainLanguage = "main_language";
    public static final String KEY_NameBuyer = "name_buyer";
    public static final String KEY_CityBuyer = "city_buyer";
    public static final String KEY_DocumentCurrency = "documentation_currency";
    public static final String KEY_AmountCurrency = "amount_currency";
    public static final String KEY_DateStart = "date_start";
    public static final String KEY_DateStop = "date_stop";
    public static final String KEY_DateDocumentation = "date_documentation";
    public static final String KEY_ColorName = "color_name";
    public static final String KEY_NameContract = "name_contract";
    public static final String KEY_IdDocument = "id_document";
    public static final String KEY_nameDocument = "name_document";
    public static final String KEY_NameAwardCriteria = "name_award_criteria";
    public static final String KEY_CountryFlagTender = "country_flag_tender";
    public static final String KEY_CategoriesTender = "categories_tender";
    public static final String KEY_UrlTender = "url_tender";
    public static final String KEY_IdNUTS = "idNUTS";
    public static final String KEY_TagTender = "tag_tender";
    public static final String KEY_IdBuyer = "id_buyer";
    public static final String KEY_IdTender = "id_tender";
    public static final String KEY_IdContract = "id_contract";
    public static final String KEY_IdProcedure = "id_procedure";
    public static final String KEY_CityTender = "city_tender";
    public static final String KEY_CountryTender = "country_tender";
    public static final String KEY_DocumentationPrice = "documentation_price";
    public static final String KEY_AmountTender = "amount_tender";
    public static final String KEY_ID = "key_id";
    public static final String KEY_IsReaded = "key_isreaded";
    public static final String KEY_IsIconShow = "is_icon_show";

    public static final String KEY_COUNTRY_NAME = "key_country_name";
    public static final String KEY_City_NAME = "key_city_name";


    // for help section
    public static final String Key_help_Id = "key_id_help";
    public static final String Key_help_IdHelp = "help_idhelp";
    public static final String Key_help_IdTopic = "help_idTopic";
    public static final String Key_help_NameTopic = "help_nametopic";
    public static final String Key_help_QuesHelp = "help_questionHelp";
    public static final String Key_help_UpdateHelp = "help_UpdateHelp";
    public static final String Key_help_IdAnsware = "help_IdAnsware";
    public static final String Key_help_TitleAnsware = "help_titleAnsware";
    public static final String Key_help_TextAnsware = "help_textAnsware";


    //for tender detils
    public static final String Key_tendetails_language_name = "tenDet_language_name";
    public static final String Key_tendetails_language_tender = "tenDet_languageTender";
    public static final String Key_tendetails_TitleTender = "tenDet_titleTender";
    public static final String Key_tendetails_SubtitleTender = "tenDet_subtitleTender";
    public static final String Key_tendetails_DescriptionTender = "tenDet_descriptionTender";
    public static final String Key_tendetails_FlagTender = "tenDet_flagTender";
    public static final String Key_tendetails_IdAttachment = "tenDet_idAttachment";
    public static final String Key_tendetails_DocType = "DocType";

    public static final String Key_tendetails_LanguageAttachment = "tenDet_lanuageAttachment";
    public static final String Key_tendetails_NameAttachment = "tenDet_nameAttachment";
    public static final String Key_tendetails_NameEllipsize = "tenDet_nameEllipsize";
    public static final String Key_tendetails_Flag_attachment = "tenDet_flagAttachment";
    public static final String Key_tendetails_IdtenderLink = "tendDet_idTenderLink";
    public static final String Key_tendetails_TypeTenderLink = "tenDet_typeTenderLink";
    public static final String Key_tendetails_TitleTenderLink = "tenDet_TitleTenderLink";
    public static final String Key_tendetails_DateStartLink = "tenDet_dateStartLink";
    public static final String Key_tendetails_DateStopLink = "tenDet_datestopLink";

    //public static final String Key_tendetails_ = " ";
    //public static final String Key_tendetails_ = " ";
    //public static final String Key_tendetails_ = " ";
    //public static final String Key_tendetails_ = " ";


    //ADVICE LIST
    public static final String kEY_ADVICE_List_haveRateAdvice = "haveRateAdvice";
    public static final String kEY_ADVICE_List_idTheme = "idTheme";
    public static final String kEY_ADVICE_List_countryFlagAdvice = "countryFlagAdvice";
    public static final String kEY_ADVICE_List_countryAdvice = "countryAdvice";
    public static final String kEY_ADVICE_List_documentAdvice = "documentAdvice";
    public static final String kEY_ADVICE_List_idAdvice = "idAdvice";
    public static final String kEY_ADVICE_List_nameTheme = "nameTheme";
    public static final String kEY_ADVICE_List_publicatedAdvice = "publicatedAdvice";
    public static final String kEY_ADVICE_List_sourceAdvice = "sourceAdvice";
    public static final String kEY_ADVICE_List_statusAdvice = "statusAdvice";
    public static final String kEY_ADVICE_List_titleAdvice = "titleAdvice";
    public static final String kEY_ADVICE_List_typeAdvice = "typeAdvice";
    public static final String kEY_ADVICE_List_updatedAdvice = "updatedAdvice";

    //ADVICE DETAILS
    public static final String kEY_ADVICE_DETAILS_idAdvice = "idAdvice";
    public static final String kEY_ADVICE_DETAILS_TypeAdvice = "typeAdvice";
    public static final String kEY_ADVICE_DETALIS_idTender = "idTender";
    public static final String kEY_ADVICE_DETALIS_titleAdvice = "titleAdvice";
    public static final String kEY_ADVICE_DETALIS_questionAdvice = "questionAdvice";
    public static final String kEY_ADVICE_DETALIS_textAdvice = "textAdvice";
    public static final String kEY_ADVICE_DETAILS_updateAdvice = "updateAdvice";
    public static final String kEY_ADVICE_DETALIS_publicatedAdvice = "publicatedAdvice";

    //advice answare
    public static final String Key_Advice_QusAnsware_keyid = "keyid";
    public static final String Key_Advice_QusAnsware_idQuestion = "id_question";
    public static final String Key_Advice_QusAnsware_TitleQues = "title_question";
    public static final String Key_Advice_QusAnsware_DetialQues = "detial_question";

    // tag table
    public static final String Key_tag_tagIndex = "tagIndex";
    public static final String Key_tag_tagtext = "tagText";
    public static final String Key_tag_colorId = "colorId";
    public static final String Key_tag_colorCSS = "colorCSS";
    public static final String Key_tag_colorName = "colorName";
    public static final String Key_tag_Active = "active";

    //filter
    public static final String Key_filter_filterType = "filterType";
    public static final String Key_filter_filterId = "filterId";
    public static final String Key_filter_filterName = "filterName";
    public static final String Key_filter_filterAll = "filterAll";
    public static final String Key_filter_Active = "active";
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_IdTender + " TEXT NOT NULL UNIQUE, "
            + KEY_NameProcedure + " TEXT, " + KEY_TypeTender + " TEXT, " + KEY_SourceTender + " TEXT, " + KEY_TitleTender + " TEXT, " + KEY_AssignedTender + " TEXT, " +
            KEY_OptionsTender + " TEXT, " + KEY_StatusTender + " TEXT, " + KEY_MainLanguage + " TEXT, " + KEY_NameBuyer + " TEXT, " + KEY_CityBuyer + " TEXT, " + KEY_DocumentCurrency + " TEXT, " + KEY_AmountCurrency + " TEXT, " +
            KEY_DateStart + " TEXT, " + KEY_DateStop + " TEXT, " + KEY_DateDocumentation + " TEXT, " + KEY_ColorName + " TEXT, " + KEY_NameContract + " TEXT, " + KEY_IdDocument + " TEXT, " + KEY_nameDocument + " TEXT, " +
            KEY_NameAwardCriteria + " TEXT, " + KEY_CountryFlagTender + " TEXT, " + KEY_CategoriesTender + " TEXT, " + KEY_UrlTender + " TEXT, " + KEY_IdNUTS + " TEXT, " + KEY_TagTender + " TEXT, " + KEY_IdBuyer + " TEXT, " + KEY_IdContract + " TEXT, " + KEY_IdProcedure + " TEXT, " + KEY_CityTender + " TEXT, " + KEY_CountryTender + " TEXT, " +
            KEY_DocumentationPrice + " TEXT, " + KEY_AmountTender + " TEXT, " + KEY_IsReaded + " varchar(1)," + KEY_DateTime + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + KEY_IsIconShow + " varchar(1) )";
    public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    /* public static final String CREATE_TABLE = "CREATE TABLE `tender_list` (`id` INTEGER  PRIMARY KEY AUTOINCREMENT , `name_procedure` VARCHAR(1024) NOT NULL, `type_tender` VARCHAR(1024) NOT NULL, `source_tender` VARCHAR(1024) NOT NULL, `title_tender` VARCHAR(1024) NOT NULL, `assigned_tender` VARCHAR(1024) NOT NULL, `options_tender` VARCHAR(1024) NOT NULL, `status_tender` VARCHAR(1024) NOT NULL, `main_language` VARCHAR(255) NOT NULL, `name_buyer` VARCHAR(1024) NOT NULL, `city_buyer` VARCHAR(1024) NOT NULL, `documentation_currency` VARCHAR(255) NOT NULL, `amount_currency` VARCHAR(255) NOT NULL, `date_start` VARCHAR(255) NOT NULL, `date_stop` VARCHAR(255) NOT NULL, `date_documentation` VARCHAR(255) NOT NULL, `color_name` VARCHAR(255) NOT NULL, `name_contract` " +
           "VARCHAR(1024) NOT NULL, `id_document` VARCHAR(1) NOT NULL, `name_document` VARCHAR(255) NOT NULL, `name_award_criteria` VARCHAR(255) NOT NULL, `country_flag_tender` VARCHAR(255) NOT NULL, `categories_tender` VARCHAR(255) NOT NULL, `url_tender` VARCHAR(255) NOT NULL, `idNUTS` VARCHAR(255) NOT NULL, `tag_tender` VARCHAR(255) NOT NULL, `id_buyer` VARCHAR(255) NOT NULL, `id_tender` VARCHAR(255) NOT NULL, `id_contract` VARCHAR(255) NOT NULL, `id_procedure` VARCHAR(255) NOT NULL, `city_tender` VARCHAR(255) NOT NULL, `country_tender` VARCHAR(255) NOT NULL, `documentation_price` VARCHAR(50) NOT NULL, `amount_tender` VARCHAR(50) NOT NULL, `email_address` VARCHAR(255) NOT NULL, UNIQUE (`id_tender`)) ";
*/
    public static final String CREATE_TABLE_HELPTITLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_HELPTITLE + " ( " + Key_help_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Key_help_NameTopic + " TEXT, " + Key_help_IdTopic + " TEXT," + Key_help_IdHelp + " TEXT UNIQUE, "
            + Key_help_QuesHelp + " TEXT, " + Key_help_UpdateHelp + " TEXT )";
    public static final String DELETE_TABLE_HELPTITLE = "DROP TABLE IF EXISTS " + TABLE_NAME_HELPTITLE;
    ;
    public static final String CREATE_TABLE_HELPDETAILS = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_HELPDETAIL + " ( " + Key_help_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Key_help_IdHelp + " TEXT, " + Key_help_IdAnsware + " TEXT, " +
            Key_help_TitleAnsware + " TEXT, " + Key_help_TextAnsware + " TEXT )";
    public static final String DELETE_TABLE_HELPDETAILS = "DROP TABLE IF EXISTS " + TABLE_NAME_HELPDETAIL;
    // tender details _details
    public static final String CREATE_TABLE_TENDER_Detail_Details = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_TENDER_DETAIL__DETAILS + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_IdTender + " TEXT, " + Key_tendetails_language_tender + " TEXT, " + Key_tendetails_TitleTender + " TEXT," + Key_tendetails_SubtitleTender + " TEXT , "
            + Key_tendetails_DescriptionTender + " TEXT, " + Key_tendetails_FlagTender + " TEXT ," + Key_tendetails_language_name + " TEXT  )";
    public static final String DELETE_TABLE_TENDER_Detail_Details = "DROP TABLE IF EXISTS " + TABLE_NAME_TENDER_DETAIL__DETAILS;
    //tender details_detials main
    public static final String CREATE_TABLE_TENDER_DETIALS_MAIN = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_TENDER_DETAIL_MAIN + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_IdTender + " TEXT NOT NULL UNIQUE, "
            + KEY_NameProcedure + " TEXT, " + KEY_TypeTender + " TEXT, " + KEY_SourceTender + " TEXT, " + KEY_TitleTender + " TEXT, " + KEY_AssignedTender + " TEXT, " +
            KEY_OptionsTender + " TEXT, " + KEY_StatusTender + " TEXT, " + KEY_MainLanguage + " TEXT, " + KEY_NameBuyer + " TEXT, " + KEY_CityBuyer + " TEXT, " + KEY_DocumentCurrency + " TEXT, " + KEY_AmountCurrency + " TEXT, " +
            KEY_DateStart + " TEXT, " + KEY_DateStop + " TEXT, " + KEY_DateDocumentation + " TEXT, " + KEY_ColorName + " TEXT, " + KEY_NameContract + " TEXT, " + KEY_IdDocument + " TEXT, " + KEY_nameDocument + " TEXT, " +
            KEY_NameAwardCriteria + " TEXT, " + KEY_CountryFlagTender + " TEXT, " + KEY_CategoriesTender + " TEXT, " + KEY_UrlTender + " TEXT, " + KEY_IdNUTS + " TEXT, " + KEY_TagTender + " TEXT, " + KEY_IdBuyer + " TEXT, " + KEY_IdContract + " TEXT, " + KEY_IdProcedure + " TEXT, " + KEY_CityTender + " TEXT, " + KEY_CountryTender + " TEXT, " +
            KEY_DocumentationPrice + " TEXT, " + KEY_AmountTender + " TEXT ," + KEY_COUNTRY_NAME + " TEXT," + KEY_City_NAME + " TEXT )";
    public static final String DELETE_TABLE_TENDER_DETIALS_MAIN = "DROP TABLE IF EXISTS " + TABLE_NAME_TENDER_DETAIL_MAIN;
    //TENDER detials attachment
    public static final String CREATE_TABLE_TENDER_Detail_AttaDoc = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_TENDER_DETAIL_ATT_DOC + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_IdTender + " TEXT, " + Key_tendetails_IdAttachment + " TEXT, " + Key_tendetails_LanguageAttachment + " TEXT," + Key_tendetails_NameAttachment + " TEXT , "
            + Key_tendetails_NameEllipsize + " TEXT, " + Key_tendetails_DocType + " TEXT, " + Key_tendetails_Flag_attachment + " TEXT )";
    public static final String DELETE_TABLE_TENDER_Detail_AttaDoc = "DROP TABLE IF EXISTS " + TABLE_NAME_TENDER_DETAIL_ATT_DOC;
    //TENDER detials Links
    public static final String CREATE_TABLE_TENDER_Detail_Links = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_TENDER_DETAIL_LINKS + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_IdTender + " TEXT, " + Key_tendetails_IdtenderLink + " TEXT, " + Key_tendetails_TypeTenderLink + " TEXT," + Key_tendetails_TitleTenderLink + " TEXT , "
            + Key_tendetails_DateStartLink + " TEXT, " + Key_tendetails_DateStopLink + " TEXT )";
    public static final String DELETE_TABLE_TENDER_Detail_Links = "DROP TABLE IF EXISTS " + TABLE_NAME_TENDER_DETAIL_LINKS;
    //tender advice list
    public static final String CREATE_TABLE_ADVICE_LIST = "CREATE TABLE " + TABLE_NAME_ADVICE_LIST + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + kEY_ADVICE_List_haveRateAdvice + " varchar(255), " + kEY_ADVICE_List_idTheme + " varchar(255), "
            + kEY_ADVICE_List_countryFlagAdvice + " varchar(255), " + kEY_ADVICE_List_countryAdvice + " varchar(255), " + kEY_ADVICE_List_documentAdvice + " varchar(255), " +
            kEY_ADVICE_List_idAdvice + " varchar(255), " + kEY_ADVICE_List_nameTheme + " varchar(255), " + kEY_ADVICE_List_publicatedAdvice + " varchar(255), "
            + kEY_ADVICE_List_sourceAdvice + " varchar(255), " + kEY_ADVICE_List_titleAdvice + " varchar(255), " +
            kEY_ADVICE_List_typeAdvice + " varchar(255), " + kEY_ADVICE_List_updatedAdvice + " varchar(255) ," + kEY_ADVICE_List_statusAdvice + " varchar(255) )";
    public static final String DELETE_TABLE_ADVICE_LIST = "DROP TABLE IF EXISTS " + TABLE_NAME_ADVICE_LIST;
    //TENDER ADVICE DETAILS
    public static final String CREATE_TBALE_ADVICE_DETAILS = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_ADVICE_DETIALS + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + kEY_ADVICE_DETAILS_idAdvice + " text, " + kEY_ADVICE_DETAILS_TypeAdvice + " text, "
            + kEY_ADVICE_DETALIS_idTender + " text, " + kEY_ADVICE_DETALIS_titleAdvice + " text, " + kEY_ADVICE_DETALIS_questionAdvice + " text, " + kEY_ADVICE_List_statusAdvice + " text, " + kEY_ADVICE_DETAILS_updateAdvice + " text, " + kEY_ADVICE_DETALIS_publicatedAdvice + " text, "
            + kEY_ADVICE_DETALIS_textAdvice + " text )";
    public static final String DELETE_TBALE_ADVICE_DETAILS = "DROP TABLE IF EXISTS " + TABLE_NAME_ADVICE_DETIALS;
    /*
        CREATE TABLE advice_list (key_id int PRIMARY key,haveRateAdvice varchar(255),idTheme varchar(255),countryFlagAdvice varchar(255), documentAdvice varchar(255), idAdvice varchar(255), nameTheme varchar(255), publicatedAdvice varchar(255), sourceAdvice varchar(255), titleAdvice varchar(255), typeAdvice varchar(255), updatedAdvice varchar(255))*/
    //advice_questio_answare
    public static final String CREATE_TABLE_ADVICE_ANSWARE = "CREATE TABLE " + TABLE_NAME_ADVICE_QUESANSWARE + " ( " + Key_Advice_QusAnsware_keyid + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Key_Advice_QusAnsware_idQuestion + " varchar(20), "
            + Key_Advice_QusAnsware_TitleQues + " varchar(255), " + Key_Advice_QusAnsware_DetialQues + " text )";
    public static final String DELETE_TABLE_ADVICE_ANSWARE = "DROP TABLE IF EXISTS " + TABLE_NAME_ADVICE_QUESANSWARE;
    public static final String CREATE_TABLE_TAG = "CREATE TABLE " + TABLE_NAME_TAG + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Key_tag_tagIndex + " TEXT , " + Key_tag_tagtext + " TEXT , " + Key_tag_colorId + " TEXT , " + Key_tag_colorCSS + " TEXT, " + Key_tag_Active + " TEXT )";
    public static final String DELETE_TABLE_TAG = "DROP TABLE IF EXISTS " + TABLE_NAME_TAG;
    public static final String CREATE_TABLE_FILTER = "CREATE TABLE " + TABLE_NAME_FILTER + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Key_filter_filterType + " TEXT , " + Key_filter_filterId + " TEXT , " + Key_filter_filterName + " TEXT , " + Key_filter_filterAll + " TEXT, " + Key_filter_Active + " TEXT )";
    public static final String DELETE_TABLE_FILTER = "DROP TABLE IF EXISTS " + TABLE_NAME_FILTER;
    SQLiteDatabase dbRead, dbWrite;

    public DataBaseHalper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        dbRead = this.getReadableDatabase();
        dbWrite = this.getWritableDatabase();
    }

    public DataBaseHalper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
        sqLiteDatabase.execSQL(CREATE_TABLE_HELPDETAILS);
        sqLiteDatabase.execSQL(CREATE_TABLE_HELPTITLE);
        sqLiteDatabase.execSQL(CREATE_TABLE_TENDER_DETIALS_MAIN);
        sqLiteDatabase.execSQL(CREATE_TABLE_TENDER_Detail_Details);
        sqLiteDatabase.execSQL(CREATE_TABLE_TENDER_Detail_AttaDoc);
        sqLiteDatabase.execSQL(CREATE_TABLE_TENDER_Detail_Links);
        sqLiteDatabase.execSQL(CREATE_TABLE_ADVICE_LIST);
        sqLiteDatabase.execSQL(CREATE_TBALE_ADVICE_DETAILS);
        sqLiteDatabase.execSQL(CREATE_TABLE_ADVICE_ANSWARE);
        sqLiteDatabase.execSQL(CREATE_TABLE_TAG);
        sqLiteDatabase.execSQL(CREATE_TABLE_FILTER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // checking for the database version is changed.
        if (i != i1) {
            sqLiteDatabase.execSQL(DELETE_TABLE);
            sqLiteDatabase.execSQL(DELETE_TABLE_HELPDETAILS);
            sqLiteDatabase.execSQL(DELETE_TABLE_HELPTITLE);
            sqLiteDatabase.execSQL(DELETE_TABLE_TENDER_DETIALS_MAIN);
            sqLiteDatabase.execSQL(DELETE_TABLE_TENDER_Detail_Details);
            sqLiteDatabase.execSQL(DELETE_TABLE_TENDER_Detail_AttaDoc);
            sqLiteDatabase.execSQL(DELETE_TABLE_TENDER_Detail_Links);
            sqLiteDatabase.execSQL(DELETE_TABLE_ADVICE_LIST);
            sqLiteDatabase.execSQL(DELETE_TBALE_ADVICE_DETAILS);
            sqLiteDatabase.execSQL(DELETE_TABLE_ADVICE_ANSWARE);
            sqLiteDatabase.execSQL(DELETE_TABLE_TAG);
            sqLiteDatabase.execSQL(DELETE_TABLE_FILTER);
            onCreate(sqLiteDatabase);
        }
    }

    public void InsertHelpTitle(ArrayList<ModelHelpList> modelHelpListArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Cursor cursor = null;
        try {


            int arraySize = modelHelpListArrayList.size();

            for (int i = 0; i < arraySize; i++) {

                int idhelp = modelHelpListArrayList.get(i).getIdhelp();


                ContentValues values = new ContentValues();
                values.put(Key_help_NameTopic, modelHelpListArrayList.get(i).getNameTopic());
                values.put(Key_help_IdTopic, modelHelpListArrayList.get(i).getIdTopic());
                values.put(Key_help_IdHelp, modelHelpListArrayList.get(i).getIdhelp());
                values.put(Key_help_QuesHelp, modelHelpListArrayList.get(i).getQuestionHelp());
                values.put(Key_help_UpdateHelp, modelHelpListArrayList.get(i).getUpdateHelp());

                String query = "SELECT * FROM " + TABLE_NAME_HELPTITLE + " WHERE " + Key_help_IdHelp + " = '" + idhelp + "'";

                cursor = dbRead.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    String rawupdate = "UPDATE " + TABLE_NAME_HELPTITLE + " set " + Key_help_NameTopic + " = '" + modelHelpListArrayList.get(i).getNameTopic()
                            + "' , " + Key_help_IdTopic + " ='" + modelHelpListArrayList.get(i).getIdTopic() + "' ," + Key_help_IdHelp
                            + " = '" + modelHelpListArrayList.get(i).getIdhelp() + "' ," + Key_help_QuesHelp + " ='" + modelHelpListArrayList.get(i).getQuestionHelp() + "' ,"
                            + Key_help_UpdateHelp + "='" + modelHelpListArrayList.get(i).getUpdateHelp() + "' where " + Key_help_IdHelp + " ='" + modelHelpListArrayList.get(i).getIdhelp() + "'";

                    db.rawQuery(rawupdate, null);
                    //db.update(TABLE_NAME_HELPTITLE,values,Key_help_IdHelp,new String[]{cursor.getString(cursor.getColumnIndex(Key_help_IdHelp))});
                } else {
                    db.insert(TABLE_NAME_HELPTITLE, null, values);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbRead.close();
            db.close();
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
    }

    public HashMap<String, String> getHelpId(String questionHelp) {
        HashMap<String, String> hashmapData = new HashMap<>();
        String helpId = "-1";
        String helpIdAvail = "-1";
        String help_TopicName = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        try {


            String query = "SELECT * FROM " + TABLE_NAME_HELPTITLE + " WHERE " + Key_help_QuesHelp + " = '" + questionHelp + "'";

            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    help_TopicName = cursor.getString(cursor.getColumnIndex(Key_help_NameTopic));
                    helpId = cursor.getString(cursor.getColumnIndex(Key_help_IdHelp));

                    String serchhelpid = "SELECT * FROM " + TABLE_NAME_HELPDETAIL + " WHERE " + Key_help_IdHelp + " = '" + helpId + "'";
                    Cursor cursorInner = db.rawQuery(serchhelpid, null);

                    if (cursorInner.moveToFirst()) {
                        do {
                            helpIdAvail = "1";
                        } while (cursorInner.moveToNext());
                    }

                } while (cursor.moveToNext());

            }
            hashmapData.put(GlobalDataAcess.Database_helpid_aval, helpIdAvail);
            hashmapData.put(GlobalDataAcess.Datatbase_help_id, helpId);
            hashmapData.put(GlobalDataAcess.Database_help_Title, help_TopicName);
            return hashmapData;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            return hashmapData;
        }
    }


    public void InsertHelpDetail(ArrayList<ModelHelpDetail> modelHelpDetailArrayList, String help_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int arraySize = modelHelpDetailArrayList.size();

        for (int i = 0; i < arraySize; i++) {


            ContentValues values = new ContentValues();
            values.put(Key_help_IdHelp, help_id);
            values.put(Key_help_IdAnsware, modelHelpDetailArrayList.get(i).getIdAnsware());
            values.put(Key_help_TitleAnsware, modelHelpDetailArrayList.get(i).getTitleAnsware());
            values.put(Key_help_TextAnsware, modelHelpDetailArrayList.get(i).getTextAnsware());
            db.insert(TABLE_NAME_HELPDETAIL, null, values);

        }
        db.close();
    }

    public ArrayList<ModelHelpDetail> getHelpDetail(String helpId) {
        ArrayList<ModelHelpDetail> modelHelpDetailArrayList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            String query = "SELECT * FROM " + TABLE_NAME_HELPDETAIL + " WHERE " + Key_help_IdHelp + " = '" + helpId + "'";
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    ModelHelpDetail modelHelpDetail = new ModelHelpDetail();
                    modelHelpDetail.setTextAnsware(cursor.getString(cursor.getColumnIndex(Key_help_TextAnsware)));
                    modelHelpDetail.setTitleAnsware(cursor.getString(cursor.getColumnIndex(Key_help_TitleAnsware)));
                    modelHelpDetail.setIdAnsware(cursor.getInt(cursor.getColumnIndex(Key_help_IdAnsware)));

                    modelHelpDetailArrayList.add(modelHelpDetail);
                } while (cursor.moveToNext());
            }

            return modelHelpDetailArrayList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }

            return modelHelpDetailArrayList;
        }
    }

    public LinkedHashMap<String, List<String>> getHelpTitle() {

        LinkedHashMap<String, List<String>> helpDetailHashmap = new LinkedHashMap<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String rawQuery = "SELECT * FROM " + TABLE_NAME_HELPTITLE + " group by " + Key_help_NameTopic;
        Cursor cursor = db.rawQuery(rawQuery, null);

        if (cursor.moveToFirst()) {
            do {

                String topicNameString = cursor.getString(cursor.getColumnIndex(Key_help_NameTopic));

                String rawQueryarray = "SELECT * FROM " + TABLE_NAME_HELPTITLE + " where " + Key_help_NameTopic + " = '" + topicNameString + "'";

                //creating the list of data
                Cursor cursorInnear = db.rawQuery(rawQueryarray, null);
                List<String> stringarray = new ArrayList<>();
                if (cursorInnear.moveToFirst()) {
                    do {
                        stringarray.add(cursorInnear.getString(cursorInnear.getColumnIndex(Key_help_QuesHelp)));

                    } while (cursorInnear.moveToNext());
                    helpDetailHashmap.put(topicNameString, stringarray);

                }


            } while (cursor.moveToNext());
        }


        return helpDetailHashmap;
    }

    public synchronized void InsertTenderList(ModelTenderList modelTenderList) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbRead = this.getReadableDatabase();

        Cursor cursor = null;
        try {


            ContentValues values = new ContentValues();
            values.put(KEY_NameProcedure, modelTenderList.getNameProcedure());
            values.put(KEY_TypeTender, modelTenderList.getTypeTender());
            values.put(KEY_SourceTender, modelTenderList.getSourcetender());
            values.put(KEY_TitleTender, modelTenderList.getTitleTender());
            values.put(KEY_AssignedTender, modelTenderList.getAssignedTender());
            values.put(KEY_OptionsTender, modelTenderList.getOptionsTender());
            values.put(KEY_StatusTender, modelTenderList.getStatusTender());
            values.put(KEY_MainLanguage, modelTenderList.getMainLanguage());
            values.put(KEY_NameBuyer, modelTenderList.getNameBuyer());
            values.put(KEY_CityBuyer, modelTenderList.getCityBuyer());
            values.put(KEY_DocumentCurrency, modelTenderList.getDocumentationCurrency());
            values.put(KEY_AmountCurrency, modelTenderList.getAmountCurrency());
            values.put(KEY_DateStart, modelTenderList.getDateStart());
            values.put(KEY_DateStop, modelTenderList.getDateStop());
            values.put(KEY_DateDocumentation, modelTenderList.getDateDocumentation());
            values.put(KEY_ColorName, modelTenderList.getColorName());
            values.put(KEY_NameContract, modelTenderList.getNameContract());
            values.put(KEY_IdDocument, modelTenderList.getIdDocument());
            values.put(KEY_nameDocument, modelTenderList.getNameDocument());
            values.put(KEY_NameAwardCriteria, modelTenderList.getNameAwardCriteria());
            values.put(KEY_CountryFlagTender, modelTenderList.getCountryFlagTender());
            values.put(KEY_CategoriesTender, modelTenderList.getCategoriesTender());
            values.put(KEY_UrlTender, modelTenderList.getUrlTender());
            values.put(KEY_IdNUTS, modelTenderList.getIdNUTS());
            values.put(KEY_TagTender, modelTenderList.getTagTender());
            values.put(KEY_IdBuyer, modelTenderList.getIdBuyer());
            values.put(KEY_IdTender, modelTenderList.getIdTender());
            values.put(KEY_IdContract, modelTenderList.getIdContract());
            values.put(KEY_IdProcedure, modelTenderList.getIdProcedure());
            values.put(KEY_CityTender, modelTenderList.getCityTender());
            values.put(KEY_CountryTender, modelTenderList.getCountryTender());
            values.put(KEY_DocumentationPrice, modelTenderList.getDocumentationPrice());
            values.put(KEY_AmountTender, modelTenderList.getAmountTender());
            if (modelTenderList.isReaded()) {
                values.put(KEY_IsReaded, 1);
            } else {
                values.put(KEY_IsReaded, 0);
            }
            values.put(KEY_IsIconShow, 1);

        /*if (modelTenderList.isiconShow()){

        }else{
            values.put(KEY_IsIconShow,0);
        }*/

            //values.put( , );

            //serch that existing datat is already exists

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_IdTender + " = '" + modelTenderList.getIdTender() + "'";

            cursor = dbRead.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                // db.update(TABLE_NAME,values,KEY_IdTender,new String[]{cursor.getString(cursor.getColumnIndex(KEY_IdTender))});
            } else
                db.insert(TABLE_NAME, null, values);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
            if (dbRead != null) {
                dbRead = null;
            }
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    public String getLastTenderAssignedDate() {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try {

            String query = "SELECT " + KEY_AssignedTender + " from " + TABLE_NAME + " order by " + KEY_AssignedTender + " DESC LIMIT 1";
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
//                SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                simpleDateFormatGMT.parse(cursor.getString(cursor.getColumnIndex(KEY_AssignedTender)));
//
//                java.util.Calendar calendar2 =
//                java.util.Calendar calendar = simpleDateFormatGMT.getCalendar();
//                calendar.add(java.util.Calendar.SECOND,1);
//                Date date_return = calendar.getTime();

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date date = dateFormat.parse(cursor.getString(cursor.getColumnIndex(KEY_AssignedTender)));
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(java.util.Calendar.SECOND, 1);

                    return dateFormat.format(calendar.getTime());
                } catch (Exception e) {
                    e.printStackTrace();
                    return cursor.getString(cursor.getColumnIndex(KEY_AssignedTender));
                }

                // return date_return.toString();
            } else {
                return "0000-00-00 00:00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }

        }
        return "0000-00-00 00:00:00";
    }

    public String getLastAdviceAssignedDate() {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try {

            String query = "SELECT " + kEY_ADVICE_List_updatedAdvice + " from " + TABLE_NAME_ADVICE_LIST + " order by " + kEY_ADVICE_List_updatedAdvice + " DESC LIMIT 1";
            cursor = db.rawQuery(query, null);
            if (!cursor.isClosed())
                if (cursor.moveToFirst()) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    try {
                        Date date = dateFormat.parse(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_updatedAdvice)));
                        java.util.Calendar calendar = java.util.Calendar.getInstance();
                        calendar.setTime(date);
                        calendar.add(java.util.Calendar.SECOND, 1);

                        return dateFormat.format(calendar.getTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_updatedAdvice));
                    }
                } else {
                    return "0000-00-00 00:00:00";
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return "0000-00-00 00:00:00";
    }


    public String getLastHelpAssignedDate() {
        Cursor cursor = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try {

            String query = "SELECT " + Key_help_UpdateHelp + " from " + TABLE_NAME_HELPTITLE + " order by " + Key_help_UpdateHelp + " DESC LIMIT 1";
            cursor = db.rawQuery(query, null);
            if (!cursor.isClosed())
                if (cursor.moveToFirst()) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date date = dateFormat.parse(cursor.getString(cursor.getColumnIndex(Key_help_UpdateHelp)));
                        java.util.Calendar calendar = java.util.Calendar.getInstance();
                        calendar.setTime(date);
                        calendar.add(java.util.Calendar.SECOND, 1);

                        return dateFormat.format(calendar.getTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return cursor.getString(cursor.getColumnIndex(Key_help_UpdateHelp));
                    }
                } else {
                    return "0000-00-00 00:00:00";
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return "0000-00-00 00:00:00";
    }


    public boolean GetTenderList() {

        SQLiteDatabase db = this.getReadableDatabase();


        String Query = "SELECT * from " + TABLE_NAME + " order by " + KEY_ID + "+0 DESC ";

        // ArrayList<ModelTenderList> modelTenderListArrayList = new ArrayList<>();
        Cursor cursor = null;
        try {

            cursor = db.rawQuery(Query, null);
            int counttender = 0;
            if (cursor.moveToFirst()) {
                FragmentTender.filteredArraylist.clear();

                int index[] = new int[35];
                index[0] = cursor.getColumnIndex(DataBaseHalper.KEY_NameProcedure);
                index[1] = cursor.getColumnIndex((DataBaseHalper.KEY_TypeTender));
                index[2] = cursor.getColumnIndex(DataBaseHalper.KEY_SourceTender);
                index[3] = cursor.getColumnIndex(DataBaseHalper.KEY_TitleTender);
                index[4] = cursor.getColumnIndex(DataBaseHalper.KEY_AssignedTender);
                index[5] = cursor.getColumnIndex(DataBaseHalper.KEY_OptionsTender);
                index[6] = cursor.getColumnIndex(DataBaseHalper.KEY_StatusTender);
                index[7] = cursor.getColumnIndex(DataBaseHalper.KEY_MainLanguage);
                index[8] = cursor.getColumnIndex(DataBaseHalper.KEY_NameBuyer);
                index[9] = cursor.getColumnIndex(DataBaseHalper.KEY_CityBuyer);
                index[10] = cursor.getColumnIndex(DataBaseHalper.KEY_DocumentCurrency);
                index[11] = cursor.getColumnIndex(DataBaseHalper.KEY_AmountCurrency);
                index[12] = cursor.getColumnIndex(DataBaseHalper.KEY_DateStart);
                index[13] = cursor.getColumnIndex(DataBaseHalper.KEY_DateStop);
                index[14] = cursor.getColumnIndex(DataBaseHalper.KEY_DateDocumentation);
                index[15] = cursor.getColumnIndex(DataBaseHalper.KEY_ColorName);
                index[16] = cursor.getColumnIndex(DataBaseHalper.KEY_NameContract);
                index[17] = cursor.getColumnIndex(DataBaseHalper.KEY_IdDocument);
                index[18] = cursor.getColumnIndex(DataBaseHalper.KEY_nameDocument);
                index[19] = cursor.getColumnIndex(DataBaseHalper.KEY_NameAwardCriteria);
                index[20] = cursor.getColumnIndex(DataBaseHalper.KEY_CountryFlagTender);
                index[21] = cursor.getColumnIndex(DataBaseHalper.KEY_CategoriesTender);
                index[22] = cursor.getColumnIndex(DataBaseHalper.KEY_UrlTender);
                index[23] = cursor.getColumnIndex(DataBaseHalper.KEY_IdNUTS);
                index[24] = cursor.getColumnIndex(DataBaseHalper.KEY_TagTender);
                index[25] = cursor.getColumnIndex(DataBaseHalper.KEY_IdBuyer);
                index[26] = cursor.getColumnIndex(DataBaseHalper.KEY_IdTender);
                index[27] = cursor.getColumnIndex(DataBaseHalper.KEY_IdContract);
                index[28] = cursor.getColumnIndex(DataBaseHalper.KEY_IdProcedure);
                index[29] = cursor.getColumnIndex(DataBaseHalper.KEY_CityTender);
                index[30] = cursor.getColumnIndex(DataBaseHalper.KEY_CountryTender);
                index[31] = cursor.getColumnIndex(DataBaseHalper.KEY_AmountTender);
                index[32] = cursor.getColumnIndex(DataBaseHalper.KEY_DocumentationPrice);
                index[33] = cursor.getColumnIndex(DataBaseHalper.KEY_IsReaded);
                index[34] = cursor.getColumnIndex(DataBaseHalper.KEY_IsIconShow);


                do {
                    ModelTenderList modelTenderList = new ModelTenderList();

                    modelTenderList.setNameProcedure(cursor.getString(index[0]));
                    modelTenderList.setTypeTender(cursor.getString(index[1]));
                    modelTenderList.setSourcetender(cursor.getString(index[2]));
                    modelTenderList.setTitleTender(cursor.getString(index[3]));
                    modelTenderList.setAssignedTender(cursor.getString(index[4]));
                    modelTenderList.setOptionsTender(cursor.getString(index[5]));
                    modelTenderList.setStatusTender(cursor.getString(index[6]));
                    modelTenderList.setMainLanguage(cursor.getString(index[7]));
                    modelTenderList.setNameBuyer(cursor.getString(index[8]));
                    modelTenderList.setCityBuyer(cursor.getString(index[9]));
                    modelTenderList.setDocumentationCurrency(cursor.getString(index[10]));
                    modelTenderList.setAmountCurrency(cursor.getString(index[11]));
                    modelTenderList.setDateStart(cursor.getString(index[12]));
                    modelTenderList.setDateStop(cursor.getString(index[13]));
                    modelTenderList.setDateDocumentation(cursor.getString(index[14]));
                    modelTenderList.setColorName(cursor.getString(index[15]));
                    modelTenderList.setNameContract(cursor.getString(index[16]));
                    modelTenderList.setIdDocument(cursor.getString(index[17]));
                    modelTenderList.setNameDocument(cursor.getString(index[18]));
                    modelTenderList.setNameAwardCriteria(cursor.getString(index[19]));
                    modelTenderList.setCountryFlagTender(cursor.getString(index[20]));
                    modelTenderList.setCategoriesTender(cursor.getString(index[21]));
                    modelTenderList.setUrlTender(cursor.getString(index[22]));
                    modelTenderList.setIdNUTS(cursor.getString(index[23]));
                    modelTenderList.setTagTender(cursor.getString(index[24]));
                    modelTenderList.setIdBuyer(cursor.getString(index[25]));
                    modelTenderList.setIdTender(cursor.getString(index[26]));
                    //  Log.e(TAG," tender id from database "+cursor.getString(cursor.getColumnIndex(DataBaseHalper.KEY_IdTender ))+"at position "+(counttender++));
                    modelTenderList.setIdContract(cursor.getString(index[27]));
                    modelTenderList.setIdProcedure(cursor.getString(index[28]));
                    modelTenderList.setCityTender(cursor.getString(index[29]));
                    modelTenderList.setCountryTender(cursor.getString(index[30]));
                    modelTenderList.setAmountTender(Double.parseDouble(cursor.getString(index[31])));
                    modelTenderList.setDocumentationPrice(Double.parseDouble(cursor.getString(index[32])));

//                    int isreadedInt = cursor.getInt(index[33]); // change according to the status tender values
//                    if (isreadedInt == 1) {
//                        modelTenderList.setReaded(true);
//                    } else {
//                        modelTenderList.setReaded(false);
//                    }

                    if ((Integer.parseInt(modelTenderList.getStatusTender()) & 1) == 1) {
                        modelTenderList.setReaded(true);
                    } else {
                        modelTenderList.setReaded(false);
                    }

                    int isIconshow = cursor.getInt(index[34]);
                    if (isIconshow == 1) {
                        modelTenderList.setIsiconShow(true);
                    } else {
                        modelTenderList.setIsiconShow(false);
                    }

                /*modelTenderList.(cursor.getString(cursor.getColumnIndex(DataBaseHalper. )));
                modelTenderList.(cursor.getString(cursor.getColumnIndex(DataBaseHalper. )));
                */
                    FragmentTender.filteredArraylist.add(modelTenderList);

                    // FragmentTender.adapter.notifyDataSetChanged();
                    // modelTenderListArrayList.add(modelTenderList);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return true;
    }

    public boolean DeleteTenderData(String tenderId, String updating_value) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbRead = this.getReadableDatabase();
        boolean data = false;
        String findTender = "select * from " + TABLE_NAME + " where " + KEY_IdTender + " ='" + tenderId + "'";
        Cursor cursorRead = dbRead.rawQuery(findTender, null);

        if (cursorRead.moveToFirst()) {
            String deleteQwery = "delete  from " + TABLE_NAME + " where " + KEY_IdTender + " ='" + tenderId + "'";
            //Cursor cursor = db.rawQuery(deleteQwery, null);

            //data = (db.delete(TABLE_NAME,KEY_IdTender+" = ?",new String[]{String.valueOf(tenderId)}))>0;

            ContentValues values = new ContentValues();
            values.put(KEY_StatusTender, updating_value);
            values.put(KEY_IsReaded, 1);
            data = (db.update(TABLE_NAME, values, KEY_IdTender + " = ?", new String[]{String.valueOf(tenderId)})) > 0;
            if (data) {
                //  Log.e(TAG,"delete or undelete  btn click successfull with value is :"+updating_value);
                db.close();
                return data;
            } else {
                //  Log.e(TAG,"delete or undelete  btn click  unsuccessfull :"+updating_value);
                db.close();
                return data;
            }
        } else {
            dbRead.close();
            db.close();
            return data;
        }
    }


    //delete tender

    //set readed and unreaded
    public synchronized boolean SetReadedStatus(String idTender, int readStatus, long statusTender) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbread = null;

        String qwery = "update " + TABLE_NAME + " set " + KEY_IsReaded + " ='" + readStatus + "'," + KEY_IsIconShow + " = '0' where " + KEY_IdTender + " ='" + idTender + "' ";
        boolean data = false;
        //Cursor cursor = db.rawQuery(qwery,null);
        if (statusTender == -1) {
            dbread = this.getReadableDatabase();
            String readqwery = "select " + KEY_StatusTender + " from " + TABLE_NAME + " where " + KEY_IdTender + " ='" + idTender + "' ";
            Cursor cursorRead = dbread.rawQuery(readqwery, null);
            if (cursorRead.moveToFirst())
                statusTender = cursorRead.getInt(cursorRead.getColumnIndex(KEY_StatusTender));
            cursorRead.close();
            // dbread.close();
        }


        if (readStatus == 1) {
            statusTender |= 1;
        } else {
            statusTender &= -1;
        }


        ContentValues values = new ContentValues();
        values.put(KEY_IsReaded, readStatus);
        values.put(KEY_IsIconShow, "0");
        values.put(KEY_StatusTender, statusTender);
//        db.update(TABLE_NAME, values, KEY_IdTender + " = ?", new String[]{String.valueOf(idTender)});
        data = (db.update(TABLE_NAME, values, KEY_IdTender + " = ?", new String[]{String.valueOf(idTender)})) > 0;
//        data=  (db.update(TABLE_NAME,values,KEY_IdTender+" = "+String.valueOf(readStatus),null))>0;
        if (data) {
            if (dbread != null && dbread.isOpen()) {
                dbread.close();
                dbread = null;
            }
            db.close();
            return data;
        } else {
            if (dbread != null && dbread.isOpen()) {
                dbread.close();
                dbread = null;
            }
            db.close();
            return data;
        }


    }

    //for set or unset flag fro the recyclerview
    public boolean SetFlagStatus(String tender_id, String flag_status) {
        SQLiteDatabase db = this.getWritableDatabase();

        String qwery = "update " + TABLE_NAME + " set " + KEY_StatusTender + " ='" + flag_status + "' where " + KEY_IdTender + " ='" + tender_id + "' ";
        boolean data = false;
        //Cursor cursor = db.rawQuery(qwery,null);

        ContentValues values = new ContentValues();
        values.put(KEY_StatusTender, flag_status);
        // values.put(KEY_IsIconShow,"0");
        data = (db.update(TABLE_NAME, values, KEY_IdTender + " = ?", new String[]{String.valueOf(tender_id)})) > 0;
        //data=  (db.update(TABLE_NAME,values,KEY_IdTender+" = "+String.valueOf(readStatus),null))>0;
        if (data) {
            db.close();
            return data;
        } else {
            db.close();
            return data;
        }
    }

    public boolean UpdateTagList(String tagValues, String tender_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        boolean data = false;
        values.put(KEY_TagTender, tagValues);

        data = (db.update(TABLE_NAME, values, KEY_IdTender + " = ?", new String[]{String.valueOf(tender_id)})) > 0;
        if (data) {
            db.close();
            return data;
        } else {
            db.close();
            return data;
        }
    }

    //update tag status in list

    public void InsertTenDetailsMain(ModelTenderDetails modelTenderList) {

        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Cursor cursor = null;

        try {


            ContentValues values = new ContentValues();
            values.put(KEY_NameProcedure, modelTenderList.getNameProcedure());
            values.put(KEY_TypeTender, modelTenderList.getTypeTender());
            values.put(KEY_SourceTender, modelTenderList.getSourcetender());
            values.put(KEY_TitleTender, modelTenderList.getTitleTender());
            values.put(KEY_AssignedTender, modelTenderList.getAssignedTender());
            values.put(KEY_OptionsTender, modelTenderList.getOptionsTender());
            values.put(KEY_StatusTender, modelTenderList.getStatusTender());
            values.put(KEY_MainLanguage, modelTenderList.getMainLanguage());
            values.put(KEY_NameBuyer, modelTenderList.getNameBuyer());
            values.put(KEY_CityBuyer, modelTenderList.getCityBuyer());
            values.put(KEY_DocumentCurrency, modelTenderList.getDocumentationCurrency());
            values.put(KEY_AmountCurrency, modelTenderList.getAmountCurrency());
            values.put(KEY_DateStart, modelTenderList.getDateStart());
            values.put(KEY_DateStop, modelTenderList.getDateStop());
            values.put(KEY_DateDocumentation, modelTenderList.getDateDocumentation());
            values.put(KEY_ColorName, modelTenderList.getColorName());
            values.put(KEY_NameContract, modelTenderList.getNameContract());
            values.put(KEY_IdDocument, modelTenderList.getIdDocument());
            values.put(KEY_nameDocument, modelTenderList.getNameDocument());
            values.put(KEY_NameAwardCriteria, modelTenderList.getNameAwardCriteria());
            values.put(KEY_CountryFlagTender, modelTenderList.getCountryFlagTender());
            values.put(KEY_CategoriesTender, modelTenderList.getCategoriesTender());
            values.put(KEY_UrlTender, modelTenderList.getUrlTender());
            values.put(KEY_IdNUTS, modelTenderList.getIdNUTS());
            values.put(KEY_TagTender, modelTenderList.getTagTender());
            values.put(KEY_IdBuyer, modelTenderList.getIdBuyer());
            values.put(KEY_IdTender, modelTenderList.getIdTender());
            values.put(KEY_IdContract, modelTenderList.getIdContract());
            values.put(KEY_IdProcedure, modelTenderList.getIdProcedure());
            values.put(KEY_CityTender, modelTenderList.getCityTender());
            values.put(KEY_CountryTender, modelTenderList.getCountryTender());
            values.put(KEY_DocumentationPrice, modelTenderList.getDocumentationPrice());
            values.put(KEY_AmountTender, modelTenderList.getAmountTender());
            values.put(KEY_COUNTRY_NAME, modelTenderList.getCountry_name());
            values.put(KEY_City_NAME, modelTenderList.getCity_name());
            //values.put( , );

            //serch that existing datat is already exists

            String query = "SELECT * FROM " + TABLE_NAME_TENDER_DETAIL_MAIN + " WHERE " + KEY_IdTender + " = '" + modelTenderList.getIdTender() + "'";

            cursor = dbRead.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                //  db.update(TABLE_NAME,values,KEY_IdTender,new String[]{cursor.getString(cursor.getColumnIndex(KEY_IdTender))});
            } else
                db.insert(TABLE_NAME_TENDER_DETAIL_MAIN, null, values);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
    }

    //inset tender detials main table

    public void InsertTenderDetailsDetisls(ArrayList<ModelTenDetalDetails> modelTenDetalDetailsArrayList, String idTender) {

        SQLiteDatabase db = this.getWritableDatabase();
        //SQLiteDatabase dbRead = this.getReadableDatabase();

        int arraySize = modelTenDetalDetailsArrayList.size();

        for (int i = 0; i < arraySize; i++) {

            //int idhelp  = modelTenDetalDetailsArrayList.get(i);


            ContentValues values = new ContentValues();
            values.put(KEY_IdTender, idTender);
            values.put(Key_tendetails_language_tender, modelTenDetalDetailsArrayList.get(i).getLanguageTender());
            values.put(Key_tendetails_TitleTender, modelTenDetalDetailsArrayList.get(i).getTitleTender());
            values.put(Key_tendetails_SubtitleTender, modelTenDetalDetailsArrayList.get(i).getSubtitleTender());
            values.put(Key_tendetails_DescriptionTender, modelTenDetalDetailsArrayList.get(i).getDescriptionTender());
            values.put(Key_tendetails_FlagTender, modelTenDetalDetailsArrayList.get(i).getFlagTender());
            values.put(Key_tendetails_language_name, modelTenDetalDetailsArrayList.get(i).getLanguageTenderName());

            db.insert(TABLE_NAME_TENDER_DETAIL__DETAILS, null, values);

            /*String query = "SELECT * FROM "+TABLE_NAME_HELPTITLE+" WHERE "+ Key_help_IdHelp+ " = '"+idhelp+"'";

            Cursor cursor = dbRead.rawQuery(query,null);

            if (cursor.moveToFirst()){
                String rawupdate = "UPDATE "+TABLE_NAME_HELPTITLE+" set "+Key_help_NameTopic+" = '"+ modelHelpListArrayList.get(i).getNameTopic()
                        +"' , "+Key_help_IdTopic+" ='"+modelHelpListArrayList.get(i).getIdTopic()+"' ,"+Key_help_IdHelp
                        +" = '"+modelHelpListArrayList.get(i).getIdhelp()+"' ,"+Key_help_QuesHelp+" ='"+modelHelpListArrayList.get(i).getQuestionHelp()+"' ,"
                        +Key_help_UpdateHelp+"='"+modelHelpListArrayList.get(i).getUpdateHelp()+"' where "+Key_help_IdHelp+" ='"+modelHelpListArrayList.get(i).getIdhelp()+"'";

                db.rawQuery(rawupdate,null);
                //db.update(TABLE_NAME_HELPTITLE,values,Key_help_IdHelp,new String[]{cursor.getString(cursor.getColumnIndex(Key_help_IdHelp))});
            }else
            {
                db.insert(TABLE_NAME_HELPTITLE,null,values);
            }
*/

        }
        db.close();

    }

    public void InsertTenderDetailsAttachments(ArrayList<ModelTenDetailAttach> modelTenDetalDetailsArrayList, String idTender) {

        SQLiteDatabase db = this.getWritableDatabase();
        //SQLiteDatabase dbRead = this.getReadableDatabase();

        int arraySize = modelTenDetalDetailsArrayList.size();

        for (int i = 0; i < arraySize; i++) {

            //int idhelp  = modelTenDetalDetailsArrayList.get(i);
//            modelDetails.setDocType("2");
//           // modelDetails.setIdAttachment(jsonDetils.optString("idDocument"));
//           // modelDetails.setLanguageAttachment(jsonDetils.optString("languageDocument"));
//           // modelDetails.setNameAttach(jsonDetils.optString("nameDocument"));
//           // modelDetails.setNameElipsize(jsonDetils.optString("nameEllipsize"));
//           // modelDetails.setFlagAttachment(jsonDetils.optString("flagDocument"));

            ContentValues values = new ContentValues();
            values.put(KEY_IdTender, idTender);
            values.put(Key_tendetails_IdAttachment, modelTenDetalDetailsArrayList.get(i).getIdAttachment());
            values.put(Key_tendetails_LanguageAttachment, modelTenDetalDetailsArrayList.get(i).getLanguageAttachment());
            values.put(Key_tendetails_NameAttachment, modelTenDetalDetailsArrayList.get(i).getNameAttach());
            values.put(Key_tendetails_NameEllipsize, modelTenDetalDetailsArrayList.get(i).getNameElipsize());
            values.put(Key_tendetails_Flag_attachment, modelTenDetalDetailsArrayList.get(i).getFlagAttachment());
            values.put(Key_tendetails_DocType, modelTenDetalDetailsArrayList.get(i).getDocType());

            db.insert(TABLE_NAME_TENDER_DETAIL_ATT_DOC, null, values);

            /*String query = "SELECT * FROM "+TABLE_NAME_HELPTITLE+" WHERE "+ Key_help_IdHelp+ " = '"+idhelp+"'";

            Cursor cursor = dbRead.rawQuery(query,null);

            if (cursor.moveToFirst()){
                String rawupdate = "UPDATE "+TABLE_NAME_HELPTITLE+" set "+Key_help_NameTopic+" = '"+ modelHelpListArrayList.get(i).getNameTopic()
                        +"' , "+Key_help_IdTopic+" ='"+modelHelpListArrayList.get(i).getIdTopic()+"' ,"+Key_help_IdHelp
                        +" = '"+modelHelpListArrayList.get(i).getIdhelp()+"' ,"+Key_help_QuesHelp+" ='"+modelHelpListArrayList.get(i).getQuestionHelp()+"' ,"
                        +Key_help_UpdateHelp+"='"+modelHelpListArrayList.get(i).getUpdateHelp()+"' where "+Key_help_IdHelp+" ='"+modelHelpListArrayList.get(i).getIdhelp()+"'";

                db.rawQuery(rawupdate,null);
                //db.update(TABLE_NAME_HELPTITLE,values,Key_help_IdHelp,new String[]{cursor.getString(cursor.getColumnIndex(Key_help_IdHelp))});
            }else
            {
                db.insert(TABLE_NAME_HELPTITLE,null,values);
            }
*/

        }
        db.close();

    }

    public void InsertTenderDetailsLinks(ArrayList<ModelTenDetailLinks> modelTenDetalDetailsArrayList, String idTender) {

        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbRead = this.getReadableDatabase();

        int arraySize = modelTenDetalDetailsArrayList.size();

        for (int i = 0; i < arraySize; i++) {

            //int idhelp  = modelTenDetalDetailsArrayList.get(i);


            ContentValues values = new ContentValues();
            values.put(KEY_IdTender, idTender);
            values.put(Key_tendetails_IdtenderLink, modelTenDetalDetailsArrayList.get(i).getIdTenderLinks());
            values.put(Key_tendetails_TypeTenderLink, modelTenDetalDetailsArrayList.get(i).getTypeTenderLink());
            values.put(Key_tendetails_TitleTenderLink, modelTenDetalDetailsArrayList.get(i).getTitleTenderLink());
            values.put(Key_tendetails_DateStartLink, modelTenDetalDetailsArrayList.get(i).getDateStartLink());
            values.put(Key_tendetails_DateStopLink, modelTenDetalDetailsArrayList.get(i).getDateStopLink());

            //String search_qwery = "select * from "+TABLE_NAME_TENDER_DETAIL_LINKS+ " where "+;

            db.insert(TABLE_NAME_TENDER_DETAIL_LINKS, null, values);

            /*String query = "SELECT * FROM "+TABLE_NAME_HELPTITLE+" WHERE "+ Key_help_IdHelp+ " = '"+idhelp+"'";

            Cursor cursor = dbRead.rawQuery(query,null);

            if (cursor.moveToFirst()){
                String rawupdate = "UPDATE "+TABLE_NAME_HELPTITLE+" set "+Key_help_NameTopic+" = '"+ modelHelpListArrayList.get(i).getNameTopic()
                        +"' , "+Key_help_IdTopic+" ='"+modelHelpListArrayList.get(i).getIdTopic()+"' ,"+Key_help_IdHelp
                        +" = '"+modelHelpListArrayList.get(i).getIdhelp()+"' ,"+Key_help_QuesHelp+" ='"+modelHelpListArrayList.get(i).getQuestionHelp()+"' ,"
                        +Key_help_UpdateHelp+"='"+modelHelpListArrayList.get(i).getUpdateHelp()+"' where "+Key_help_IdHelp+" ='"+modelHelpListArrayList.get(i).getIdhelp()+"'";

                db.rawQuery(rawupdate,null);
                //db.update(TABLE_NAME_HELPTITLE,values,Key_help_IdHelp,new String[]{cursor.getString(cursor.getColumnIndex(Key_help_IdHelp))});
            }else
            {
                db.insert(TABLE_NAME_HELPTITLE,null,values);
            }
*/

        }
        db.close();

    }

    public boolean GetTenDetilAval(String idTender) {
        SQLiteDatabase db = this.getReadableDatabase();

        String SearchQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_MAIN + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorMainTable = db.rawQuery(SearchQuery, null);

        if (cursorMainTable.moveToFirst()) {
            db.close();
            return true;
        } else {
            db.close();
            return false;
        }

    }

    public int GetTenderListCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        try {
            String SearchQuery = "Select * from " + TABLE_NAME;

            Cursor cursorMainTable = db.rawQuery(SearchQuery, null);

            int count = cursorMainTable.getCount();
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            return 0;
        }
    }

    public boolean GetTenderDetialsMainTemp(String idTender) {
        ModelTenderDetails modelTenderDetails = new ModelTenderDetails();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorMainTable = null;

        String SearchQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_MAIN + " where " + KEY_IdTender + "='" + idTender + "'";
        try {


            cursorMainTable = db.rawQuery(SearchQuery, null);


            if (cursorMainTable.moveToFirst()) {

                TenderDetails.modelTenderDetails.setNameProcedure(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameProcedure)));
                TenderDetails.modelTenderDetails.setTypeTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TypeTender)));
                TenderDetails.modelTenderDetails.setSourcetender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_SourceTender)));
                TenderDetails.modelTenderDetails.setTitleTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TitleTender)));
                TenderDetails.modelTenderDetails.setAssignedTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AssignedTender)));
                TenderDetails.modelTenderDetails.setOptionsTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_OptionsTender)));
                TenderDetails.modelTenderDetails.setStatusTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_StatusTender)));
                TenderDetails.modelTenderDetails.setMainLanguage(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_MainLanguage)));
                TenderDetails.modelTenderDetails.setNameBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameBuyer)));
                TenderDetails.modelTenderDetails.setCityBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CityBuyer)));
                TenderDetails.modelTenderDetails.setDocumentationCurrency(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DocumentCurrency)));
                TenderDetails.modelTenderDetails.setAmountCurrency(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AmountCurrency)));
                TenderDetails.modelTenderDetails.setDateStart(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateStart)));
                TenderDetails.modelTenderDetails.setDateStop(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateStop)));
                TenderDetails.modelTenderDetails.setDateDocumentation(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateDocumentation)));
                TenderDetails.modelTenderDetails.setColorName(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_ColorName)));
                TenderDetails.modelTenderDetails.setNameContract(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameContract)));
                TenderDetails.modelTenderDetails.setIdDocument(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdDocument)));
                TenderDetails.modelTenderDetails.setNameDocument(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_nameDocument)));
                TenderDetails.modelTenderDetails.setNameAwardCriteria(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameAwardCriteria)));
                TenderDetails.modelTenderDetails.setCountryFlagTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CountryFlagTender)));
                TenderDetails.modelTenderDetails.setCategoriesTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CategoriesTender)));
                TenderDetails.modelTenderDetails.setUrlTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_UrlTender)));
                TenderDetails.modelTenderDetails.setIdNUTS(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdNUTS)));
                TenderDetails.modelTenderDetails.setTagTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TagTender)));
                TenderDetails.modelTenderDetails.setIdBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdBuyer)));
                TenderDetails.modelTenderDetails.setIdTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdTender)));
                TenderDetails.modelTenderDetails.setIdContract(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdContract)));
                TenderDetails.modelTenderDetails.setIdProcedure(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdProcedure)));
                TenderDetails.modelTenderDetails.setCityTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CityTender)));
                TenderDetails.modelTenderDetails.setCountryTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CountryTender)));
                //  Log.e(TAG,"data from the database for tender detials country tender "+cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CountryTender )));
                TenderDetails.modelTenderDetails.setAmountTender(Double.parseDouble(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AmountTender))));
                TenderDetails.modelTenderDetails.setDocumentationPrice(Double.parseDouble(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DocumentationPrice))));
                TenderDetails.modelTenderDetails.setCountry_name(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_COUNTRY_NAME)));
                TenderDetails.modelTenderDetails.setCity_name(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_City_NAME)));

                db.close();
                //  Log.e(TAG,"tender detials summy return true");
                // return  true;
            } else {
                //   Log.e(TAG,"tender detials summy return false");
                db.close();
                // return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursorMainTable != null) {
                cursorMainTable.close();
                cursorMainTable = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }

        return true;
    }

    public boolean GetTederDetailsTemp(String idTender) {
        //details
        SQLiteDatabase db = this.getReadableDatabase();
        String DetailsQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL__DETAILS + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorDetails = null;
        try {


            cursorDetails = db.rawQuery(DetailsQuery, null);


            ArrayList<ModelTenDetalDetails> modelTenDetalDetailsArrayList = new ArrayList<>();

            if (cursorDetails.moveToFirst()) {
                do {
                    ModelTenDetalDetails modelTenDetalDetails = new ModelTenDetalDetails();
                    modelTenDetalDetails.setLanguageTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_language_tender)));
                    modelTenDetalDetails.setLanguageTenderName(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_language_name)));
                    modelTenDetalDetails.setTitleTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_TitleTender)));
                    modelTenDetalDetails.setSubtitleTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_SubtitleTender)));
                    modelTenDetalDetails.setDescriptionTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_DescriptionTender)));
                    modelTenDetalDetails.setFlagTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_FlagTender)));
                    modelTenDetalDetailsArrayList.add(modelTenDetalDetails);
                } while (cursorDetails.moveToNext());
                TenderDetails.modelTenderDetails.setDetailsArrayList(modelTenDetalDetailsArrayList);
                //   Log.e(TAG,"size of the details array is ="+modelTenDetalDetailsArrayList.size());
                db.close();
                //  Log.e(TAG,"tender detials Details return true");
                // return true;
            } else {
                db.close();
                //  Log.e(TAG,"tender detials details return false");
                //return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursorDetails != null) {
                cursorDetails.close();
                cursorDetails = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return true;
    }

    public boolean GetTenderAttachTemp(String idTender) {
        SQLiteDatabase db = this.getReadableDatabase();
        //attachments
        String AttachQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_ATT_DOC + " where " + KEY_IdTender + "='" + idTender + "'";
        Cursor cursorAttach = null;

        try {


            cursorAttach = db.rawQuery(AttachQuery, null);

            ArrayList<ModelTenDetailAttach> modelTenDetailAttachArrayList = new ArrayList<>();

            if (cursorAttach.moveToFirst()) {
                do {
                    ModelTenDetailAttach modelTenDetailAttach = new ModelTenDetailAttach();
                    modelTenDetailAttach.setIdAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_IdAttachment)));
                    modelTenDetailAttach.setLanguageAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_LanguageAttachment)));
                    modelTenDetailAttach.setNameAttach(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_NameAttachment)));
                    modelTenDetailAttach.setNameElipsize(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_NameEllipsize)));
                    modelTenDetailAttach.setFlagAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_Flag_attachment)));
                    modelTenDetailAttach.setDocType(cursorAttach.getString(cursorAttach.getColumnIndexOrThrow(Key_tendetails_DocType)));
                    modelTenDetailAttachArrayList.add(modelTenDetailAttach);
                } while (cursorAttach.moveToNext());
                TenderDetails.modelTenderDetails.setAttachmentArraylist(modelTenDetailAttachArrayList);
                //  Log.e(TAG,"size of the details array is ="+modelTenDetailAttachArrayList.size());
                // Log.e(TAG,"tender detials attach return true");
                db.close();
                // return true;
            } else {
                //  Log.e(TAG,"tender detials attach return false");
                db.close();
                //return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursorAttach != null) {
                cursorAttach.close();
                cursorAttach = null;
            }
        }
        return true;
    }

    public boolean GetTenderLinksTemp(String idTender) {

        SQLiteDatabase db = this.getReadableDatabase();
        //Links
        String LinkQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_LINKS + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorLink = null;

        try {


            cursorLink = db.rawQuery(LinkQuery, null);

            ArrayList<ModelTenDetailLinks> modelTenDetailLinksArrayList = new ArrayList<>();

            if (cursorLink.moveToFirst()) {
                do {
                    ModelTenDetailLinks modelTenDetailLinks = new ModelTenDetailLinks();
                    modelTenDetailLinks.setIdTenderLinks(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_IdtenderLink)));
                    modelTenDetailLinks.setTypeTenderLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_TypeTenderLink)));
                    modelTenDetailLinks.setTitleTenderLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_TitleTenderLink)));
                    modelTenDetailLinks.setDateStartLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_DateStartLink)));
                    modelTenDetailLinks.setDateStopLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_DateStopLink)));
                    modelTenDetailLinksArrayList.add(modelTenDetailLinks);
                } while (cursorLink.moveToNext());
                TenderDetails.modelTenderDetails.setLinksArrayList(modelTenDetailLinksArrayList);
                //  Log.e(TAG,"size of the details array is ="+modelTenDetailLinksArrayList.size());
                // Log.e(TAG,"tender detials links return true");
                db.close();
                //  return true;
            } else {
                db.close();
                //  Log.e(TAG,"tender detials links return false");
                // return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursorLink != null) {
                cursorLink.close();
                cursorLink = null;
            }
        }
        return true;
    }

    public boolean GetTenderDetials(String idTender) {

        ModelTenderDetails modelTenderDetails = new ModelTenderDetails();

        SQLiteDatabase db = this.getReadableDatabase();

        String SearchQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_MAIN + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorMainTable = db.rawQuery(SearchQuery, null);


        if (cursorMainTable.moveToFirst()) {

            modelTenderDetails.setNameProcedure(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameProcedure)));
            modelTenderDetails.setTypeTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TypeTender)));
            modelTenderDetails.setSourcetender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_SourceTender)));
            modelTenderDetails.setTitleTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TitleTender)));
            modelTenderDetails.setAssignedTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AssignedTender)));
            modelTenderDetails.setOptionsTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_OptionsTender)));
            modelTenderDetails.setStatusTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_StatusTender)));
            modelTenderDetails.setMainLanguage(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_MainLanguage)));
            modelTenderDetails.setNameBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameBuyer)));
            modelTenderDetails.setCityBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CityBuyer)));
            modelTenderDetails.setDocumentationCurrency(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DocumentCurrency)));
            modelTenderDetails.setAmountCurrency(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AmountCurrency)));
            modelTenderDetails.setDateStart(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateStart)));
            modelTenderDetails.setDateStop(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateStop)));
            modelTenderDetails.setDateDocumentation(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DateDocumentation)));
            modelTenderDetails.setColorName(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_ColorName)));
            modelTenderDetails.setNameContract(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameContract)));
            modelTenderDetails.setIdDocument(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdDocument)));
            modelTenderDetails.setNameDocument(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_nameDocument)));
            modelTenderDetails.setNameAwardCriteria(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_NameAwardCriteria)));
            modelTenderDetails.setCountryFlagTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CountryFlagTender)));
            modelTenderDetails.setCategoriesTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CategoriesTender)));
            modelTenderDetails.setUrlTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_UrlTender)));
            modelTenderDetails.setIdNUTS(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdNUTS)));
            modelTenderDetails.setTagTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_TagTender)));
            modelTenderDetails.setIdBuyer(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdBuyer)));
            modelTenderDetails.setIdTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdTender)));
            modelTenderDetails.setIdContract(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdContract)));
            modelTenderDetails.setIdProcedure(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_IdProcedure)));
            modelTenderDetails.setCityTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CityTender)));
            modelTenderDetails.setCountryTender(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_CountryTender)));
            modelTenderDetails.setAmountTender(Double.parseDouble(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_AmountTender))));
            modelTenderDetails.setDocumentationPrice(Double.parseDouble(cursorMainTable.getString(cursorMainTable.getColumnIndex(DataBaseHalper.KEY_DocumentationPrice))));

        }


        //details

        String DetailsQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL__DETAILS + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorDetails = db.rawQuery(DetailsQuery, null);

        ArrayList<ModelTenDetalDetails> modelTenDetalDetailsArrayList = new ArrayList<>();

        if (cursorDetails.moveToFirst()) {
            do {
                ModelTenDetalDetails modelTenDetalDetails = new ModelTenDetalDetails();
                modelTenDetalDetails.setLanguageTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_language_tender)));
                modelTenDetalDetails.setLanguageTenderName(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_language_name)));
                modelTenDetalDetails.setTitleTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_TitleTender)));
                modelTenDetalDetails.setSubtitleTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_SubtitleTender)));
                modelTenDetalDetails.setDescriptionTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_DescriptionTender)));
                modelTenDetalDetails.setFlagTender(cursorDetails.getString(cursorDetails.getColumnIndex(Key_tendetails_FlagTender)));
                modelTenDetalDetailsArrayList.add(modelTenDetalDetails);
            } while (cursorDetails.moveToNext());
            modelTenderDetails.setDetailsArrayList(modelTenDetalDetailsArrayList);
        }


        //attachments
        String AttachQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_ATT_DOC + " where " + KEY_IdTender + "='" + idTender + "'";

        Cursor cursorAttach = db.rawQuery(AttachQuery, null);

        ArrayList<ModelTenDetailAttach> modelTenDetailAttachArrayList = new ArrayList<>();

        if (cursorAttach.moveToFirst()) {
            do {
                ModelTenDetailAttach modelTenDetailAttach = new ModelTenDetailAttach();
                modelTenDetailAttach.setIdAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_IdAttachment)));
                modelTenDetailAttach.setLanguageAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_LanguageAttachment)));
                modelTenDetailAttach.setNameAttach(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_NameAttachment)));
                modelTenDetailAttach.setNameElipsize(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_NameEllipsize)));
                modelTenDetailAttach.setFlagAttachment(cursorAttach.getString(cursorAttach.getColumnIndex(Key_tendetails_Flag_attachment)));
                modelTenDetailAttachArrayList.add(modelTenDetailAttach);
            } while (cursorAttach.moveToNext());
            modelTenderDetails.setAttachmentArraylist(modelTenDetailAttachArrayList);
        }


        //Links
        String LinkQuery = "Select * from " + TABLE_NAME_TENDER_DETAIL_LINKS + " where " + KEY_IdTender + "='" + idTender + "'";


        Cursor cursorLink = db.rawQuery(LinkQuery, null);

        ArrayList<ModelTenDetailLinks> modelTenDetailLinksArrayList = new ArrayList<>();

        if (cursorLink.moveToFirst()) {
            do {
                ModelTenDetailLinks modelTenDetailLinks = new ModelTenDetailLinks();
                modelTenDetailLinks.setIdTenderLinks(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_IdtenderLink)));
                modelTenDetailLinks.setTypeTenderLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_TypeTenderLink)));
                modelTenDetailLinks.setTypeTenderLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_TitleTenderLink)));
                modelTenDetailLinks.setDateStartLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_DateStartLink)));
                modelTenDetailLinks.setDateStopLink(cursorLink.getString(cursorLink.getColumnIndex(Key_tendetails_DateStopLink)));
                modelTenDetailLinksArrayList.add(modelTenDetailLinks);
            } while (cursorLink.moveToNext());
            modelTenderDetails.setLinksArrayList(modelTenDetailLinksArrayList);
        }

        /*InteTenDetNotifier inteTenDetNotifier = new TenderDetails();
        inteTenDetNotifier.DataFatchNotifier();

       */
        db.close();

        TenderDetails.modelTenderDetails = modelTenderDetails;
        Log.e(TAG, "data is taken from database  size of details " + modelTenderDetails.getDetailsArrayList().size() + " attachment size " + modelTenderDetails
                .getAttachmentArraylist().size() + " related data size = " + modelTenderDetails.getLinksArrayList());
        return true;
    }

    public void InsertAdvicdList(ArrayList<ModelAdviceList> modelAdviceList) {
        InsertAdviceListAsync InsertAdviceListAsync = new InsertAdviceListAsync(modelAdviceList);

        InsertAdviceListAsync.execute();
//        ContentValues values = new ContentValues();
//        values.put(kEY_ADVICE_List_idAdvice,modelAdviceList.getIdAdvice());
//        values.put(kEY_ADVICE_List_typeAdvice,modelAdviceList.getTypeAdvice());
//        values.put(kEY_ADVICE_List_idTheme,modelAdviceList.getIdTheme());
//        values.put(kEY_ADVICE_List_nameTheme,modelAdviceList.getNameTheme());
//        values.put(kEY_ADVICE_List_countryFlagAdvice,modelAdviceList.getCountryFlagAdvice());
//        values.put(kEY_ADVICE_List_countryAdvice,modelAdviceList.getCountryAdvice());
//        values.put(kEY_ADVICE_List_titleAdvice,modelAdviceList.getTitleAdvice());
//        values.put(kEY_ADVICE_List_documentAdvice,modelAdviceList.getDocumentAdvice());
//        values.put(kEY_ADVICE_List_updatedAdvice,modelAdviceList.getUpdatedAdvice());
//        values.put(kEY_ADVICE_List_publicatedAdvice,modelAdviceList.getPublicatedAdvice());
//        values.put(kEY_ADVICE_List_haveRateAdvice,modelAdviceList.getHaveRateAdvice());
//        values.put(kEY_ADVICE_List_statusAdvice,modelAdviceList.getStatusAdvice());
//       // values.put();
//
//        /*String query = " Select * from "+TABLE_NAME_ADVICE_LIST +" where "+kEY_ADVICE_List_idAdvice+ " = '"+modelAdviceList.getIdAdvice()+"'";
//        Cursor cursor = dbread.rawQuery(query,null);
//        if (cursor.moveToFirst()){
//            Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//        }else
//        {*/
//
//           if (db.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
//              // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//           }else{
//               //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//           }
//
//            String insertQwery = "INSERT INTO "+TABLE_NAME_ADVICE_LIST+" set "+kEY_ADVICE_List_idAdvice+" = '"+modelAdviceList.getIdAdvice()+"' , "+
//                    kEY_ADVICE_List_typeAdvice+" = '"+modelAdviceList.getTypeAdvice()+"' , "+kEY_ADVICE_List_idTheme+" ='"+modelAdviceList.getIdTheme()+"',"
//                    +kEY_ADVICE_List_nameTheme+" = '"+modelAdviceList.getNameTheme()+" ', "+kEY_ADVICE_List_countryFlagAdvice+" = ' "+modelAdviceList.getCountryFlagAdvice()+" ' , "
//                    +kEY_ADVICE_List_titleAdvice+" =' "+modelAdviceList.getTitleAdvice()+"' , "+kEY_ADVICE_List_documentAdvice+" ='"+modelAdviceList.getDocumentAdvice()+
//                    " ',"+kEY_ADVICE_List_updatedAdvice+" =' "+modelAdviceList.getUpdatedAdvice()+" ' , "+kEY_ADVICE_List_publicatedAdvice+" = ' "+modelAdviceList.getPublicatedAdvice()+" ' , "
//                    +kEY_ADVICE_List_haveRateAdvice+" = ' "+modelAdviceList.getHaveRateAdvice()+" ' , "+kEY_ADVICE_List_statusAdvice+" = '"+modelAdviceList.getStatusAdvice()+"'";
//
//          //  Log.e(TAG,"inset query "+insertQwery);
//
//          //  db.rawQuery(insertQwery,null);
//
//       // }
//        db.close();
//        dbread.close();

    }

    public void forAdviceList(ModelAdviceList modelAdviceList) {

        SQLiteDatabase db_write = this.getWritableDatabase();
        try {


            ContentValues values = new ContentValues();
            values.put(kEY_ADVICE_List_idAdvice, modelAdviceList.getIdAdvice());
            values.put(kEY_ADVICE_List_typeAdvice, modelAdviceList.getTypeAdvice());
            values.put(kEY_ADVICE_List_idTheme, modelAdviceList.getIdTheme());
            values.put(kEY_ADVICE_List_nameTheme, modelAdviceList.getNameTheme());
            values.put(kEY_ADVICE_List_countryFlagAdvice, modelAdviceList.getCountryFlagAdvice());
            values.put(kEY_ADVICE_List_countryAdvice, modelAdviceList.getCountryAdvice());
            values.put(kEY_ADVICE_List_titleAdvice, modelAdviceList.getTitleAdvice());
            values.put(kEY_ADVICE_List_documentAdvice, modelAdviceList.getDocumentAdvice());
            values.put(kEY_ADVICE_List_updatedAdvice, modelAdviceList.getUpdatedAdvice());
            values.put(kEY_ADVICE_List_publicatedAdvice, modelAdviceList.getPublicatedAdvice());
            values.put(kEY_ADVICE_List_haveRateAdvice, modelAdviceList.getHaveRateAdvice());
            values.put(kEY_ADVICE_List_statusAdvice, modelAdviceList.getStatusAdvice());
            // values.put();

        /*String query = " Select * from "+TABLE_NAME_ADVICE_LIST +" where "+kEY_ADVICE_List_idAdvice+ " = '"+modelAdviceList.getIdAdvice()+"'";
        Cursor cursor = dbread.rawQuery(query,null);
        if (cursor.moveToFirst()){
            Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
        }else
        {*/

            if (db_write.insert(TABLE_NAME_ADVICE_LIST, null, values) != -1) {
                // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
            } else {
                //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db_write != null) {
                db_write.close();
                db_write = null;
            }
        }

    }

    public void forAdviceUserList(ModelAdviceList modelAdviceList) {

        SQLiteDatabase db_read = DataBaseHalper.this.getReadableDatabase();
        SQLiteDatabase db_Write = DataBaseHalper.this.getWritableDatabase();
        Cursor cursor = null;

        try {


            ContentValues values = new ContentValues();
            values.put(kEY_ADVICE_List_idAdvice, modelAdviceList.getIdAdvice());
            values.put(kEY_ADVICE_List_typeAdvice, modelAdviceList.getTypeAdvice());
            values.put(kEY_ADVICE_List_idTheme, modelAdviceList.getIdTheme());
            values.put(kEY_ADVICE_List_nameTheme, modelAdviceList.getNameTheme());
            values.put(kEY_ADVICE_List_countryFlagAdvice, modelAdviceList.getCountryFlagAdvice());
            values.put(kEY_ADVICE_List_countryAdvice, modelAdviceList.getCountryAdvice());
            values.put(kEY_ADVICE_List_titleAdvice, modelAdviceList.getTitleAdvice());
            values.put(kEY_ADVICE_List_documentAdvice, modelAdviceList.getDocumentAdvice());
            values.put(kEY_ADVICE_List_updatedAdvice, modelAdviceList.getUpdatedAdvice());
            values.put(kEY_ADVICE_List_publicatedAdvice, modelAdviceList.getPublicatedAdvice());
            values.put(kEY_ADVICE_List_haveRateAdvice, modelAdviceList.getHaveRateAdvice());
            values.put(kEY_ADVICE_List_statusAdvice, modelAdviceList.getStatusAdvice());
            // values.put();

            String query = " Select * from " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_idAdvice + " = '" + modelAdviceList.getIdAdvice() + "'" + " and " + kEY_ADVICE_List_typeAdvice + " = 'user'";
            // Log.e(TAG,"user aentry data is "+query.toString());
            cursor = db_read.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                //   Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
                if (db_Write.update(TABLE_NAME_ADVICE_LIST, values, kEY_ADVICE_List_idAdvice + " = ? AND " + kEY_ADVICE_List_typeAdvice + " = ?", new String[]{modelAdviceList.getIdAdvice(), modelAdviceList.getTypeAdvice()}) > 0) {
                    Log.e(TAG, "update data inserted title is " + modelAdviceList.getTitleAdvice());
                } else {
                    Log.e(TAG, "not update data isdata inserted title is " + modelAdviceList.getTitleAdvice());
                }

            } else {

                if (db_Write.insert(TABLE_NAME_ADVICE_LIST, null, values) != -1) {
                    Log.e(TAG, "SUccessfully inserted advice for type is : " + modelAdviceList.getTypeAdvice());
                } else {
                    Log.e(TAG, "error for inserting  inserted advice for type is : " + modelAdviceList.getTypeAdvice());
                }
            }
            //if (modelAdviceList)

/*
        if (db.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
            // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
        }else{
            //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
        }*/

            String insertQwery = "INSERT INTO " + TABLE_NAME_ADVICE_LIST + " set " + kEY_ADVICE_List_idAdvice + " = '" + modelAdviceList.getIdAdvice() + "' , " +
                    kEY_ADVICE_List_typeAdvice + " = '" + modelAdviceList.getTypeAdvice() + "' , " + kEY_ADVICE_List_idTheme + " ='" + modelAdviceList.getIdTheme() + "',"
                    + kEY_ADVICE_List_nameTheme + " = '" + modelAdviceList.getNameTheme() + " ', " + kEY_ADVICE_List_countryFlagAdvice + " = ' " + modelAdviceList.getCountryFlagAdvice() + " ' , "
                    + kEY_ADVICE_List_titleAdvice + " =' " + modelAdviceList.getTitleAdvice() + "' , " + kEY_ADVICE_List_documentAdvice + " ='" + modelAdviceList.getDocumentAdvice() +
                    " '," + kEY_ADVICE_List_updatedAdvice + " =' " + modelAdviceList.getUpdatedAdvice() + " ' , " + kEY_ADVICE_List_publicatedAdvice + " = ' " + modelAdviceList.getPublicatedAdvice() + " ' , "
                    + kEY_ADVICE_List_haveRateAdvice + " = ' " + modelAdviceList.getHaveRateAdvice() + " ' , " + kEY_ADVICE_List_statusAdvice + " = '" + modelAdviceList.getStatusAdvice() + "'";

            //  Log.e(TAG,"inset query "+insertQwery);

            //  db.rawQuery(insertQwery,null);

            // }
//            dbWrite.close();
//            dbRead.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db_Write != null) {
                db_Write.close();
                db_Write = null;
            }
            if (db_read != null) {
                db_read.close();
                db_read = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }

    }

    public void InsertAdvicdListUser(ModelAdviceList modelAdviceList) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbread = this.getReadableDatabase();

        InsertAdviceListUserAsync insertAdviceListUserAsync = new InsertAdviceListUserAsync(modelAdviceList, dbread, db);
        insertAdviceListUserAsync.execute();

//        ContentValues values = new ContentValues();
//        values.put(kEY_ADVICE_List_idAdvice,modelAdviceList.getIdAdvice());
//        values.put(kEY_ADVICE_List_typeAdvice,modelAdviceList.getTypeAdvice());
//        values.put(kEY_ADVICE_List_idTheme,modelAdviceList.getIdTheme());
//        values.put(kEY_ADVICE_List_nameTheme,modelAdviceList.getNameTheme());
//        values.put(kEY_ADVICE_List_countryFlagAdvice,modelAdviceList.getCountryFlagAdvice());
//        values.put(kEY_ADVICE_List_countryAdvice,modelAdviceList.getCountryAdvice());
//        values.put(kEY_ADVICE_List_titleAdvice,modelAdviceList.getTitleAdvice());
//        values.put(kEY_ADVICE_List_documentAdvice,modelAdviceList.getDocumentAdvice());
//        values.put(kEY_ADVICE_List_updatedAdvice,modelAdviceList.getUpdatedAdvice());
//        values.put(kEY_ADVICE_List_publicatedAdvice,modelAdviceList.getPublicatedAdvice());
//        values.put(kEY_ADVICE_List_haveRateAdvice,modelAdviceList.getHaveRateAdvice());
//        values.put(kEY_ADVICE_List_statusAdvice,modelAdviceList.getStatusAdvice());
//        // values.put();
//
//        String query = " Select * from "+TABLE_NAME_ADVICE_LIST +" where "+kEY_ADVICE_List_idAdvice+ " = '"+modelAdviceList.getIdAdvice()+"'"+" and "+kEY_ADVICE_List_typeAdvice +" = 'user'";
//       // Log.e(TAG,"user aentry data is "+query.toString());
//        Cursor cursor = dbread.rawQuery(query,null);
//        if (cursor.moveToFirst()){
//         //   Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//            if (db.update(TABLE_NAME_ADVICE_LIST,values,kEY_ADVICE_List_idAdvice+" = ? AND "+kEY_ADVICE_List_typeAdvice +" = ?",new String[]{modelAdviceList.getIdAdvice(),modelAdviceList.getTypeAdvice()})>0){
//           //     Log.e(TAG,"update data inserted title is "+modelAdviceList.getTitleAdvice());
//            }else{
//             //   Log.e(TAG,"not update data isdata inserted title is "+modelAdviceList.getTitleAdvice());
//            }
//
//        }else {
//
//            if (db.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
//               // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//            }else{
//               // Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//            }
//        }
//        //if (modelAdviceList)
//
///*
//        if (db.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
//            // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//        }else{
//            //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//        }*/
//
//        String insertQwery = "INSERT INTO "+TABLE_NAME_ADVICE_LIST+" set "+kEY_ADVICE_List_idAdvice+" = '"+modelAdviceList.getIdAdvice()+"' , "+
//                kEY_ADVICE_List_typeAdvice+" = '"+modelAdviceList.getTypeAdvice()+"' , "+kEY_ADVICE_List_idTheme+" ='"+modelAdviceList.getIdTheme()+"',"
//                +kEY_ADVICE_List_nameTheme+" = '"+modelAdviceList.getNameTheme()+" ', "+kEY_ADVICE_List_countryFlagAdvice+" = ' "+modelAdviceList.getCountryFlagAdvice()+" ' , "
//                +kEY_ADVICE_List_titleAdvice+" =' "+modelAdviceList.getTitleAdvice()+"' , "+kEY_ADVICE_List_documentAdvice+" ='"+modelAdviceList.getDocumentAdvice()+
//                " ',"+kEY_ADVICE_List_updatedAdvice+" =' "+modelAdviceList.getUpdatedAdvice()+" ' , "+kEY_ADVICE_List_publicatedAdvice+" = ' "+modelAdviceList.getPublicatedAdvice()+" ' , "
//                +kEY_ADVICE_List_haveRateAdvice+" = ' "+modelAdviceList.getHaveRateAdvice()+" ' , "+kEY_ADVICE_List_statusAdvice+" = '"+modelAdviceList.getStatusAdvice()+"'";
//
//        //  Log.e(TAG,"inset query "+insertQwery);
//
//        //  db.rawQuery(insertQwery,null);
//
//        // }
//        db.close();
//        dbread.close();

    }

    public int getAdviceListCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        String SearchQuery = "Select * from " + TABLE_NAME_ADVICE_LIST;

        Cursor cursorMainTable = db.rawQuery(SearchQuery, null);

        int count = cursorMainTable.getCount();
        return count;
    }

    // public ArrayList<ModelAdviceList> getAdviceList(String type_advice,int idTheme){
    public boolean getAdviceList(String type_advice, int idTheme) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query;
        Cursor cursor = null;

        try {


            ArrayList<ModelAdviceList> modelAdviceLists = new ArrayList<>();
            if (idTheme == -1)
                query = "SELECT * FROM " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_typeAdvice + " = '" + type_advice + "' order by  " + kEY_ADVICE_List_idAdvice + "+0 desc ";
            else {
                // query = "SELECT * FROM " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_typeAdvice + " = '" + type_advice + "' AND " + kEY_ADVICE_List_idTheme + " & '" + idTheme + "' order by  " + KEY_ID + " desc ";
                query = "SELECT * FROM " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_typeAdvice + " = '" + type_advice + "' AND " + kEY_ADVICE_List_idTheme + " = '" + idTheme + "' order by  " + KEY_ID + "+0 desc ";

            }
            // Log.e(TAG,"advice list qwery "+query);
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    ModelAdviceList modelAdviceList = new ModelAdviceList();
                    modelAdviceList.setTypeAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_typeAdvice)));
                    modelAdviceList.setIdAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_idAdvice)));
                    modelAdviceList.setIdTheme(cursor.getInt(cursor.getColumnIndex(kEY_ADVICE_List_idTheme)));
                    modelAdviceList.setNameTheme(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_nameTheme)));
                    modelAdviceList.setCountryFlagAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_countryFlagAdvice)));
                    modelAdviceList.setCountryAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_countryAdvice)));
                    modelAdviceList.setSourceAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_sourceAdvice)));
                    modelAdviceList.setTitleAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_titleAdvice)));
                    modelAdviceList.setDocumentAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_documentAdvice)));
                    modelAdviceList.setUpdatedAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_updatedAdvice)));
                    modelAdviceList.setPublicatedAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_publicatedAdvice)));
                    modelAdviceList.setHaveRateAdvice(cursor.getInt(cursor.getColumnIndex(kEY_ADVICE_List_haveRateAdvice)));
                    modelAdviceList.setStatusAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_statusAdvice)));

                    //modelAdviceLists.add(modelAdviceList);
                    Fragment_advice.modelAdviceListList.add(modelAdviceList);
                    Fragment_advice.adviceDetailAdapter.notifyDataSetChanged();

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }


        }
        Fragment_advice.updateTest();
        return true;
    }

    public ArrayList<ModelAdviceList> getAdviceListarray() {
        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<ModelAdviceList> modelAdviceLists = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_NAME_ADVICE_LIST;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ModelAdviceList modelAdviceList = new ModelAdviceList();
                modelAdviceList.setTypeAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_typeAdvice)));
                modelAdviceList.setIdAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_idAdvice)));
                modelAdviceList.setIdTheme(cursor.getInt(cursor.getColumnIndex(kEY_ADVICE_List_idTheme)));
                modelAdviceList.setNameTheme(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_nameTheme)));
                modelAdviceList.setCountryFlagAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_countryFlagAdvice)));
                modelAdviceList.setCountryAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_countryAdvice)));
                modelAdviceList.setSourceAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_sourceAdvice)));
                modelAdviceList.setTitleAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_titleAdvice)));
                modelAdviceList.setDocumentAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_documentAdvice)));
                modelAdviceList.setUpdatedAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_updatedAdvice)));
                modelAdviceList.setPublicatedAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_publicatedAdvice)));
                modelAdviceList.setHaveRateAdvice(cursor.getInt(cursor.getColumnIndex(kEY_ADVICE_List_haveRateAdvice)));
                modelAdviceList.setStatusAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_statusAdvice)));

                modelAdviceLists.add(modelAdviceList);

            } while (cursor.moveToNext());
        }
        return modelAdviceLists;
    }

    public void updateStatusAdvice(String idAdvice, String typeAdvice, String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;
        try {


            String qwery = "update " + TABLE_NAME_ADVICE_LIST + " set " + kEY_ADVICE_List_statusAdvice + "= " + status + " where " + kEY_ADVICE_List_idAdvice + " = '" + idAdvice + "' AND " + kEY_ADVICE_List_typeAdvice + " = '" + typeAdvice + "'";
            //  Log.e(TAG,"send_update status query "+qwery);

            cursor = db.rawQuery(qwery, null);
            if (cursor.moveToFirst()) {
                //    Log.e(TAG,"status  updation is done successfully ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }

    }

    public void InsAdvRatingSent(String idAdvice, String typeAdvice) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        try {


            String qwery = "update " + TABLE_NAME_ADVICE_LIST + " set " + kEY_ADVICE_List_haveRateAdvice + " = 1 ," + kEY_ADVICE_List_statusAdvice + "= 4 where " + kEY_ADVICE_List_idAdvice + " = '" + idAdvice + "' AND " + kEY_ADVICE_List_typeAdvice + " = '" + typeAdvice + "'";
            //  Log.e(TAG,"send_rating qwery "+qwery);

            cursor = db.rawQuery(qwery, null);
            if (cursor.moveToFirst()) {
                //    Log.e(TAG,"rating updation is done successfully ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }

    }

    public int GetAdvRating(String idAdvice) {
        SQLiteDatabase db = this.getReadableDatabase();
        int rating_value;
        String qwery = "select * from " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_idAdvice + " = '" + idAdvice + "' AND  " + kEY_ADVICE_List_typeAdvice + " = 'user'";
        Cursor cursor = db.rawQuery(qwery, null);

        if (cursor.moveToFirst()) {

            rating_value = cursor.getInt(cursor.getColumnIndex(kEY_ADVICE_List_haveRateAdvice));
            String textAdvice = cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETALIS_textAdvice));
            //   Log.e(TAG,"rating value is "+rating_value+" text advices "+textAdvice);
            if (textAdvice.equals("")) {
                //     Log.e(TAG,"rating text and empty ");
                return rating_value = 1;
            } else {
                //   Log.e(TAG,"rating text is not empty ");
                return rating_value;
            }
        } else {
            return rating_value = 1;
        }

    }

    public void InsertAdviceDetails(ModelAdviceDetials modelAdviceDetials) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbread = this.getReadableDatabase();

        Cursor cursor = null;
        try {

            ContentValues values = new ContentValues();
            values.put(kEY_ADVICE_DETAILS_idAdvice, modelAdviceDetials.getIdAdvice());
            values.put(kEY_ADVICE_DETALIS_idTender, modelAdviceDetials.getIdTender());
            values.put(kEY_ADVICE_DETALIS_titleAdvice, modelAdviceDetials.getTitleAdvice());
            values.put(kEY_ADVICE_DETALIS_questionAdvice, modelAdviceDetials.getQuestionAdvice());
            values.put(kEY_ADVICE_DETALIS_textAdvice, modelAdviceDetials.getTextAdvice());
            values.put(kEY_ADVICE_List_statusAdvice, modelAdviceDetials.getStatusAdvice());
            values.put(kEY_ADVICE_DETAILS_updateAdvice, modelAdviceDetials.getUpdateAdvice());
            values.put(kEY_ADVICE_DETALIS_publicatedAdvice, modelAdviceDetials.getPublicatedAdvice());
            values.put(kEY_ADVICE_DETAILS_TypeAdvice, modelAdviceDetials.getTypeAdvice());

            String qwery = "SELECT * FROM " + TABLE_NAME_ADVICE_DETIALS + " WHERE " + kEY_ADVICE_DETAILS_idAdvice + " = '" + modelAdviceDetials.getIdAdvice() + "' AND  " + kEY_ADVICE_List_typeAdvice + " = '" + modelAdviceDetials.getTypeAdvice() + "'";

            cursor = dbread.rawQuery(qwery, null);
            if (cursor.moveToFirst()) {

                String where = kEY_ADVICE_DETAILS_idAdvice + "=? AND  " + kEY_ADVICE_DETAILS_TypeAdvice + "=?";
                String[] whereArgs = new String[]{modelAdviceDetials.getIdAdvice(), modelAdviceDetials.getTypeAdvice()};

                if (db.update(TABLE_NAME, values, where, whereArgs) > 0) {
                    //  Log.e(TAG,"answere update for "+modelAdviceDetials.getTitleAdvice());
                }
            } else {
                db.insert(TABLE_NAME_ADVICE_DETIALS, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (dbread != null) {
                dbread.close();
                dbread = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
        }
    }

    public boolean checkidAdviceAval(String idAdvice, String typeAdvice) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;

        try {
            String qwery = "Select * from " + TABLE_NAME_ADVICE_DETIALS + " where " + kEY_ADVICE_DETAILS_idAdvice + " = '" + idAdvice + "' AND " + kEY_ADVICE_DETAILS_TypeAdvice + " = '" + typeAdvice + "'";
            cursor = db.rawQuery(qwery, null);
            if (cursor.moveToFirst()) {
                db.close();
                return true;

            } else {


            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
            return false;
        }
    }

    public ModelAdviceDetials getAdviceDetails(String idAdvice, String typeAdvice) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;

        ModelAdviceDetials modelAdviceDetials = new ModelAdviceDetials();

        String qwery = "Select * from " + TABLE_NAME_ADVICE_DETIALS + " where " + kEY_ADVICE_DETAILS_idAdvice + " = '" + idAdvice + "' AND " + kEY_ADVICE_DETAILS_TypeAdvice + " ='" + typeAdvice + "'";

        try {


            cursor = db.rawQuery(qwery, null);
            if (cursor.moveToFirst()) {
                modelAdviceDetials.setIdAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETAILS_idAdvice)));
                modelAdviceDetials.setIdTender(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETALIS_idTender)));
                modelAdviceDetials.setTitleAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETALIS_titleAdvice)));
                modelAdviceDetials.setQuestionAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETALIS_questionAdvice)));
                modelAdviceDetials.setTextAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_DETALIS_textAdvice)));
                modelAdviceDetials.setStatusAdvice(cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_statusAdvice)));
                modelAdviceDetials.setUpdateAdvice(cursor.getString(cursor.getColumnIndexOrThrow(kEY_ADVICE_DETAILS_updateAdvice)));
                modelAdviceDetials.setPublicatedAdvice(cursor.getString(cursor.getColumnIndexOrThrow(kEY_ADVICE_DETALIS_publicatedAdvice)));
            }
            db.close();
            return modelAdviceDetials;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (db != null) {
                db.close();
                db = null;
            }
            return modelAdviceDetials;
        }
    }

    //adviceanswer
    public void Insert_adviceanswer(ModelAdviceAnsware modelAdviceAnsware) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbread = this.getReadableDatabase();

        Cursor cursor = null;

        try {
            ContentValues values = new ContentValues();
            values.put(Key_Advice_QusAnsware_idQuestion, modelAdviceAnsware.getIdQuestion());
            values.put(Key_Advice_QusAnsware_TitleQues, modelAdviceAnsware.getTitleQuestion());
            values.put(Key_Advice_QusAnsware_DetialQues, modelAdviceAnsware.getDetialsAnsware());
            String query = "SELECT * FROM " + TABLE_NAME_ADVICE_QUESANSWARE + " where " + Key_Advice_QusAnsware_idQuestion + " ='" + modelAdviceAnsware.getIdQuestion() + "'";

            cursor = dbread.rawQuery(query, null);

            if (cursor.moveToFirst()) {

            } else {
                db.insert(TABLE_NAME_ADVICE_QUESANSWARE, null, values);
                //   Log.e(TAG," insert data succes "+modelAdviceAnsware.getIdQuestion()+ " in database " );
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (dbread != null) {
                dbread.close();
                dbread = null;
            }
        }

    }

    public ArrayList<ModelAdviceAnsware> GetAdviceAnsware() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ModelAdviceAnsware> modelAdviceAnswareArrayList = new ArrayList<>();

        String qwery = "SELECT * FROM " + TABLE_NAME_ADVICE_QUESANSWARE;
        Cursor cursor = db.rawQuery(qwery, null);

        if (cursor.moveToFirst()) {
            do {
                ModelAdviceAnsware modelAdviceAnsware = new ModelAdviceAnsware();
                modelAdviceAnsware.setIdQuestion(cursor.getString(cursor.getColumnIndex(Key_Advice_QusAnsware_idQuestion)));
                modelAdviceAnsware.setTitleQuestion(cursor.getString(cursor.getColumnIndex(Key_Advice_QusAnsware_TitleQues)));
                modelAdviceAnsware.setDetialsAnsware(cursor.getString(cursor.getColumnIndex(Key_Advice_QusAnsware_DetialQues)));
                modelAdviceAnswareArrayList.add(modelAdviceAnsware);
            } while (cursor.moveToNext());
        }
        return modelAdviceAnswareArrayList;
    }

    public void getDeleteAllTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from " + TABLE_NAME_HELPTITLE);
        db.execSQL("delete from " + TABLE_NAME_HELPDETAIL);
        db.execSQL("delete from " + TABLE_NAME_TENDER_DETAIL_MAIN);
        db.execSQL("delete from " + TABLE_NAME_TENDER_DETAIL__DETAILS);
        db.execSQL("delete from " + TABLE_NAME_TENDER_DETAIL_ATT_DOC);
        db.execSQL("delete from " + TABLE_NAME_TENDER_DETAIL_LINKS);
        db.execSQL("delete from " + TABLE_NAME_ADVICE_LIST);
        db.execSQL("delete from " + TABLE_NAME_ADVICE_DETIALS);
        db.execSQL("delete from " + TABLE_NAME_ADVICE_QUESANSWARE);
        db.execSQL("delete from " + TABLE_NAME_TAG);
        db.execSQL("delete from " + TABLE_NAME_FILTER);

        db.close();
    }

    public void getDeleteHelpTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_NAME_HELPTITLE);
        db.execSQL("delete from " + TABLE_NAME_HELPDETAIL);
        db.close();
    }

    //get fetch_unanswered answer list
    public String getUnAnsweredAdvice() {
        SQLiteDatabase db = this.getReadableDatabase();
        String list_string = "";
        Cursor cursor = null;

        String query = "select * from " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_typeAdvice + " ='user' AND (" + kEY_ADVICE_List_statusAdvice + " &4) <> 4 ";
        try {


            ArrayList<Long> list = new ArrayList<>();


            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    //list = list+","+cursor.getString(cursor.getColumnIndex(kEY_ADVICE_List_idAdvice));
                    list.add(cursor.getLong(cursor.getColumnIndex(kEY_ADVICE_List_idAdvice)));
                } while (cursor.moveToNext());
            }
            list_string = list.toString().replace("[", "").replace("]", "");
            //  Log.e(TAG,"query for getting unanswered data "+query+" and list of data is "+list_string);

            return list_string;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            return list_string;
        }
    }

    public String getTenderStatus(String tenderId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select " + KEY_StatusTender + " from  " + TABLE_NAME + " where " + KEY_IdTender + " ='" + tenderId + "'";
        String statusTender = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            statusTender = cursor.getString(cursor.getColumnIndex(KEY_StatusTender));
        }

        if (db != null) {
            db.close();
            db = null;
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }
        return statusTender;
    }

    public String getTenderTag(String tenderId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select " + KEY_TagTender + " from  " + TABLE_NAME + " where " + KEY_IdTender + " ='" + tenderId + "'";
        String tagTender = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            tagTender = cursor.getString(cursor.getColumnIndex(KEY_TagTender));
        }
        if (db != null) {
            db.close();
            db = null;
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }
        return tagTender;
    }

    public int getPaggingDataCount(String qwery) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
//        String countQwery = "select ("+qwery+")/25";

        Cursor cursor_count = dbRead.rawQuery(qwery, null);

        if (cursor_count.moveToFirst()) {
//            FragmentTender.totalPages = cursor_count.getInt(0);
//            return cursor_count.getCount() / 25;
            return (cursor_count.getCount() / 25) + 1;// change on 14-12-2017 due to cursor count issue.
        }
        if (cursor_count != null) {
            cursor_count.close();
            cursor_count = null;
        }
        return 0;
    }

    public void getPaddingTenderList(String qwery, int lowerLimit, int aboveLimit, int currentPage, InterPagTenderlist interPagTenderlist) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        String final_qwery = qwery + " limit " + lowerLimit + " , 25";

//        String countQwery = "select ("+final_qwery+")/25";
//
//        Cursor cursor_count = dbRead.rawQuery(countQwery, null);
//        if (cursor_count.moveToFirst()){
//          //  FragmentTender.totalPages = cursor_count.getInt(0);
//        }
        GetTenderPagAsync getTenderPagAsync = new GetTenderPagAsync(final_qwery, dbRead, interPagTenderlist);
        getTenderPagAsync.execute();

    }

    public class TnderTenderAsyncTask extends AsyncTask<Void, Void, Void> {

        ModelTenderList modelTenderList;
        SQLiteDatabase db;
        SQLiteDatabase dbRead;

        public TnderTenderAsyncTask(ModelTenderList modelTenderList, SQLiteDatabase dbRead, SQLiteDatabase dbWrite) {
            this.modelTenderList = modelTenderList;
            db = dbWrite;
            this.dbRead = dbRead;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ContentValues values = new ContentValues();
            values.put(KEY_NameProcedure, modelTenderList.getNameProcedure());
            values.put(KEY_TypeTender, modelTenderList.getTypeTender());
            values.put(KEY_SourceTender, modelTenderList.getSourcetender());
            values.put(KEY_TitleTender, modelTenderList.getTitleTender());
            values.put(KEY_AssignedTender, modelTenderList.getAssignedTender());
            values.put(KEY_OptionsTender, modelTenderList.getOptionsTender());
            values.put(KEY_StatusTender, modelTenderList.getStatusTender());
            values.put(KEY_MainLanguage, modelTenderList.getMainLanguage());
            values.put(KEY_NameBuyer, modelTenderList.getNameBuyer());
            values.put(KEY_CityBuyer, modelTenderList.getCityBuyer());
            values.put(KEY_DocumentCurrency, modelTenderList.getDocumentationCurrency());
            values.put(KEY_AmountCurrency, modelTenderList.getAmountCurrency());
            values.put(KEY_DateStart, modelTenderList.getDateStart());
            values.put(KEY_DateStop, modelTenderList.getDateStop());
            values.put(KEY_DateDocumentation, modelTenderList.getDateDocumentation());
            values.put(KEY_ColorName, modelTenderList.getColorName());
            values.put(KEY_NameContract, modelTenderList.getNameContract());
            values.put(KEY_IdDocument, modelTenderList.getIdDocument());
            values.put(KEY_nameDocument, modelTenderList.getNameDocument());
            values.put(KEY_NameAwardCriteria, modelTenderList.getNameAwardCriteria());
            values.put(KEY_CountryFlagTender, modelTenderList.getCountryFlagTender());
            values.put(KEY_CategoriesTender, modelTenderList.getCategoriesTender());
            values.put(KEY_UrlTender, modelTenderList.getUrlTender());
            values.put(KEY_IdNUTS, modelTenderList.getIdNUTS());
            values.put(KEY_TagTender, modelTenderList.getTagTender());
            values.put(KEY_IdBuyer, modelTenderList.getIdBuyer());
            values.put(KEY_IdTender, modelTenderList.getIdTender());
            values.put(KEY_IdContract, modelTenderList.getIdContract());
            values.put(KEY_IdProcedure, modelTenderList.getIdProcedure());
            values.put(KEY_CityTender, modelTenderList.getCityTender());
            values.put(KEY_CountryTender, modelTenderList.getCountryTender());
            values.put(KEY_DocumentationPrice, modelTenderList.getDocumentationPrice());
            values.put(KEY_AmountTender, modelTenderList.getAmountTender());
            if (modelTenderList.isReaded()) {
                values.put(KEY_IsReaded, 1);
            } else {
                values.put(KEY_IsReaded, 0);
            }
            values.put(KEY_IsIconShow, 1);

        /*if (modelTenderList.isiconShow()){

        }else{
            values.put(KEY_IsIconShow,0);
        }*/

            //values.put( , );

            //serch that existing datat is already exists

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_IdTender + " = '" + modelTenderList.getIdTender() + "'";

            Cursor cursor = dbRead.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                // db.update(TABLE_NAME,values,KEY_IdTender,new String[]{cursor.getString(cursor.getColumnIndex(KEY_IdTender))});
            } else
                db.insert(TABLE_NAME, null, values);


            if (db != null)
                db.close();

            if (dbRead != null)
                dbRead = null;

            if (cursor != null) {
                cursor.close();
            }
            return null;
        }
    }
//    public boolean InsertTag(){
//
//    }

    public class InsertAdviceListAsync extends AsyncTask<Void, Void, Void> {

        ArrayList<ModelAdviceList> modelAdviceLists;


        public InsertAdviceListAsync(ArrayList<ModelAdviceList> modelAdviceLists) {
            this.modelAdviceLists = modelAdviceLists;


        }

        @Override
        protected Void doInBackground(Void... params) {

            for (ModelAdviceList modelAdviceList : modelAdviceLists) {
                if (modelAdviceList.getTypeAdvice().equals("user")) {
                    forAdviceUserList(modelAdviceList);
                } else
                    forAdviceList(modelAdviceList);
            }


//
//            ContentValues values = new ContentValues();
//            values.put(kEY_ADVICE_List_idAdvice,modelAdviceList.getIdAdvice());
//            values.put(kEY_ADVICE_List_typeAdvice,modelAdviceList.getTypeAdvice());
//            values.put(kEY_ADVICE_List_idTheme,modelAdviceList.getIdTheme());
//            values.put(kEY_ADVICE_List_nameTheme,modelAdviceList.getNameTheme());
//            values.put(kEY_ADVICE_List_countryFlagAdvice,modelAdviceList.getCountryFlagAdvice());
//            values.put(kEY_ADVICE_List_countryAdvice,modelAdviceList.getCountryAdvice());
//            values.put(kEY_ADVICE_List_titleAdvice,modelAdviceList.getTitleAdvice());
//            values.put(kEY_ADVICE_List_documentAdvice,modelAdviceList.getDocumentAdvice());
//            values.put(kEY_ADVICE_List_updatedAdvice,modelAdviceList.getUpdatedAdvice());
//            values.put(kEY_ADVICE_List_publicatedAdvice,modelAdviceList.getPublicatedAdvice());
//            values.put(kEY_ADVICE_List_haveRateAdvice,modelAdviceList.getHaveRateAdvice());
//            values.put(kEY_ADVICE_List_statusAdvice,modelAdviceList.getStatusAdvice());
//            // values.put();
//
//        /*String query = " Select * from "+TABLE_NAME_ADVICE_LIST +" where "+kEY_ADVICE_List_idAdvice+ " = '"+modelAdviceList.getIdAdvice()+"'";
//        Cursor cursor = dbread.rawQuery(query,null);
//        if (cursor.moveToFirst()){
//            Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//        }else
//        {*/
//
//            if (dbWrite.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
//                // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//            }else{
//                //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
//            }
//
//
//            dbWrite.close();
//            dbRead.close();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            EventBus.getDefault().post(true);

        }
    }

    public class InsertAdviceListUserAsync extends AsyncTask<Void, Void, Void> {

        ModelAdviceList modelAdviceList;
        SQLiteDatabase dbRead;
        SQLiteDatabase dbWrite;

        public InsertAdviceListUserAsync(ModelAdviceList modelAdviceList, SQLiteDatabase dbRead, SQLiteDatabase dbWrite) {
            this.modelAdviceList = modelAdviceList;
            this.dbRead = dbRead;
            this.dbWrite = dbWrite;
        }

        @Override
        protected Void doInBackground(Void... params) {

            ContentValues values = new ContentValues();
            values.put(kEY_ADVICE_List_idAdvice, modelAdviceList.getIdAdvice());
            values.put(kEY_ADVICE_List_typeAdvice, modelAdviceList.getTypeAdvice());
            values.put(kEY_ADVICE_List_idTheme, modelAdviceList.getIdTheme());
            values.put(kEY_ADVICE_List_nameTheme, modelAdviceList.getNameTheme());
            values.put(kEY_ADVICE_List_countryFlagAdvice, modelAdviceList.getCountryFlagAdvice());
            values.put(kEY_ADVICE_List_countryAdvice, modelAdviceList.getCountryAdvice());
            values.put(kEY_ADVICE_List_titleAdvice, modelAdviceList.getTitleAdvice());
            values.put(kEY_ADVICE_List_documentAdvice, modelAdviceList.getDocumentAdvice());
            values.put(kEY_ADVICE_List_updatedAdvice, modelAdviceList.getUpdatedAdvice());
            values.put(kEY_ADVICE_List_publicatedAdvice, modelAdviceList.getPublicatedAdvice());
            values.put(kEY_ADVICE_List_haveRateAdvice, modelAdviceList.getHaveRateAdvice());
            values.put(kEY_ADVICE_List_statusAdvice, modelAdviceList.getStatusAdvice());
            // values.put();

            String query = " Select * from " + TABLE_NAME_ADVICE_LIST + " where " + kEY_ADVICE_List_idAdvice + " = '" + modelAdviceList.getIdAdvice() + "'" + " and " + kEY_ADVICE_List_typeAdvice + " = 'user'";
            // Log.e(TAG,"user aentry data is "+query.toString());
            Cursor cursor = dbRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                //   Log.e(TAG, "Advice is already  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
                if (dbWrite.update(TABLE_NAME_ADVICE_LIST, values, kEY_ADVICE_List_idAdvice + " = ? AND " + kEY_ADVICE_List_typeAdvice + " = ?", new String[]{modelAdviceList.getIdAdvice(), modelAdviceList.getTypeAdvice()}) > 0) {
                    //     Log.e(TAG,"update data inserted title is "+modelAdviceList.getTitleAdvice());
                } else {
                    //   Log.e(TAG,"not update data isdata inserted title is "+modelAdviceList.getTitleAdvice());
                }

            } else {

                if (dbWrite.insert(TABLE_NAME_ADVICE_LIST, null, values) != -1) {
                    // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
                } else {
                    // Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
                }
            }
            //if (modelAdviceList)

/*
        if (db.insert(TABLE_NAME_ADVICE_LIST, null,values) != -1){
            // Log.e(TAG, "SUccessfully inserted advice for type is : "+modelAdviceList.getTypeAdvice());
        }else{
            //Log.e(TAG, "error for inserting  inserted advice for type is : "+modelAdviceList.getTypeAdvice());
        }*/

            String insertQwery = "INSERT INTO " + TABLE_NAME_ADVICE_LIST + " set " + kEY_ADVICE_List_idAdvice + " = '" + modelAdviceList.getIdAdvice() + "' , " +
                    kEY_ADVICE_List_typeAdvice + " = '" + modelAdviceList.getTypeAdvice() + "' , " + kEY_ADVICE_List_idTheme + " ='" + modelAdviceList.getIdTheme() + "',"
                    + kEY_ADVICE_List_nameTheme + " = '" + modelAdviceList.getNameTheme() + " ', " + kEY_ADVICE_List_countryFlagAdvice + " = ' " + modelAdviceList.getCountryFlagAdvice() + " ' , "
                    + kEY_ADVICE_List_titleAdvice + " =' " + modelAdviceList.getTitleAdvice() + "' , " + kEY_ADVICE_List_documentAdvice + " ='" + modelAdviceList.getDocumentAdvice() +
                    " '," + kEY_ADVICE_List_updatedAdvice + " =' " + modelAdviceList.getUpdatedAdvice() + " ' , " + kEY_ADVICE_List_publicatedAdvice + " = ' " + modelAdviceList.getPublicatedAdvice() + " ' , "
                    + kEY_ADVICE_List_haveRateAdvice + " = ' " + modelAdviceList.getHaveRateAdvice() + " ' , " + kEY_ADVICE_List_statusAdvice + " = '" + modelAdviceList.getStatusAdvice() + "'";

            //  Log.e(TAG,"inset query "+insertQwery);

            //  db.rawQuery(insertQwery,null);

            // }
//            dbWrite.close();
//            dbRead.close();


            return null;
        }
    }

    public class GetTenderPagAsync extends AsyncTask<Void, Void, Void> {

        public ArrayList<ModelTenderList> tenderArrayList;
        String qwery;
        SQLiteDatabase dbRead;
        InterPagTenderlist interPagTenderlist;

        public GetTenderPagAsync(String qwery, SQLiteDatabase dbRead, InterPagTenderlist interPagTenderlist) {
            this.qwery = qwery;
            this.dbRead = dbRead;
            this.interPagTenderlist = interPagTenderlist;
            tenderArrayList = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... params) {
            tenderArrayList.clear();
            Cursor cursor = dbRead.rawQuery(qwery, null);

            if (cursor.moveToFirst()) {

                int index[] = new int[35];
                index[0] = cursor.getColumnIndex(DataBaseHalper.KEY_NameProcedure);
                index[1] = cursor.getColumnIndex((DataBaseHalper.KEY_TypeTender));
                index[2] = cursor.getColumnIndex(DataBaseHalper.KEY_SourceTender);
                index[3] = cursor.getColumnIndex(DataBaseHalper.KEY_TitleTender);
                index[4] = cursor.getColumnIndex(DataBaseHalper.KEY_AssignedTender);
                index[5] = cursor.getColumnIndex(DataBaseHalper.KEY_OptionsTender);
                index[6] = cursor.getColumnIndex(DataBaseHalper.KEY_StatusTender);
                index[7] = cursor.getColumnIndex(DataBaseHalper.KEY_MainLanguage);
                index[8] = cursor.getColumnIndex(DataBaseHalper.KEY_NameBuyer);
                index[9] = cursor.getColumnIndex(DataBaseHalper.KEY_CityBuyer);
                index[10] = cursor.getColumnIndex(DataBaseHalper.KEY_DocumentCurrency);
                index[11] = cursor.getColumnIndex(DataBaseHalper.KEY_AmountCurrency);
                index[12] = cursor.getColumnIndex(DataBaseHalper.KEY_DateStart);
                index[13] = cursor.getColumnIndex(DataBaseHalper.KEY_DateStop);
                index[14] = cursor.getColumnIndex(DataBaseHalper.KEY_DateDocumentation);
                index[15] = cursor.getColumnIndex(DataBaseHalper.KEY_ColorName);
                index[16] = cursor.getColumnIndex(DataBaseHalper.KEY_NameContract);
                index[17] = cursor.getColumnIndex(DataBaseHalper.KEY_IdDocument);
                index[18] = cursor.getColumnIndex(DataBaseHalper.KEY_nameDocument);
                index[19] = cursor.getColumnIndex(DataBaseHalper.KEY_NameAwardCriteria);
                index[20] = cursor.getColumnIndex(DataBaseHalper.KEY_CountryFlagTender);
                index[21] = cursor.getColumnIndex(DataBaseHalper.KEY_CategoriesTender);
                index[22] = cursor.getColumnIndex(DataBaseHalper.KEY_UrlTender);
                index[23] = cursor.getColumnIndex(DataBaseHalper.KEY_IdNUTS);
                index[24] = cursor.getColumnIndex(DataBaseHalper.KEY_TagTender);
                index[25] = cursor.getColumnIndex(DataBaseHalper.KEY_IdBuyer);
                index[26] = cursor.getColumnIndex(DataBaseHalper.KEY_IdTender);
                index[27] = cursor.getColumnIndex(DataBaseHalper.KEY_IdContract);
                index[28] = cursor.getColumnIndex(DataBaseHalper.KEY_IdProcedure);
                index[29] = cursor.getColumnIndex(DataBaseHalper.KEY_CityTender);
                index[30] = cursor.getColumnIndex(DataBaseHalper.KEY_CountryTender);
                index[31] = cursor.getColumnIndex(DataBaseHalper.KEY_AmountTender);
                index[32] = cursor.getColumnIndex(DataBaseHalper.KEY_DocumentationPrice);
                index[33] = cursor.getColumnIndex(DataBaseHalper.KEY_IsReaded);
                index[34] = cursor.getColumnIndex(DataBaseHalper.KEY_IsIconShow);


                do {
                    ModelTenderList modelTenderList = new ModelTenderList();

                    modelTenderList.setNameProcedure(cursor.getString(index[0]));
                    modelTenderList.setTypeTender(cursor.getString(index[1]));
                    modelTenderList.setSourcetender(cursor.getString(index[2]));
                    modelTenderList.setTitleTender(cursor.getString(index[3]));
                    modelTenderList.setAssignedTender(cursor.getString(index[4]));
                    modelTenderList.setOptionsTender(cursor.getString(index[5]));
                    modelTenderList.setStatusTender(cursor.getString(index[6]));
                    modelTenderList.setMainLanguage(cursor.getString(index[7]));
                    modelTenderList.setNameBuyer(cursor.getString(index[8]));
                    modelTenderList.setCityBuyer(cursor.getString(index[9]));
                    modelTenderList.setDocumentationCurrency(cursor.getString(index[10]));
                    modelTenderList.setAmountCurrency(cursor.getString(index[11]));
                    modelTenderList.setDateStart(cursor.getString(index[12]));
                    modelTenderList.setDateStop(cursor.getString(index[13]));
                    modelTenderList.setDateDocumentation(cursor.getString(index[14]));
                    modelTenderList.setColorName(cursor.getString(index[15]));
                    modelTenderList.setNameContract(cursor.getString(index[16]));
                    modelTenderList.setIdDocument(cursor.getString(index[17]));
                    modelTenderList.setNameDocument(cursor.getString(index[18]));
                    modelTenderList.setNameAwardCriteria(cursor.getString(index[19]));
                    modelTenderList.setCountryFlagTender(cursor.getString(index[20]));
                    modelTenderList.setCategoriesTender(cursor.getString(index[21]));
                    modelTenderList.setUrlTender(cursor.getString(index[22]));
                    modelTenderList.setIdNUTS(cursor.getString(index[23]));
                    modelTenderList.setTagTender(cursor.getString(index[24]));
                    modelTenderList.setIdBuyer(cursor.getString(index[25]));
                    modelTenderList.setIdTender(cursor.getString(index[26]));
                    //  Log.e(TAG," tender id from database "+cursor.getString(cursor.getColumnIndex(DataBaseHalper.KEY_IdTender ))+"at position "+(counttender++));
                    modelTenderList.setIdContract(cursor.getString(index[27]));
                    modelTenderList.setIdProcedure(cursor.getString(index[28]));
                    modelTenderList.setCityTender(cursor.getString(index[29]));
                    modelTenderList.setCountryTender(cursor.getString(index[30]));
                    modelTenderList.setAmountTender(Double.parseDouble(cursor.getString(index[31])));
                    modelTenderList.setDocumentationPrice(Double.parseDouble(cursor.getString(index[32])));


 /* int isreadedInt = cursor.getInt(index[33]); // change on 14-12-2017
                    if (isreadedInt == 1) {
                        modelTenderList.setReaded(true);
                    } else {
                        modelTenderList.setReaded(false);
                    }
*/
                    if ((Long.parseLong(modelTenderList.getStatusTender()) & 1) == 1) {
                        modelTenderList.setReaded(true);
                    } else {
                        modelTenderList.setReaded(false);
                    }

                    int isIconshow = cursor.getInt(index[34]);
                    if (isIconshow == 1) {
                        modelTenderList.setIsiconShow(true);
                    } else {
                        modelTenderList.setIsiconShow(false);
                    }

                /*modelTenderList.(cursor.getString(cursor.getColumnIndex(DataBaseHalper. )));
                modelTenderList.(cursor.getString(cursor.getColumnIndex(DataBaseHalper. )));
                */
                    tenderArrayList.add(modelTenderList);

                    // FragmentTender.adapter.notifyDataSetChanged();
                    // modelTenderListArrayList.add(modelTenderList);
                } while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (dbRead != null) {
                dbRead.close();
                dbRead = null;
            }
            interPagTenderlist.getTenderlist(tenderArrayList);
        }
    }

}
