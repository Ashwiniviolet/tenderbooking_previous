package com.tenderbooking.HelperClasses;

import android.os.AsyncTask;
import android.util.Log;

import com.tenderbooking.Splash;

import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * Created by laxmikant bolya on 04-05-2017.
 */

public class VersionChecker extends AsyncTask<String, String, String> {

    public String TAG = VersionChecker.class.getSimpleName();
    String newVersion = "1.0";

    @Override
    protected String doInBackground(String... params) {

        try {

            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + Splash.PackageName /*"board.result.tenth.twelve"*/ + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();
        } catch (IOException e) {
            Log.e(TAG, "geting exception while geting the veriosn limit type of error " + e.getMessage());
            e.printStackTrace();
        }

        Log.e(VersionChecker.class.getSimpleName(), "value of the version is " + newVersion + " value of the packagename " + Splash.PackageName);

        return newVersion;
    }
}