package com.tenderbooking.HelperClasses;

import android.content.Context;
import android.widget.ExpandableListView;

/**
 * Created by laxmikant bolya on 20-04-2017.
 */

public class CustExpListView extends ExpandableListView {
    int intGroupPosition, intChildPosition, intGroupid;


    public CustExpListView(Context context) {
        super(context);
    }


    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        /*widthMeasureSpec = MeasureSpec.makeMeasureSpec(960,
                MeasureSpec.AT_MOST);*/

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(770,
                MeasureSpec.AT_MOST);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(2000,
                MeasureSpec.AT_MOST);
        /*heightMeasureSpec = MeasureSpec.makeMeasureSpec(600,
                MeasureSpec.AT_MOST);*/
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        // super.onMeasure(widthMeasureSpec, LayoutParams.WRAP_CONTENT);
    }

}
