package com.tenderbooking.HelperClasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tenderbooking.Model.ModelAdviceList;
import com.tenderbooking.R;
import com.tenderbooking.Services.MYBrodcastReciver;
import com.tenderbooking.Services.NotificaitonServices;
import com.tenderbooking.Volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


/**
 * Created by Abhishek jain on 01-03-2017.
 */

public class GlobalDataAcess {


    public static final String user_Broadcast = "com.tenderbooking.userbroadcast";
    public static final String TAG = GlobalDataAcess.class.getSimpleName();
    public static final String DefaultAPiSig = "32875a26a8f3713ef7843d2bd1de647e";
    public static final String Helptenderlist = "Tenders";
    public static final String Helpadviceslist = "Adviser";
    public static final String HelpDetail = "helpDetail";
    public static final String HelpTitle = "helpTitle";
    public static final String Datatbase_help_id = "helpId_database";
    public static final String Database_helpid_aval = "helpid_avail_database";
    public static final String Database_help_Title = "help_Title_database";
    public static final String About_title = "about_title";
    public static final String About_text = "about_text";
    public static final String About_usertype = "about_usertype";
    public static final String Ask_advice_tendAval = "ask_ad_tender_id_aval";
    public static final String Ask_advice_tenderId = "ask_ad_ten_Id";
    public static final String Ask_advice_countryId = "ask_ad_ten_cunt_id";
    //advice list
    public static final String Ask_advice_typeAdvice = "ask_advice_typeAdvice";
    public static final String Ask_advice_idAdvice = "ask_advice_idAdvice";
    public static final String Ask_advice_documentName = "ask_advice_documentName";
    public static final String Ask_advice_haveRated = "ask_advice_haveRated";
    public static final String Ask_advice_toolbar_title = "advice_toolbar_title";
    public static final String Ask_advice_title = "advice_title";
    public static final String Ask_advice_position = "position";
    public static final String Ask_advice_status = "advice_status";
    public static final String TypeService = "type_service";
    //Firebase
    public static final String Firebase_userEmail = "User_email";
    public static final String Documents_id = "documents_id";
    public static final String Type_documents = "type_documents";
    public static final String Class_name = "class_name";
    public static final String Tender_id = "tender_id";
    public static final String Time_Date = "time_date";
    public static final String Advice_title = "advice_title";
    public static final String Advice_id = "advice_id";
    public static final String Tender_download_status = "tender_download_status";
    public static final String Advice_detail_error = "advice_detail_error";
    public static final String Notification_type = "notification_type";
    public static final String Menu_option = "menu_option";
    public static final String Menu_option_chile = "menu_option_child";
    public static final String IsAppRemoved = "is_app_removed";
    public static final String MenuOptionAdvice = "menu_option_advice";
    public static final String MenuOptionAdviceMain = "menu_option_advice_main";
    public static final String MenuOptionAdviceChild = "menu_option_advice_child";
    public static final int Position = 123123;
    public static final int Data = 212121;
    public static int group_selected = -1;
    public static int group_selected_child = -1;
    public static int group_select_advice = -1;
    public static int group_select_advice_main = -1;
    public static int group_select_advice_child = -1;
    public static int pager_position = 0;
    public static int mainValue = -1;
    public static String subValue = null;
    // for advice section
    public static String default_string = null;
    public static int default_int = -2;
    public static String titleString = " ";
    boolean send = true;
    String selectedCountryId = "";

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean hasConnection(Context ctx) {
        if (ctx != null) {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiNetwork != null && wifiNetwork.isConnected()) {
                return true;
            }
            NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobileNetwork != null && mobileNetwork.isConnected()) {
                return true;
            }
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();


            return activeNetwork != null && activeNetwork.isConnected();
        } else {
            return false;
        }
    }

    public static void apiDialog(final Activity activity, String TitleText, String DescText) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_api);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        //ImageView closeImage = (ImageView) dialog.findViewById(R.id.closeImage);
        TextView titleText = (TextView) dialog.findViewById(R.id.titleText);
        TextView descText = (TextView) dialog.findViewById(R.id.descText);

        titleText.setText(TitleText);
        descText.setText(DescText);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public final static String GetAndroidVersion() {
        StringBuilder builder = new StringBuilder();
        builder.append("android : ").append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" : ").append(fieldName).append(" : ");
                builder.append("sdk=").append(fieldValue);
            }
        }
        return "hello";
    }

    public static final String MD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void hideSoftKeyboard(Activity mActivity) {

        if (mActivity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static String ConvertedDate(String data) {
        //Log.e("converted time value","input date "+date+" output date :"+date.substring(8,9)+"."+date.substring(5,6)+"."+date.substring(0,3));
        return " " + data.substring(8, 10) + "." + data.substring(5, 7) + "." + data.substring(0, 4) + ".";
    }


    public static boolean IsWifiEnable(Activity mActivity) {
       /* boolean br = false;
        ConnectivityManager connManager = (ConnectivityManager)mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getActiveNetworkInfo();
        br =  ((null != mWifi) && (mWifi.isConnected()) && (mWifi.getType()== ConnectivityManager.TYPE_WIFI));
        return br;*/
        ConnectivityManager connManager = (ConnectivityManager) mActivity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public static int GetSizeInDp(int sizeInDp, Context mContext) {
        float scale = mContext.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
        return dpAsPixels;
    }

    /*    public static Drawable drawableFromUrl(String url,Context mContext) throws IOException {
            Bitmap x;
            Log.e(TAG,"value of the url is "+url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();

            x = BitmapFactory.decodeStream(input);
            return new BitmapDrawable(mContext.getResources(),x);
        }
        public static Drawable drawableFromUrl_two(String url,Context mContext) throws IOException {
            Bitmap x;
            Log.e(TAG,"value of the url is "+url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();

            x = BitmapFactory.decodeStream(input);
            return new BitmapDrawable(mContext.getResources(),x);
        }
        public static Drawable drawableFromUrl_three(String url,Context mContext) throws IOException {
            Bitmap x;
            Log.e(TAG,"value of the url is "+url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();

            x = BitmapFactory.decodeStream(input);
            return new BitmapDrawable(mContext.getResources(),x);
        }*/
    public static String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public static void openFile(Context context, File url) throws IOException {
        // Create URI


        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", url);


        //version check
        /*if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            // android:authorities="${applicationId}.provider"

            //    Log.e(TAG,"value of the path is "+url.getAbsolutePath()+ " simple path is "+url.getPath()+ " conical path "+url.getCanonicalPath());
            File temp_file = new File(url, "temp_TendriLive_File." + fileExt(url.getAbsolutePath()));
            try {
                //uri = FileProvider.getUriForFile(context,BuildConfig.APPLICATION_ID+".provider_paths",temp_file);
                uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", temp_file);
            } catch (Exception e) {
                Log.e(TAG, "crash at time of geting uri of file");
                uri = Uri.fromFile(url);
                e.printStackTrace();
            }
            //uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
        } else {
            uri = Uri.fromFile(url);
        }*/

        Intent intent = new Intent(Intent.ACTION_VIEW);
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (uri != null) {
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //  context.startActivity(intent);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(intent, "Open File"));
        }
    }

    public static boolean isTimeBetweenTwoTime(String argStartTime,
                                               String argEndTime, String argCurrentTime) throws ParseException {
        String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        //
        if (argStartTime.matches(reg) && argEndTime.matches(reg)
                && argCurrentTime.matches(reg)) {
            boolean valid = false;
            // Start Time
            @SuppressLint("SimpleDateFormat") java.util.Date startTime = new SimpleDateFormat("HH:mm:ss")
                    .parse(argStartTime);
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(startTime);

            // Current Time
            @SuppressLint("SimpleDateFormat") java.util.Date currentTime = new SimpleDateFormat("HH:mm:ss")
                    .parse(argCurrentTime);
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(currentTime);

            // End Time
            @SuppressLint("SimpleDateFormat") java.util.Date endTime = new SimpleDateFormat("HH:mm:ss")
                    .parse(argEndTime);
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(endTime);

            //
            if (currentTime.compareTo(endTime) < 0) {

                currentCalendar.add(Calendar.DATE, 1);
                currentTime = currentCalendar.getTime();

            }

            if (startTime.compareTo(endTime) < 0) {

                startCalendar.add(Calendar.DATE, 1);
                startTime = startCalendar.getTime();

            }
            //
            if (currentTime.before(startTime)) {

                System.out.println(" Time is Lesser ");

                valid = false;
            } else {

                if (currentTime.after(endTime)) {
                    endCalendar.add(Calendar.DATE, 1);
                    endTime = endCalendar.getTime();

                }

                //System.out.println("Comparing , Start Time /n " + startTime);
                //System.out.println("Comparing , End Time /n " + endTime);
                //System.out.println("Comparing , Current Time /n " + currentTime);

                if (currentTime.before(endTime)) {
                    System.out.println("RESULT, Time lies b/w");
                    valid = true;
                } else {
                    valid = false;
                    System.out.println("RESULT, Time does not lies b/w");
                }

            }
            return valid;

        } else {
            throw new IllegalArgumentException(
                    "Not a valid time, expecting HH:MM:SS format");
        }
    }

    public static String getGMTTime() {
        SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

        String GMtDate = simpleDateFormatGMT.format(new Date());
        return GMtDate;
    }

    public static void getDatabaseBackup() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//com.tenderbooking//databases//" + DataBaseHalper.DATABASE_NAME;
                String backupDBPath = "tender_databaser.sqlite";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
        }
    }

    public static void SendSyncAction(final Context mContext, final Map<String, String> deviceinfoMap, final String actionType, final String action) {
        final String mURL = UtillClasses.BaseUrl + "send_tender_action";

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String message = " synchronization sucess , url :" + mURL + " action type " + " action type " + actionType + " , action detials " + action + ", responce: " + response.toString();
                       /* try {
                            Logger.getInstance().createFileOnDevice(true, message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mProgressDialog.hide();
                String message = " synchronization failed , url :" + mURL + " action type " + " action type " + actionType + " , action detials " + action + ", error: " + error.getCause() + " error message " + error.getMessage();
                /*try {
                    Logger.getInstance().createFileOnDevice(true, message);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();


                // if (ask_adv_spinner!= null)
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                } else if (error instanceof AuthFailureError) {
                    //
                } else if (error instanceof ServerError) {

                }
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                mapFooder.put(UtillClasses.Action_ActionType, actionType);
                mapFooder.put(UtillClasses.Action_ActionDetails, action);

                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

    public static void resetAlarm(Context mcontext, long duration) {
        //that cancel current alarm
        Intent intent = new Intent(mcontext, MYBrodcastReciver.class);
        PendingIntent sender = PendingIntent.getBroadcast(mcontext, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) mcontext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 0, duration, sender);
//        Toast.makeText(mcontext, "clear alarm notification", Toast.LENGTH_SHORT).show();
    }

    public AlertDialog getAskQuesDialog(final Activity mContext, String tenderId, final String countryId, final int isTenidAval) {

        final EditText ask_adv_qText, ask_adv_qTitle, ask_adv_tenderId;
        CheckBox Ask_adv_checkBoc;
        final Spinner ask_adv_spinner;
        Button ask_adv_sendBtn, ask_adv_cancel;
        ArrayAdapter<String> adapterSpinner;
        List<String> listcountry;
        // String selectedCountryId ="" ;


        final SessionManager sessionManager = new SessionManager(mContext);
        int size = sessionManager.GetSizeTheme();
        int theme = R.style.AppTheme_medium;
        switch (size) {
            case 1:
                theme = R.style.AppTheme_Small;
                break;
            case 2:
                theme = R.style.AppTheme_medium;
                break;
            case 3:
                theme = R.style.AppTheme_Large;
                break;
            default:
                theme = R.style.AppTheme_medium;
                break;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);


        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_ask_advisor, null);

        ask_adv_qText = (EditText) view.findViewById(R.id.ask_adv_qText);
        ask_adv_qTitle = (EditText) view.findViewById(R.id.ask_adv_qTitle);
        ask_adv_tenderId = (EditText) view.findViewById(R.id.ask_adv_tenderId);
        Ask_adv_checkBoc = (CheckBox) view.findViewById(R.id.Ask_adv_checkBoc);
        ask_adv_sendBtn = (Button) view.findViewById(R.id.ask_adv_sendBtn);
        ask_adv_cancel = (Button) view.findViewById(R.id.ask_adv_cancel);
        ask_adv_spinner = (Spinner) view.findViewById(R.id.ask_adv_spinner);

        ask_adv_qText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        ask_adv_qText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(view);
        //builder.setTitle(mContext.getString(R.string.Ask_advisor));

        builder.setCustomTitle(LayoutInflater.from(mContext).inflate(R.layout.custom_title, null));
        final AlertDialog alertDialog = builder.create();

        Ask_adv_checkBoc.setEnabled(false);
        ask_adv_tenderId.setEnabled(false);

        listcountry = new ArrayList<>();
        listcountry.add("Srbija");
        //listcountry.add("Crna Gora");
        listcountry.add(mContext.getString(R.string.bosnia_and_herzengovina));
        // listcountry.add("Croatia");

        if (isTenidAval == 0) {
            adapterSpinner = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, listcountry);
            tenderId = "0";

        } else {
            List<String> tempList = new ArrayList<>();
            tempList.add(countryId);
            adapterSpinner = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, tempList);
        }

        ask_adv_spinner.setAdapter(adapterSpinner);
        if (isTenidAval == 0) {
            Ask_adv_checkBoc.setChecked(false);
            ask_adv_tenderId.setText("0");
            ask_adv_tenderId.setVisibility(View.GONE);
        } else {
            Ask_adv_checkBoc.setVisibility(View.GONE);
            ask_adv_tenderId.setVisibility(View.GONE);
            ask_adv_spinner.setVisibility(View.GONE);
            Ask_adv_checkBoc.setChecked(true);
            ask_adv_tenderId.setText(tenderId);
            ask_adv_spinner.setSelection(0);
            ask_adv_spinner.setEnabled(false);
        }

        ask_adv_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                switch (ask_adv_spinner.getSelectedItemPosition()) {
                    case 0:
                        selectedCountryId = "00100";
                        break;
                    case 1:
                        selectedCountryId = "00300";//for bosnia and herzegovina
                        // selectedCountryId = "00200";
                        break;

                    case 2:
                        // selectedCountryId = "00300";
                        break;

                    case 3:
                        //selectedCountryId= "00400";
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCountryId = "0";
            }
        });


        ask_adv_qText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    GlobalDataAcess.hideSoftKeyboard(mContext);
                }
                return false;
            }
        });

        final String finalTenderId = tenderId;
        ask_adv_sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (send == true) {
                    if (!true) {
                        if (ask_adv_spinner != null) {
                            Snackbar.make(ask_adv_spinner, mContext.getResources().getString(R.string.please_select_country), Snackbar.LENGTH_LONG).show();
                        }
                    } else if (ask_adv_qTitle.getText().toString().trim().length() == 0) {
                        ask_adv_qTitle.setError(mContext.getResources().getString(R.string.please_enter_title));
                        ask_adv_qTitle.requestFocus();
                    } else if (ask_adv_qText.getText().toString().trim().length() == 0) {
                        ask_adv_qText.setError(mContext.getResources().getString(R.string.please_enter_question));
                        ask_adv_qText.requestFocus();
                    } else if (isTenidAval == 0 && selectedCountryId.equals("0")) {
                        if (ask_adv_spinner != null) {
                            Snackbar.make(ask_adv_spinner, mContext.getResources().getString(R.string.select_country_first), Snackbar.LENGTH_SHORT).show();
                        }
                    } else {

                        // mProgressDialog.show();
                        // GlobalDataAcess.hideSoftKeyboard();
                        Map<String, String> mapAdvice = new HashMap<String, String>();
                        //String tenderID = "";

                  /*  if (Ask_adv_checkBoc.isSelected()){
                        tenderID = ask_adv_tenderId.getText().toString().trim();
                    }
                    else
                        tenderID = "0";*/

                        String typeAdvice = "user";
                        // selected country id
                        //String countryAdvice = ask_adv_spinner.getSelectedItemPosition()+"";
                        String countryAdvice;
                        if (isTenidAval == 0) {
                            countryAdvice = selectedCountryId;
                        } else {
                            countryAdvice = countryId;
                        }

                        send = false;
                        String titleQuestion = ask_adv_qTitle.getText().toString().trim();
                        String textQuestion = ask_adv_qText.getText().toString().trim();

                        mapAdvice.put(UtillClasses.TypeAdvice, typeAdvice);
                        mapAdvice.put(UtillClasses.idtender, finalTenderId);
                        mapAdvice.put(UtillClasses.CountryAdvice, countryAdvice);
                        //mapAdvice.put(UtillClasses.CountryAdvice,"17700");
                        mapAdvice.put(UtillClasses.TitleQuestion, titleQuestion);
                        mapAdvice.put(UtillClasses.DetailsQuestion, textQuestion);
                        SendAdviceQuestion(mapAdvice, mContext, alertDialog, sessionManager.GetDeviceInfo());
                    }
                } else {
                    Snackbar.make(ask_adv_spinner, "Wait! your request still in progress", Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        ask_adv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        alertDialog.show();
        return alertDialog;
    }

    public void SendAdviceQuestion(final Map<String, String> mapAdviceQues, final Context mContext, final AlertDialog alertDialog, final Map<String, String> deviceinfoMap) {

        //final String mURL = UtillClasses.BaseUrl+"send_advice_question";

        // final Map<String,String> deviceinfoMap = sessionManager.GetDeviceInfo();
        final String mURL = UtillClasses.BaseUrl + "send_advice_question";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") == 200) {
                                if (jsonObject.getInt("data") > 0) {
                                    //firebase
                                    Bundle bundle = new Bundle();
                                    //bundle.putString();

                                   /* try {
                                        Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , responce :" + response);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/

                                    // call send
                                    int questionId = jsonObject.optInt("data");
                                    // editing in server

                                    //String date_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());

                                    SimpleDateFormat simpleDateFormatGMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    simpleDateFormatGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

                                    String GMtDate = simpleDateFormatGMT.format(new Date());

                                    ModelAdviceList modelAdviceList = new ModelAdviceList();
                                    modelAdviceList.setIdAdvice(jsonObject.optString("data"));
                                    modelAdviceList.setTitleAdvice(mapAdviceQues.get(UtillClasses.TitleQuestion));
                                    modelAdviceList.setTypeAdvice("user");
                                    modelAdviceList.setStatusAdvice("1");
                                    modelAdviceList.setHaveRateAdvice(1);

                                    if (mapAdviceQues.get(UtillClasses.CountryAdvice).equals("00100")) {
                                        modelAdviceList.setCountryFlagAdvice("rs");
                                    } else if (mapAdviceQues.get(UtillClasses.CountryAdvice).equals("00300")) {
                                        modelAdviceList.setCountryFlagAdvice("ba");
                                    }

                                    //temp do on 10-08 laxmikant chhipa
//                                    dataBaseHalper = new DataBaseHalper(AskAdvisor.this);
//                                    dataBaseHalper.InsertAdvicdListUser(modelAdviceList);
//                                    Fragment_advice.modelAdviceListList.add(0,modelAdviceList);
//
                                    //temp do on 10-08 laxmikant chhipa

//                                    sessionManager.SetLastCheckAdvAns(GMtDate);
//                                    Toast.makeText(AskAdvisor.this, getResources().getString(R.string.send_suces), Toast.LENGTH_SHORT).show();// Intent serviceIntent = new Intent(AskAdvisor.this, NotiIntentService.class);
                                    // serviceIntent.putExtra(GlobalDataAcess.TypeService,1);
                                    //  startService(serviceIntent);

                                    Intent intent = new Intent(mContext, NotificaitonServices.class);
                                    intent.putExtra(GlobalDataAcess.Notification_type, 1);
                                    mContext.startService(intent);
                                    send = true;
                                    //  mProgressDialog.hide();
                                    alertDialog.hide();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //   mProgressDialog.hide();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mProgressDialog.hide();
                Log.e(TAG, " tender list responce Error " + error.getMessage() + " cause " + error.getCause());
                // progressDialog.hide();

               /* try {
                    Logger.getInstance().createFileOnDevice(true, " url : " + mURL + " , failuer :" + error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                // if (ask_adv_spinner!= null)
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    //Log.e(TAG ," Error Network timeout! Try again");
                    // Snackbar.make(mRecyclerView,"Something Went Wrong! Try Again Later ",Snackbar.LENGTH_SHORT).show();
//                        Snackbar snackbar = Snackbar
//                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
//                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        SendAdviceQuestion(mapAdviceQues);
//                                    }
//                                });
//
//// Changing message text color
//                        snackbar.setActionTextColor(Color.RED);
//
//// Changing action button text color
//                        View sbView = snackbar.getView();
//                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(Color.YELLOW);
//                        snackbar.show();

                } else if (error instanceof AuthFailureError) {
                    //
                } else if (error instanceof ServerError) {
                    //
//                        Snackbar snackbar = Snackbar
//                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
//                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        SendAdviceQuestion(mapAdviceQues);
//                                    }
//                                });
//
//// Changing message text color
//                        snackbar.setActionTextColor(Color.RED);
//
//// Changing action button text color
//                        View sbView = snackbar.getView();
//                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(Color.YELLOW);
//                        snackbar.show();
                } else if (error instanceof NetworkError) {
                    //
//                        Snackbar snackbar = Snackbar
//                                .make(ask_adv_spinner, getResources().getString(R.string.some_went_wrong), Snackbar.LENGTH_INDEFINITE)
//                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        SendAdviceQuestion(mapAdviceQues);
//                                    }
//                                });
//
//// Changing message text color
//                        snackbar.setActionTextColor(Color.RED);
//
//// Changing action button text color
//                        View sbView = snackbar.getView();
//                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(Color.YELLOW);
//                        snackbar.show();
                } else if (error instanceof ParseError) {
                    //
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapheader = new HashMap<String, String>();

                mapheader.put(UtillClasses.ApiKey, UtillClasses.ApiKeyValue);
                mapheader.put(UtillClasses.x_email, deviceinfoMap.get(SessionManager.EMAIL));
                mapheader.put(UtillClasses.x_api_signature, deviceinfoMap.get(SessionManager.SIGNATURE));
                mapheader.put(UtillClasses.x_uuid, deviceinfoMap.get(SessionManager.UUID));
                mapheader.put(UtillClasses.x_model, deviceinfoMap.get(SessionManager.MODEL));
                mapheader.put(UtillClasses.x_platform, deviceinfoMap.get(SessionManager.PLATFORM));
                mapheader.put(UtillClasses.x_platform_ver, deviceinfoMap.get(SessionManager.PLATFORM_VER));
                mapheader.put(UtillClasses.x_device_name, deviceinfoMap.get(SessionManager.DeviceNmae));
                mapheader.put(UtillClasses.x_app_ver, deviceinfoMap.get(SessionManager.App_Ver));
                mapheader.put(UtillClasses.x_user_language, deviceinfoMap.get(SessionManager.User_lang));
                //mapheader.put(UtillClasses.x_user_language,"english");
                //mapheader.put(UtillClasses.Content_Type ,UtillClasses.multipart);
                mapheader.put("Content-Type", "application/x-www-form-urlencoded");
                return mapheader;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapFooder = new HashMap<>();

                mapFooder.put(UtillClasses.TypeAdvice, mapAdviceQues.get(UtillClasses.TypeAdvice));
                mapFooder.put(UtillClasses.idtender, mapAdviceQues.get(UtillClasses.idtender));
                mapFooder.put(UtillClasses.CountryAdvice, mapAdviceQues.get(UtillClasses.CountryAdvice));
                mapFooder.put(UtillClasses.TitleQuestion, mapAdviceQues.get(UtillClasses.TitleQuestion));
                mapFooder.put(UtillClasses.DetailsQuestion, mapAdviceQues.get(UtillClasses.DetailsQuestion));

                return mapFooder;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        AppController.getmInstance().addToRequestQueue(stringRequest);
    }

}
