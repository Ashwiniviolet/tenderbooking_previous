package com.tenderbooking.Volley;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.tenderbooking.HelperClasses.GlobalDataAcess;
import com.tenderbooking.HelperClasses.Logger;
import com.tenderbooking.HelperClasses.SessionManager;
import com.tenderbooking.HelperClasses.UtillClasses;
import com.tenderbooking.Services.MYBrodcastReciver;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;


public class AppController extends Application {


    public static final String TAG = AppController.class.getSimpleName();
    public static SessionManager sessionManager;
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    //lacal
    private Locale locale = null;

    public static synchronized AppController getmInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //super.onConfigurationChanged(newConfig);
        /*if (locale != null){
          //  newConfig.locale = locale;
           // Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig,getBaseContext().getResources().getDisplayMetrics());
        }*/

//        if (newConfig != null) {
//            Log.e(TAG, "from the configuraion change ");
//            //  newConfig.locale = locale;
//            // Locale.setDefault(locale);
//            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
//
//        }

//        GlobalDataAcess.getDatabaseBackup();

        if (sessionManager == null) {
            sessionManager = new SessionManager(getApplicationContext());
        }

        GlobalDataAcess.getDatabaseBackup();

        String lang = sessionManager.getAppLangName();
        if (!"".equals(lang) && !newConfig.locale.getLanguage().equals(lang)) {
            locale = new Locale(lang);

            newConfig.locale = locale;
            getApplicationContext().getResources().updateConfiguration(newConfig, getApplicationContext().getResources().getDisplayMetrics());

        }

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sessionManager = new SessionManager(this);

        GlobalDataAcess.getDatabaseBackup();
        Log.e(TAG, "is notienable " + sessionManager.GetNotiEnable() + " notification  time interval for  " + sessionManager.GetNotiTimeInterval() + " chche interval is " + sessionManager.getChacheInterval());

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                try {
                    // laxmikant toast
                    // Toast.makeText(AppController.this, "crash called", Toast.LENGTH_SHORT).show();
                    Logger.getInstance().createFileOnDevice(true, "\n  applicaton crash " + e.getMessage());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                System.exit(1);
            }
        });
        if (sessionManager.GetNotiEnable()) {

//                Intent intent_service  = new Intent(this, NotificaitonServices.class); disable due to start alarm service
//                startService(intent_service);

            if (sessionManager.getIsNotiFirst()) ;
            {
                sessionManager.SetIsNotiFirst(false);

                //NotiIntentService
                AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, MYBrodcastReciver.class);
                intent.putExtra(UtillClasses.AlarmType, 1);
                // PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

                Calendar calendar = Calendar.getInstance();
                if (manager != null) {
                    manager.setInexactRepeating(AlarmManager.RTC, 0, sessionManager.GetNotiTimeInterval(), pendingIntent);
                }
//                 manager.setRepeating(AlarmManager.RTC_WAKEUP, 0, 1000 * 60 * 3, pendingIntent);
                // notiIntent Service

                //notification service
                AlarmManager manager_noti = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                Intent intent_noti = new Intent(this, MYBrodcastReciver.class);
                intent_noti.putExtra(UtillClasses.AlarmType, 3);
                // PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                PendingIntent pendingIntent_noti = PendingIntent.getBroadcast(this, 1, intent_noti, 0);


                // manager.setInexactRepeating(AlarmManager.RTC, 0, sessionManager.GetNotiTimeInterval(), pendingIntent); // TODO: 17-11-2017 this part uncommented when want set notification according to set time
                if (manager_noti != null) {
                    manager_noti.setRepeating(AlarmManager.RTC_WAKEUP, 0, 1000 * 60 * 2, pendingIntent_noti); // TODO: 17-11-2017 this part should be commented and used for the synch action purpose and advice answer
                }
                // notification service

                Log.e(TAG, "Appcontroller hit tender noti");
                //manager.setInexactRepeating(AlarmManager.RTC,0,1000*60*2,pendingIntent);
            }
        }
        Log.e(TAG, "cheche is active status " + sessionManager.GetChacheInter());
        if (sessionManager.GetChacheInter()) {
            AlarmManager managerChache = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            Intent intentChache = new Intent(this, MYBrodcastReciver.class);
            intentChache.putExtra(UtillClasses.AlarmType, 2);
            PendingIntent pendingIntentChache = PendingIntent.getBroadcast(this, 0, intentChache, PendingIntent.FLAG_ONE_SHOT);
            Log.e(TAG, "Appcontroller cheche clear hit");
            // managerChache.setInexactRepeating(AlarmManager.RTC,0,sessionManager.getChacheInterval(),pendingIntentChache);
            managerChache.cancel(pendingIntentChache);
            // managerChache.setRepeating(AlarmManager.RTC_WAKEUP, 0, sessionManager.getChacheInterval(), pendingIntentChache);
            // managerChache.setInexactRepeating(AlarmManager.RTC, 0, 1000 * 60 * 3, pendingIntentChache);
        }


        //app language
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        Configuration config = getApplicationContext().getResources().getConfiguration();

        String lang = sessionManager.getAppLangName();
        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
            locale = new Locale(lang);

            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());


//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//
////
//                Resources resources = getResources();
//                config.setLocale(locale);
//                getApplicationContext().createConfigurationContext(config);
//
//            } else {
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//            }
        }


    }

    public RequestQueue getmRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getmImageLoader() {
        getmRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getmRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getmRequestQueue().add(request);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}
